<?php
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::group(['middleware' => 'admin', 'prefix' => 'admin', 'namespace' => 'Admin'], function (){

    Route::get('/', 'UsersController@getIndex');
    Route::get('dashboard', 'UsersController@getIndex');

    Route::group(['prefix' => 'language'],function (){
        Route::get('list', 'LanguageController@getList');
        Route::get('create', 'LanguageController@getCreate');
        Route::get('edit/{id}','LanguageController@getEdit');
        Route::get('delete/{id}','LanguageController@getDelete');
        Route::post('create','LanguageController@postCreate');
        Route::post('delete','LanguageController@postDelete');
    });

    Route::group(['prefix' => 'film'], function () {
        Route::get('list', 'FilmController@getIndex');
        Route::get('create', 'FilmController@getCreate');
        Route::get('edit/{id}', 'FilmController@getEdit');
        Route::get('delete/{id}', 'FilmController@getDelete');
        Route::get('check-link', 'FilmController@getCheckLink');
        Route::get('check-status/{type}/{id}', 'FilmController@getCheckStatus');
        Route::post('create', 'FilmController@postCreate');
        Route::post('delete', 'FilmController@postDelete');
        Route::post('delete-image', 'FilmController@postDeleteImage');
        Route::post('images', 'FilmController@postAvatar');
        Route::post('search-info', 'FilmController@postSearchInfo');
        Route::post('images-link', 'FilmController@postImagesLink');
        Route::get('delete-image-link/{id}', 'FilmController@getDeleteImageLink');
        Route::post('cover', 'FilmController@postCover');
        Route::post('delete-cover', 'FilmController@postDeleteCover');
        Route::post('cover-link', 'FilmController@postCoverLink');
        Route::get('delete-cover-link/{id}', 'FilmController@getDeleteCoverLink');
        Route::post('background', 'FilmController@postBackground');
        Route::post('delete-background', 'FilmController@postDeleteBackground');
        Route::post('background-link', 'FilmController@postBackgroundLink');
        Route::get('delete-background-link/{id}', 'FilmController@getDeleteBackgroundLink');
    });

    Route::group(['prefix' => 'film-series'], function () {
        Route::get('list', 'FilmDetailController@getIndex');
        Route::get('create', 'FilmDetailController@getCreate');
        Route::get('edit/{id}', 'FilmDetailController@getEdit');
        Route::get('delete/{id}', 'FilmDetailController@getDelete');
        Route::get('check-link', 'FilmDetailController@getCheckLink');
        Route::get('check-status/{type}/{id}', 'FilmDetailController@getCheckStatus');
        Route::post('create', 'FilmDetailController@postCreate');
        Route::post('delete', 'FilmController@postDelete');
        Route::post('delete-image', 'FilmDetailController@postDeleteImage');
        Route::post('images', 'FilmDetailController@postAvatar');
        Route::post('search-info', 'FilmDetailController@postSearchInfo');
        Route::post('images-link', 'FilmDetailController@postImagesLink');
        Route::get('delete-image-link/{id}', 'FilmDetailController@getDeleteImageLink');
        Route::post('cover', 'FilmDetailController@postCover');
        Route::post('delete-cover', 'FilmDetailController@postDeleteCover');
        Route::post('cover-link', 'FilmDetailController@postCoverLink');
        Route::get('delete-cover-link/{id}', 'FilmDetailController@getDeleteCoverLink');
        Route::post('background', 'FilmDetailController@postBackground');
        Route::post('delete-background', 'FilmDetailController@postDeleteBackground');
        Route::post('background-link', 'FilmDetailController@postBackgroundLink');
        Route::get('delete-background-link/{id}', 'FilmDetailController@getDeleteBackgroundLink');
    });

    Route::group(['prefix' => 'link-film'], function(){
        Route::post('create', 'LinkFilmController@postCreate');
        Route::get('delete/{id}', 'LinkFilmController@getDelete');
        Route::post('find-link-film', 'LinkFilmController@postFindLinkFilm');
    });

    Route::group(['prefix' => 'link-film-detail'], function(){
        Route::post('create', 'LinkFilmDetailController@postCreate');
        Route::get('delete/{id}', 'LinkFilmDetailController@getDelete');
        Route::post('find-link-film', 'LinkFilmDetailController@postFindLinkFilm');
    });

    Route::group(['prefix' => 'subtitle'], function(){
        Route::post('create', 'SubTitleController@postCreate');
        Route::get('delete/{id}', 'SubTitleController@getDelete');        
    });

    Route::group(['prefix' => 'subtitle-detail'], function(){
        Route::post('create', 'SubTitleDetailController@postCreate');
        Route::get('delete/{id}', 'SubTitleDetailController@getDelete');        
    });

//    Route::group(['prefix' => 'cast'], function (){
//        Route::get('/', 'ProductCategoryController@getIndex');
//        Route::get('create','ProductCategoryController@getCreate');
//        Route::get('edit/{id}','ProductCategoryController@getEdit');
//        Route::get('update-order/{id}','ProductCategoryController@getUpdateOrder');
//        Route::get('delete/{id}','ProductCategoryController@getDelete');
//        Route::get('check-link','ProductCategoryController@getCheckLink');
//        Route::get('check-status/{id}','ProductCategoryController@getCheckStatus');
//        Route::post('create','ProductCategoryController@postCreate');
//        Route::post('meta-data','ProductCategoryController@postMetaData');
//        Route::post('delete','ProductCategoryController@postDelete');
//        Route::post('delete-image','ProductCategoryController@postDeleteImage');
//        Route::post('images','ProductCategoryController@postAvatar');
//        Route::post('multiple-images','ProductCategoryController@postMultipleImages');
//        Route::post('search-info','ProductCategoryController@postSearchInfo');
//    });
//    Route::group(['prefix' => 'director'], function (){
//        Route::get('/', 'ColorController@getIndex');
//        Route::get('create','ColorController@getCreate');
//        Route::get('edit/{id}','ColorController@getEdit');
//        Route::get('update-order/{id}','ColorController@getUpdateOrder');
//        Route::get('delete/{id}','ColorController@getDelete');
//        Route::get('check-link','ColorController@getCheckLink');
//        Route::get('check-status/{id}','ColorController@getCheckStatus');
//        Route::post('create','ColorController@postCreate');
//        Route::post('meta-data','ColorController@postMetaData');
//        Route::post('delete','ColorController@postDelete');
//        Route::post('delete-image','ColorController@postDeleteImage');
//        Route::post('images','ColorController@postAvatar');
//        Route::post('multiple-images','ColorController@postMultipleImages');
//        Route::post('search-info','ColorController@postSearchInfo');
//    });
//
//    Route::group(['prefix' => '{slug}/category'], function (){
//        Route::get('/', 'TypeController@getIndex');
//        Route::get('create','TypeController@getCreate');
//        Route::get('edit/{id}','TypeController@getEdit');
//        Route::get('update-order/{id}','TypeController@getUpdateOrder');
//        Route::get('delete/{id}','TypeController@getDelete');
//        Route::get('check-link','TypeController@getCheckLink');
//        Route::get('check-status/{id}','TypeController@getCheckStatus');
//        Route::post('create','TypeController@postCreate');
//        Route::post('meta-data','TypeController@postMetaData');
//        Route::post('delete','TypeController@postDelete');
//        Route::post('delete-image','TypeController@postDeleteImage');
//        Route::post('images','TypeController@postAvatar');
//        Route::post('multiple-images','TypeController@postMultipleImages');
//    });


    Route::group(['prefix' => 'user'], function (){
        Route::get('logout','UsersController@getLogout');
        Route::get('list','UsersController@getListUser');
        Route::get('change-group/{id}','UsersController@getChangeGroup');
        Route::get('create','UsersController@getCreate');
        Route::get('edit/{id}','UsersController@getCreate');
        Route::get('delete/{id}','UsersController@getDelete');
        Route::get('export-pdf','UsersController@exportPDF');
        Route::get('export-xls','UsersController@exportXLS');
        Route::post('create','UsersController@postCreate');
        Route::post('edit-profile','UsersController@postEditProfile');
        Route::post('login','UsersController@postLogin');
        Route::post('delete','UsersController@postDelete');
        Route::post('images','UsersController@postAvatar');
        Route::post('test-image','UsersController@postTestImage');
        Route::post('delete-image','UsersController@postDeleteImage');
        Route::get('check-active/{id}','UsersController@getCheckActive');
    });

    Route::group(['prefix' => 'setting'], function (){
        Route::get('edit','SettingController@getIndex');
        Route::post('avatar','SettingController@postAvatar');
        Route::post('meta-data','SettingController@postMetaData');
        Route::post('content','SettingController@postContent');
        Route::post('delete-image','SettingController@postDeleteImage');
        Route::post('meta-images','SettingController@postMetaImages');
        Route::post('delete-meta-image','SettingController@postDeleteMetaImage');
    });

    Route::group(['prefix' => 'menu'],function (){
        Route::get('/','MenuController@getIndex');
        Route::get('create','MenuController@getCreate');
        Route::get('edit/{id}','MenuController@getEdit');
        Route::get('update-order/{id}','MenuController@getUpdateOrder');
        Route::get('delete/{id}','MenuController@getDelete');
        Route::get('check-link','MenuController@getCheckLink');
        Route::get('check-status/{id}','MenuController@getCheckStatus');
        Route::post('create','MenuController@postCreate');
        Route::post('meta-data','MenuController@postMetaData');
        Route::post('delete','MenuController@postDelete');
    });

    Route::group(['prefix' => 'server-film'], function (){
        Route::get('list','ServerPlayFilmController@getIndex');
        Route::get('create','ServerPlayFilmController@getCreate');
        Route::get('edit/{id}','ServerPlayFilmController@getEdit');
        Route::get('delete/{id}','ServerPlayFilmController@getDelete');
        Route::get('check-link','ServerPlayFilmController@getCheckLink');
        Route::get('check-status/{id}','ServerPlayFilmController@getCheckStatus');
        Route::post('create','ServerPlayFilmController@postCreate');
        Route::post('meta-data','ServerPlayFilmController@postMetaData');
        Route::post('delete','ServerPlayFilmController@postDelete');
    });

    Route::group(['prefix' => 'film-country'], function (){
        Route::get('list','CountryFilmController@getIndex');
        Route::get('create','CountryFilmController@getCreate');
        Route::get('edit/{id}','CountryFilmController@getEdit');
        Route::get('delete/{id}','CountryFilmController@getDelete');
        Route::get('check-link','CountryFilmController@getCheckLink');
        Route::get('check-status/{id}','CountryFilmController@getCheckStatus');
        Route::post('create','CountryFilmController@postCreate');
        Route::post('meta-data','CountryFilmController@postMetaData');
        Route::post('delete','CountryFilmController@postDelete');
    });

    Route::group(['prefix' => 'genre'], function (){
        Route::get('list','CategoryFilmController@getIndex');
        Route::get('create','CategoryFilmController@getCreate');
        Route::get('edit/{id}','CategoryFilmController@getEdit');
        Route::get('delete/{id}','CategoryFilmController@getDelete');
        Route::get('check-link','CategoryFilmController@getCheckLink');
        Route::get('check-status/{id}','CategoryFilmController@getCheckStatus');
        Route::post('create','CategoryFilmController@postCreate');
        Route::post('meta-data','CategoryFilmController@postMetaData');
        Route::post('delete','CategoryFilmController@postDelete');
    });

    Route::group(['prefix' => 'type'], function (){
        Route::get('list','TypeFilmController@getIndex');
        Route::get('create','TypeFilmController@getCreate');
        Route::get('edit/{id}','TypeFilmController@getEdit');
        Route::get('delete/{id}','TypeFilmController@getDelete');
        Route::get('check-link','TypeFilmController@getCheckLink');
        Route::get('check-status/{id}','TypeFilmController@getCheckStatus');
        Route::post('create','TypeFilmController@postCreate');
        Route::post('meta-data','TypeFilmController@postMetaData');
        Route::post('delete','TypeFilmController@postDelete');
    });

    Route::group(['prefix' => 'playlist'], function (){
        Route::get('list','PlayListController@getIndex');
        Route::get('create', 'PlayListController@getCreate');
        Route::get('edit/{id}', 'PlayListController@getEdit');
        Route::get('delete/{id}', 'PlayListController@getDelete');
        Route::get('check-link', 'PlayListController@getCheckLink');
        Route::get('check-status/{id}', 'PlayListController@getCheckStatus');
        Route::post('create', 'PlayListController@postCreate');
        Route::post('delete', 'PlayListController@postDelete');
        Route::post('delete-image', 'PlayListController@postDeleteImage');
        Route::post('images', 'PlayListController@postAvatar');
        Route::post('search-info', 'PlayListController@postSearchInfo');
        Route::post('images-link', 'PlayListController@postImagesLink');
        Route::get('delete-image-link/{id}', 'PlayListController@getDeleteImageLink');
        Route::post('cover', 'PlayListController@postCover');
        Route::post('delete-cover', 'PlayListController@postDeleteCover');
        Route::post('cover-link', 'PlayListController@postCoverLink');
        Route::get('delete-cover-link/{id}', 'PlayListController@getDeleteCoverLink');
        Route::post('background', 'PlayListController@postBackground');
        Route::post('delete-background', 'PlayListController@postDeleteBackground');
        Route::post('background-link', 'PlayListController@postBackgroundLink');
        Route::get('delete-background-link/{id}', 'PlayListController@getDeleteBackgroundLink');
    });

//    Route::group(['prefix' => '{slug}/category'], function (){
//        Route::get('/', 'TypeController@getIndex');
//        Route::get('create','TypeController@getCreate');
//        Route::get('edit/{id}','TypeController@getEdit');
//        Route::get('update-order/{id}','TypeController@getUpdateOrder');
//        Route::get('delete/{id}','TypeController@getDelete');
//        Route::get('check-link','TypeController@getCheckLink');
//        Route::get('check-status/{id}','TypeController@getCheckStatus');
//        Route::post('create','TypeController@postCreate');
//        Route::post('meta-data','TypeController@postMetaData');
//        Route::post('delete','TypeController@postDelete');
//        Route::post('delete-image','TypeController@postDeleteImage');
//        Route::post('images','TypeController@postAvatar');
//        Route::post('multiple-images','TypeController@postMultipleImages');
//    });

//    Route::group(['prefix' => 'media'],function (){
//        Route::post('delete-image','SettingController@postDeleteMediaImage');
//        Route::post('update-name','SettingController@postUpdateNameMedia');
//        Route::post('update-order','SettingController@postUpdateOrderMedia');
//    });


});

Route::get('admin/login', 'Admin\UsersController@getLogin');
Route::post('admin/sign-in', 'Admin\UsersController@postLogin');
Route::post('admin/forgot-password', 'Admin\UsersController@postForgotPassword');
Route::post('admin/register', 'Admin\UsersController@postRegister');

Route::get('facebook/redirect', 'Admin\SocialController@redirectToProvider');
Route::get('facebook/callback', 'Admin\SocialController@handleProviderCallback');
Route::get('google/redirect', 'Admin\SocialController@redirectToProviderGoogle');
Route::get('google/callback', 'Admin\SocialController@handleProviderCallbackGoogle');

Route::group(['prefix' => 'users'],function (){
    Route::get('active', 'UsersController@getActive');
    Route::get('login', 'UsersController@getLogin');
    Route::get('logout', 'UsersController@getLogout');
    Route::post('login', 'UsersController@postLogin');
});

//**************Demon Dragon******************//
Route::get('/', 'HomeController@index');
Route::get('genres/{slug}', 'HomeController@getGenres');
Route::get('country/{slug}', 'HomeController@getCountry');

