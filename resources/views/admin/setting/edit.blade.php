@extends('admin.master')
@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(Session::get('delete'))
                <div class="alert alert-success text-center">
                    Success
                </div>
            @endif
        </div>
        <div class="col-md-6">
            <!-- General Data Block -->
            <div class="block">
                <!-- General Data Title -->
                <div class="block-title">
                    <h2><i class="fa fa-pencil"></i> <strong>General</strong> Data</h2>
                </div>
                <!-- END General Data Title -->
                
                <!-- General Data Content -->
                <form action="{!! url('admin/setting/content') !!}" method="post" class="form-horizontal form-bordered">
                    {!! csrf_field() !!}
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="name">Name</label>
                        <div class="col-md-9">
                            <input type="text" id="name" name="name" class="form-control" value="{!! ($result)?$result->name:old('name') !!}" placeholder="Company name...">
                        </div>
                    </div>                    
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="email">Email</label>
                        <div class="col-md-9">
                            <input type="text" id="email" name="email" class="form-control" value="{!! ($result)?$result->email:old('email') !!}" placeholder="Email">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="title">Title</label>
                        <div class="col-md-9">
                            <input type="text" id="title" name="title" class="form-control" value="{!! ($result)?$result->title:old('title') !!}" placeholder="Enter meta title..">                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="keywords">Keywords</label>
                        <div class="col-md-9">
                            <input type="text" id="keywords" name="keywords" class="form-control" value="{!! ($result)?$result->keywords:old('keywords') !!}" placeholder="keyword1, keyword2, keyword3">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="description">Description</label>
                        <div class="col-md-9">
                            <textarea id="description" name="description" class="form-control" rows="6" placeholder="Enter meta description..">{!! ($result)?$result->description:old('description') !!}</textarea>                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="google_analytic">Google Analytic</label>
                        <div class="col-md-9">
                            <input id="google_analytic" name="google_analytic" class="form-control" value="{!! ($result)?$result->google_analytic:old('google_analytic') !!}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="api_youtube">API Youtube</label>
                        <div class="col-md-9">
                            <input type="text" id="api_youtube" name="api_youtube" class="form-control" value="{!! ($result)?$result->api_youtube:old('api_youtube') !!}" placeholder="API Key Youtube">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="app_id_face">ID APP Facebook</label>
                        <div class="col-md-9">
                            <input type="text" id="app_id_face" name="app_id_face" class="form-control" value="{!! ($result)?$result->app_id_face:old('app_id_face') !!}" placeholder="ID APP Facebook">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="facebook_url">Fanpage</label>
                        <div class="col-md-9">
                            <input type="text" id="facebook_url" name="facebook_url" class="form-control" value="{!! ($result)?$result->facebook_url:old('facebook_url') !!}" placeholder="Enter facebook fanpage..">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="google_url">Google+</label>
                        <div class="col-md-9">
                            <input type="text" id="google_url" name="google_url" class="form-control" value="{!! ($result)?$result->google_url:old('google_url') !!}" placeholder="Enter google plus">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="key_jwplayer">Key Jwplayer</label>
                        <div class="col-md-9">
                            <input type="text" id="key_jwplayer" name="key_jwplayer" class="form-control" value="{!! ($result)?$result->key_jwplayer:old('key_jwplayer') !!}" placeholder="Enter Key Jwplayer">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="api_player">API Player</label>
                        <div class="col-md-9">
                            <input type="text" id="api_player" name="api_player" class="form-control" value="{!! ($result)?$result->api_player:old('api_player') !!}" placeholder="Enter api player">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="api_download">API Download</label>
                        <div class="col-md-9">
                            <input type="text" id="api_download" name="api_download" class="form-control" value="{!! ($result)?$result->api_download:old('api_download') !!}" placeholder="Enter api download">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="cache">Time Cache(min)</label>
                        <div class="col-md-9">
                            <input type="number" id="cache" name="cache" class="form-control" value="{!! ($result)?$result->cache:old('cache') !!}" placeholder="Time clear cache">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="film_home">Film Home</label>
                        <div class="col-md-9">
                            <input type="number" id="film_home" name="film_home" class="form-control" value="{!! ($result)?$result->film_home:old('film_home') !!}" placeholder="Film Home Widget">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="film_page">Film Page</label>
                        <div class="col-md-9">
                            <input type="number" id="film_page" name="film_page" class="form-control" value="{!! ($result)?$result->film_page:old('film_page') !!}" placeholder="Film Page Number">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="theme">Choose Theme</label>
                        <div class="col-md-9">
                            <div class="form-check">
                                <label class="form-check-label-1">
                                    <input class="form-check-input-1" type="radio" name="theme" id="theme_1" value="1" {!! ($result && $result->theme==1)?'checked':'' !!}>
                                    Theme 1
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label-2">
                                    <input class="form-check-input-2" type="radio" name="theme" id="theme_2" value="2" {!! ($result && $result->theme==2)?'checked':'' !!}>
                                    Theme 2
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END General Data Content -->
            </div>
            <!-- END General Data Block -->

        </div>
        <div class="col-md-6">
            <!-- Product Images Block -->
            <div class="block">
                <!-- Product Images Title -->
                <div class="block-title">
                    <h2><i class="fa fa-picture-o"></i> <strong>Logo</strong> Images</h2>
                </div>
                <!-- END Product Images Title -->

                <!-- Product Images Content -->
                <div class="block-section">
                    <form id="my-awesome-dropzone" data-multiple="false" data-maxfile="1" action="{!! url('admin/setting/avatar') !!}" class="dropzone images">
                        {!! csrf_field() !!}
                        <div class="dropzone-previews"></div>
                        <div class="fallback"> <!-- this is the fallback if JS isn't working -->
                            <input name="file" type="file" multiple />
                        </div>
                    </form>
                    <div class="form-horizontal" style="padding-top: 15px;">
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button id="submit-all" type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                            </div>
                        </div>
                    </div>
                    <table class="table table-bordered table-striped table-vcenter">
                        <tbody>
                        @if($result != null && $result->images != null)
                            <tr>
                                <td style="width: 20%;">
                                    <a href="{!! url(Storage::url($result->images)) !!}" data-toggle="lightbox-image">
                                        <img src="{!! url(Storage::url($result->images)) !!}" alt="" class="img-responsive center-block" style="max-width: 110px;">
                                    </a>
                                </td>                                
                                <td class="text-right">
                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger delete-image" data-url="{!! url('admin/setting/delete-image') !!}" data-id="{!! $result->id !!}" data-token="{!! csrf_token() !!}"><i class="fa fa-trash-o"></i> Delete</a>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    <!-- END Product Images Content -->
                </div>

                <!-- END Product Images Content -->
            </div>
            <!-- END Product Images Block -->

            <div class="block">
                <!-- Product Images Title -->
                <div class="block-title">
                    <h2><i class="fa fa-picture-o"></i> <strong>Meta</strong> Images</h2>
                </div>
                <!-- END Product Images Title -->
                <!-- Product Images Content -->
                <div class="block-section">
                    <form id="multiple-dropzone" data-multiple="false" data-maxfile="5" action="{!! url('admin/setting/meta-images') !!}" class="dropzone images">
                        {!! csrf_field() !!}
                        <input type="hidden" name="id" value="{!! (!empty($result))?$result->id:0 !!}">
                        <div class="dropzone-previews"></div>
                        <div class="fallback"> <!-- this is the fallback if JS isn't working -->
                            <input name="file" type="file" multiple />
                        </div>
                    </form>
                    <div class="form-horizontal" style="padding-top: 15px;">
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button id="multiple-images" type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                            </div>
                        </div>
                    </div>
                    <table class="table table-bordered table-striped table-vcenter">
                        <tbody>
                        @if(isset($result->meta_images))
                            <tr>
                                <td style="width: 20%;">
                                    <a href="{!! url(Storage::url($result->meta_images)) !!}" data-toggle="lightbox-image">
                                        <img src="{!! url(Storage::url($result->meta_images)) !!}" alt="" class="img-responsive center-block" style="max-width: 110px;">
                                    </a>
                                </td>

                                <td class="text-right">
                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger delete-image" data-url="{!! url('admin/setting/delete-meta-image') !!}" data-id="{!! $result->id !!}" data-token="{!! csrf_token() !!}"><i class="fa fa-trash-o"></i> Delete</a>
                                </td>
                            </tr>

                        @endif
                        </tbody>
                    </table>
                    <!-- END Product Images Content -->
                </div>
                <!-- END Product Images Content -->
            </div>
        </div>
    </div>
@endsection
