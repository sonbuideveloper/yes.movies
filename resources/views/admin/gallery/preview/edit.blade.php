@extends('admin.master')
@section('content')
    <div class="content-header">
        <div class="header-section">
            <h1>{{--<i class="fa fa-spinner fa-3x fa-spin"></i>--}}<i class="gi gi-pencil"></i> Gallery<br><small>Create and Edit Gallery</small></h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li>Pages</li>
        <li><a href="{!! url('admin/gallery/list') !!}">Gallery Center</a></li>
        <li>{!! $action !!} Gallery</li>
    </ul>
    @if($action == 'Edit')
        <div class="row text-center">
            <div class="col-sm-6 col-lg-3">
                <a href="{!! url('admin/gallery/create') !!}" class="widget widget-hover-effect2">
                    <div class="widget-extra themed-background-success">
                        <h4 class="widget-content-light"><strong>Add New</strong> Gallery</h4>
                    </div>
                    <div class="widget-extra-full"><span class="h2 text-success animation-expandOpen"><i class="fa fa-plus"></i></span></div>
                </a>
            </div>
        </div>
    @endif
    @if(Session::get('success'))
        <div class="alert alert-success text-center">
            Success
        </div>
    @endif
    <div class="row">
        <div class="col-lg-6">
            <!-- General Data Block -->

            <div class="block">
                <!-- General Data Title -->
                <div class="block-title">
                    <h2><i class="fa fa-pencil"></i> <strong>General</strong> Data</h2>
                </div>
                <form action="{!! url('admin/gallery/create') !!}" name="post-content-form" method="post" class="form-horizontal form-bordered">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="{!! (!empty($result))?$result->id:0 !!}">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <ul class="nav nav-tabs" data-toggle="tabs">
                        <li class="active"><a href="#tabs-content-general" data-toggle="tooltip" title="General">General</a></li>
                    </ul>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="name">Name</label>
                        <div class="col-md-9">
                            <input type="text" id="name" name="name" data-lang="general" data-id="{!! (!empty($result))?$result->id:0 !!}" data-url="{!! url('admin/article/check-link') !!}" class="form-control" value="{!! (!empty($result))?$result->name:old('name') !!}" placeholder="Name...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="slug">Slug</label>
                        <div class="col-md-9">
                            <input type="text" id="slug" name="slug" class="form-control" value="{!! (!empty($result))?$result->slug:old('slug') !!}" placeholder="Slug">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Category Gallery</label>
                        <div class="col-md-9">
                            {!! $type !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="parent_id_film">HashtagFilm</label>
                        <div class="col-md-9">                    
                            <select id="parent_id_film" name="parent_id_film[]" class="select-chosen" data-placeholder="Choose film..." style="width: 250px;" multiple>
                                <option></option>
                                @if($films_select)
                                    @foreach($films_select as $item)                                
                                        <option value="{{ $item->id }}" selected="selected">{{ $item->name }}</option>
                                    @endforeach
                                @endif
                                @foreach($films as $item)                                
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="parent_id_cast">HashtagCast</label>
                        <div class="col-md-9">
                            <select id="parent_id_cast" name="parent_id_cast[]" class="select-chosen" data-placeholder="Choose cast..." style="width: 250px;" multiple>
                                <option></option>
                                @if($cast_select)
                                    @foreach($cast_select as $item)                                
                                        <option value="{{ $item->id }}" selected="selected">{{ $item->name }}</option>
                                    @endforeach
                                @endif
                                @foreach($casts as $item)                                
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>                                    
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="parent_id_director">HashtagDirector</label>
                        <div class="col-md-9">
                            <select id="parent_id_director" name="parent_id_director[]" class="select-chosen" data-placeholder="Choose director..." style="width: 250px;" multiple>
                                <option></option>
                                @if($director_select)
                                    @foreach($director_select as $item)                                
                                        <option value="{{ $item->id }}" selected="selected">{{ $item->name }}</option>
                                    @endforeach
                                @endif
                                @foreach($directors as $item)                                    
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>                                
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="extraTag">Hagtag Extra</label>
                        <div class="col-md-9 content-cast">
                            <input type="text" id="extraTag" name="extraTag" class="tags-input" value="{!! (!empty($result))?$extra_tag:"" !!}">
                            <input type="hidden" id="extraTagAll" value="{{ $extra_tag_all }}">                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="topicTag">Hagtag Topic</label>
                        <div class="col-md-9 content-cast">
                            <input type="text" id="topicTag" name="topicTag" class="tags-input" value="{!! (!empty($result))?$topic_tag:"" !!}">
                            <input type="hidden" id="topicTagAll" value="{{ $topic_tag_all }}">                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="manualTag">To Add Manually</label>
                        <div class="col-md-9 content-cast">
                            <input type="text" id="manualTag" name="manualTag" class="tags-input" value="{!! (!empty($result))?$manual_tag:"" !!}">
                            <input type="hidden" id="manualTagAll" value="{{ $manual_tag_all }}">                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="randomTag">Hagtag Random</label>
                        <div class="col-md-9 content-cast">
                            <input type="text" id="randomTag" name="randomTag" class="tags-input" value="{!! (!empty($result))?$random_tag:"" !!}">
                            <input type="hidden" id="randomTagAll" value="{{ $random_tag_all }}">                            
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="meta-title">Meta Title</label>
                        <div class="col-md-9">
                            <input type="text" id="meta-title" name="meta-title" class="form-control" value="{!! (!empty($result))?$result->title:old('meta-title') !!}" placeholder="Enter meta title..">
                            <div class="help-block">55 Characters Max</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="meta-keywords">Meta Keywords</label>
                        <div class="col-md-9">
                            <input type="text" id="meta-keywords" name="meta-keywords" class="form-control" value="{!! (!empty($result))?$result->keywords:old('meta-keywords') !!}" placeholder="keyword1, keyword2, keyword3">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="meta-description">Meta Description</label>
                        <div class="col-md-9">
                            <textarea id="meta-description" name="meta-description" class="form-control" rows="6" placeholder="Enter meta description..">{!! (!empty($result))?$result->descriptions:old('meta-description') !!}</textarea>
                            <div class="help-block">115 Characters Max</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="content">Content</label>
                        <div class="col-md-12">
                            <textarea id="content" name="content" class="ckeditor" data-id="content">{!! (!empty($result))?stripslashes($result->content):old('content') !!}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Published?</label>
                        <div class="col-md-9">
                            <label class="switch switch-primary">
                                <input type="checkbox" id="publish" name="publish" @if(!empty($result) && $result->publish == 1) checked @elseif($action == 'Create') checked @endif><span></span>
                            </label>
                        </div>
                    </div>

                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        </div>
                    </div>
                    <!-- END General Data Content -->

                </form>
            </div>
            <!-- END General Data Block -->
        </div>
        <div class="col-lg-6">
            @if(!empty($result))
            <div class="block">
                <!-- Product Images Title -->
                <div class="block-title">
                    <h2><i class="fa fa-picture-o"></i> <strong>Multiple</strong> Images</h2>
                </div>
                <!-- END Product Images Title -->

                <!-- Product Images Content -->
                <div class="block-section">
                    <form id="multiple-dropzone" data-multiple="true" data-maxfile="5" action="{!! url('admin/gallery/multiple-images') !!}" class="dropzone images">
                        {!! csrf_field() !!}
                        <input type="hidden" name="id" value="{!! (!empty($result))?$result->id:0 !!}">
                        <div class="dropzone-previews"></div>
                        <div class="fallback"> <!-- this is the fallback if JS isn't working -->
                            <input name="file" type="file" multiple />
                        </div>                        
                        <div class="help-block">Max file size 20MB</div>
                    </form>
                    <div class="form-horizontal" style="padding-top: 15px;">
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button id="multiple-images" type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                            </div>
                        </div>
                    </div>
                    <table class="table table-bordered table-striped table-vcenter">
                        <tbody>
                        @if(count($result->mediaData) > 0)
                            @foreach($result->mediaData as $media)
                            <tr>
                                <td style="width: 20%;">
                                    <a href="{!! url(Storage::url($media->images)) !!}" data-toggle="lightbox-image">
                                        <img src="{!! url(Storage::url($media->images)) !!}" alt="" class="img-responsive center-block" style="max-width: 110px;">
                                    </a>
                                </td>
                                <td class="text-center">
                                    <input name="media_title" value="{{ $media->name }}" placeholder="Title for image"  data-url="{!! url('admin/media/update-name') !!}" data-id="{!! $media->id !!}" data-token="{!! csrf_token() !!}" class="form-control">
                                    <input name="media_order" value="{{ $media->order_by }}" placeholder="Order"  data-url="{!! url('admin/media/update-order') !!}" data-id="{!! $media->id !!}" data-token="{!! csrf_token() !!}" class="form-control">
                                </td>
                                <td class="text-center">
                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger delete-image" data-url="{!! url('admin/media/delete-image') !!}" data-id="{!! $media->id !!}" data-token="{!! csrf_token() !!}"><i class="fa fa-trash-o"></i> Delete</a>
                                </td>
                            </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                    <!-- END Product Images Content -->
                </div>

                <!-- END Product Images Content -->
            </div>
            @endif
            <!-- END Product Images Block -->
        </div>
    </div>
@endsection
