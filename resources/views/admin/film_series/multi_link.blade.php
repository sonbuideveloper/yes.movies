@extends('admin.master')
@section('content')
    <div class="content-header">
        <div class="header-section">
            <h1><i class="gi gi-justify"></i>{{ $film->name }} Links<br><small>Your Links Center</small></h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li>Pages</li>
        <li>Multi Links Center</li>
        <li><a href="">View Multi Links</a></li>
    </ul>
    <!-- Quick Stats -->
    <div class="row text-center">
        <div class="pull-left col-sm-6 col-lg-3">
            <a href="{!! url('admin/film-series/list-episode/'.$film->id) !!}" class="widget widget-hover-effect2">
                <div class="widget-extra themed-background-success">
                    <h4 class="widget-content-light"><strong>Edit</strong> Episode</h4>
                </div>
                <div class="widget-extra-full"><span class="h2 text-success animation-expandOpen"><i class="fa fa-pencil"></i></span></div>
            </a>
        </div>
    </div>
    <!-- END Quick Stats -->
    <!-- All Products Block -->
    <div class="block full">
        <!-- All Products Title -->
        <div class="block-title">
            <div class="block-options pull-right">
                <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Settings"><i class="fa fa-cog"></i></a>
            </div>
            <h2><strong>All</strong> Links</h2>
        </div>
        <!-- END All Products Title -->

        @if(Session::get('delete'))
            <div class="alert alert-success text-center">
                Success
            </div>
        @endif
        @if(Session::get('error'))
            <div class="alert alert-danger text-center">
                {!! Session::get('error') !!}
            </div>
        @endif

        <div>
            <p>Name_episode|your_link|your_email;<br>
                When u don't have email: Name_episode|your_link|;<br>
                * U can put link backup in there when They have the same name: ep 1|link|;<br>
                ep 1|link2|;<br>
                ep 3|link2|;</p>
        </div>
        <!-- All Products Content -->
        <input type="hidden" value="{{ $film->id }}" name="film_id" id="film_id">
        <input type="hidden" value="{{ $server_play[0]->id }}" name="server_id" id="server_id">
        <select class="form-control select-server-play-multi">
            @foreach($server_play as $value)
                <option value="{{ $value->id }}">{{ $value->name }}</option>
            @endforeach
        </select>
        <br>
        <?php
            $result = "";
            if($film->film_detail && count($film->film_detail)>0){
                foreach ($film->film_detail as $item){
                    if($item->link_play && count($item->link_play)>0){
                        foreach ($item->link_play as $element){
                            if($element->server_play == $server_play[0]->id){
                                $result .= $item->name.'|'.$element->link.'|'.$element->email.';'."\n";
                            }
                        }
                    }
                }
            }
        ?>

        <textarea class="form-control" id="data-content" rows="20" name="data_content">{{ $result }}</textarea>
        <br>
        <button class="btn btn-success btn-submit-data-multi" type="submit">Save</button>
    </div>

@endsection
