@extends('admin.master')
@section('content')
    <style>
        #general-table_wrapper>.row{
            display: none;
        }
    </style>
    <div class="content-header">
        <div class="header-section">
            <h1><i class="gi gi-pushpin"></i> TV Series<br><small>Your TV Series Center</small></h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li>Pages</li>
        <li>TV Series Center</li>
        <li><a href="{!! url('admin/film-series/list') !!}">View TV Series</a></li>
    </ul>
    <!-- Quick Stats -->
    <div class="row text-center">
        <div class="pull-left col-sm-6 col-lg-3">
            <a href="{!! url('admin/film-series/create') !!}" class="widget widget-hover-effect2">
                <div class="widget-extra themed-background-success">
                    <h4 class="widget-content-light"><strong>Add New</strong> Episode</h4>
                </div>
                <div class="widget-extra-full"><span class="h2 text-success animation-expandOpen"><i class="fa fa-plus"></i></span></div>
            </a>
        </div>
    </div>
    <!-- END Quick Stats -->

    <!-- All Products Block -->
    <div class="block full">
        <!-- All Products Title -->
        <div class="block-title">
            <div class="block-options pull-right">
                <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Settings"><i class="fa fa-cog"></i></a>
            </div>
            <h2><strong>All</strong> Films</h2>
        </div>
        <!-- END All Products Title -->

        @if(Session::get('delete'))
            <div class="alert alert-success text-center">
                Success
            </div>
        @endif
        <div class="pull-right search">
            <form action="" method="get">
                <input name="key" type="text" placeholder="Search" class="form-control" value="{{ isset($key)?$key:'' }}">
            </form>
        </div>
        <div class="clearfix"></div>
        <br>
        <!-- All Products Content -->
        <form action="{!! url('admin/film-series/delete') !!}" method="post">
            {!! csrf_field() !!}
            <table id="general-table" class="table table-bordered table-striped table-vcenter">
                <thead>
                <tr>
                    <th style="width: 80px;" class="text-center"><input type="checkbox"></th>
                    <th>Name</th>
                    <th>Parent</th>
                    <th>Status</th>
                    <th>Added</th>
                    <th style="width: 150px;" class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                {!! $result['html'] !!}
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="6">
                        {!! HtmlTfoot() !!}
                    </td>
                </tr>
                <tr>
                    <td colspan="6" class="text-right">
                        {!! $result['paginate'] !!}
                    </td>
                </tr>
                </tfoot>
            </table>
        </form>
        <!-- END All Products Content -->
    </div>
    <!-- Quick Stats -->
    <div class="row text-center">
        <div class="pull-right col-sm-6 col-lg-3">
            <a href="{!! url('admin/film-series/create') !!}" class="widget widget-hover-effect2">
                <div class="widget-extra themed-background-success">
                    <h4 class="widget-content-light"><strong>Add New</strong> Episode</h4>
                </div>
                <div class="widget-extra-full"><span class="h2 text-success animation-expandOpen"><i class="fa fa-plus"></i></span></div>
            </a>
        </div>
    </div>
    <!-- END Quick Stats -->
@endsection