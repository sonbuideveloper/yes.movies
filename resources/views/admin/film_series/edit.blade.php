@extends('admin.master')
@section('content')
    <div class="content-header">
        <div class="header-section">
            <h1><i class="gi gi-pencil"></i> Films<br><small>Create and Edit Films</small></h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li>Pages</li>
        <li><a href="{!! url('admin/film-series/list') !!}">Films Center</a></li>
        <li>{!! $action !!} Films</li>
    </ul>
    @if($action == 'Edit')
        <div class="row text-center">
            <div class="col-sm-6 col-lg-3">
                <a href="{!! url('admin/film-series/create') !!}" class="widget widget-hover-effect2">
                    <div class="widget-extra themed-background-success">
                        <h4 class="widget-content-light"><strong>Add New</strong> Film Series</h4>
                    </div>
                    <div class="widget-extra-full"><span class="h2 text-success animation-expandOpen"><i class="fa fa-plus"></i></span></div>
                </a>
            </div>
        </div>
    @endif
    @if(Session::get('success'))
        <div class="alert alert-success text-center">
            Success
        </div>
    @endif
    @if(Session::get('error'))
        <div class="alert alert-danger text-center">
            {{ Session::get('error') }}
        </div>
    @endif
    <div class="row">
        <div class="col-lg-6">
            <!-- General Data Block -->

            <div class="block">
                <!-- General Data Title -->
                <div class="block-title">
                    <h2><i class="fa fa-pencil"></i> <strong>General</strong> Data</h2>
                </div>
                <form action="{!! url('admin/film-series/create') !!}" method="post" class="form-horizontal form-bordered">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="{!! (!empty($result))?$result->id:0 !!}">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="name">Name</label>
                        <div class="col-md-9">
                            <input required type="text" id="name" name="name" data-lang="general" data-id="{!! (!empty($result))?$result->id:0 !!}" data-url="{!! url('admin/films/check-link') !!}" class="form-control" value="{!! (!empty($result))?$result->name:old('name') !!}" placeholder="Name...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="slug">Slug</label>
                        <div class="col-md-9">
                            <input type="text" id="slug" name="slug" class="form-control" value="{!! (!empty($result))?$result->slug:old('slug') !!}" placeholder="Slug">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="film_id">Film</label>
                        <div class="col-md-9">
                            <select name="film_id" id="film_id" class="select-select2" style="width: 100%;" data-placeholder="Film">
                                @foreach($film as $item)
                                    <option value="{{ $item->id }}" @if(!empty($result) && $result->film_id == $item->id) selected @endif>{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="duration">Duration</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <input type="number" id="duration" name="duration" class="form-control" value="{!! (!empty($result))?$result->duration:old('duration') !!}" placeholder="Duration">
                                <span class="input-group-addon">Min</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="quality">Quality</label>
                        <div class="col-md-9">
                            <select id="quality" name="quality" class="select-select2" style="width: 100%;" data-placeholder="Choose quality">
                                <option value="trailer">Trailer</option>
                                <option value="scam">Scam</option>
                                <option value="sd" selected>SD</option>
                                <option value="hd">HD</option>
                                <option value="1080p">1080p</option>
                                <option value="4k">4K</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="content">Content</label>
                        <div class="col-md-12 text-content">
                            <textarea id="content" name="content" class="ckeditor" data-id="content">{!! (!empty($result))?stripslashes($result->content):old('content') !!}</textarea>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label">Published?</label>
                        <div class="col-md-9">
                            <label class="switch switch-primary">
                                <input type="checkbox" id="publish" name="publish" @if(!empty($result) && $result->publish == 1) checked @elseif($action == 'Create') checked @endif><span></span>
                            </label>
                        </div>
                    </div>

                    <input type="hidden" name="id_films" id="id_films">

                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        </div>
                    </div>
                    <!-- END General Data Content -->

                </form>
            </div>
            <!-- END General Data Block -->
        </div>
        <div class="col-lg-6">
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-picture-o"></i> Link <strong>Film</strong></h2>
                </div>
                <div class="block-section">
                    <form class="form-horizontal" action="{{ url('admin/link-film-detail/create') }}" method="post">
                        {!! csrf_field() !!}
                        <input type="hidden" name="film_id" value="{{ (!empty($result))?$result->id:0 }}">
                        <input type="hidden" name="id" value="0">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="server_play">Server Film</label>
                            <div class="col-md-9">
                                <select class="form-control" name="server_play">
                                    @foreach($server_play as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-3 control-label" for="link">Link Film</label>
                            <div class="col-md-9">
                                <input required type="text" class="form-control" value="" name="link" placeholder="Link">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="email">Email</label>
                            <div class="col-md-9">
                                <input type="email" class="form-control" value="" name="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add</button>
                            </div>
                        </div>
                    </form>
                    <form action="{!! url('admin/link-film-detail/delete') !!}" method="post">
                        {!! csrf_field() !!}
                        <table id="general-table" class="table table-bordered table-striped table-vcenter">
                            <thead>
                                <th style="width: 80px;" class="text-center"><input type="checkbox"></th>
                                <th>Server</th>
                                <th>Link</th>
                                <th>Email</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                @if( !empty($result) && count($result->link_play) > 0)
                                    @foreach($result->link_play as $item)
                                        <tr>
                                            <td class="text-center">
                                                <input type="checkbox" id="checkbox-{{ $item->id }}" name="id[]" value="{{ $item->id }}">
                                            </td>
                                            <td>{{ $item->server->name }}</td>
                                            <td style="max-width: 100px; overflow: hidden;">{{ $item->link }}</td>
                                            <td style="max-width: 100px; overflow: hidden;">{{ $item->email }}</td>
                                            <td class="text-center">
                                                <div class="btn-group btn-group-xs">
                                                    <a data-id="{{ $item->id }}" data-url="{{ url('/') }}" href="javascript:void(0)" data-toggle="tooltip" title="Edit" class="btn btn-default btn-detail-edit"><i class="fa fa-pencil"></i></a>
                                                    {!! HtmlDeleteRecord(url('admin/link-film-detail/delete/'.$item->id)) !!}
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                <a style="display: none;" data-toggle="modal" data-target="#modalEdit" class="btn btn-default btn-submit"><i class="fa fa-pencil"></i></a>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="5">
                                    {!! HtmlTfoot() !!}
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </form>
                </div>
            </div>
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-picture-o"></i> Subtitle <strong>Film</strong></h2>
                </div>
                <div class="block-section">
                    <form class="form-horizontal" action="{{ url('admin/subtitle-detail/create') }}" method="post" enctype="multipart/form-data">
                        {!! csrf_field() !!} 
                        <input type="hidden" name="film_id" value="{{ (!empty($result))?$result->id:0 }}">
                        <input type="hidden" name="id" value="0">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="language">Language</label>
                            <div class="col-md-9">
                                <input required class="form-control" type="text" id="language" name="language">
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-3 control-label" for="subtitle">Link Film</label>
                            <div class="col-md-9">
                                <input required class="form-control" type="file" id="subtitle" name="subtitle">
                            </div>
                        </div>
                        
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add</button>
                            </div>
                        </div>
                    </form>
                    <table class="table table-bordered table-striped table-vcenter">
                        <thead>
                            <th>Language</th>
                            <th>Name</th>                           
                            <th>Delete</th>
                        </thead>
                        <tbody>
                            @if( !empty($result) && count($result->subtitle) > 0)
                                @foreach($result->subtitle as $item)
                                    <tr>                                    
                                        <td>{{ $item->language }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td class="text-center">
                                            <div class="btn-group btn-group-xs">                                                
                                                {!! HtmlDeleteRecord(url('admin/subtitle-detail/delete/'.$item->id)) !!}
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach                               
                            @endif

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">        
            <form class="form-horizontal" action="{{ url('admin/link-film-detail/create') }}" method="post">
                <div class="modal-body">
                    {!! csrf_field() !!}
                    <input type="hidden" name="film_id" id="film_id" value="{{ (!empty($result))?$result->id:0 }}">
                    <input type="hidden" name="id" value="" id="link_film_id">
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="server_play">Server Film</label>
                        <div class="col-md-9">
                            <select class="form-control" name="server_play" id="server_play">
                                @foreach($server_play as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                    <label class="col-md-3 control-label" for="link">Link Film</label>
                        <div class="col-md-9">
                            <input required type="text" class="form-control" id="link_film" name="link" placeholder="Link">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="email">Email</label>
                        <div class="col-md-9">
                            <input type="email" class="form-control email" id="email" value="" name="email" placeholder="Email">
                        </div>
                    </div>                            
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
