@extends('admin.master')
@section('content')
    <div class="content-header">
        <div class="header-section">
            <h1><i class="gi gi-justify"></i>{{ $film->name }} Links<br><small>Your Links Center</small></h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li>Pages</li>
        <li>Multi Links Center</li>
        <li><a href="">View Multi Links</a></li>
    </ul>

    <!-- All Products Block -->
    <div class="block full">
        <!-- All Products Title -->
        <div class="block-title">
            <div class="block-options pull-right">
                <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Settings"><i class="fa fa-cog"></i></a>
            </div>
            <h2><strong>All</strong> Links</h2>
        </div>
        <!-- END All Products Title -->

        @if(Session::get('delete'))
            <div class="alert alert-success text-center">
                Success
            </div>
    @endif
    <!-- All Products Content -->
        <form action="{!! url('admin/film-series/multi-link') !!}" method="post">
            {!! csrf_field() !!}
            <input type="hidden" value="{{ $film->id }}" name="film_id">

            @if(count($episode)>0)
                <?php $i=0; ?>
                @foreach($episode as $item)
                    <?php $i++; ?>
                    <table class="table table-bordered table-striped table-vcenter table_{{ $i }}">
                        <thead>
                            <tr><th colspan="3">Episode {{ $i }}</th></tr>
                        </thead>
                        <tbody class="tbody_{{ $i }}">
                            @if(count($item->link_play)>0)
                                <?php $j=0; ?>
                                @foreach($item->link_play as $element)
                                    <?php $j++; ?>
                                    <tr>
                                        <input type="hidden" value="{{ $i }}" name="episode[]">
                                        <td>Link</td>
                                        <td style="width: 120px;">
                                            <select class="form-control" name="server_play[]">
                                                @foreach($server_play as $value)
                                                    <option value="{{ $value->id }}" {{ $element->server_play==$value->id?'selected':'' }}>{{ $value->name }}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td><input type="text" class="form-control" placeholder="Link" value="{{ $element->link }}" name="link[]"></td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                        <tfoot>
                            <tr class="text-right">
                                <td colspan="3">
                                    <a data-url="{{ url('/') }}" data-episode="{{ $i }}" data-link="{{ $j }}" href="javascript:void(0)" class="btn btn-sm btn-primary btn-addmore">Add more</a>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                @endforeach
            @endif
            <button class="btn btn-success" type="submit">Save</button>
        </form>
        <!-- END All Products Content -->
    </div>

@endsection
