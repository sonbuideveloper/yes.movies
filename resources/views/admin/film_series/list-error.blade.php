@extends('admin.master')
@section('content')
    <style>
        #general-table_wrapper>.row{
            display: none;
        }
    </style>
    <div class="content-header">
        <div class="header-section">
            <h1><i class="gi gi-pushpin"></i> Link Error<br><small>Your Link Error Center</small></h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li>Pages</li>
        <li>Link Error Center</li>
        <li><a href="">View Link Error</a></li>
    </ul>

    <!-- All Products Block -->
    <div class="block full">
        <!-- All Products Title -->
        <div class="block-title">
            <div class="block-options pull-right">
                <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Settings"><i class="fa fa-cog"></i></a>
            </div>
            <h2><strong>All</strong> Link Error</h2>
        </div>
        <!-- END All Products Title -->

        @if(Session::get('success'))
            <div class="alert alert-success text-center">
                Success
            </div>
        @endif
        @if(Session::get('error'))
            <div class="alert alert-danger text-center">
                Error
            </div>
        @endif
        <form action="" method="get">
            <div class="col-md-6 col-xs-12 pull-left">
                <select name="sort" id="select-order" class="form-control" style="max-width: 300px;">
                    <option value="asc" {{ $sort=='asc'?'selected':'' }}>Ascending</option>
                    <option value="desc" {{ $sort=='desc'?'selected':'' }}>Decrease</option>
                </select>
            </div>
        </form>
        <form action="" method="get">
            <div class="col-md-6 col-xs-12">
                <div class="pull-right">
                    <input name="key" type="text"  style="max-width: 300px;" placeholder="Search" class="form-control" value="{{ isset($key)?$key:'' }}">
                </div>
            </div>
        </form>
        <div class="clearfix"></div>
        <br>
        <!-- All Products Content -->
        <form id="link-film-detail-error-form" action="" method="post">
            {!! csrf_field() !!}
            <table id="general-table" class="table table-bordered table-striped table-vcenter">
                <thead>
                <tr>
                    <th style="width: 30px;" class="text-center"><input type="checkbox"></th>
                    <th>Film</th>
                    <th>Episode</th>
                    <th>Thumb</th>
                    <th>Server Film</th>
                    <th style="width: 100px; overflow: hidden">Link</th>
                    <th>Status</th>
                    <th>Email</th>
                    <th style="width: 150px;" class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @if(count($link_film_detail_error)>0)
                    @foreach($link_film_detail_error as $item)
                        <?php
                        if($item->film->film->images != ''){
                            $images = "<a href=\"".url(getImage($item->film->film->images))."\" data-toggle='lightbox-image'>
                                    <img src=\"".url(getImage($item->film->film->images))."\" class='img-responsive center-block' style='max-width: 40px;'>
                                  </a>";
                        }elseif ($item->film->film->images_link != '') {
                            $images = "<a href=\"".$item->film->film->images_link."\" data-toggle='lightbox-image'>
                                    <img src=\"".$item->film->film->images_link."\" class='img-responsive center-block' style='max-width: 40px;'>
                                  </a>";
                        }else{
                            $images = '';
                        }
                        ?>
                        <tr>
                            <td class="text-center">
                                <input type="checkbox" id="checkbox-{{ $item->id }}" name="id[]" value="{{ $item->id }}">
                            </td>
                            <td><a href="{{ url('admin/film/edit/'.$item->film->film->id) }}">{{ $item->film->film->name }}</a></td>
                            <td><a href="{{ url('admin/film-series/edit/'.$item->film->id) }}">{{ $item->film->name }}</a></td>
                            <td>{!! $images !!}</td>
                            <td>{{ $item->server->name }}</td>
                            <td class="link_film" style="max-width: 220px; overflow: hidden">{{ $item->link }}</td>
                            <td class="text-center"><a class="label label-danger" href="{{ url('admin/link-film-detail/check-status/'.$item->id) }}">Error</a></td>
                            <td class="text-center email" style="max-width: 100px; overflow: hidden">{{ $item->email }}</td>
                            <td class="text-center">
                                <div class="btn-group btn-group-xs">
                                    <a href="{{ url('admin/link-film-detail/check-status/'.$item->id) }}" data-toggle="tooltip" title="Active" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                                    <a data-id="{{ $item->id }}" data-url="{{ url('/') }}" href="javascript:void(0)" data-toggle="tooltip" title="Edit" class="btn btn-default btn-link-film-detail-error"><i class="fa fa-pencil"></i></a>
                                    {!! HtmlDeleteRecord(url('admin/link-film-detail/delete/'.$item->id)) !!}
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endif
                <a style="display: none;" data-toggle="modal" data-target="#modalEdit" class="btn btn-default btn-submit"><i class="fa fa-pencil"></i></a>
                <a style="display: none;" data-toggle="modal" data-target="#modalDetailEdit" class="btn btn-default btn-detail-submit"><i class="fa fa-pencil"></i></a>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="9">
                        <a href="javascript:void(0)" data-url="{{ url('admin/link-film-detail/delete') }}" class="btn btn-sm btn-danger btn-all-link-detail-submit" data-toggle="tooltip" title="Delete Selected"><i class="fa fa-times"></i> Delete All</a>
                        <a href="javascript:void(0)" data-url="{{ url('admin/link-film-detail/active') }}" class="btn btn-sm btn-primary btn-all-link-detail-submit" data-toggle="tooltip" title="Active Selected"><i class="fa fa-times"></i> Active All</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="9" class="text-right">
                        {!! $link_film_detail_error->appends(['key' => $key])->render() !!}
                    </td>
                </tr>
                </tfoot>
            </table>
        </form>
        <!-- END All Products Content -->
    </div>

    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Link</h4>
                </div>
                <form class="form-horizontal" action="{{ url('admin/link-film/link-film-error/edit') }}" method="post">
                    <div class="modal-body">
                        {!! csrf_field() !!}
                        <input type="hidden" name="id" value="" id="link_film_id">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="link">Link Film</label>
                            <div class="col-md-9">
                                <input required type="text" class="form-control" id="link_film" name="link" placeholder="Link">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="email">Email</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDetailEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Link</h4>
                </div>
                <form class="form-horizontal" action="{{ url('admin/link-film-detail/link-film-error/edit') }}" method="post">
                    <div class="modal-body">
                        {!! csrf_field() !!}
                        <input type="hidden" name="id" value="" id="link_film_detail_id">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="link">Link Film</label>
                            <div class="col-md-9">
                                <input required type="text" class="form-control" id="link_film_detail" name="link" placeholder="Link">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="email">Email</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="email_detail" name="email" placeholder="Email">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection