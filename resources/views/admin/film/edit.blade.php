@extends('admin.master')
@section('content')
    <div class="content-header">
        <div class="header-section">
            <h1><i class="gi gi-pencil"></i> Films<br><small>Create and Edit Films</small></h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li>Pages</li>
        <li><a href="{!! url('admin/film/list') !!}">Films Center</a></li>
        <li>{!! $action !!} Films</li>
    </ul>
    @if($action == 'Edit')
        <div class="row text-center">
            <div class="col-sm-6 col-lg-3">
                <a href="{!! url('admin/film/create') !!}" class="widget widget-hover-effect2">
                    <div class="widget-extra themed-background-success">
                        <h4 class="widget-content-light"><strong>Add New</strong> Film</h4>
                    </div>
                    <div class="widget-extra-full"><span class="h2 text-success animation-expandOpen"><i class="fa fa-plus"></i></span></div>
                </a>
            </div>
        </div>
    @endif
    @if(Session::get('success'))
        <div class="alert alert-success text-center">
            Success
        </div>
    @endif
    @if(Session::get('error'))
        <div class="alert alert-danger text-center">
            {{ Session::get('error') }}
        </div>
    @endif
    <div class="row">
        <div class="col-lg-6">
            <!-- General Data Block -->

            <div class="block">
                <!-- General Data Title -->
                <div class="block-title">
                    <h2><i class="fa fa-pencil"></i> <strong>General</strong> Data</h2>
                </div>
                <form action="{!! url('admin/film/create') !!}" method="post" class="form-horizontal form-bordered">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="{!! (!empty($result))?$result->id:0 !!}">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if($action == 'Create')
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="url_film">URL Film</label>
                        <div class="col-md-9">
                            <input type="text" id="url_film" name="url_film" class="form-control" placeholder="URL film...">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <a class="btn btn-sm btn-primary search_info_film"><i style="display: none;" id="buttonloading" class="fa fa-spinner fa-spin"></i><i class="fa fa-search"></i> Search</a>
                        </div>
                    </div>
                    @endif
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="name">Name</label>
                        <div class="col-md-9">
                            <input required type="text" id="name" name="name" data-lang="general" data-id="{!! (!empty($result))?$result->id:0 !!}" data-url="{!! url('admin/films/check-link') !!}" class="form-control" value="{!! (!empty($result))?$result->name:old('name') !!}" placeholder="Name...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="slug">Slug</label>
                        <div class="col-md-9">
                            <input type="text" id="slug" name="slug" class="form-control" value="{!! (!empty($result))?$result->slug:old('slug') !!}" placeholder="Slug">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="meta-title">Title</label>
                        <div class="col-md-9">
                            <input type="text" id="meta-title" name="meta-title" class="form-control" value="{!! (!empty($result))?$result->title:old('meta-title') !!}" placeholder="Enter meta title..">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="meta-keywords">Keywords</label>
                        <div class="col-md-9">
                            <input type="text" id="meta-keywords" name="meta-keywords" class="form-control" value="{!! (!empty($result))?$result->keywords:old('meta-keywords') !!}" placeholder="keyword1, keyword2, keyword3">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="meta-description">Description</label>
                        <div class="col-md-9">
                            <textarea id="meta-description" name="meta-description" class="form-control" rows="6" placeholder="Enter meta description..">{!! (!empty($result))?$result->descriptions:old('meta-description') !!}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="country_film_id">Country</label>
                        <div class="col-md-9" id="country_film_select">
                            <select name="country_film_id" id="country_film_id" class="form-control" style="width: 100%;" data-placeholder="Country">
                                @foreach($film_country as $item)
                                    <option value="{{ $item->id }}" @if(!empty($result) && $result->country_film_id == $item->id) selected @endif>{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="genre_film_id">Genres</label>
                        <div class="col-md-offset-3" style="max-height: 500px;" id="genre_film_checkbox">
                            @foreach($film_genre as $item)
                                <?php
                                    $status_genre = 0;
                                    if(!empty($result) && $result->genres){
                                        foreach ($result->genres as $element){
                                            if($element->id == $item->id){
                                                $status_genre = 1;
                                                break;
                                            }
                                        }
                                    }
                                ?>
                                <div class="col-md-6">
                                    <label class="checkbox-inline" for="genre_film_id_{{ $item->id }}">
                                        <input {{ $status_genre?'checked':'' }} type="checkbox" id="genre_film_id_{{ $item->id }}" name="genre_film_id[]" value="{{ $item->id }}"> {{ $item->name }}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="type_film_id">Types</label>
                        <div class="col-md-9" style="max-height: 500px;">
                            @foreach($film_type as $item)
                                <?php
                                $status_type = 0;
                                if(!empty($result) && $result->type){
                                    foreach ($result->type as $element){
                                        if($element->id == $item->id){
                                            $status_type = 1;
                                            break;
                                        }
                                    }
                                }
                                ?>
                                <div class="checkbox">
                                    <label for="type_film_id_{{ $item->id }}">
                                        <input {{ $status_type?'checked':'' }} type="checkbox" id="type_film_id_{{ $item->id }}" name="type_film_id[]" value="{{ $item->id }}"> {{ $item->name }}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="sex">Sex</label>
                        <div class="col-md-9">
                            <div class="radio">
                                <label for="sex_male">
                                    <input @if(!empty($result) && $result->sex == 'all') checked @elseif($action=='Create') checked @endif type="radio" id="sex_male" name="sex" value="all"> None
                                </label>
                            </div>
                            <div class="radio">
                                <label for="sex_male">
                                    <input {{ (!empty($result) && $result->sex == 'male')?'checked':'' }} type="radio" id="sex_male" name="sex" value="male"> Male
                                </label>
                            </div>
                            <div class="radio">
                                <label for="sex_female">
                                    <input {{ (!empty($result) && $result->sex == 'female')?'checked':'' }} type="radio" id="sex_female" name="sex" value="female"> Female
                                </label>
                            </div>
                            <div class="radio">
                                <label for="sex_other">
                                    <input {{ (!empty($result) && $result->sex == 'other')?'checked':'' }} type="radio" id="sex_other" name="sex" value="other"> Other
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="age">Age</label>
                        <div class="col-md-9">
                            <input type="number" id="age" name="age" class="form-control" value="{!! (!empty($result))?$result->age:old('age') !!}" placeholder="Age">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="duration">Duration</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <input type="number" id="duration" name="duration" class="form-control" value="{!! (!empty($result))?$result->duration:old('duration') !!}" placeholder="Duration">
                                <span class="input-group-addon">Min</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="quality">Quality</label>
                        <div class="col-md-9">
                            <select id="quality" name="quality" class="select-select2" style="width: 100%;" data-placeholder="Choose quality">
                                <option {{ (!empty($result) && $result->quality=='trailer')?'selected':'' }} value="trailer">Trailer</option>
                                <option {{ (!empty($result) && $result->quality=='scam')?'selected':'' }} value="scam">Scam</option>
                                <option {{ (!empty($result) && $result->quality=='sd')?'selected':'' }} value="sd">SD</option>
                                <option {{ (!empty($result) && $result->quality=='hd')?'selected':'' }} value="hd">HD</option>
                                <option {{ (!empty($result) && $result->quality=='1080p')?'selected':'' }} value="1080p">1080p</option>
                                <option {{ (!empty($result) && $result->quality=='4k')?'selected':'' }} value="4k">4K</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="trailer">Trailer</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="trailer" id="trailer" value="{!! (!empty($result))?$result->trailer:old('trailer') !!}" placeholder="URL Trailer">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="chanel_id">Chanel</label>
                        <div class="col-md-9">
                            <select id="chanel_id" name="chanel_id" class="form-control" style="width: 100%;" data-placeholder="Choose chanel">
                                <option value="0">None</option>
                                @foreach($chanel as $item)
                                    <option {{ (!empty($result) && $result->chanel_id == $item->id)?'selected':'' }} value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="producer_id">Producer</label>
                        <div class="col-md-9">
                            <select id="producer_id" name="producer_id" class="form-control" style="width: 100%;" data-placeholder="Choose producer">
                                <option value="0">None</option>
                                @foreach($producer as $item)
                                    <option {{ (!empty($result) && $result->producer_id == $item->id)?'selected':'' }} value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="release">Release Year</label>
                        <div class="col-md-9">
                            <input type="number" id="release" name="release" class="form-control" value="{!! (!empty($result))?$result->release:old('release') !!}" placeholder="Release year">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="day_release">Day Release</label>
                        <div class="col-md-9">
                            <input type="text" id="day_release" name="day_release" class="form-control input-datepicker-close" data-date-format="yyyy-mm-dd" value="{!! (!empty($result))?$result->day_release:old('day_release') !!}" placeholder="Day Release">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="end_day">End Day</label>
                        <div class="col-md-9">
                            <input type="text" id="end_day" name="end_day" class="form-control input-datepicker-close" data-date-format="yyyy-mm-dd" value="{!! (!empty($result))?$result->end_day:old('end_day') !!}" placeholder="End Day">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="IMDB">IMDb</label>
                        <div class="col-md-9">
                            <input type="text" id="IMDB" name="IMDB" class="form-control" value="{!! (!empty($result))?$result->IMDB:old('IMDB') !!}" placeholder="IMDb">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="tag-film">Hagtag Film</label>
                        <div class="col-md-9">
                            <input id="tag-film" type="text" name="tag-film" placeholder="Hatgtag film" class="form-control" value="{!! (!empty($result) && $tag_film_item)?$tag_film_item:"" !!}">
                            <input type="hidden" id="tag_all" value="{{ $tag_film_all }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="cast">Hagtag Actor</label>
                        <div class="col-md-9 content-cast">
                            <input type="text" id="cast" name="cast" class="tags-input" value="{!! (!empty($result))?$cast:"" !!}">
                            <input type="hidden" id="cast_all" value="{{ $cast_all }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="director">Hagtag Director</label>
                        <div class="col-md-9 content-director">
                            <input type="text" id="director" name="director" class="tags-input" value="{!! (!empty($result))?$director:"" !!}">
                            <input type="hidden" id="director_all" value="{{ $director_all }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="content">Content</label>
                        <div class="col-md-12 text-content">
                            <textarea id="content" name="content" class="ckeditor" data-id="content">{!! (!empty($result))?stripslashes($result->content):old('content') !!}</textarea>
                        </div>
                    </div>
                    <div class="form-group profile_image_div" style="display: none;">
                        <label class="col-md-3 control-label" for="">Posters</label>
                        <div class="col-md-9">
                            <img id="profile_image" name="" src="" style="width: 50%;">
                            <input id="profile_image_input" type="hidden" name="image_api" value="">
                        </div>
                    </div>
                    <div class="form-group backdrops_image_div" style="display: none;">
                        <label class="col-md-3 control-label" for="">Backdrops</label>
                        <div class="col-md-9">
                            <img id="backdrops_image" name="" src="" style="width: 50%;">
                            <input id="backdrops_image_input" type="hidden" name="multi_image_api" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Slider</label>
                        <div class="col-md-9">
                            <label class="switch switch-success">
                                <input type="checkbox" id="slider" name="slider" @if(!empty($result) && $result->slider == 1) checked @endif><span></span>
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Is Movie Series?</label>
                        <div class="col-md-9">
                            <label class="switch switch-success">
                                <input type="checkbox" id="movie_series" name="movie_series" @if(!empty($result) && $result->movie_series == 1) checked @endif><span></span>
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Published?</label>
                        <div class="col-md-9">
                            <label class="switch switch-primary">
                                <input type="checkbox" id="publish" name="publish" @if(!empty($result) && $result->publish == 1) checked @elseif($action == 'Create') checked @endif><span></span>
                            </label>
                        </div>
                    </div>

                    <input type="hidden" name="id_films" id="id_films">

                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        </div>
                    </div>
                    <!-- END General Data Content -->

                </form>
            </div>
            <!-- END General Data Block -->
        </div>
        <div class="col-lg-6">
            <div class="row">
                <div class="col-md-6">
                    <!-- Product Images Block -->
                    <div class="block">
                        <!-- Product Images Title -->
                        <div class="block-title">
                            <h2><i class="fa fa-picture-o"></i> <strong>Thumb</strong></h2>
                        </div>
                        <!-- END Product Images Title -->
                        <!-- Product Images Content -->
                        <div class="block-section">
                            <ul class="nav nav-tabs" data-toggle="tabs">
                                <li class="@if(!empty($result) && $result->images_link == '') active @elseif(empty($result)) active @endif"><a href="#images_upload" data-toggle="tooltip" title="Upload">Upload</a></li>
                                <li class="{{ (!empty($result) && $result->images_link)?'active':'' }}"><a href="#images_link" data-toggle="tooltip" title="Link">Link</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane @if(!empty($result) && $result->images_link == '') active @elseif(empty($result)) active @endif" id="images_upload">
                                    <br>
                                    <form id="my-awesome-dropzone" data-multiple="false" data-maxfile="1" action="{!! url('admin/film/images') !!}" class="dropzone images">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="id" value="{!! (!empty($result))?$result->id:0 !!}">
                                        <div class="dropzone-previews"></div>
                                        <div class="fallback"> <!-- this is the fallback if JS isn't working -->
                                            <input name="file" type="file" multiple />
                                        </div>
                                        <div class="help-block">1 files Max</div>
                                        <div class="help-block">Max file size 20MB</div>
                                    </form>
                                    <div class="form-horizontal" style="padding-top: 15px;">
                                        <div class="form-group form-actions">
                                            <div class="col-md-9 col-md-offset-3">
                                                <button id="submit-all" type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                                            </div>
                                        </div>
                                    </div>
                                    @if((!empty($result)) && $result->images)
                                    <table class="table table-bordered table-striped table-vcenter">
                                        <tbody>
                                            <tr>
                                                <td style="width: 20%;">
                                                    <a href="{!! url(Storage::url($result->images)) !!}" data-toggle="lightbox-image">
                                                        <img src="{!! url(Storage::url($result->images)) !!}" alt="" class="img-responsive center-block" style="max-width: 110px;">
                                                    </a>
                                                </td>
                                                <td class="text-right">
                                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger delete-image" data-url="{!! url('admin/film/delete-image') !!}" data-id="{!! $result->id !!}" data-token="{!! csrf_token() !!}"><i class="fa fa-trash-o"></i> Delete</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    @endif
                                    <!-- END Product Images Content -->
                                </div>
                                <div class="tab-pane {{ (!empty($result) && $result->images_link)?'active':'' }}" id="images_link">
                                    <br>
                                    <form class="form-horizontal" action="{!! url('admin/film/images-link') !!}" method="post">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="id" value="{{ (!empty($result))?$result->id:0 }}">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="images_link">Link Images</label>
                                            <div class="col-md-9">
                                                <input type="text" id="images_link" name="images_link" class="form-control" value="{!! (!empty($result))?$result->images_link:old('images_link') !!}" placeholder="Link Images">
                                            </div>
                                        </div>
                                        <div class="form-group form-actions">
                                            <div class="col-md-9 col-md-offset-3">
                                                <button id="submit-all" type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                                            </div>
                                        </div>
                                    </form>
                                    @if((!empty($result) && $result->images_link))
                                    <table class="table table-bordered table-striped table-vcenter">
                                        <tbody>
                                        <tr>
                                            <td style="width: 20%;">
                                                <a href="{!! url($result->images_link) !!}" data-toggle="lightbox-image">
                                                    <img src="{!! url($result->images_link) !!}" alt="" class="img-responsive center-block" style="max-width: 110px;">
                                                </a>
                                            </td>
                                            <td class="text-right">
                                                <a href="{!! url('admin/film/delete-image-link/'.$result->id) !!}" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i> Delete</a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">

                    <div class="block">
                        <!-- Product Images Title -->
                        <div class="block-title">
                            <h2><i class="fa fa-picture-o"></i> <strong>Cover</strong></h2>
                        </div>
                        <!-- END Product Images Title -->
                        <!-- Product Images Content -->
                        <div class="block-section">
                            <ul class="nav nav-tabs" data-toggle="tabs">
                                <li class="@if(!empty($result) && $result->cover_link == '') active @elseif(empty($result)) active @endif"><a href="#cover_upload_panel" data-toggle="tooltip" title="Upload">Upload</a></li>
                                <li class="{{ (!empty($result) && $result->cover_link)?'active':'' }}"><a href="#cover_link" data-toggle="tooltip" title="Link">Link</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane @if(!empty($result) && $result->cover_link == '') active @elseif(empty($result)) active @endif" id="cover_upload_panel">
                                    <br>
                                    <form method="post" id="favicon-dropzone" data-multiple="false" data-maxfile="1" action="{!! url('admin/film/cover') !!}" class="dropzone images">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="id" value="{!! (!empty($result))?$result->id:0 !!}">
                                        <div class="dropzone-previews"></div>
                                        <div class="fallback"> <!-- this is the fallback if JS isn't working -->
                                            <input name="file" type="file" multiple />
                                        </div>
                                        <div class="help-block">1 files Max</div>
                                        <div class="help-block">Max file size 20MB</div>
                                    </form>
                                    <div class="form-horizontal" style="padding-top: 15px;">
                                        <div class="form-group form-actions">
                                            <div class="col-md-9 col-md-offset-3">
                                                <button id="cover_upload_submit" type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                                            </div>
                                        </div>
                                    </div>
                                    @if((!empty($result)) && $result->cover)
                                        <table class="table table-bordered table-striped table-vcenter">
                                            <tbody>
                                            <tr>
                                                <td style="width: 20%;">
                                                    <a href="{!! url(Storage::url($result->cover)) !!}" data-toggle="lightbox-image">
                                                        <img src="{!! url(Storage::url($result->cover)) !!}" alt="" class="img-responsive center-block" style="max-width: 110px;">
                                                    </a>
                                                </td>
                                                <td class="text-right">
                                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger delete-image" data-url="{!! url('admin/film/delete-cover') !!}" data-id="{!! $result->id !!}" data-token="{!! csrf_token() !!}"><i class="fa fa-trash-o"></i> Delete</a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                @endif
                                <!-- END Product Images Content -->
                                </div>
                                <div class="tab-pane {{ (!empty($result) && $result->cover_link)?'active':'' }}" id="cover_link">
                                    <br>
                                    <form class="form-horizontal" action="{!! url('admin/film/cover-link') !!}" method="post">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="id" value="{{ (!empty($result))?$result->id:0 }}">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="cover_link">Link Cover</label>
                                            <div class="col-md-9">
                                                <input type="text" id="cover_link" name="cover_link" class="form-control" value="{!! (!empty($result))?$result->cover_link:old('cover_link') !!}" placeholder="Link Cover">
                                            </div>
                                        </div>
                                        <div class="form-group form-actions">
                                            <div class="col-md-9 col-md-offset-3">
                                                <button id="submit-all" type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                                            </div>
                                        </div>
                                    </form>
                                    @if((!empty($result) && $result->cover_link))
                                        <table class="table table-bordered table-striped table-vcenter">
                                            <tbody>
                                            <tr>
                                                <td style="width: 20%;">
                                                    <a href="{!! url($result->cover_link) !!}" data-toggle="lightbox-image">
                                                        <img src="{!! url($result->cover_link) !!}" alt="" class="img-responsive center-block" style="max-width: 110px;">
                                                    </a>
                                                </td>
                                                <td class="text-right">
                                                    <a href="{!! url('admin/film/delete-cover-link/'.$result->id) !!}" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i> Delete</a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="block">
                <!-- Product Images Title -->
                <div class="block-title">
                    <h2><i class="fa fa-picture-o"></i> <strong>Background</strong></h2>
                </div>
                <!-- END Product Images Title -->
                <!-- Product Images Content -->
                <div class="block-section">
                    <ul class="nav nav-tabs" data-toggle="tabs">
                        <li class="@if(!empty($result) && $result->background_link == '') active @elseif(empty($result)) active @endif"><a href="#background_panel_upload" data-toggle="tooltip" title="Upload">Upload</a></li>
                        <li class="{{ (!empty($result) && $result->background_link)?'active':'' }}"><a href="#background_panel_link" data-toggle="tooltip" title="Link">Link</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane @if(!empty($result) && $result->background_link == '') active @elseif(empty($result)) active @endif" id="background_panel_upload">
                            <br>
                            <form method="post" id="multiple-dropzone" data-multiple="false" data-maxfile="1" action="{!! url('admin/film/background') !!}" class="dropzone images">
                                {!! csrf_field() !!}
                                <input type="hidden" name="id" value="{!! (!empty($result))?$result->id:0 !!}">
                                <div class="dropzone-previews"></div>
                                <div class="fallback"> <!-- this is the fallback if JS isn't working -->
                                    <input name="file" type="file" multiple />
                                </div>
                                <div class="help-block">1 files Max</div>
                                <div class="help-block">Max file size 20MB</div>
                            </form>
                            <div class="form-horizontal" style="padding-top: 15px;">
                                <div class="form-group form-actions">
                                    <div class="col-md-9 col-md-offset-3">
                                        <button id="multiple-images" type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                                    </div>
                                </div>
                            </div>
                            @if((!empty($result)) && $result->background)
                                <table class="table table-bordered table-striped table-vcenter">
                                    <tbody>
                                    <tr>
                                        <td style="width: 20%;">
                                            <a href="{!! url(Storage::url($result->background)) !!}" data-toggle="lightbox-image">
                                                <img src="{!! url(Storage::url($result->background)) !!}" alt="" class="img-responsive center-block" style="max-width: 110px;">
                                            </a>
                                        </td>
                                        <td class="text-right">
                                            <a href="javascript:void(0)" class="btn btn-xs btn-danger delete-image" data-url="{!! url('admin/film/delete-background') !!}" data-id="{!! $result->id !!}" data-token="{!! csrf_token() !!}"><i class="fa fa-trash-o"></i> Delete</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                        @endif
                        <!-- END Product Images Content -->
                        </div>
                        <div class="tab-pane {{ (!empty($result) && $result->background_link)?'active':'' }}" id="background_panel_link">
                            <br>
                            <form class="form-horizontal" action="{!! url('admin/film/background-link') !!}" method="post">
                                {!! csrf_field() !!}
                                <input type="hidden" name="id" value="{{ (!empty($result))?$result->id:0 }}">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="">Link Cover</label>
                                    <div class="col-md-9">
                                        <input type="text" id="" name="background_link" class="form-control" value="{!! (!empty($result))?$result->background_link:old('background_link') !!}" placeholder="Link Background">
                                    </div>
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-9 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                                    </div>
                                </div>
                            </form>
                            @if((!empty($result) && $result->background_link))
                                <table class="table table-bordered table-striped table-vcenter">
                                    <tbody>
                                    <tr>
                                        <td style="width: 20%;">
                                            <a href="{!! url($result->background_link) !!}" data-toggle="lightbox-image">
                                                <img src="{!! url($result->background_link) !!}" alt="" class="img-responsive center-block" style="max-width: 110px;">
                                            </a>
                                        </td>
                                        <td class="text-right">
                                            <a href="{!! url('admin/film/delete-background-link/'.$result->id) !!}" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i> Delete</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @if(!empty($result) && $result->movie_series==0)
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-picture-o"></i> Link <strong>Film</strong></h2>
                </div>
                <div class="block-section">
                    <form class="form-horizontal">
                        <input type="hidden" name="film_id" id="film_link_id" value="{{ (!empty($result))?$result->id:0 }}">
                        <input type="hidden" name="id" value="0">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="server_play">Server Film</label>
                            <div class="col-md-9">
                                <select class="form-control" name="server_play" id="server_play">
                                    @foreach($server_play as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-3 control-label" for="link">Link Film</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" value="" name="link" id="link" placeholder="Link">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="email">Email</label>
                            <div class="col-md-9">
                                <input type="email" class="form-control" value="" name="email" id="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <a class="btn btn-sm btn-primary" id="btn-add-link"><i class="fa fa-plus"></i> Add</a>
                            </div>
                        </div>
                    </form>

                    <form action="{!! url('admin/link-film/delete') !!}" method="post">
                        {!! csrf_field() !!}
                        <table id="general-table" class="table table-bordered table-striped table-vcenter">
                            <thead>
                                <th style="width: 80px;" class="text-center"><input type="checkbox"></th>
                                <th>Server</th>
                                <th>Link</th>
                                <th>Email</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                @if(count($result->link_play) > 0)
                                    @foreach($result->link_play as $item)
                                        <tr>
                                            <td class="text-center">
                                                <input type="checkbox" class="checkbox_id" id="checkbox-{{ $item->id }}" name="id[]" value="{{ $item->id }}">
                                            </td>
                                            <td>{{ $item->server->name }}</td>
                                            <td style="max-width: 100px; overflow: hidden;">{{ $item->link }}</td>
                                            <td style="max-width: 100px; overflow: hidden;">{{ $item->email }}</td>
                                            <td class="text-center">
                                                <div class="btn-group btn-group-xs">
                                                    <a data-id="{{ $item->id }}" data-url="{{ url('/') }}" href="javascript:void(0)" data-toggle="tooltip" title="Edit" class="btn btn-default btn-edit"><i class="fa fa-pencil"></i></a>
                                                    {!! HtmlDeleteAjax($item->id) !!}
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="5">
                                    <div class="btn-group btn-group-sm">
                                        <a href="javascript:void(0)" class="btn btn-danger form-submit-delete-all" data-toggle="tooltip" title="Delete Selected"><i class="fa fa-times"></i> Delete All</a>
                                    </div>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </form>
                </div>
            </div>
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-picture-o"></i> Subtitle <strong>Film</strong></h2>
                </div>
                <div class="block-section">
                    <form class="form-horizontal" action="{{ url('admin/subtitle/create') }}" method="post" enctype="multipart/form-data">
                        {!! csrf_field() !!} 
                        <input type="hidden" name="film_id" value="{{ (!empty($result))?$result->id:0 }}">
                        <input type="hidden" name="id" value="0">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="language">Language</label>
                            <div class="col-md-9">
                                <input required class="form-control" type="text" id="language" name="language">
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-3 control-label" for="subtitle">Link Film</label>
                            <div class="col-md-9">
                                <input required class="form-control" type="file" id="subtitle" name="subtitle">
                            </div>
                        </div>
                        
                        <div class="form-group form-actions">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add</button>
                            </div>
                        </div>
                    </form>
                    <table class="table table-bordered table-striped table-vcenter">
                        <thead>
                            <th>Language</th>
                            <th>Name</th>                           
                            <th>Delete</th>
                        </thead>
                        <tbody>
                            @if(count($result->subtitle) > 0)                            
                                @foreach($result->subtitle as $item)
                                    <tr>                                    
                                        <td>{{ $item->language }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td class="text-center">
                                            <div class="btn-group btn-group-xs">                                                
                                                {!! HtmlDeleteRecord(url('admin/subtitle/delete/'.$item->id)) !!}
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach                               
                            @endif
                            <a style="display: none;" data-toggle="modal" data-target="#modalEdit" class="btn btn-default btn-submit"><i class="fa fa-pencil"></i></a>
                        </tbody>
                    </table>
                </div>
            </div>
            @elseif(!empty($result))
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-picture-o"></i> Add <strong>Episode</strong></h2>
                </div>
                <div class="block-section">
                    <a href="{{ url('admin/film-series/add-episode/'.$result->id) }}">Add Episode</a>
                </div>
            </div>
            <div class="block">
                <div class="block-title">
                    <h2><i class="fa fa-picture-o"></i> Add <strong>Links</strong></h2>
                </div>
                <div class="block-section">
                    <a href="{{ url('admin/film-series/multi-link/'.$result->id) }}">Add Multi Links</a>
                </div>
            </div>
            @endif
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">        
            <form class="form-horizontal">
                <div class="modal-body">
                    <input type="hidden" name="film_id" id="film_id_edit" value="{{ (!empty($result))?$result->id:0 }}">
                    <input type="hidden" name="id" value="" id="film_link_id_edit">
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="server_play">Server Film</label>
                        <div class="col-md-9">
                            <select class="form-control" name="server_play" id="server_play_edit">
                                @foreach($server_play as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                    <label class="col-md-3 control-label" for="link">Link Film</label>
                        <div class="col-md-9">
                            <input required type="text" class="form-control" id="link_edit" name="link" placeholder="Link">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="email">Email</label>
                        <div class="col-md-9">
                            <input type="email" class="form-control email" id="email_edit" value="" name="email" placeholder="Email">
                        </div>
                    </div>                            
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <a class="btn btn-primary" id="btn-edit-link">Save</a>
                </div>
            </form>
        </div>
      </div>
    </div>
@endsection
