@extends('admin.master')
@section('content')
    <div class="content-header">
        <div class="header-section">
            <h1><i class="gi gi-justify"></i>{{ $film->name }} Links<br><small>Your Links Center</small></h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li>Pages</li>
        <li>Multi Links Center</li>
        <li><a href="">View Multi Links</a></li>
    </ul>
    <!-- All Products Block -->
    <div class="block full content">
        <!-- All Products Title -->
        <div class="block-title">
            <div class="block-options pull-right">
                <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Settings"><i class="fa fa-cog"></i></a>
            </div>
            <h2><strong>All</strong> Links</h2>
        </div>
        <!-- END All Products Title -->

        @if(Session::get('delete'))
            <div class="alert alert-success text-center">
                Success
            </div>
        @endif
        @if(Session::get('error'))
            <div class="alert alert-danger text-center">
                {!! Session::get('error') !!}
            </div>
        @endif

        <div>
            <p>your_link|your_email;<br>
            When u don't have email: your_link|;<br>
            U can put link backup in there when you put more link<br>
            link1|;<br>
            link2|;
            </p>
        </div>
        <!-- All Products Content -->
        <input type="hidden" value="{{ $film->id }}" name="film_id" id="film_id">
        <input type="hidden" value="{{ $server_play[0]->id }}" name="server_id" id="server_id">
        <select class="form-control select-server-play">
            @foreach($server_play as $value)
                <option value="{{ $value->id }}">{{ $value->name }}</option>
            @endforeach
        </select>
        <br>
        <?php
            $result = "";
            if($film){
                if($film->link_play && count($film->link_play)>0){
                    foreach ($film->link_play as $element){
                        if($element->server_play == $server_play[0]->id){
                            $result .= $element->link.'|'.$element->email.';'."\n";
                        }
                    }
                }
            }
        ?>

        <textarea class="form-control" id="data-content" rows="10" name="data_content">{{ $result }}</textarea>
        <br>
        <button class="btn btn-success btn-submit-data" type="submit">Save</button>

    </div>

@endsection
