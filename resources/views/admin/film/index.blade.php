@extends('admin.master')
@section('content')
    <style>
        #general-table_wrapper>.row{
            display: none;
        }
    </style>
    <div class="content-header">
        <div class="header-section">
            <h1><i class="gi gi-pushpin"></i> Films<br><small>Your Films Center</small></h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li>Pages</li>
        <li>Films Center</li>
        <li><a href="{!! url('admin/film/list') !!}">View Films</a></li>
    </ul>
    <!-- Quick Stats -->
    <div class="row text-center">
        <div class="pull-left col-sm-6 col-lg-3">
            <a href="{!! url('admin/film/create') !!}" class="widget widget-hover-effect2">
                <div class="widget-extra themed-background-success">
                    <h4 class="widget-content-light"><strong>Add New</strong> Films</h4>
                </div>
                <div class="widget-extra-full"><span class="h2 text-success animation-expandOpen"><i class="fa fa-plus"></i></span></div>
            </a>
        </div>
    </div>
    <!-- END Quick Stats -->

    <!-- All Products Block -->
    <div class="block full">
        <!-- All Products Title -->
        <div class="block-title">
            <div class="block-options pull-right">
                <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Settings"><i class="fa fa-cog"></i></a>
            </div>
            <h2><strong>All</strong> Films</h2>
        </div>
        <!-- END All Products Title -->

        @if(Session::get('success'))
            <div class="alert alert-success text-center">
                Success
            </div>
        @endif
        @if(Session::get('error'))
            <div class="alert alert-danger text-center">
                Error
            </div>
        @endif
        <!-- All Products Content -->
        <div class="row">
            <form method="get" id="form_filter_film">
                <div class="col-md-2 col-xs-6">
                    <div class="form-group">
                        <select name="type" class="form-control filter_film">
                            <option {{ $key=='all'?'selected':'' }} value="all">Type Movies</option>
                            <option {{ $key=='movies'?'selected':'' }} value="movies">Movies</option>
                            <option {{ $key=='tv-series'?'selected':'' }} value="tv-series">TV-Series</option>
                            @foreach($type as $item)
                                <option {{ $key==$item->slug?'selected':'' }} value="{{ $item->slug }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-offset-8 col-md-2 col-xs-6">
                    <div class="form-group">
                        <select name="sort" class="form-control filter_film">
                            <option {{ $sort=='desc'?'selected':'' }} value="desc">Decrease</option>
                            <option {{ $sort=='asc'?'selected':'' }} value="asc">Ascending</option>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-2 col-xs-6">
                    <div class="form-group">
                        <select name="status" class="form-control filter_film">
                            <option {{ $status=='all'?'selected':'' }} value="all">All</option>
                            <option {{ $status=='publish'?'selected':'' }} value="publish">Public</option>
                            <option {{ $status=='hidden'?'selected':'' }} value="hidden">Hidden</option>
                            <option {{ $status=='featured'?'selected':'' }} value="featured">Featured</option>
                            <option {{ $status=='normal'?'selected':'' }} value="normal">Normal</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-offset-8 col-md-2 col-xs-6">
                    <div class="pull-right">
                        <input name="keyword" type="text"  style="max-width: 300px;" placeholder="Search" class="form-control" value="{{ isset($keyword)?$keyword:'' }}">
                    </div>
                </div>
            </form>
        </div>
        <div class="clearfix"></div>
        <br>
        <form action="{!! url('admin/film/delete') !!}" method="post">
            {!! csrf_field() !!}
            <table id="general-table" class="table table-bordered table-striped table-vcenter">
                <thead>
                <tr>
                    <th style="width: 80px;" class="text-center"><input type="checkbox"></th>
                    <th>Name</th>
                    <th class="text-center">Thumb</th>
                    <th>User</th>
                    <th>Status|Featured</th>
                    <th>Added</th>
                    <th style="width: 150px;" class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                {!! $result['html'] !!}
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="7">
                        {!! HtmlTfoot() !!}
                    </td>
                </tr>
                <tr>
                    <td colspan="7" class="text-right">
                        {!! $result['paginate'] !!}
                    </td>
                </tr>
                </tfoot>
            </table>
        </form>
        <!-- END All Products Content -->
    </div>
    <!-- Quick Stats -->
    <div class="row text-center">
        <div class="pull-right col-sm-6 col-lg-3">
            <a href="{!! url('admin/film/create') !!}" class="widget widget-hover-effect2">
                <div class="widget-extra themed-background-success">
                    <h4 class="widget-content-light"><strong>Add New</strong> Films</h4>
                </div>
                <div class="widget-extra-full"><span class="h2 text-success animation-expandOpen"><i class="fa fa-plus"></i></span></div>
            </a>
        </div>
    </div>
    <!-- END Quick Stats -->
@endsection