@extends('admin.master')
@section('content')
    <div class="content-header">
        <div class="header-section">
            <h1><i class="gi gi-pencil"></i> Playlist<br><small>Create and Edit Playlist</small></h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li>Pages</li>
        <li><a href="{!! url('admin/playlist/list') !!}">Playlist Center</a></li>
        <li>{!! $action !!} Portfolio</li>
    </ul>
    @if($action == 'Edit')
        <div class="row text-center">
            <div class="col-sm-6 col-lg-3">
                <a href="{!! url('admin/playlist/create') !!}" class="widget widget-hover-effect2">
                    <div class="widget-extra themed-background-success">
                        <h4 class="widget-content-light"><strong>Add New</strong> Playlist</h4>
                    </div>
                    <div class="widget-extra-full"><span class="h2 text-success animation-expandOpen"><i class="fa fa-plus"></i></span></div>
                </a>
            </div>
        </div>
    @endif
    @if(Session::get('success'))
        <div class="alert alert-success text-center">
            Success
        </div>
    @endif
    <div class="row">
        <div class="col-lg-6">
            <!-- General Data Block -->
            <div class="block">
                <!-- General Data Title -->
                <div class="block-title">
                    <h2><i class="fa fa-pencil"></i> <strong>General</strong> Data</h2>
                </div>
                <form action="{!! url('admin/playlist/create') !!}" method="post" class="form-horizontal form-bordered">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="{!! (!empty($result))?$result->id:0 !!}">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="name">Name</label>
                        <div class="col-md-9">
                            <input required type="text" id="name" name="name" data-lang="general" data-id="{!! (!empty($result))?$result->id:0 !!}" data-url="{!! url('admin/films/check-link') !!}" class="form-control" value="{!! (!empty($result))?$result->name:old('name') !!}" placeholder="Name...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="slug">Slug</label>
                        <div class="col-md-9">
                            <input type="text" id="slug" name="slug" class="form-control" value="{!! (!empty($result))?$result->slug:old('slug') !!}" placeholder="Slug">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="meta-title">Title</label>
                        <div class="col-md-9">
                            <input type="text" id="meta-title" name="meta-title" class="form-control" value="{!! (!empty($result))?$result->title:old('meta-title') !!}" placeholder="Enter meta title..">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="meta-keywords">Keywords</label>
                        <div class="col-md-9">
                            <input type="text" id="meta-keywords" name="meta-keywords" class="form-control" value="{!! (!empty($result))?$result->keywords:old('meta-keywords') !!}" placeholder="keyword1, keyword2, keyword3">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="meta-description">Description</label>
                        <div class="col-md-9">
                            <textarea id="meta-description" name="meta-description" class="form-control" rows="6" placeholder="Enter meta description..">{!! (!empty($result))?$result->descriptions:old('meta-description') !!}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="tag-film">Hagtag Film</label>
                        <div class="col-md-9">
                            <input id="tag-film" type="text" name="tag-film" placeholder="Hatgtag film" class="form-control" value="{!! (!empty($result) && $tag_film_item)?$tag_film_item:"" !!}">
                            <input type="hidden" id="tag_all" value="{{ $tag_film_all }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Published?</label>
                        <div class="col-md-9">
                            <label class="switch switch-primary">
                                <input type="checkbox" id="publish" name="publish" @if(!empty($result) && $result->publish == 1) checked @elseif($action == 'Create') checked @endif><span></span>
                            </label>
                        </div>
                    </div>

                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        </div>
                    </div>
                    <!-- END General Data Content -->

                </form>
            </div>
            <!-- END General Data Block -->
        </div>
        <div class="col-lg-6">
            <div class="row">
                <div class="col-md-6">
                    <!-- Product Images Block -->
                    <div class="block">
                        <!-- Product Images Title -->
                        <div class="block-title">
                            <h2><i class="fa fa-picture-o"></i> <strong>Thumb</strong></h2>
                        </div>
                        <!-- END Product Images Title -->
                        <!-- Product Images Content -->
                        <div class="block-section">
                            <ul class="nav nav-tabs" data-toggle="tabs">
                                <li class="@if(!empty($result) && $result->images_link == '') active @elseif(empty($result)) active @endif"><a href="#images_upload" data-toggle="tooltip" title="Upload">Upload</a></li>
                                <li class="{{ (!empty($result) && $result->images_link)?'active':'' }}"><a href="#images_link" data-toggle="tooltip" title="Link">Link</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane @if(!empty($result) && $result->images_link == '') active @elseif(empty($result)) active @endif" id="images_upload">
                                    <br>
                                    <form id="my-awesome-dropzone" data-multiple="false" data-maxfile="1" action="{!! url('admin/playlist/images') !!}" class="dropzone images">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="id" value="{!! (!empty($result))?$result->id:0 !!}">
                                        <div class="dropzone-previews"></div>
                                        <div class="fallback"> <!-- this is the fallback if JS isn't working -->
                                            <input name="file" type="file" multiple />
                                        </div>
                                        <div class="help-block">1 files Max</div>
                                        <div class="help-block">Max file size 20MB</div>
                                    </form>
                                    <div class="form-horizontal" style="padding-top: 15px;">
                                        <div class="form-group form-actions">
                                            <div class="col-md-9 col-md-offset-3">
                                                <button id="submit-all" type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                                            </div>
                                        </div>
                                    </div>
                                    @if((!empty($result)) && $result->images)
                                        <table class="table table-bordered table-striped table-vcenter">
                                            <tbody>
                                            <tr>
                                                <td style="width: 20%;">
                                                    <a href="{!! url(Storage::url($result->images)) !!}" data-toggle="lightbox-image">
                                                        <img src="{!! url(Storage::url($result->images)) !!}" alt="" class="img-responsive center-block" style="max-width: 110px;">
                                                    </a>
                                                </td>
                                                <td class="text-right">
                                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger delete-image" data-url="{!! url('admin/playlist/delete-image') !!}" data-id="{!! $result->id !!}" data-token="{!! csrf_token() !!}"><i class="fa fa-trash-o"></i> Delete</a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                @endif
                                <!-- END Product Images Content -->
                                </div>
                                <div class="tab-pane {{ (!empty($result) && $result->images_link)?'active':'' }}" id="images_link">
                                    <br>
                                    <form class="form-horizontal" action="{!! url('admin/playlist/images-link') !!}" method="post">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="id" value="{{ (!empty($result))?$result->id:0 }}">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="images_link">Link Images</label>
                                            <div class="col-md-9">
                                                <input type="text" id="images_link" name="images_link" class="form-control" value="{!! (!empty($result))?$result->images_link:old('images_link') !!}" placeholder="Link Images">
                                            </div>
                                        </div>
                                        <div class="form-group form-actions">
                                            <div class="col-md-9 col-md-offset-3">
                                                <button id="submit-all" type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                                            </div>
                                        </div>
                                    </form>
                                    @if((!empty($result) && $result->images_link))
                                        <table class="table table-bordered table-striped table-vcenter">
                                            <tbody>
                                            <tr>
                                                <td style="width: 20%;">
                                                    <a href="{!! url($result->images_link) !!}" data-toggle="lightbox-image">
                                                        <img src="{!! url($result->images_link) !!}" alt="" class="img-responsive center-block" style="max-width: 110px;">
                                                    </a>
                                                </td>
                                                <td class="text-right">
                                                    <a href="{!! url('admin/playlist/delete-image-link/'.$result->id) !!}" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i> Delete</a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="block">
                        <!-- Product Images Title -->
                        <div class="block-title">
                            <h2><i class="fa fa-picture-o"></i> <strong>Cover</strong></h2>
                        </div>
                        <!-- END Product Images Title -->
                        <!-- Product Images Content -->
                        <div class="block-section">
                            <ul class="nav nav-tabs" data-toggle="tabs">
                                <li class="@if(!empty($result) && $result->cover_link == '') active @elseif(empty($result)) active @endif"><a href="#cover_upload_panel" data-toggle="tooltip" title="Upload">Upload</a></li>
                                <li class="{{ (!empty($result) && $result->cover_link)?'active':'' }}"><a href="#cover_link" data-toggle="tooltip" title="Link">Link</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane @if(!empty($result) && $result->cover_link == '') active @elseif(empty($result)) active @endif" id="cover_upload_panel">
                                    <br>
                                    <form method="post" id="favicon-dropzone" data-multiple="false" data-maxfile="1" action="{!! url('admin/playlist/cover') !!}" class="dropzone images">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="id" value="{!! (!empty($result))?$result->id:0 !!}">
                                        <div class="dropzone-previews"></div>
                                        <div class="fallback"> <!-- this is the fallback if JS isn't working -->
                                            <input name="file" type="file" multiple />
                                        </div>
                                        <div class="help-block">1 files Max</div>
                                        <div class="help-block">Max file size 20MB</div>
                                    </form>
                                    <div class="form-horizontal" style="padding-top: 15px;">
                                        <div class="form-group form-actions">
                                            <div class="col-md-9 col-md-offset-3">
                                                <button id="cover_upload_submit" type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                                            </div>
                                        </div>
                                    </div>
                                    @if((!empty($result)) && $result->cover)
                                        <table class="table table-bordered table-striped table-vcenter">
                                            <tbody>
                                            <tr>
                                                <td style="width: 20%;">
                                                    <a href="{!! url(Storage::url($result->cover)) !!}" data-toggle="lightbox-image">
                                                        <img src="{!! url(Storage::url($result->cover)) !!}" alt="" class="img-responsive center-block" style="max-width: 110px;">
                                                    </a>
                                                </td>
                                                <td class="text-right">
                                                    <a href="javascript:void(0)" class="btn btn-xs btn-danger delete-image" data-url="{!! url('admin/playlist/delete-cover') !!}" data-id="{!! $result->id !!}" data-token="{!! csrf_token() !!}"><i class="fa fa-trash-o"></i> Delete</a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                @endif
                                <!-- END Product Images Content -->
                                </div>
                                <div class="tab-pane {{ (!empty($result) && $result->cover_link)?'active':'' }}" id="cover_link">
                                    <br>
                                    <form class="form-horizontal" action="{!! url('admin/playlist/cover-link') !!}" method="post">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="id" value="{{ (!empty($result))?$result->id:0 }}">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="cover_link">Link Cover</label>
                                            <div class="col-md-9">
                                                <input type="text" id="cover_link" name="cover_link" class="form-control" value="{!! (!empty($result))?$result->cover_link:old('cover_link') !!}" placeholder="Link Cover">
                                            </div>
                                        </div>
                                        <div class="form-group form-actions">
                                            <div class="col-md-9 col-md-offset-3">
                                                <button id="submit-all" type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                                            </div>
                                        </div>
                                    </form>
                                    @if((!empty($result) && $result->cover_link))
                                        <table class="table table-bordered table-striped table-vcenter">
                                            <tbody>
                                            <tr>
                                                <td style="width: 20%;">
                                                    <a href="{!! url($result->cover_link) !!}" data-toggle="lightbox-image">
                                                        <img src="{!! url($result->cover_link) !!}" alt="" class="img-responsive center-block" style="max-width: 110px;">
                                                    </a>
                                                </td>
                                                <td class="text-right">
                                                    <a href="{!! url('admin/playlist/delete-cover-link/'.$result->id) !!}" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i> Delete</a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="block">
                <!-- Product Images Title -->
                <div class="block-title">
                    <h2><i class="fa fa-picture-o"></i> <strong>Background</strong></h2>
                </div>
                <!-- END Product Images Title -->
                <!-- Product Images Content -->
                <div class="block-section">
                    <ul class="nav nav-tabs" data-toggle="tabs">
                        <li class="@if(!empty($result) && $result->background_link == '') active @elseif(empty($result)) active @endif"><a href="#background_panel_upload" data-toggle="tooltip" title="Upload">Upload</a></li>
                        <li class="{{ (!empty($result) && $result->background_link)?'active':'' }}"><a href="#background_panel_link" data-toggle="tooltip" title="Link">Link</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane @if(!empty($result) && $result->background_link == '') active @elseif(empty($result)) active @endif" id="background_panel_upload">
                            <br>
                            <form method="post" id="multiple-dropzone" data-multiple="false" data-maxfile="1" action="{!! url('admin/playlist/background') !!}" class="dropzone images">
                                {!! csrf_field() !!}
                                <input type="hidden" name="id" value="{!! (!empty($result))?$result->id:0 !!}">
                                <div class="dropzone-previews"></div>
                                <div class="fallback"> <!-- this is the fallback if JS isn't working -->
                                    <input name="file" type="file" multiple />
                                </div>
                                <div class="help-block">1 files Max</div>
                                <div class="help-block">Max file size 20MB</div>
                            </form>
                            <div class="form-horizontal" style="padding-top: 15px;">
                                <div class="form-group form-actions">
                                    <div class="col-md-9 col-md-offset-3">
                                        <button id="multiple-images" type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                                    </div>
                                </div>
                            </div>
                            @if((!empty($result)) && $result->background)
                                <table class="table table-bordered table-striped table-vcenter">
                                    <tbody>
                                    <tr>
                                        <td style="width: 20%;">
                                            <a href="{!! url(Storage::url($result->background)) !!}" data-toggle="lightbox-image">
                                                <img src="{!! url(Storage::url($result->background)) !!}" alt="" class="img-responsive center-block" style="max-width: 110px;">
                                            </a>
                                        </td>
                                        <td class="text-right">
                                            <a href="javascript:void(0)" class="btn btn-xs btn-danger delete-image" data-url="{!! url('admin/playlist/delete-background') !!}" data-id="{!! $result->id !!}" data-token="{!! csrf_token() !!}"><i class="fa fa-trash-o"></i> Delete</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                        @endif
                        <!-- END Product Images Content -->
                        </div>
                        <div class="tab-pane {{ (!empty($result) && $result->background_link)?'active':'' }}" id="background_panel_link">
                            <br>
                            <form class="form-horizontal" action="{!! url('admin/playlist/background-link') !!}" method="post">
                                {!! csrf_field() !!}
                                <input type="hidden" name="id" value="{{ (!empty($result))?$result->id:0 }}">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="">Link Cover</label>
                                    <div class="col-md-9">
                                        <input type="text" id="" name="background_link" class="form-control" value="{!! (!empty($result))?$result->background_link:old('background_link') !!}" placeholder="Link Background">
                                    </div>
                                </div>
                                <div class="form-group form-actions">
                                    <div class="col-md-9 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                                    </div>
                                </div>
                            </form>
                            @if((!empty($result) && $result->background_link))
                                <table class="table table-bordered table-striped table-vcenter">
                                    <tbody>
                                    <tr>
                                        <td style="width: 20%;">
                                            <a href="{!! url($result->background_link) !!}" data-toggle="lightbox-image">
                                                <img src="{!! url($result->background_link) !!}" alt="" class="img-responsive center-block" style="max-width: 110px;">
                                            </a>
                                        </td>
                                        <td class="text-right">
                                            <a href="{!! url('admin/playlist/delete-background-link/'.$result->id) !!}" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i> Delete</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
