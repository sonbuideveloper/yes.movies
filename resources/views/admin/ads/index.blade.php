@extends('admin.master')
@section('content')
    <div class="content-header">
        <div class="header-section">
            <h1><i class="fa fa-file-text"></i> Ads<br><small>Your Ads Center</small></h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li>Pages</li>
        <li>Ads Center</li>
        <li><a href="{!! url('admin/ads/list') !!}">View Ads</a></li>
    </ul>

    <!-- All Products Block -->
    <div class="block full">
        <!-- All Products Title -->
        <div class="block-title">
            <h2><strong>All</strong> Ads</h2>
        </div>
        <!-- END All Products Title -->

        @if(Session::get('delete'))
            <div class="alert alert-success text-center">
                Success
            </div>
        @endif
        <!-- All Products Content -->
        <form action="{!! url('admin/ads/delete') !!}" method="post">
            {!! csrf_field() !!}
            <table id="general-table" class="table table-bordered table-striped table-vcenter">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Status</th>
                    <th style="width: 150px;" class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                {!! $result !!}
                </tbody>
            </table>
        </form>
        <!-- END All Products Content -->
    </div>
@endsection