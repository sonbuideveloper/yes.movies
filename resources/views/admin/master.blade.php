<?php
    $baseUrl = Storage::url('public/media');
?>
<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">

    <title>DD - Admin</title>

    <meta name="description" content="DD Admin">
    <meta name="author" content="Demon Dragon">
    <meta name="robots" content="noindex, nofollow">

    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="{{ url('backend/img/favicon.png') }}">
    <link rel="apple-touch-icon" href="{{ url('backend/img/icon57.png') }}" sizes="57x57">
    <link rel="apple-touch-icon" href="{{ url('backend/img/icon72.png') }}" sizes="72x72">
    <link rel="apple-touch-icon" href="{{ url('backend/img/icon76.png') }}" sizes="76x76">
    <link rel="apple-touch-icon" href="{{ url('backend/img/icon114.png') }}" sizes="114x114">
    <link rel="apple-touch-icon" href="{{ url('backend/img/icon120.png') }}" sizes="120x120">
    <link rel="apple-touch-icon" href="{{ url('backend/img/icon144.png') }}" sizes="144x144">
    <link rel="apple-touch-icon" href="{{ url('backend/img/icon152.png') }}" sizes="152x152">
    <link rel="apple-touch-icon" href="{{ url('backend/img/icon180.png') }}" sizes="180x180">
    <!-- END Icons -->

    <!-- Stylesheets -->
    <!-- Bootstrap is included in its original form, unaltered -->
    <link rel="stylesheet" href="{{ url('backend/css/bootstrap.min.css') }}">

    <!-- Related styles of various icon packs and plugins -->
    <link rel="stylesheet" href="{{ url('backend/css/plugins.css') }}">
    <link rel="stylesheet" href="{{ url('backend/css/icheck/minimal/blue.css') }}">

    <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
    <link rel="stylesheet" href="{{ url('backend/css/main.css') }}">

    <!-- Include a specific file here from /backend/css/themes/ folder to alter the default theme of the template -->

    <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
    <link rel="stylesheet" href="{{ url('backend/css/themes.css') }}">
    <link rel="stylesheet" href="{{ url('backend/css/themes/flatie.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('backend/jQuery-Tags-Input/css/typeahead.tagging.css') }}">
    <!-- END Stylesheets -->
    <link rel="stylesheet" href="{{ url('backend/jQuery-autoComplete/jquery.auto-complete.css') }}">
    <link href="{{ url('backend/js/helpers/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css">
    <!-- Modernizr (browser feature detection library) & Respond.js (enables responsive CSS code on browsers that don't support it, eg IE8) -->
    <script src="{{ url('backend/js/vendor/modernizr-respond.min.js') }}"></script>
</head>
<style>
    #general-table_wrapper{
        overflow-x: scroll;
    }
</style>
<body>
<!-- Page Wrapper -->
<!-- In the PHP version you can set the following options from inc/config file -->
<!--
    Available classes:

    'page-loading'      enables page preloader
-->
<div id="page-wrapper">
    <!-- Preloader -->
    <!-- Preloader functionality (initialized in /backend/js/app.js) - pageLoading() -->
    <!-- Used only if page preloader is enabled from inc/config (PHP version) or the class 'page-loading' is added in #page-wrapper element (HTML version) -->
    <div class="preloader themed-background">
        <h1 class="push-top-bottom text-light text-center"><strong>Pro</strong>UI</h1>
        <div class="inner">
            <h3 class="text-light visible-lt-ie9 visible-lt-ie10"><strong>Loading..</strong></h3>
            <div class="preloader-spinner hidden-lt-ie9 hidden-lt-ie10"></div>
        </div>
    </div>
    <div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations">
        <!-- Alternative Sidebar -->
        <div id="sidebar-alt">
            <!-- Wrapper for scrolling functionality -->
            <div id="sidebar-alt-scroll">
                <!-- Sidebar Content -->
                @include('admin.page_chat')
                <!-- END Sidebar Content -->
            </div>
            <!-- END Wrapper for scrolling functionality -->
        </div>
        <!-- END Alternative Sidebar -->

        <!-- Main Sidebar -->
        <div id="sidebar">
            <!-- Wrapper for scrolling functionality -->
            <div id="sidebar-scroll">
                <!-- Sidebar Content -->
                @include('admin.left')
                <!-- END Sidebar Content -->
            </div>
            <!-- END Wrapper for scrolling functionality -->
        </div>
        <!-- END Main Sidebar -->

        <!-- Main Container -->
        <div id="main-container">

            @include('admin.header')
            <!-- END Header -->

            <!-- Page content -->
            <div id="page-content">
                <!-- Dashboard Header -->
                <!-- For an image header add the class 'content-header-media' and an image as in the following example -->
                @yield('content')
                <!-- END Widgets Row -->
            </div>
            <!-- END Page Content -->

            <!-- Footer -->
            <footer class="clearfix">
                <div class="pull-right">
                    <span id="">Copyright  &copy;  {{ date('Y') }} </span> <a href="{{ url('/') }}" target="_blank">DD ADMIN</a>
                </div>
            </footer>
            <!-- END Footer -->
        </div>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
</div>
<!-- END Page Wrapper -->

<!-- Scroll to top link, initialized in /backend/js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>


<!-- END User Settings -->

<!-- Remember to include excanvas for IE8 chart support -->
<!--[if IE 8]><script src="{{ url('backend/js/helpers/excanvas.min.js') }}"></script><![endif]-->

<!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
<script src="{{ url('backend/js/vendor/jquery-1.12.0.min.js') }}"></script>
<script src="{{ url('backend/js/vendor/bootstrap.min.js') }}"></script>
<script src="{{ url('backend/js/plugins.js') }}"></script>
<script src="{{ url('backend/js/app.js') }}"></script>

<script src="http://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.10.4/typeahead.bundle.min.js"></script>
<script src="{{ url('backend/jQuery-Tags-Input/js/typeahead.tagging.js') }}"></script>

<script src="{{ url('backend/jQuery-autoComplete/jquery.auto-complete.js') }}"></script>

<script src="{{ url('backend/js/helpers/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>
<script src="{{ url('backend/js/vendor/jquery.sweet-alert.init.js') }}"></script>

<!-- Load and execute javascript code used only in this page -->
<script src="{{ url('backend/js/pages/index.js') }}"></script>
<script src="{{ url('backend/js/helpers/ckeditor/ckeditor.js') }}"></script>
<script src="{{ url('backend/js/jquery.slimscroll.min.js') }}"></script>
<script src="{{ url('backend/js/icheck.min.js') }}"></script>
<script src="{{ url('backend/js/helpers/jquery.price_format.2.0.min.js') }}"></script>
<script src="{{ url('backend/js/myscript.js') }}"></script>

<script>
    var hidden_delete = parseInt(<?=(Auth::user()->type == 2)?1:0?>);
    $("div.alert").delay(5000).slideUp();
</script>
<script src="{{ url('backend/js/main.js') }}"></script>
@if($active == 'film')
    <script type="text/javascript">
        var cast_all = $('#cast_all').val().split(",");
        var director_all = $('#director_all').val().split(",");
        $('#cast').tagging(cast_all);
        $('#director').tagging(director_all);
    </script>
@endif
@if($active == 'news' || $active == 'gallery' || $active == 'video')
    <script type="text/javascript">        
        var extraTagAll = $('#extraTagAll').val().split(",");
        var topicTagAll = $('#topicTagAll').val().split(",");
        var manualTagAll = $('#manualTagAll').val().split(",");
        var randomTagAll = $('#randomTagAll').val().split(",");
        $('#extraTag').tagging(extraTagAll);
        $('#topicTag').tagging(topicTagAll); 
        $('#manualTag').tagging(manualTagAll); 
        $('#randomTag').tagging(randomTagAll); 
    </script>
@endif

</body>
</html>