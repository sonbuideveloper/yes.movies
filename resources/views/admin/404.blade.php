<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">

    <title>404</title>

    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="robots" content="noindex, nofollow">

    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="{{ url('backend/img/favicon.png') }}">
    <link rel="apple-touch-icon" href="{{ url('backend/img/icon57.png') }}" sizes="57x57">
    <link rel="apple-touch-icon" href="{{ url('backend/img/icon72.png') }}" sizes="72x72">
    <link rel="apple-touch-icon" href="{{ url('backend/img/icon76.png') }}" sizes="76x76">
    <link rel="apple-touch-icon" href="{{ url('backend/img/icon114.png') }}" sizes="114x114">
    <link rel="apple-touch-icon" href="{{ url('backend/img/icon120.png') }}" sizes="120x120">
    <link rel="apple-touch-icon" href="{{ url('backend/img/icon144.png') }}" sizes="144x144">
    <link rel="apple-touch-icon" href="{{ url('backend/img/icon152.png') }}" sizes="152x152">
    <link rel="apple-touch-icon" href="{{ url('backend/img/icon180.png') }}" sizes="180x180">
    <!-- END Icons -->

    <!-- Stylesheets -->
    <!-- Bootstrap is included in its original form, unaltered -->
    <link rel="stylesheet" href="{{ url('backend/css/bootstrap.min.css') }}">

    <!-- Related styles of various icon packs and plugins -->
    <link rel="stylesheet" href="{{ url('backend/css/plugins.css') }}">
    <link rel="stylesheet" href="{{ url('backend/css/icheck/minimal/blue.css') }}">

    <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
    <link rel="stylesheet" href="{{ url('backend/css/main.css') }}">

    <!-- Include a specific file here from /backend/css/themes/ folder to alter the default theme of the template -->

    <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
    <link rel="stylesheet" href="{{ url('backend/css/themes.css') }}">
    <link rel="stylesheet" href="{{ url('backend/css/themes/flatie.css') }}">
    <!-- END Stylesheets -->

    <!-- Modernizr (browser feature detection library) & Respond.js (enables responsive CSS code on browsers that don't support it, eg IE8) -->
    <script src="{{ url('backend/js/vendor/modernizr-respond.min.js') }}"></script>
</head>
<body><div id="error-container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 text-center">
            <h1 class="animation-pulse"><i class="fa fa-exclamation-circle text-warning"></i> 404</h1>
            <h2 class="h3">Oops, we are sorry but the page you are looking for was not found..<br>But do not worry, we will have a look into it..</h2>
        </div>
    </div>
</div>
</body>
</html>