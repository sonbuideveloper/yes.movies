@extends('admin.master')
@section('content')
    <div class="content-header">
        <div class="header-section">
            <h1><i class="gi gi-pencil"></i> Server Film<br><small>Create and Edit Server Film</small></h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li>Pages</li>
        <li><a href="{!! url('admin/server-film/list') !!}">Server Film Center</a></li>
        <li>{!! $action !!} Portfolio</li>
    </ul>
    @if($action == 'Edit')
        <div class="row text-center">
            <div class="col-sm-6 col-lg-3">
                <a href="{!! url('admin/server-film/create') !!}" class="widget widget-hover-effect2">
                    <div class="widget-extra themed-background-success">
                        <h4 class="widget-content-light"><strong>Add New</strong> Server Film</h4>
                    </div>
                    <div class="widget-extra-full"><span class="h2 text-success animation-expandOpen"><i class="fa fa-plus"></i></span></div>
                </a>
            </div>
        </div>
    @endif
    @if(Session::get('success'))
        <div class="alert alert-success text-center">
            Success
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <!-- General Data Block -->

            <div class="block">
                <!-- General Data Title -->
                <div class="block-title">
                    <h2><i class="fa fa-pencil"></i> <strong>General</strong> Data</h2>
                </div>
                <form action="{!! url('admin/server-film/create') !!}" method="post" class="form-horizontal form-bordered">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="{!! (!empty($result))?$result->id:0 !!}">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="name">Name</label>
                        <div class="col-md-9">
                            <input type="text" id="name" name="name" data-lang="general" data-id="{!! (!empty($result))?$result->id:0 !!}" class="form-control" value="{!! (!empty($result))?$result->name:old('name') !!}" placeholder="Name...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="slug">Slug</label>
                        <div class="col-md-9">
                            <input type="text" id="slug" name="slug" class="form-control" value="{!! (!empty($result))?$result->slug:old('address') !!}" placeholder="Slug">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="embedded">Active Embed</label>
                        <div class="col-md-9">
                            <label class="switch switch-success">
                                <input type="checkbox" id="embedded" name="embedded" @if(!empty($result) && $result->embedded == 1) checked @endif><span></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Published?</label>
                        <div class="col-md-9">
                            <label class="switch switch-primary">
                                <input type="checkbox" id="publish" name="publish" @if(!empty($result) && $result->publish == 1) checked @elseif($action == 'Create') checked @endif><span></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        </div>
                    </div>
                    <!-- END General Data Content -->
                </form>
            </div>
            <!-- END General Data Block -->
        </div>
    </div>
@endsection
