<div class="sidebar-content">
    <!-- Brand -->
    <a href="{!! url('admin/dashboard') !!}" class="sidebar-brand">
        <i class="gi gi-flash"></i><span class="sidebar-nav-mini-hide"><strong>DD</strong> ADMIN</span>
    </a>
    <!-- END Brand -->

    <!-- User Info -->
    <div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
        <div class="sidebar-user-avatar">
            <a href="javascript:void(0)">
                @if(Auth::user()->social == '')
                <img src="{!! url((Auth::user()->images == '')? 'backend/img/placeholders/avatars/avatar2.jpg' : getImage(Auth::user()->images)) !!}" alt="avatar">
                @else
                <img src="{!! url((Auth::user()->images == '')? 'backend/img/placeholders/avatars/avatar2.jpg' : Auth::user()->images) !!}" alt="avatar">
                @endif
            </a>
        </div>
        <div class="sidebar-user-name">{!! $user->name !!}</div>
        <div class="sidebar-user-links">
            @if(Auth::user()->social == '')
            <a href="{!! url('admin/user/edit/'.Auth::user()->id) !!}" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user"></i></a>            
            @endif
            <a href="{!! url('admin/user/logout') !!}" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
        </div>
    </div>
    <!-- END User Info -->

    <!-- Sidebar Navigation -->
    <ul class="sidebar-nav">
        <li>
            <a href="{!! url('admin') !!}" class="@if($active == 'dashboard') active @endif"><i class="gi gi-stopwatch sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard</span></a>
        </li>

        <li class="sidebar-header">
            <span class="sidebar-header-options clearfix"><a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"></a></span>
            <span class="sidebar-header-title">Attribute</span>
        </li>
        {{-- <li>
            <a href="{!! url('admin/menu') !!}" class="@if($active == 'menu') active @endif"><i class="gi gi-justify sidebar-nav-icon"></i>Menu</a>
        </li> --}}
        <li>
            <a href="{!! url('admin/film/list') !!}" class="@if($active == 'film') active @endif"><i class="fa fa-film sidebar-nav-icon"></i>Films</a>
        </li>
        <li>
            <a href="{!! url('admin/film/upload') !!}" class="@if($active == 'upload') active @endif"><i class="fa fa-upload sidebar-nav-icon"></i>Upload Films</a>
        </li>
        <li>
            <a href="{!! url('admin/film/slider-home') !!}" class="@if($active == 'slider-home') active @endif"><i class="fa fa-sliders sidebar-nav-icon"></i>Slider Home</a>
        </li>
        <li>
            <a href="{!! url('admin/film-series/list') !!}" class="@if($active == 'film-series') active @endif"><i class="fa fa-file-video-o sidebar-nav-icon"></i>Episodes</a>
        </li>
        <li>
            <a href="{!! url('admin/film/link-error') !!}" class="@if($active == 'link-film-error') active @endif"><i class="fa fa-list sidebar-nav-icon"></i>Link Film Error</a>
        </li>
        <li>
            <a href="{!! url('admin/film-series/link-error') !!}" class="@if($active == 'link-episode-error') active @endif"><i class="fa fa-list sidebar-nav-icon"></i>Link Episode Error</a>
        </li>
        <li>
            <a href="{!! url('admin/playlist/list') !!}" class="@if($active == 'playlist') active @endif"><i class="fa fa-list-ul sidebar-nav-icon"></i>Playlist</a>
        </li>
        <li>
            <a href="{!! url('admin/chanel/list') !!}" class="@if($active == 'chanel') active @endif"><i class="fa fa-area-chart sidebar-nav-icon"></i>Chanel</a>
        </li>
        <li>
            <a href="{!! url('admin/producer/list') !!}" class="@if($active == 'producer') active @endif"><i class="fa fa-product-hunt sidebar-nav-icon"></i>Producer</a>
        </li>
        <li>
            <a href="{!! url('admin/film-country/list') !!}" class="@if($active == 'film_country') active @endif"><i class="fa fa-globe sidebar-nav-icon"></i>Countries</a>
        </li>
        <li>
            <a href="{!! url('admin/genre/list') !!}" class="@if($active == 'genre') active @endif"><i class="fa fa-sticky-note-o sidebar-nav-icon"></i>Genres</a>
        </li>
        <li>
            <a href="{!! url('admin/type/list') !!}" class="@if($active == 'type') active @endif"><i class="fa fa-file-text sidebar-nav-icon"></i>Type Films</a>
        </li>
        <li>
            <a href="{!! url('admin/server-film/list') !!}" class="@if($active == 'server_film') active @endif"><i class="fa fa-server sidebar-nav-icon"></i>Server Films</a>
        </li>
        <li>
            <a href="{!! url('admin/article/list') !!}" class="@if($active == 'article') active @endif"><i class="fa fa-file-text sidebar-nav-icon"></i>List Articles</a>
        </li>
        <li>
            <a href="{!! url('admin/ads/list') !!}" class="@if($active == 'ads') active @endif"><i class="fa fa-wrench sidebar-nav-icon"></i>Ads Setting</a>
        </li>
        <li class="sidebar-header">
            <span class="sidebar-header-options clearfix"><a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"></a></span>
            <span class="sidebar-header-title">System</span>
        </li>
        <li>
            <a href="{!! url('admin/setting/clear-cache') !!}"><i class="gi gi-refresh sidebar-nav-icon"></i> Clear Cache</a>
        </li>
        <li>
            <a href="{!! url('admin/user/list') !!}" class="@if($active == 'users') active @endif"><i class="gi gi-group sidebar-nav-icon"></i> Users</a>
        </li>
        <li>
            <a href="{!! url('admin/footer/edit') !!}" class="@if($active == 'footer') active @endif"><i class="fa fa-external-link sidebar-nav-icon"></i>Footer</a>
        </li>
        <li>
            <a href="{!! url('admin/setting/edit') !!}" class="@if($active == 'setting') active @endif"><i class="fa fa-wrench sidebar-nav-icon"></i>Setting</a>
        </li>
    </ul>
    <!-- END Sidebar Navigation -->

    <!-- Sidebar Notifications -->
    <div class="sidebar-header sidebar-nav-mini-hide">
        <span class="sidebar-header-options clearfix">
            <a href="javascript:void(0)" onclick="location.reload();"data-toggle="tooltip" title="Refresh"><i class="gi gi-refresh"></i></a>
        </span>
        <span class="sidebar-header-title">Activity</span>
    </div>
    <!-- END Sidebar Notifications -->
</div>

