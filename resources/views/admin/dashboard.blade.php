@extends('admin.master')
@section('content')
<div class="content-header content-header-media">
    <div class="header-section">
        <div class="row">
            <!-- Main Title (hidden on small devices for the statistics to fit) -->
            <div class="col-md-4 col-lg-6 hidden-xs hidden-sm">
                <h1>Welcome <strong>{!! $user->name !!}</strong><br><small>You Look Awesome!</small></h1>
            </div>
            <!-- END Main Title -->
        </div>
    </div>
    <!-- For best results use an image with a resolution of 2560x248 pixels (You can also use a blurred image with ratio 10:1 - eg: 1000x100 pixels - it will adjust and look great!) -->
    <img src="{{ url('backend/img/placeholders/headers/dashboard_header.jpg') }}" alt="header image" class="animation-pulseSlow">
</div>
<!-- END Dashboard Header -->

<!-- Mini Top Stats Row -->
<div class="row">
    <div class="col-sm-6 col-lg-3">
        <!-- Widget -->
        <a href="{!! (Auth::user()->type == 1) ? url('admin/user/list') : '' !!}" class="widget widget-hover-effect1">
            <div class="widget-simple">
                <div class="widget-icon pull-left themed-background-autumn animation-fadeIn">
                    <i class="fa fa-users"></i>
                </div>
                <h3 class="widget-content text-right animation-pullDown">
                    {{ $users }} <strong>Members</strong><br>
                    <small>Total Users</small>
                </h3>
            </div>
        </a>
        <!-- END Widget -->
    </div>
    <div class="col-sm-6 col-lg-3">
        <!-- Widget -->
        <a href="{!! (Auth::user()->type == 1) ? url('admin/film/list') : '' !!}" class="widget widget-hover-effect1">
            <div class="widget-simple">
                <div class="widget-icon pull-left themed-background-fire animation-fadeIn">
                    <i class="fa fa-video-camera"></i>
                </div>
                <h3 class="widget-content text-right animation-pullDown">
                    {{ $films }} <strong>Films</strong>
                    <small>Total Films</small>
                </h3>
            </div>
        </a>
        <!-- END Widget -->
    </div>
    <div class="col-sm-6 col-lg-3">
        <!-- Widget -->
        <a href="{{ (Auth::user()->type == 1) ? url('admin/article/list') : '' }}" class="widget widget-hover-effect1">
            <div class="widget-simple">
                <div class="widget-icon pull-left themed-background-spring animation-fadeIn">
                    <i class="fa fa-file-text"></i>
                </div>
                <h3 class="widget-content text-right animation-pullDown">
                    {{ $posts }} <strong>Posts</strong><br>
                    <small>Total Posts</small>
                </h3>
            </div>
        </a>
        <!-- END Widget -->
    </div>

    <div class="col-sm-6 col-lg-3">
        <!-- Widget -->
        <a href="{!! (Auth::user()->type == 1) ? url('admin/film/link-error') : '' !!}" class="widget widget-hover-effect1">
            <div class="widget-simple">
                <div class="widget-icon pull-left themed-background-amethyst animation-fadeIn">
                    <i class="fa fa-exclamation-triangle"></i>
                </div>
                <h3 class="widget-content text-right animation-pullDown">
                    {{ $error_link }} <strong>Error Links</strong>
                    <small>Total error links</small>
                </h3>
            </div>
        </a>
        <!-- END Widget -->
    </div>
</div>
<!-- END Mini Top Stats Row -->

<!-- Widgets Row -->
<div class="row">
    <div class="col-md-6">
        <!-- Timeline Widget -->
        <div class="widget">
            <div class="widget-extra themed-background-dark">
                <h3 class="widget-content-light">
                    Latest <strong>Reviews</strong>
                    <small><a href="{{ url('admin/review/list') }}"><strong>View all</strong></a></small>
                </h3>
            </div>
            <div class="widget-extra">
                <!-- Timeline Content -->
                <div class="timeline">
                    <ul class="timeline-list">
                        @foreach($latest_reviews as $item)
                        <li class="active">
                            <div class="timeline-icon"><i class="gi gi-airplane"></i></div>
                            <div class="timeline-time"><small>{!! date('M j, Y H:i:s',strtotime($item->created_at)) !!}</small></div>
                            <div class="timeline-content">
                                <p class="push-bit"><a href="{{ url('admin/review/edit/'.$item->id) }}"><strong>{{ $item->name }}</strong></a></p>
                                <p class="push-bit">{!! substr($item->content,0,100) !!} ...</p>
                                <div class="row push">
                                    @if(isset($item->images))
                                    <div class="col-sm-9 col-md-6">
                                        <a href="{{ url(Storage::url($item->images)) }}" data-toggle="lightbox-image">
                                            <img src="{{ url(Storage::url($item->images)) }}" alt="image">
                                        </a>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <!-- END Timeline Content -->
            </div>
        </div>
        <!-- END Timeline Widget -->
    </div>
    <div class="col-md-6">
        <!-- Your Plan Widget -->
        <div class="widget">
            <div class="widget-extra themed-background-dark">
                <h3 class="widget-content-light">
                    All <strong>Photos</strong>
                    <small><a href="{{ (Auth::user()->type == 1) ? url('admin/gallery/list') : '' }}"><strong>View all</strong></a></small>
                </h3>
            </div>
        </div>
        <!-- END Your Plan Widget -->

        <!-- Advanced Gallery Widget -->
        <div class="widget">
            <div class="widget-advanced">
                <!-- Widget Main -->
                <div class="widget-main">
                    <div class="gallery gallery-widget" data-toggle="lightbox-gallery">
                        <div class="row">
                            @foreach($latest_photos as $item)
                                @if(isset($item->mediaData))
                                    @foreach($item->mediaData as $value)
                                        <div class="col-xs-6 col-sm-4">
                                            <a href="{{ url(Storage::url($value->images)) }}" class="gallery-link" title="Image Info">
                                                <img src="{{ url(Storage::url($value->images)) }}" alt="image">
                                            </a>
                                        </div>
                                    @endforeach
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
                <!-- END Widget Main -->
            </div>
        </div>
        <!-- END Advanced Gallery Widget -->
    </div>
</div>
@endsection