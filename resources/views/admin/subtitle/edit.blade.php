@extends('admin.master')
@section('content')
    <div class="content-header">
        <div class="header-section">
            <h1><i class="gi gi-pencil"></i> Subtitle<br><small>Multi Subtitle</small></h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li>Pages</li>
        <li>Multi Subtitle</li>
    </ul>

    @if(Session::get('success'))
        <div class="alert alert-success text-center">
            Success
        </div>
    @endif
    @if(Session::get('error'))
        <div class="alert alert-danger text-center">
            {{ Session::get('error') }}
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12">
            <div class="block">
                <!-- General Data Title -->
                <div class="block-title">
                    <h2><i class="fa fa-pencil"></i> <strong>Add</strong> Subtitle</h2>
                </div>
                <form method="post" enctype='multipart/form-data' class="form-horizontal form-bordered">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="{!! $id !!}">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif


                    <div class="form-group">
                        <label class="col-md-3 control-label" for="language">Language</label>
                        <div class="col-md-9">
                            <input required type="text" id="language" name="language" class="form-control" value="{!! (!empty($result))?$result->language:old('language') !!}" placeholder="Language">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="subtitle">File</label>
                        <div class="col-md-9">
                            <input required class="form-control" type="file" id="subtitle" name="subtitle">
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-floppy-o"></i> Save</button>
                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        </div>
                    </div>
                    <!-- END General Data Content -->

                </form>
            </div>

            <!-- General Data Block -->
            <div class="block">
                <!-- General Data Title -->
                <div class="block-title">
                    <h2><i class="fa fa-list"></i> <strong>List</strong> Subtitle</h2>
                </div>
                <table class="table table-bordered table-striped table-vcenter">
                    <thead>
                    <th>Language</th>
                    <th>Name</th>
                    <th class="text-center">Delete</th>
                    </thead>
                    <tbody>
                    @if(count($subtitle) > 0)
                        @foreach($subtitle as $item)
                            <tr>
                                <td>{{ $item->language }}</td>
                                <td>{{ $item->name }}</td>
                                <td class="text-center">
                                    <div class="btn-group btn-group-xs">
                                        {!! HtmlDeleteRecord(url('admin/subtitle/delete/'.$item->id)) !!}
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    <a style="display: none;" data-toggle="modal" data-target="#modalEdit" class="btn btn-default btn-submit"><i class="fa fa-pencil"></i></a>
                    </tbody>
                </table>
            </div>
            <!-- END General Data Block -->
        </div>
    </div>
@endsection
