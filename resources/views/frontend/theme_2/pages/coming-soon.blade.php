@extends('frontend.theme_2.master')
@section('content')


    <!-- main -->
    <div id="main" class="page-news page-news-view">
        <div class="container">
            <div class="pad"></div>
            <div class="main-content">
                <div id="bread">
                    <ol class="breadcrumb">
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li><a href="{{ url('news') }}">News</a></li>
                        <li class="active">{{ $news->name }}</li>
                    </ol>
                </div>
                <div class="news-block">
                    <div class="news-content news-view">
                        <div class="box-content news-view-content">
                            <div class="nvc-thumb" style="background-image: url({{ Storage::url($news->images) }});"></div>
                            <h1 class="title">{{ $news->name }}</h1>
                            <p class="time">
                                <i class="fa fa-clock-o mr5"></i>Ngày đăng: {{ date('d/m/Y',strtotime($news->created_at)) }}
                            </p>

                            {!! stripslashes($news->content) !!}

                            <div class="content-padding" id="comment">
                                <h3 class="title">Comments</h3>
                                <div class="fb-comments" data-colorscheme="dark" data-href="{{ url()->current() }}" data-width="100%" data-numposts="5"></div>
                            </div>
                        </div>
                    </div>
                    <div class="news-sidebar">
                        <div class="box hot-news">
                            <div class="box-head">
                                <div class="nlh">LATEST NEWS</div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="ns-list">
                                @if(count($article)>0)
                                    @foreach($article as $item)
                                    <div class="news-list-item">
                                        <a href="{{ url('news/'.$item->slug) }}" title="{{ $item->name }}" class="thumb">
                                            <img src="{{ Storage::url($item->images) }}" alt="{{ $item->name }}">
                                        </a>
                                        <div class="info">
                                            <h2><a href="{{ url('news/'.$item->slug) }}" title="{{ $item->name }}">{{ $item->name }}</a></h2>

                                            <p class="time">
                                                <i class="fa fa-clock-o mr5"></i>Posted: {{ date('m/d/Y',strtotime($item->created_at)) }}
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!--/main -->

@stop