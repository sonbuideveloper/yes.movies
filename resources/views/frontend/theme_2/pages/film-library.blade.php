@extends('frontend.theme_2.master')
@section('content')

<div id="main" class="">
    <div class="container">
        <div class="pad"></div>
        @if($page_list_top_page && $page_list_top_page->content)
            <div class="banner-content">
                {!! stripslashes($page_list_top_page->content) !!}
            </div>
        @endif
        <?php $data_array = array('0-9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','W','X','Y','Z'); ?>
        <div class="main-content main-category">
            <div class="movies-list-wrap mlw-category">
                <div class="ml-title ml-title-page"><span>{{ $title }}</span><br/>
                    <ul role="tablist" class="nav nav-tabs">
                        @foreach($data_array as $item)
                        <li class="{{ $q==$item?'active':'' }}">
                            <a class="{{ $q==$item?'active':'' }}" title="{{$item}}" href="{{ url('library-film?q='.$item) }}">{{$item}}</a>
                        </li>
                        @endforeach
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="movies-list movies-list-full">
                    @if(count($results)>0)
                        @foreach($results as $item)
                            <div class="ml-item">
                                <a href="{{ url('movie/'.$item->slug.'-'.$item->id) }}" data-id="{{ $item->id }}" data-url="{{ url('/') }}" data-jtip="#f-movies-0-{{ $item->id }}" class="ml-mask jt film-detail-short" title="{{ $item->name }}">
                                    @if(!$item->movie_series)
                                        <span class="mli-quality" style="text-transform: uppercase">{{ $item->quality }}</span>
                                    @endif
                                    <img data-original="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif"
                                         src="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif"
                                         class="lazy thumb mli-thumb" alt="{{ $item->name }}"> <span class="mli-info"><h2>{{ $item->name }}</h2></span>
                                    @if($item->movie_series)
                                        <span class="mli-eps">Eps<i>{{ count($item->film_detail) }}</i></span>
                                    @endif
                                </a>
                            </div>
                        @endforeach
                    @endif
                    <div class="clearfix"></div>
                </div>
                <br>
                @if($page_list_bottom_page && $page_list_bottom_page->content)
                    <div class="banner-content">
                        {!! stripslashes($page_list_bottom_page->content) !!}
                    </div>
                @endif
                <br>
                <div class="text-center">
                    {!! $results->appends(['q' => $q])->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@stop