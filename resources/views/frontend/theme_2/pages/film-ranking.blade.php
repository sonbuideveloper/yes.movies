@extends('frontend.theme_2.master')
@section('content')
<style>
    .movies-letter a.btn-letter{
        width: 9%;
    }
    #year_filter{
        height: 30px;
        background-color: #ffffff;
        border: none;
        padding: 0px 10px;
    }
    @media screen and (max-width: 799px){
        .movies-letter a.btn-letter{
            width: 30%;
        }
    }
</style>

<div id="main">
    <div class="container">
        <div class="pad"></div>
        @if($page_list_top_page && $page_list_top_page->content)
            <div class="banner-content">
                {!! stripslashes($page_list_top_page->content) !!}
            </div>
        @endif
        <div class="main-content main-category">
            <div class="movies-list-wrap mlw-category">
                <div class="ml-title"><span class="pull-left">{{ $title }}</span>
                    <ul role="tablist" class="nav nav-tabs">
                        <li class="{{ $q=='top-views'?'active':'' }}">
                            <a class="{{ $q=='top-views'?'active':'' }}" title="Top Views" href="{{ url('ranking-film?q=top-views') }}">Top Views</a>
                        </li>
                        <li class="{{ $q=='spring'?'active':'' }}">
                            <a class="{{ $q=='spring'?'active':'' }}" title="Spring" href="{{ url('ranking-film?q=spring') }}">Spring</a>
                        </li>
                        <li class="{{ $q=='summer'?'active':'' }}">
                            <a class="{{ $q=='summer'?'active':'' }}" title="Summer" href="{{ url('ranking-film?q=summer') }}">Summer</a>
                        </li>
                        <li class="{{ $q=='fall'?'active':'' }}">
                            <a class="{{ $q=='fall'?'active':'' }}" title="Fall" href="{{ url('ranking-film?q=fall') }}">Fall</a>
                        </li>
                        <li class="{{ $q=='winter'?'active':'' }}">
                            <a class="{{ $q=='winter'?'active':'' }}" title="Winter" href="{{ url('ranking-film?q=winter') }}">Winter</a>
                        </li>
                        <li class="{{ $q=='year'?'active':'' }}">
                            <a class="{{ $q=='year'?'active':'' }}" title="Year" href="{{ url('ranking-film?q=year') }}">Year</a>
                        </li>
                        <li class="{{ $q=='favorite'?'active':'' }}">
                            <a class="{{ $q=='favorite'?'active':'' }}" title="Favorite" href="{{ url('ranking-film?q=favorite') }}">Favorite</a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="movies-list movies-list-full">
                    @if(count($results)>0)
                        @foreach($results as $item)
                            <div class="ml-item">
                                <a href="{{ url('movie/'.$item->slug.'-'.$item->id) }}" data-id="{{ $item->id }}" data-url="{{ url('/') }}" data-jtip="#f-movies-0-{{ $item->id }}" class="ml-mask jt film-detail-short" title="{{ $item->name }}">
                                    @if(!$item->movie_series)
                                        <span class="mli-quality" style="text-transform: uppercase">{{ $item->quality }}</span>
                                    @endif
                                    <img data-original="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif"
                                         src="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif"
                                         class="lazy thumb mli-thumb" alt="{{ $item->name }}"> <span class="mli-info"><h2>{{ $item->name }}</h2></span>
                                    @if($item->movie_series)
                                        <span class="mli-eps">Eps<i>{{ count($item->film_detail) }}</i></span>
                                    @endif
                                </a>
                            </div>
                        @endforeach
                    @endif
                    <div class="clearfix"></div>
                </div>
                <br>
                @if($page_list_bottom_page && $page_list_bottom_page->content)
                    <div class="banner-content">
                        {!! stripslashes($page_list_bottom_page->content) !!}
                    </div>
                @endif
                <br>
                <div class="text-center">
                    {!! $results->appends(['q' => $q])->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@stop