@extends('frontend.theme_2.master')
@section('content')

    <!-- main -->
    <div id="main" class="main-wrap-home">
        <div class="container">
            <div class="home-main">
                <div class="hm-logo"><a id="logo" href="{{ url('site') }}" title="" style="background-image: url({{ url(Storage::url($setting->images)) }});"></a></div>
                <div class="mb10">
                    <div class="addthis_inline_share_toolbox" style="text-align: left;">
                        <div class="fb-like" data-href="{{ url('/') }}" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
                        <div class="fb-send" data-href="{{ url('/') }}"></div>
                    </div>
                </div>
                <div id="hm-search">
                    <div class="search-content">
                        <form method="get" action="{{ url('search') }}">
                        <input maxlength="100" autocomplete="off" name="key" class="form-control search-input" placeholder="Enter Movies or Series name" type="text">
                        <button type="submit" class="search-submit" title="Search">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
                <div class="hm-button">
                    <a href="{{ url('site') }}" class="btn btn-lg btn-success">Use the old {{ $setting->name }}? Click here</a>
                </div>
            </div>
        </div>
    </div>
    <!--/main -->

@stop