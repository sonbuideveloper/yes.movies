@extends('frontend.theme_2.master')
@section('content')

    <div id="main">
        <div class="container">
            <div class="pad"></div>
            @if($page_list_top_page && $page_list_top_page->content)
                <div class="banner-content">
                    {!! stripslashes($page_list_top_page->content) !!}
                </div>
            @endif
            <div class="main-content main-category">
                <div class="movies-list-wrap mlw-category">
                    <div class="ml-title"><span class="pull-left">{{ $title }}</span>
                        <ul role="tablist" class="nav nav-tabs">
                            <li class="{{ $sub_active=='all'?'active':'' }}">
                                <a href="{{ url('top-imdb/all') }}">All</a>
                            </li>
                            <li class="{{ $sub_active=='movies'?'active':'' }}">
                                <a href="{{ url('top-imdb/movies') }}">Movies</a>
                            </li>
                            <li class="{{ $sub_active=='tv-series'?'active':'' }}">
                                <a href="{{ url('top-imdb/tv-series') }}">TV-Series</a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="movies-list movies-list-full">
                        @if(count($results)>0)
                            @foreach($results as $item)
                            <div class="ml-item">
                                <a href="{{ url('movie/'.$item->slug.'-'.$item->id) }}" data-id="{{ $item->id }}" data-url="{{ url('/') }}" data-jtip="#f-movies-0-{{ $item->id }}" class="ml-mask jt film-detail-short" title="{{ $item->name }}">
                                    @if(!$item->movie_series)
                                        <span class="mli-quality" style="text-transform: uppercase">{{ $item->quality }}</span>
                                    @endif
                                    <img data-original="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif"
                                         src="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif"
                                         class="lazy thumb mli-thumb" alt="{{ $item->name }}"> <span class="mli-info"><h2>{{ $item->name }}</h2></span>
                                    @if($item->movie_series)
                                        <span class="mli-eps">Eps<i>{{ count($item->film_detail) }}</i></span>
                                    @endif
                                </a>
                            </div>
                            @endforeach
                        @endif
                        <div class="clearfix"></div>
                    </div>
                    <br>
                    @if($page_list_bottom_page && $page_list_bottom_page->content)
                        <div class="banner-content">
                            {!! stripslashes($page_list_bottom_page->content) !!}
                        </div>
                    @endif
                    <br>
                    <div class="text-center">
                        {!! $results->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/main -->
@stop
