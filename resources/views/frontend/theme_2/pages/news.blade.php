@extends('frontend.theme_2.master')
@section('content')

    <!-- main -->
    <div id="main" class="page-news">
        <div class="container">
            <div class="pad"></div>
            <div class="main-content">
                <h2 class="page-title">Movies News</h2>
                <div class="news-block">
                    <div class="news-content news-content-full news-list">
                        <div class="content-kussss" style="text-align: center; margin: 20px 0; padding: 15px;">
                            {{--Ads--}}
                        </div>
                        <div class="news-list-body">
                            @if(count($news)>0)
                                @foreach($news as $item)
                                    <div class="news-list-item">
                                        <a href="{{ url('news/'.$item->slug) }}" class="thumb">
                                            <img src="{{ Storage::url($item->images) }}" title="{{ $item->name }}" alt="{{ $item->name }}">
                                        </a>
                                        <div class="info">
                                            <h2>
                                                <a href="{{ url('news/'.$item->slug) }}" title="{{ $item->name }}">{{ $item->name }}</a>
                                            </h2>
                                            <p class="desc">{{ $item->descriptions }}</p>
                                            <p class="time"><i class="fa fa-clock-o mr5"></i>Posted: {{ date('m/d/Y',strtotime($item->created_at)) }}</p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                @endforeach
                            @endif
                            <div class="clearfix"></div>
                        </div>
                        <div class="text-center">
                            @if(count($news)>0)
                            {!! $news->render() !!}
                            @endif
                        </div>
                        <div class="content-kussss" style="text-align: center; margin: 20px 0; padding: 15px;">
                            {{--Ads--}}
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!--/main -->

@stop