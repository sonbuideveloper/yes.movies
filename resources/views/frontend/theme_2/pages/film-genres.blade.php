@extends('frontend.theme_2.master')
@section('content')

    <div id="main">
        <div class="container">
            <div class="pad"></div>
            @if($page_list_top_page && $page_list_top_page->content)
                <div class="banner-content">
                    {!! stripslashes($page_list_top_page->content) !!}
                </div>
            @endif
            <div class="main-content main-category">
                <div class="movies-list-wrap mlw-category">
                    <div class="ml-title ml-title-page">
                        <span>{{ $title }}</span>
                        <div class="filter-toggle"><i class="fa fa-sort mr5"></i>Filter</div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="filter">
                        <form id="form_filter" method="get">
                            Order:
                            <select name="order"  class="selectpicker">
                                <option value="0">Default</option>
                                <option  value="new">New Update</option>
                                <option  value="view">Most Viewed</option>
                                <option  value="year">Release Year</option>
                                <option  value="name">Movies name</option>
                                <option  value="imdb">IMDB</option>
                            </select>
                            Country:
                            <select name="countryid"  class="selectpicker">
                                <option value="0">All</option>
                                @foreach($country as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                            Season:
                            <select name="season"  class="selectpicker">
                                <option value="0">All</option>
                                <option value="spring">Spring</option>
                                <option value="summer">Summer</option>
                                <option value="fall">Fall</option>
                                <option value="winter">Winter</option>
                            </select>
                            Years:
                            <select name="year" class="selectpicker">
                                <option value="0">All</option>
                                @for($i=2017; $i>1988; $i--)
                                    <option {{ (!empty($year) && $year==$i)?'selected':'' }} value="{{ $i }}">{{ $i }}</option>
                                @endfor
                            </select>
                            <div class="mt10">
                                <button type="submit" class="btn btn-success btn-block">Show</button>
                            </div>
                        </form>
                    </div>

                    <div class="movies-list movies-list-full">
                        @if(count($results)>0)
                            @foreach($results as $item)
                            <div class="ml-item">
                                <a href="{{ url('watch/'.$item->films->slug.'-'.$item->films->id) }}" data-id="{{ $item->films->id }}" data-url="{{ url('/') }}" data-jtip="#f-movies-0-{{ $item->films->id }}" class="ml-mask jt film-detail-short" title="{{ $item->films->name }}">
                                    @if(!$item->films->movie_series)
                                        <span class="mli-quality" style="text-transform: uppercase">{{ $item->films->quality }}</span>
                                    @endif
                                    <img data-original="@if($item->films->images){{ url(Storage::url($item->films->images)) }}@elseif($item->films->images_link){{ url($item->films->images_link) }}@endif"
                                         src="@if($item->films->images){{ url(Storage::url($item->films->images)) }}@elseif($item->films->images_link){{ url($item->films->images_link) }}@endif"
                                         class="lazy thumb mli-thumb" alt="{{ $item->films->name }}"> <span class="mli-info"><h2>{{ $item->films->name }}</h2></span>
                                    @if($item->films->movie_series)
                                        <span class="mli-eps">Eps<i>{{ count($item->films->film_detail) }}</i></span>
                                    @endif
                                </a>
                            </div>
                            @endforeach
                        @endif
                        <div class="clearfix"></div>
                    </div>
                    <br>
                    @if($page_list_bottom_page && $page_list_bottom_page->content)
                        <div class="banner-content">
                            {!! stripslashes($page_list_bottom_page->content) !!}
                        </div>
                    @endif
                    <br>
                    <div class="text-center">
                        {!! $results->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/main -->
@stop
