@extends('frontend.theme_2.master')
@section('content')

    <div id="main">
        <div class="container">
            <div class="pad"></div>
            @if($page_list_top_page && $page_list_top_page->content)
                <div class="banner-content">
                    {!! stripslashes($page_list_top_page->content) !!}
                </div>
            @endif
            <div class="main-content main-category">
                <div class="movies-list-wrap mlw-category">
                    <div class="ml-title ml-title-page"><span>{{ $title }}</span>
                        <div class="filter-toggle"><i class="fa fa-sort mr5"></i>Filter</div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="filter">
                        <form id="form_filter" method="get">
                            <div class="filter-btn">
                                <button type="submit" class="btn btn-lg btn-success">Filter movies</button>
                            </div>
                            <div class="filter-content row">
                                <div class="col-sm-12">
                                    <div class="fc-genre">
                                        <span class="fc-title">Order</span>
                                        <ul class="fc-genre-list">
                                            <li>
                                                <label>
                                                    <input {{ $order=='0'?'checked':'' }} class="genre-ids" value="0" name="order" type="radio" > Default
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input {{ $order=='new'?'checked':'' }} class="genre-ids" value="new" name="order" type="radio" > New Update
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input {{ $order=='view'?'checked':'' }} class="genre-ids" value="view" name="order" type="radio" > Most Viewed
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input {{ $order=='year'?'checked':'' }} class="genre-ids" value="year" name="order" type="radio" > Release Year
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input {{ $order=='name'?'checked':'' }} class="genre-ids" value="name" name="order" type="radio" > Movies name
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input {{ $order=='imdb'?'checked':'' }} class="genre-ids" value="imdb" name="order" type="radio" > IMDB
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                    @if($page!='genres')
                                        <div class="fc-genre">
                                            <span class="fc-title">Genre</span>
                                            <ul class="fc-genre-list">
                                                <li>
                                                    <label>
                                                        <input class="genre-ids" value="0" name="categoryid" type="radio" > All
                                                    </label>
                                                </li>
                                                @foreach($genres as $item)
                                                    <li>
                                                        <label>
                                                            <input {{ $categoryid==$item->id?'checked':'' }} class="genre-ids" value="{{ $item->id }}" name="categoryid" type="radio" > {{$item->name}}
                                                        </label>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                    @endif
                                    @if($page!='country')
                                        <div class="fc-country">
                                            <span class="fc-title">Country</span>
                                            <ul class="fc-country-list">
                                                <li>
                                                    <label>
                                                        <input class="country-ids" value="10" name="countryid" type="radio"> All
                                                    </label>
                                                </li>
                                                @foreach($country as $item)
                                                    <li>
                                                        <label>
                                                            <input {{ $countryid==$item->id?'checked':'' }} class="country-ids" value="{{ $item->id }}" name="countryid" type="radio" > {{ $item->name }}
                                                        </label>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                    @endif
                                    <div class="fc-release">
                                        <span class="fc-title">Season</span>
                                        <ul class="fc-release-list">
                                            <li>
                                                <label>
                                                    <input {{ (!empty($season) && $season=='0')?'checked':'' }} name="season" value="0" type="radio"> All
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input {{ (!empty($season) && $season=='spring')?'checked':'' }} name="season" value="spring" type="radio"> Spring
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input {{ (!empty($season) && $season=='summer')?'checked':'' }} name="season" value="summer" type="radio"> Summer
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input {{ (!empty($season) && $season=='fall')?'checked':'' }} name="season" value="fall" type="radio"> Fall
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input {{ (!empty($season) && $season=='winter')?'checked':'' }} name="season" value="winter" type="radio"> Winter
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="fc-release">
                                        <span class="fc-title">Release</span>
                                        <ul class="fc-release-list">
                                            <li>
                                                <label>
                                                    <input {{ (!empty($year) && $year=='0')?'checked':'' }} name="year" value="0" type="radio"> All
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input {{ (!empty($year) && $year=='2017')?'checked':'' }} name="year" value="2017" type="radio"> 2017
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input {{ (!empty($year) && $year=='2016')?'checked':'' }} name="year" value="2016" type="radio"> 2016
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input {{ (!empty($year) && $year=='2015')?'checked':'' }} name="year" value="2015" type="radio"> 2015
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input {{ (!empty($year) && $year=='2014')?'checked':'' }} name="year" value="2014" type="radio"> 2014
                                                </label>
                                            </li>
                                            <li>
                                                <label>
                                                    <input {{ (!empty($year) && $year=='2013')?'checked':'' }} name="year" value="2013" type="radio"> 2013
                                                </label>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="movies-list movies-list-full">
                        @if(count($results)>0)
                            @foreach($results as $item)
                            <div class="ml-item">
                                <a href="{{ url('movie/'.$item->slug.'-'.$item->id) }}" data-id="{{ $item->id }}" data-url="{{ url('/') }}" data-jtip="#f-movies-0-{{ $item->id }}" class="ml-mask jt film-detail-short" title="{{ $item->name }}">
                                    @if($item->movie_series)
                                        <span class="mli-eps">Eps<i>{{ count($item->film_detail) }}</i></span>
                                    @else
                                        <span class="mli-quality" style="text-transform: uppercase;">{{ $item->quality }}</span>
                                    @endif
                                    <img class="thumb mli-thumb lazy" title="{{ $item->name }}" alt="{{ $item->name }}"
                                         src="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif"
                                         data-original="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif">
                                    <span class="mli-info">
                                        <h2>{{ $item->name }}</h2>
                                    </span>
                                </a>
                            </div>
                            @endforeach
                        @else
                            <div style="font-size: 18px;border: 1px solid #dfdfdf; background-color: #fff;text-align: center;margin-top: 10px;line-height: 50px;color:#333">Not found.</div>
                        @endif
                        <div class="clearfix"></div>
                    </div>
                    <br>
                </div>
            </div>
            @if($page_list_bottom_page && $page_list_bottom_page->content)
                <div class="banner-content">
                    {!! stripslashes($page_list_bottom_page->content) !!}
                </div>
            @endif
        </div>
    </div>
    <!--/main -->
@stop
