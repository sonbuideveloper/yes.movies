@extends('frontend.theme_2.master')
@section('content')

<div id="main" class="page-detail">
    <div class="page-cover" style="background-image: url(@if($film->background) {{ url(Storage::url($film->background)) }} @elseif($film->background_link) {{ url($film->background_link) }} @endif)"></div>
    <div class="container">
        <div class="pad"></div>
        <div class="main-content main-detail ">
            <div class="md-top">
                <div id="bread">
                    <ol class="breadcrumb" itemtype="http://schema.org/BreadcrumbList" itemscope="">
                        <li itemtype="http://schema.org/ListItem" itemscope="" itemprop="itemListElement">
                            <a itemprop="item" title="Home" href="{{ url('/') }}">
                                <span itemprop="name">Home</span>
                            </a>
                        </li>
                        <li itemtype="http://schema.org/ListItem" itemscope="" itemprop="itemListElement">
                            <a itemprop="item" title="Movies" href="{{ url('movies') }}">
                                <span itemprop="name">Movies</span>
                            </a>
                        </li>
                        <li itemtype="http://schema.org/ListItem" itemscope="" itemprop="itemListElement"
                            class="active">
                            <span itemprop="name">{{ $film->name }}</span>
                        </li>
                    </ol>
                </div>
                <div id="mv-info">
                    <div class="mvi-content">
                        <div class="btn-watch-area">
                            <div class="bwa-content">
                                <div class="loader"></div>
                                <a href="{{ url('watch/'.$film->slug.'-'.$film->id) }}" class="bwac-btn"
                                   title="Click to play">
                                    <i class="fa fa-play"></i>
                                </a>
                            </div>
                        </div>
                        <div class="mvic-desc">
                            <div class="mvic-stats">
                                <?php
                                $favorite = 0;
                                if (count($film->favorite) > 0 && Auth::user()) {
                                    foreach ($film->favorite as $element) {
                                        if ($element->user_id == Auth::user()->id) {
                                            $favorite = 1;
                                            break;
                                        }
                                    }
                                }
                                ?>
                                <div class="block-fav" id="btn-favorite">
                                    @if($favorite)
                                        <a onclick="favorite_add({{$film->id}},this,0,{{(Auth::user()?1:0)}})" data-favorite="{{ $favorite }}" data-url="{{ url('/') }}" class="btn-favorite active" href="javascript:void(0)" title="Remove from favorite"><i class="icon-favorite"></i> Favorite</a>
                                    @else
                                        <a onclick="favorite_add({{$film->id}},this,0,{{(Auth::user()?1:0)}})" data-favorite="{{ $favorite }}" data-url="{{ url('/') }}" class="btn-favorite" href="javascript:void(0)" title="Add to favorite"><i class="icon-favorite"></i> Favorite</a>
                                    @endif
                                </div>
                                <div class="block-view">
                                    <i class="icon-remove_red_eye"></i> {{ $film->view }} views
                                </div>
                                {{--<div class="block-trailer">--}}
                                    {{--<a data-target="#pop-trailer" data-toggle="modal">--}}
                                        {{--<i class="icon-videocam"></i> Watch trailer--}}
                                    {{--</a>--}}
                                {{--</div>--}}
                            </div>
                            <?php
                            $rate = 0;
                            if(count($film->rate)>0 && Auth::user()){
                                foreach ($film->rate as $element){
                                    if($element->user_id == Auth::user()->id){
                                        $rate = $element->rate;
                                        break;
                                    }
                                }
                            }
                            ?>
                            <div class="detail-mod">
                                <div class="dm-thumb"><img src="@if($film->images) {{ url(Storage::url($film->images)) }} @elseif($film->images_link) {{ url($film->images_link) }} @endif" title="{{ $film->name }}" alt="{{ $film->name }}">
                                </div>
                                <h3>{{ $film->name }}</h3>
                                <div id="stars-green" class="mv-rating" data-rating="{{ $rate }}" data-film="{{ $film->id }}" data-user="{{ Auth::user()?1:0 }}"></div>
                                <div class="mobile-btn">
                                    <a class="mod-btn mod-btn-watch" href="{{ url('watch/'.$film->slug.'-'.$film->id) }}" title="Watch movie"><i class="fa fa-play mr5"></i> Watch movie</a>
                                </div>
                                <div class="mobile-view"><i class="icon-remove_red_eye"></i> {{$film->view}} views
                                </div>
                            </div>
                            <div class="block-social">
                                <div class="fb-like" data-href="{{ url('movie/'.$film->slug.'-'.$film->id.($p?'?p='.$p:'')) }}" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
                                <div class="fb-send" data-href="{{ url('movie/'.$film->slug.'-'.$film->id.($p?'?p='.$p:'')) }}"></div>
                            </div>
                            <div class="desc">{!! stripslashes($film->content) !!}</div>
                            <div class="mvic-info">
                                <div class="mvici-left">
                                    <p><strong>Genres: </strong>
                                        @if(count($film->genres)>0)
                                            <?php $i=0; ?>
                                            @foreach($film->genres as $element)
                                                <a href="{{ url('genres/'.$element->slug) }}" title="{{ $element->name }}">{{ $element->name }}</a>{{ ($i==count($film->genres)-1)?'':',' }}
                                                <?php $i++; ?>
                                            @endforeach
                                        @endif
                                    </p>
                                    <p><strong>Actors: </strong>
                                    @if(count($film->actors)>0)
                                        @if(count($film->actors)>5)
                                            <div class="actor_div_sort">
                                                <?php $i=0; ?>
                                                @foreach($film->actors as $element)
                                                    <a href="{{ url('actors/'.$element->slug) }}" title="{{ $element->name }}">{{ $element->name }}</a>{{ ($i==count($film->actors)-1)?'':',' }}
                                                    <?php
                                                    $i++;
                                                    if($i==5) break;
                                                    ?>
                                                @endforeach
                                                ...
                                                <br>
                                                <div class="clearfix"></div>
                                            </div>
                                        @else
                                            <div class="actor_div_large">
                                                <?php $i=0; ?>
                                                @foreach($film->actors as $element)
                                                    <a href="{{ url('actors/'.$element->slug) }}" title="{{ $element->name }}">{{ $element->name }}</a>{{ ($i==count($film->actors)-1)?'':',' }}
                                                    <?php $i++; ?>
                                                @endforeach
                                            </div>
                                        @endif
                                    @endif
                                    </p>
                                    <p><strong>Directors: </strong>
                                        @if(count($film->directors)>0)
                                            <?php $i=0; ?>
                                            @foreach($film->directors as $element)
                                                <a href="{{ url('directors/'.$element->slug) }}" title="{{ $element->name }}">{{ $element->name }}</a>{{ ($i==count($film->directors)-1)?'':',' }}
                                                <?php $i++; ?>
                                            @endforeach
                                        @endif
                                    </p>
                                    <p><strong>Country: </strong>
                                        <a href="{{ url('country/'.$film->country->slug) }}" title="{{ $film->country->name }} Movies">{{ $film->country->name }}</a>
                                    </p>
                                </div>
                                <div class="mvici-right">
                                    <p><strong>Duration:</strong> {{ $film->duration }} min</p>
                                    <p><strong>Quality:</strong> <span class="quality">{{ $film->quality }}</span></p>
                                    <p><strong>Release:</strong> {{ $film->release }}</p>
                                    <p><strong>IMDb:</strong> {{ $film->IMDB }}</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                            <!-- keywords -->
                            <div id="mv-keywords">
                                <strong class="mr10">Keywords:</strong>
                                <?php
                                if($film->keywords){
                                    $keywords = explode(',', $film->keywords);
                                    if(count($keywords)>0){
                                        foreach ($keywords as $element){
                                            echo "<a targget='_blank' href=\"".url('tag/'.trim($element))."\" title=\"".trim($element)."\"><h5>".trim($element)."</h5></a>";
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            @if($watching_under_info && $watching_under_info->content)
                <div class="banner-content">
                    {!! stripslashes($watching_under_info->content) !!}
                </div>
            @endif
            <!--related-->
            <div class="movies-list-wrap mlw-related">
                <div class="ml-title ml-title-page">
                    <span>You May Also Like</span>
                </div>
                <div id="movies-related" class="movies-list movies-list-full">
                    @foreach($film_random as $item)
                        <div class="ml-item">
                            <a href="{{ url('movie/'.$item->slug.'-'.$item->id) }}" data-id="{{ $item->id }}" data-url="{{ url('/') }}" data-jtip="#f-movies-0-{{ $item->id }}" class="ml-mask jt film-detail-short" title="{{ $item->name }}">
                                @if(!$item->movie_series)
                                    <span class="mli-quality" style="text-transform: uppercase">{{ $item->quality }}</span>
                                @endif
                                <img data-original="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif"
                                     src="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif"
                                     class="lazy thumb mli-thumb" alt="{{ $item->name }}">
                                <span class="mli-info"><h2>{{ $item->name }}</h2></span>
                                @if($item->movie_series)
                                    <span class="mli-eps">Eps<i>{{ count($item->film_detail) }}</i></span>
                                @endif
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
            <!--/related-->
        </div>
    </div>
</div>

@stop