@extends('frontend.theme_2.master')
@section('content')

    <div id="main">
        <div class="container">
            <div class="pad"></div>
            @if($page_list_top_page && $page_list_top_page->content)
                <div class="banner-content">
                    {!! stripslashes($page_list_top_page->content) !!}
                </div>
            @endif
            <div class="main-content main-category">
                <div class="movies-list-wrap mlw-category">
                    <div class="ml-title"><span class="pull-left">{{ $title }}</span>
                        <ul role="tablist" class="nav nav-tabs">
                            {{--<li class="{{ $sub_active=='profile'?'active':'' }}">--}}
                                {{--<a href="{{ url('users/profile') }}">Profile</a>--}}
                            {{--</li>--}}
                            <li class="{{ $sub_active=='favorite'?'active':'' }}">
                                <a href="{{ url('users/favorite') }}">Favorite</a>
                            </li>
                            <li>
                                <a href="{{ url('users/logout') }}">Logout</a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="movies-list movies-list-full">
                        @if(count($results)>0)
                            @foreach($results as $item)
                            <div class="ml-item">
                                <a href="{{ url('watch/'.$item->films->slug.'-'.$item->films->id) }}" data-id="{{ $item->films->id }}" data-url="{{ url('/') }}" data-jtip="#f-movies-0-{{ $item->films->id }}" class="ml-mask jt film-detail-short" title="{{ $item->films->name }}">
                                    @if(!$item->films->movie_series)
                                        <span class="mli-quality" style="text-transform: uppercase">{{ $item->films->quality }}</span>
                                    @endif
                                    <img data-original="@if($item->films->images){{ url(Storage::url($item->films->images)) }}@elseif($item->films->images_link){{ url($item->films->images_link) }}@endif"
                                         src="@if($item->films->images){{ url(Storage::url($item->films->images)) }}@elseif($item->films->images_link){{ url($item->films->images_link) }}@endif"
                                         class="lazy thumb mli-thumb" alt="{{ $item->name }}"> <span class="mli-info"><h2>{{ $item->name }}</h2></span>
                                    @if($item->films->movie_series)
                                        <span class="mli-eps">Eps<i>{{ count($item->films->film_detail) }}</i></span>
                                    @endif
                                </a>
                            </div>
                            @endforeach
                        @endif
                        <div class="clearfix"></div>
                    </div>
                    <br>
                    @if($page_list_bottom_page && $page_list_bottom_page->content)
                        <div class="banner-content">
                            {!! stripslashes($page_list_bottom_page->content) !!}
                        </div>
                    @endif
                    <br>
                    <div class="text-center">
                        {!! $results->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/main -->
@stop
