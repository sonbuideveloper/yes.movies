@extends('frontend.theme_2.master')
@section('content')

    <div class="top-content">
        <!-- slider -->
        @include('frontend.theme_2.block.slider')
        <!--/slider -->
        <div class="clearfix"></div>

        <!--like home-->
        <div id="like-home">
            <div class="container">
                <div class="lh-like">
                    <div class="fb-like" data-href="{{ url('/') }}" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
                    <div class="fb-send" data-href="{{ url('/') }}"></div>
                </div>
                <div class="addthis_inline_share_toolbox"></div>
                <div class="lh-text">Like and Share our website to support us.</div>
            </div>
        </div>
        <!--/like home-->

        <div class="clearfix"></div>
    </div>

    <div id="main">
        <div class="container">
            <div class="main-content">
                @if($home_top_suggestion && $home_top_suggestion->content)
                    <div class="banner-content">
                        {!! stripslashes($home_top_suggestion->content) !!}
                    </div>
                @endif
                <div class="movies-list-wrap mlw-latestmovie">
                    <div class="ml-title">
                        <span class="pull-left">Suggestion</span>
                        <a href="{{ url('movies') }}" class="pull-right cat-more">View more »</a>
                        <ul role="tablist" class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" role="tab" href="#movies-featured" aria-expanded="false">Featured Movies</a></li>
                            <li onclick="movies_by_top('today')"><a data-toggle="tab" role="tab" href="#movies-today" aria-expanded="false">Top Today</a></li>
                            <li onclick="movies_by_top('imdb')"><a data-toggle="tab" role="tab" href="#movies-imdb" aria-expanded="false">Top IMDb</a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="tab-content">
                        <div id="movies-featured" class="movies-list movies-list-full tab-pane in fade active">
                            @if(count($featured)>0)
                                @foreach($featured as$item)
                                    <div class="ml-item">
                                        <a href="{{ url('movie/'.$item->slug.'-'.$item->id) }}" data-id="{{ $item->id }}" data-url="{{ url('/') }}" data-index="0" data-jtip="#f-movies-0-{{ $item->id }}" class="ml-mask jt film-detail-short" title="{{ $item->name }}">
                                            @if(!$item->movie_series)
                                                <span class="mli-quality" style="text-transform: uppercase">{{ $item->quality }}</span>
                                            @endif
                                            <img data-original="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif" src="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif" class="lazy thumb mli-thumb" alt="{{ $item->name }}">
                                            <span class="mli-info">
                                                <h2>{{ $item->name }}</h2>
                                            </span>
                                            @if($item->movie_series)
                                                <span class="mli-eps">Eps<i>{{ count($item->film_detail) }}</i></span>
                                            @endif
                                        </a>
                                    </div>
                                @endforeach
                            @endif
                            <div class="clearfix"></div>
                        </div>
                        <div id="movies-today" class="movies-list movies-list-full tab-pane in fade"></div>
                        <div id="movies-imdb" class="movies-list movies-list-full tab-pane in fade"></div>
                    </div>
                </div>

                <!--latest movies-->
                @if($home_top_movies && $home_top_movies->content)
                    <div class="banner-content">
                        {!! stripslashes($home_top_movies->content) !!}
                    </div>
                @endif
                <div class="movies-list-wrap mlw-latestmovie">
                    <div class="ml-title">
                        <span class="pull-left">Latest Movies</span>
                        <a href="{{ url('movies') }}" class="pull-right cat-more">View more »</a>
                        <ul role="tablist" class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" role="tab" href="#latest-all"
                                   aria-expanded="true">All</a>
                            </li>
                            @if(count($genres)>3)
                                @for($i=0; $i<3; $i++)
                                    <li onclick="movies_by_genre('{{ $genres[$i]->slug }}')">
                                        <a data-toggle="tab" role="tab" href="#latest-{{ $genres[$i]->slug }}" aria-expanded="false">{{ $genres[$i]->name }}</a>
                                    </li>
                                @endfor
                            @endif
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="tab-content">
                        <div id="latest-all" class="movies-list movies-list-full tab-pane in fade active">
                            @if(count($movies)>0)
                                @foreach($movies as$item)
                                <div class="ml-item">
                                    <a href="{{ url('movie/'.$item->slug.'-'.$item->id) }}" data-id="{{ $item->id }}" data-url="{{ url('/') }}" data-index="1" data-jtip="#f-movies-1-{{ $item->id }}" class="ml-mask jt film-detail-short" title="{{ $item->name }}">
                                        @if(!$item->movie_series)
                                            <span class="mli-quality" style="text-transform: uppercase">{{ $item->quality }}</span>
                                        @endif
                                        <img data-original="@if($item->images){{url(Storage::url($item->images))}}@elseif($item->images_link){{url($item->images_link)}}@endif" src="@if($item->images){{url(Storage::url($item->images))}}@elseif($item->images_link){{url($item->images_link)}}@endif" class="lazy thumb mli-thumb" alt="{{ $item->name }}">
                                        <span class="mli-info">
                                            <h2>{{ $item->name }}</h2>
                                        </span>
                                    </a>
                                </div>
                                @endforeach
                            @endif
                            <div class="clearfix"></div>
                        </div>
                        @if(count($genres)>3)
                            @for($i=0; $i<3; $i++)
                                <div id="latest-{{ $genres[$i]->slug }}" class="movies-list movies-list-full tab-pane in fade"></div>
                            @endfor
                        @endif
                    </div>
                </div>
                <!--/latest movies-->
                <!--latest tv-series-->
                @if($home_top_tv_series && $home_top_tv_series->content)
                    <div class="banner-content">
                        {!! stripslashes($home_top_tv_series->content) !!}
                    </div>
                @endif
                <div class="movies-list-wrap mlw-tv-series">
                    <div class="ml-title">
                        <span class="pull-left">Latest TV-Series</span>
                        <a href="{{ url('tv-series') }}" class="pull-right cat-more">View more »</a>
                        <ul role="tablist" class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" role="tab" href="#latest-tv-all" aria-expanded="true">All</a>
                            </li>
                            @if(count($country)>3)
                                @for($i=0; $i<3; $i++)
                                    <li onclick="movies_by_country('{{ $country[$i]->slug }}')">
                                        <a data-toggle="tab" role="tab" href="#latest-{{ $country[$i]->slug }}" aria-expanded="false">{{ $country[$i]->name }}</a>
                                    </li>
                                @endfor
                            @endif
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="tab-content">
                        <div id="latest-tv-all" class="movies-list movies-list-full tab-pane in fade active">
                            @if(count($film_series)>0)
                                @foreach($film_series as$item)
                                    <div class="ml-item">
                                        <a href="{{ url('movie/'.$item->slug.'-'.$item->id) }}" data-id="{{ $item->id }}" data-url="{{ url('/') }}" data-index="2" data-jtip="#f-movies-2-{{ $item->id }}" class="ml-mask jt film-detail-short" title="{{ $item->name }}">
                                            <img data-original="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif" src="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif" class="lazy thumb mli-thumb" alt="{{ $item->name }}">
                                            <span class="mli-info">
                                            <h2>{{ $item->name }}</h2>
                                        </span>
                                            <span class="mli-eps">Eps<i>{{ count($item->film_detail) }}</i></span>
                                        </a>
                                    </div>
                                @endforeach
                            @endif
                            <div class="clearfix"></div>
                        </div>
                        @if(count($country)>3)
                            @for($i=0; $i<3; $i++)
                                <div id="latest-{{ $country[$i]->slug }}" class="movies-list movies-list-full tab-pane in fade"></div>
                            @endfor
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
