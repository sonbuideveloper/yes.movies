@extends('frontend.theme_2.master')
@section('content')

    <div id="main" class="page-detail page-watch type-tvseries">
        <div class="container">
            <div class="pad"></div>
            @if($watching_top_player && $watching_top_player->content)
                <div class="banner-content">
                    {!! stripslashes($watching_top_player->content) !!}
                </div>
            @endif
            <div class="main-content main-detail">
                <div class="md-top">
                    <div id="bread">
                        <ol class="breadcrumb" itemtype="http://schema.org/BreadcrumbList" itemscope="">
                            <li itemtype="http://schema.org/ListItem" itemscope="" itemprop="itemListElement">
                                <a itemprop="item" title="Home" href="{{ url('/') }}">
                                    <span itemprop="name">Home</span>
                                </a>
                            </li>
                            <li itemtype="http://schema.org/ListItem" itemscope="" itemprop="itemListElement">
                                <a itemprop="item" title="Movies" href="{{ url('movies') }}">
                                    <span itemprop="name">Movies</span>
                                </a>
                            </li>
                            <li itemtype="http://schema.org/ListItem" itemscope="" itemprop="itemListElement"
                                class="active">
                                <span itemprop="name">{{ $film->name }}</span>
                            </li>
                        </ol>
                    </div>
                    <div id="player-area">
                        <div class="pa-main">
                            <div id="media-player" class="cssload-2x cssload-center player" style="height: 500px; background: black;">
                                <div class="cssload player_iframe"><span></span></div>
                                <div class="player">
                                    <div class="mediaplayer" id="player"></div>
                                </div>
                            </div>
                            <div id="content-embed" class="cssload-2x cssload-center player_iframe video-container" style="display: none;">
                                <div id="embed-loading" class="cssload player"><span></span></div>
                                <div class="player_iframe">
                                    <iframe id="iframe-link" src="" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                            <?php
                            $favorite = 0;
                            if(count($film->favorite)>0 && Auth::user()){
                                foreach ($film->favorite as $element){
                                    if($element->user_id == Auth::user()->id){
                                        $favorite = 1;
                                        break;
                                    }
                                }
                            }
                            ?>
                            <div id="bar-player">
                                <a class="btn bp-btn-light"><i class="fa fa-lightbulb-o"></i><span></span></a>
                                <span id="btn-favorite">
                                    <a class="btn bp-btn-like btn-favorite-{{ $film->id }}" onclick="favorite_add({{ $film->id }},this,1,{{ Auth::user()?1:0 }})"  data-favorite="{{ $favorite }}" data-url="{{ url('/') }}">
                                        @if($favorite)
                                            <i class="fa fa-remove mr10"></i>Remove Favorite
                                        @else
                                            <i class="fa fa-heart mr10"></i>Add to favorite
                                        @endif
                                    </a>
                                    <div class="popover fade top in popover-like" style="display: none;">
                                        <div class="arrow" style="left: 50%;"></div>
                                        <div class="popover-content">
                                            <p id="popover-notice"></p>
                                            <p class="mt10">
                                                <a onclick="favorite(3904,'watch')" href="javascript:void(0)"
                                                   class="btn btn-success btn-sm"><i class="fa fa-plus-circle"></i> Add now</a><a
                                                        href="javascript:void(0)" class="btn btn-sm btn-default toggle-popover-like">Close</a>
                                            </p>
                                        </div>
                                    </div>
                                </span>
                                {{--<a class="btn bp-btn-auto active"><i class="fa fa-step-forward"></i> <span>Auto next: </span></a>--}}
                                <a class="btn bp-btn-report" data-url="{{ url('/') }}" data-film="{{ $film->id }}" data-link="" data-name="{{ $film->name }}" style="color: #fff000; float: right"><i class="fa fa-warning"></i> Report</a>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="pa-server">
                            <div class="pas-header">
                                <div class="pash-title">
                                    <i>Playing on server: </i><span class="playing-on label label-pink"></span>
                                </div>
                                <div class="pash-choose">
                                    <div class="btn-group">
                                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            List Server <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" id="ip_server">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="pas-list" id="list-eps">
                                @if($film->movie_series==1)
                                    @if(count($film->film_detail)>0)
                                        <?php $i=1; ?>
                                        <ul id="" class="server-list-item ps-container ps-theme-default">
                                            @foreach($film->film_detail as $element)
                                                <li class="film-eposide-item ep-item {{ $i==$p?'active':'' }}" data-index="0" data-server="14" data-id="1100628" id="ep-1100628">
                                                    <a href="javascript:void(0)" class="episode_{{ $i }} btn-eps first-ep film-eposide-item" data-id="{{ $film->id }}" data-name="{{ $i }}" data-url="{{ url('watch/'.$film->slug.'-Episode-'.$i.'-'.$film->id) }}" data-server="" title="Watch Episode {{ $i }}">
                                                        <i class="icon-play_arrow"></i>Episode {{ $i }}
                                                    </a>
                                                </li>
                                                <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;"><div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; right: 0px;"><div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                                                </div>
                                                <?php $i++; ?>
                                            @endforeach
                                        </ul>
                                    @endif
                                    <input type="hidden" id="episode_index" value="{{ $p }}">
                                @else
                                    <ul id="" class="server-list-item ps-container ps-theme-default" style="display: block;">
                                        <li class="ep-item active">
                                            <a title="{{ $film->quality }}" style="text-transform: uppercase;">
                                                <i class="icon-play_arrow"></i>{{ $film->quality  }}
                                            </a>
                                        </li>
                                    </ul>
                                @endif
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="favorite-alert" style="display: none;">
                        <div class="alert alert-success" role="alert">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <i class="fa fa-check"></i> <span id="favorite-message"></span>
                            <a target="_blank" href="../../index.html"
                               title="Your favorite" class="alert-link">your favorite.</a>
                        </div>
                    </div>
                    {{--<div class="social-share">--}}
                        {{--<div class="fb-like" data-href="{{ url('movie/'.$film->slug.'-'.$film->id.($p?'?p='.$p:'')) }}" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>--}}
                        {{--<div class="fb-send" data-href="{{ url('movie/'.$film->slug.'-'.$film->id.($p?'?p='.$p:'')) }}"></div>--}}
                    {{--</div>--}}
                    @if($watching_under_player && $watching_under_player->content)
                        <div class="banner-content">
                            {!! stripslashes($watching_under_player->content) !!}
                        </div>
                    @endif
                    <div id="mv-info">
                        <input type="hidden" id="movie_series" value="{{ $film->movie_series }}">
                        <div class="mvi-content">
                            <div class="mvic-desc">
                                <div class="mv-rating" itemtype="http://data-vocabulary.org/Review-aggregate"
                                     itemscope=""></div>
                                <div class="mvic-stats">
                                    {{--<div class="block-trailer"><a data-target="#pop-trailer" data-toggle="modal"><i--}}
                                                    {{--class="icon-videocam"></i> Watch trailer</a></div>--}}
                                    <div class="block-view"><i class="icon-remove_red_eye"></i> {{ $film->view }} views
                                    </div>
                                </div>
                                <h3><a href="{{ url('movie/'.$film->slug.'-'.$film->id) }}">{{ $film->name }}</a>
                                </h3>
                                <div class="block-social">
                                    <div class="fb-like" data-href="{{ url('movie/'.$film->slug.'-'.$film->id.($p?'?p='.$p:'')) }}" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
                                    <div class="fb-send" data-href="{{ url('movie/'.$film->slug.'-'.$film->id.($p?'?p='.$p:'')) }}"></div>
                                </div>
                                <div class="desc">
                                    {!! stripslashes($film->content) !!}
                                </div>
                                <div class="mvic-info">
                                    <img title="{{ $film->name }}" alt="{{ $film->name }}"
                                         src="@if($film->images) {{ url(Storage::url($film->images)) }} @elseif($film->images_link) {{ url($film->images_link) }} @endif" title="{{ $film->name }}" alt="{{ $film->name }}" class="hidden">
                                    <div class="mvici-left">
                                        <p><strong>Genres: </strong>
                                            @if(count($film->genres)>0)
                                                <?php $i=0; ?>
                                                @foreach($film->genres as $element)
                                                    <a href="{{ url('genres/'.$element->slug) }}" title="{{ $element->name }}">{{ $element->name }}</a>{{ ($i==count($film->genres)-1)?'':',' }}
                                                    <?php $i++; ?>
                                                @endforeach
                                            @endif
                                        </p>
                                        <p><strong>Actors: </strong>
                                        @if(count($film->actors)>0)
                                            @if(count($film->actors)>5)
                                                <div class="actor_div_sort">
                                                    <?php $i=0; ?>
                                                    @foreach($film->actors as $element)
                                                        <a href="{{ url('actors/'.$element->slug) }}" title="{{ $element->name }}">{{ $element->name }}</a>{{ ($i==count($film->actors)-1)?'':',' }}
                                                        <?php
                                                        $i++;
                                                        if($i==5) break;
                                                        ?>
                                                    @endforeach
                                                    ...
                                                    <br>
                                                    <div class="clearfix"></div>
                                                </div>
                                            @else
                                                <div class="actor_div_large">
                                                    <?php $i=0; ?>
                                                    @foreach($film->actors as $element)
                                                        <a href="{{ url('actors/'.$element->slug) }}" title="{{ $element->name }}">{{ $element->name }}</a>{{ ($i==count($film->actors)-1)?'':',' }}
                                                        <?php $i++; ?>
                                                    @endforeach
                                                </div>
                                            @endif
                                        @endif
                                        </p>
                                        <p><strong>Directors: </strong>
                                            @if(count($film->directors)>0)
                                                <?php $i=0; ?>
                                                @foreach($film->directors as $element)
                                                    <a href="{{ url('directors/'.$element->slug) }}" title="{{ $element->name }}">{{ $element->name }}</a>{{ ($i==count($film->directors)-1)?'':',' }}
                                                    <?php $i++; ?>
                                                @endforeach
                                            @endif
                                        </p>
                                        <p><strong>Country: </strong>
                                            <a href="{{ url('country/'.$film->country->slug) }}" title="{{ $film->country->name }} Movies">{{ $film->country->name }}</a>
                                        </p>
                                    </div>
                                    <div class="mvici-right">
                                        <p><strong>Duration:</strong> {{ $film->duration }} min</p>
                                        <p><strong>Quality:</strong> <span class="quality">{{ $film->quality }}</span></p>
                                        <p><strong>Release:</strong> {{ $film->release }}</p>
                                        <p><strong>IMDb:</strong> {{ $film->IMDB }}</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                                <!-- keywords -->
                                <div id="mv-keywords">
                                    <strong class="mr10">Keywords:</strong>
                                    <?php
                                    if($film->keywords){
                                        $keywords = explode(',', $film->keywords);
                                        if(count($keywords)>0){
                                            foreach ($keywords as $element){
                                                echo "<a targget='_blank' href=\"".url('tag/'.trim($element))."\" title=\"".trim($element)."\"><h5>".trim($element)."</h5></a>";
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="pw-comment">
                                <div class="content">
                                    @if($film->movie_series===1)
                                        <div class="fb-comments" data-href="{{ url('watch/'.$film->slug.'-'.$film->id.'-'.$p) }}" data-width="100%" data-numposts="10"></div>
                                    @else
                                        <div class="fb-comments" data-href="{{ url('watch/'.$film->slug.'-'.$film->id) }}" data-width="100%" data-numposts="10"></div>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                @if($watching_under_info && $watching_under_info->content)
                    <div class="banner-content">
                        {!! stripslashes($watching_under_info->content) !!}
                    </div>
                @endif
                <div class="movies-list-wrap mlw-related">
                    <div class="ml-title ml-title-page">
                        <span>You May Also Like</span>
                    </div>
                    <div id="movies-related" class="movies-list movies-list-full">
                        @foreach($film_random as $item)
                            <div class="ml-item">
                                <a href="{{ url('movie/'.$item->slug.'-'.$item->id) }}" data-id="{{ $item->id }}" data-url="{{ url('/') }}" data-jtip="#f-movies-0-{{ $item->id }}" class="ml-mask jt film-detail-short" title="{{ $item->name }}">
                                    @if(!$item->movie_series)
                                        <span class="mli-quality" style="text-transform: uppercase">{{ $item->quality }}</span>
                                    @endif
                                    <img data-original="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif"
                                         src="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif"
                                         class="lazy thumb mli-thumb" alt="{{ $item->name }}">
                                    <span class="mli-info"><h2>{{ $item->name }}</h2></span>
                                    @if($item->movie_series)
                                        <span class="mli-eps">Eps<i>{{ count($item->film_detail) }}</i></span>
                                    @endif
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
                <!--/related-->
            </div>
        </div>
    </div>
    <input type="hidden" value="{{ $subtitle_user }}" id="input_subtitle_user">
    <input type="hidden" value="0" id="link_id">
    <input type="hidden" class="token" name="_token" value="{{ csrf_token() }}">
    <script>
        var movie_series = $('#movie_series').val();
        var subtitle_user = $('#input_subtitle_user').val();
        if(subtitle_user==0){
            var subtitle_status = 0;
        }else{
            var subtitle_status = 1;
        }
        if(movie_series == 1){
            var episode = $('#episode_index').val();
            console.log(episode);
            {{--request_meta_data({{$film->id}}, episode);--}}
            request_video_jwplayer_detail({{$film->id}}, episode, {{ $s }}, subtitle_status);
        }else{
            request_video_jwplayer({{$film->id}},{{ $s }}, subtitle_status);
        }
    </script>
    <div id="overlay"></div>
@stop