<footer>
    <div id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-1 footer-block1"></div>
                <div class="col-md-12 col-sm-12 col-xs-12 footer-block footer-block2" style="columns: 5">
                    @foreach($genres as $item)
                        <p style="padding-left: 20px;"><a title="{{ $item->name }}" href="{{ $http_url('genres/'.$item->slug) }}">{{ $item->name }}</a></p>
                    @endforeach
                    @foreach($country as $item)
                        <p style="padding-left: 20px;"><a title="{{ $item->name }}" href="{{ $http_url('country/'.$item->slug) }}">{{ $item->name }}</a></p>
                    @endforeach
                </div>
                <div class="col-sm-1 footer-block7"></div>
                <div class="clearfix"></div>
            </div>
            <div id="copyright">
                <p>
                    <img alt="{{ Storage::url($setting->images) }}" border="0" src="{{ Storage::url($setting->images) }}" class="mv-ft-logo">
                </p>
                <p>Copyright &copy; <a href="{{ url('/') }}" title="{{ url('/') }}">{{ url('/') }}</a>. All Rights Reserved</p>
                <p style="font-size: 11px; line-height: 14px;">{!! $footer?stripslashes($footer->content):'' !!}</p>
            </div>
            <div id="footer-social">
                <a href="#" class="fs-icon fs-facebook"><i class="fa fa-facebook"></i></a>
                <a href="#" class="fs-icon fs-twitter"><i class="fa fa-twitter"></i></a>
                <a href="#" class="fs-icon fs-google"><i class="fa fa-google-plus"></i></a>
            </div>
            {{--<div id="footer-menu">--}}
                {{--<a href="terms.html" title="Privacy Policy">Terms & Privacy Policy</a>--}}
                {{--<a href="dmca.html" title="DMCA">DMCA</a>--}}
            {{--</div>--}}
        </div>
        {{--<div class="footer-tl text-center mt5" style="font-size:8px; line-height: 1.1em;">--}}
            {{--<style>.footer-tl a {--}}
                    {{--color: #232122;--}}
                {{--}</style>--}}
            {{--<a href="https://gostream.is/" class="mr10" title="123movies">123movies</a>--}}
        {{--</div>--}}
    </div>
</footer>