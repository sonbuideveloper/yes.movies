<div id="footer">
    <div id="footer-home">
        <div class="container">
            <div class="home-footer small text-center">Copyright © {{ url('/') }}. All Rights Reserved</div>
        </div>
    </div>
</div>