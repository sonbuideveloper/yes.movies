<!--header-->
<div id="bh-header">
    <div id="bhh-menu">
        <ul class="top-menu">
            <li class="active"><a href="{{ url('site') }}" title="HOME">HOME</a></li>
            <li><a href="{{ url('movies') }}" title="MOVIES">MOVIES</a></li>
            <li><a href="{{ url('tv-series') }}" title="TV-SERIES">TV-SERIES</a></li>
            <li><a href="{{ url('news') }}" title="NEWS">NEWS</a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
</div>
<!--/header-->
<div class="header-pad"></div>

<!--/header-->