<header>
    <div class="container">
        <div class="header-logo" style="background-image: none;">
            <h1>
                <a title="{{ $setting->name }}" href="{{ $http_url('/') }}" id="logo" style="background-image: url({{ $http_url(Storage::url($setting->images)) }});">
                </a>
            </h1>
        </div>
        <div class="mobile-menu"><i class="fa fa-reorder"></i></div>
        <div class="mobile-search"><i class="fa fa-search"></i></div>
        <div id="menu">
            <ul class="top-menu">
                <li {{ $page=='home'?'class=active':'' }}><a href="{{ $http_url('/') }}" title="Home"><i class="icon-house158"></i><span class="li-text">Home</span></a></li>
                <li {{ $page=='genres'?'class=active':'' }}>
                    <a href="#" title="Genres"><i class="icon-play111"></i><span class="li-text">Genre</span></a>
                    <div class="sub-container" style="display: none">
                        <ul class="sub-menu">
                            @foreach($genres as $item)
                                <li><a title="{{ $item->name }}" href="{{ $http_url('genres/'.$item->slug) }}">{{ $item->name }}</a></li>
                            @endforeach
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </li>
                <li {{ $page=='country'?'class=active':'' }}>
                    <a href="#" title="Country"><i class="icon-earth208"></i><span class="li-text">Country</span></a>
                    <div class="sub-container" style="display: none">
                        <ul class="sub-menu">
                            @foreach($country as $item)
                                <li><a title="{{ $item->name }}" href="{{ $http_url('country/'.$item->slug) }}">{{ $item->name }}</a></li>
                            @endforeach
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </li>
                {{--<li {{ $page=='featured'?'class=active':'' }}>--}}
                    {{--<a href="{{ $http_url('featured') }}" title="Featured">--}}
                        {{--<i class="icon-play111"></i><span class="li-text">Featured</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
                <li>
                    <a href="#" title="List Films"><i class="icon-play111"></i><span class="li-text">List Films</span></a>
                    <div class="sub-container" style="display: none">
                        <ul class="sub-menu">
                            <li><a href="{{ $http_url('movies') }}" title="Movies">Movies</a></li>
                            <li><a href="{{ $http_url('tv-series') }}" title="TV-Series">TV-Series</a></li>
                            @foreach($type as $item)
                                <li><a title="{{ $item->name }}" href="{{ $http_url('type/'.$item->slug) }}">{{ $item->name }}</a></li>
                            @endforeach
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </li>
                <li {{ $page=='playlist'?'class=active':'' }}>
                    <a href="{{ $http_url('playlist-film') }}" title="Playlist">
                        <i class="icon-play111"></i><span class="li-text">Playlist</span>
                    </a>
                </li>
                <li {{ $page=='top-imdb'?'class=active':'' }}>
                    <a href="{{ $http_url('top-imdb/all') }}" title="Top IMDb">
                        <i class="icon-golf32"></i><span class="li-text">Top IMDb</span>
                    </a>
                </li>
                <li {{ $page=='library'?'class=active':'' }}>
                    <a href="{{ $http_url('library-film') }}" title="A-Z List">
                        <i class="icon-play111"></i><span class="li-text">A-Z List</span>
                    </a>
                </li>
                <li {{ $page=='ranking'?'class=active':'' }}>
                    <a href="{{ $http_url('ranking-film') }}" title="Ranking">
                        <i class="icon-play111"></i><span class="li-text">Ranking</span>
                    </a>
                </li>
                <li {{ $page=='news'?'class=active':'' }}>
                    <a href="{{ $http_url('news') }}" title="NEWS">
                        <i class="icon-rss56"></i><span class="li-text">NEWS</span>
                    </a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        @if(!Auth::user())
            <div id="top-user">
                <div class="top-user-content guest">
                    <a href="javascript:void(0)" class="guest-login" title="Login" data-target="#pop-auth" data-toggle="modal"><i class="icon-profile27"></i>LOGIN</a>
                </div>
            </div>
        @else
            <div id="top-user">
                <div class="top-user-content logged">
                    <div class="logged-feed">
                        <a onclick="get_notify()" href="javascript:void(0)" class="btn btn-logged btn-feed" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa icon-notifications"></i>
                        </a>
                        <ul class="dropdown-menu" id="list-notify">
                            <li id="notify-loading" class="cssload-center">
                                <div class="cssload"><span></span></div>
                            </li>
                        </ul>
                    </div>
                    <div class="logged-user">
                        <!--            <a href="#" class="user-class"><span class="uc-type"></span></a>-->
                        <a href="{{ url('users/favorite') }}" class="avatar user-menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{ $http_url('frontend/theme_2/images/default_avatar.jpg') }}">
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('users/favorite') }}"><i class="icon-edit mr5"></i> My favorite</a></li>
                            <li><a href="{{ url('users/logout') }}"><i class="icon-exit_to_app mr5"></i>Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        @endif
        <div id="search">
            <div class="search-content">
                <input data-url="{{ url('/') }}" autocomplete="off" name="keyword" type="text" class="form-control search-input" placeholder="Searching..."/>
                <div id="token-search"></div>
                <a onclick="searchMovie()" class="search-submit" href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a>
                <div class="search-suggest" style="display: none;"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</header>
<div class="header-pad"></div>