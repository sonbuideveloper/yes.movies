<div id="auth-modal">
    <!-- Modal -->
    <div class="modal fade modal-cuz" id="pop-auth" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-tabs">
                <ul id="md-list" class="nav nav-tabs" role="tablist">
                    <li class="active"><a id="tab-login" href="#modal-login" role="tab" data-toggle="tab"><i
                                    class="icon-perm_identity mr5"></i><strong>Login</strong></a></li>
                    <li class="pull-right"><a class="close" data-dismiss="modal" aria-label="Close"><i
                                    class="icon-clear"></i></a></li>
                </ul>
            </div>
            <div class="modal-content tab-content">
                <div id="modal-login" class="tab-pane in fade active">
                    <div class="modal-body">
                        <div class="cuz-login-social">
                            <p class="desc text-center">Watch HD Movies Online For Free and Download the latest movies. For everybody, everywhere, everydevice, and everything ;)</p>
                            <div class="cls-list mt10">
                                <a href="{{ url('facebook/redirect') }}" class="btn btn-block btn-facebook">
                                    <i class="fa fa-facebook-square mr10"></i>Facebook
                                </a>
                                <a href="{{ url('google/redirect') }}" class="btn btn-block btn-google">
                                    <i class="fa fa-google-plus-square mr10"></i>Google+
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="modal-forgot" class="tab-pane in fade">
                    <div class="modal-body">
                        <p class="desc">We will send authorize code to your email. Please fill your email to form
                            below.</p>
                        <form id="forgot-form">
                            <div class="block mt10">
                                <input type="email" class="form-control" name="email" id="Email"
                                       placeholder="Your email">
                            </div>
                            <div style="display: none;" id="forgot-success-message" class="alert alert-success"></div>
                            <div style="display: none;" id="forgot-error-message" class="alert alert-danger"></div>
                            <button id="forgot-submit" type="submit" class="btn btn-block btn-success btn-approve mt20">
                                Submit
                            </button>
                        </form>
                    </div>
                    <div class="modal-footer text-center">
                        <a data-toggle="tab" href="#modal-login" title="Back to login">
                            <i class="fa fa-chevron-left mr10"></i> Back to login
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/ modal -->
</div>

<div class="modal fade modal-report" id="pop-report" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-close"></i></button>
                <h4 class="modal-title" id="report-label"></h4>
            </div>
            <div class="modal-body">
                <p class="desc" id="report_text">Please enter the captcha</p>
                <form id="report-form" method="POST" action="{{ url('report-link') }}">
                    {!! csrf_field() !!}
                    <?php $publickey = env('RECAPTCHA_SITE_KEY'); ?>
                    {!! recaptcha_get_html($publickey) !!}
                    <input type="hidden" name="film_id" value="{{ !empty($film)?$film->id:0 }}">
                    <input type="hidden" name="link_id" id="link_id_input" value="">
                    <button type="submit" class="btn btn-block btn-success btn-approve mt20">Submit</button>
                </form>
            </div>
            <div class="modal-footer text-center"></div>
        </div>
    </div>
</div>

