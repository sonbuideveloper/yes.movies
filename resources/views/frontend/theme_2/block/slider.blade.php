<div id="slider">
	<div class="swiper-wrapper">
		@if(count($sliders)>0)
			@foreach($sliders as $item)
				<div class="swiper-slide"
					 style="background-image: url(@if($item->background) {{ url(Storage::url($item->background)) }} @elseif($item->background_link) {{ url($item->background_link) }} @endif">
					<a href="{{ url('movie/'.$item->slug.'-'.$item->id) }}" class="slide-link" title="{{ $item->name }}">
					<span class="slide-caption">
						<h2>{{ $item->name }}</h2>
						<p class="sc-desc">{!! strlen($item->content)>250?substr(stripslashes($item->content),0,250).'...':stripslashes($item->content) !!}</p>
						<div class="btn btn-success mt10"><i class="fa fa-play"></i> Watch now</div>
					</span>
					</a>
				</div>
			@endforeach
		@endif
	</div>
	<div class="swiper-pagination"></div>
	<div class="clearfix"></div>
</div>