<?php
$http_url = env('HTTP_URL');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title class="meta-title">{{ $meta['title'] }}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="content-language" content="en"/>
    <meta name="robots" content="noodp,noydir,index,follow "/>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1"/>
    <link rel="shortcut icon" href="https://gomovies.to/assets/images/gomovies-favicon.png" type="image/x-icon"/>
    <meta http-equiv="content-language" content="en"/>
    <meta class="meta-description" name="description" content="{!! $meta['description'] !!}">
    <meta class="meta-keywords" name="keywords" content="{!! $meta['keywords'] !!}">
    <meta class="meta-title" name="title" content="{{ $meta['title'] }}">
    <meta property="fb:app_id" content="{!! $setting->app_id_face !!}">
    <meta class="meta-title" property="og:title" content="{{ $meta['title'] }}"/>
    <meta class="meta-images" property="og:image" content="{{ $meta['images'] }}"/>
    <meta property="og:site_name" content="{{ $setting->name }}"/>
    <meta class="meta-url" property="og:url" content="{{ $http_url()->current() }}"/>
    <meta class="meta-keywords" property="og:keywords" content="{{ $meta['keywords'] }}"/>
    <meta class="meta-description" property="og:description" content="{!! $meta['description'] !!}"/>
    <meta name="twitter:card" content="summary_large_image"/>
    <meta class="meta-description" name="twitter:description" content="{!! $meta['description'] !!}"/>
    <meta class="meta-title" name="twitter:title" content="{!! $meta['title'] !!}"/>
    <meta name="twitter:site" content="{{ $http_url()->current() }}"/>
    <meta class="meta-images" name="twitter:image" content="{{ $meta['images'] }}"/>

    <link rel="apple-touch-icon" href="{{ $http_url(\Storage::url($setting->favicon)) }}">
    <link rel="shortcut icon" href="{{ $http_url(\Storage::url($setting->favicon)) }}">
    <link rel="author" href="https://plus.google.com/{{ $setting->google_url }}"/>
    <link rel="canonical" href="{{ $http_url()->current() }}"/>

    <link rel="stylesheet" href="{{ $http_url('frontend/theme_2/css/bootstrap.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ $http_url('frontend/theme_2/css/main.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ $http_url('frontend/theme_2/css/all.min.css') }}" type="text/css" />
    @if($page=='index')
        <link rel="stylesheet" href="{{ $http_url('frontend/theme_2/css/home.css') }}" type="text/css" />
    @endif
    <script type="text/javascript" src="{{ $http_url('frontend/theme_2/js/jquery-1.9.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ $http_url('frontend/theme_2/js/jquery.cookie.js') }}"></script>
    <script type="text/javascript" src="{{ $http_url('frontend/theme_2/js/md5.min.js') }}"></script>
    <script type="text/javascript" src="{{ $http_url('frontend/theme_2/js/main.min.js') }}"></script>
    <script type="text/javascript" src="{{ $http_url('frontend/theme_2/plugins/jquery.cookie.min.js') }}"></script>
    <script type="text/javascript" src="{{ $http_url('frontend/theme_2/js/jwplayer/jwplayer.js') }}"></script>
    <script type="text/javascript">jwplayer.key = '{{ $setting->key_jwplayer }}';</script>
    <script type="text/javascript" src="{{ $http_url('frontend/theme_2/js/jwplayer.js') }}"></script>
    <script src="{{ $http_url('frontend/theme_2/js/detectmobilebrowser.js') }}"></script>
    @if($page=='watch')
        <script language="javascript" type="text/javascript">
            var isClose = false;
            document.onkeydown = checkKeycode
            function checkKeycode(e) {
                var keycode;
                if (window.event)
                    keycode = window.event.keyCode;
                else if (e)
                    keycode = e.which;
                if (keycode == 116) {
                    isClose = true;
                }
            }
            function somefunction() {
                isClose = true;
            }
            function doUnload() {
//                if (!isClose) {
                addviewfilm({{$film->id}}, {!! json_encode(Session::get('subtitle_upload')) !!});
//                }
            }
        </script>
    @endif

    {{--Create Adsense--}}
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-5537233573134398",
            enable_page_level_ads: true
        });
    </script>

</head>
<style>
    .banner-content{
        text-align: center;
        height: 100%;
        display: block;
        width: 100%;
        overflow: hidden;
    }
</style>
@if($page=='watch')
<body id="body-search" onbeforeunload="doUnload();" onmousedown="somefunction();">
@else
<body id="body-search">
@endif
{{--Google analytci--}}
@if($setting->google_analytic)
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', '{{ $setting->google_analytic }}', 'auto');
        ga('send', 'pageview');
    </script>
@endif

<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.9&appId={{ $setting->app_id_face }}";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

@if($page!='index')
<div id="xmain">
<!--header-->
@include('frontend.theme_2.block.header')
<!--/header-->

<!-- main -->
@include('frontend.theme_2.block.error')
@else
<div id="main">
<!--header-->
@include('frontend.theme_2.block.bh-header')
<!--/header-->
@endif

@yield('content')
<!--/main -->

@if($page!='index')
<!--footer-->
@include('frontend.theme_2.block.footer')
<!--/footet-->

</div>
<div class="clearfix"></div>

<!--modal-->
@include('frontend.theme_2.block.modal')
<!--/ modal -->
@else
<!--footer-->
@include('frontend.theme_2.block.bh-footer')
<!--/footet-->
@endif
<script>
    $(document).ready(function () {
        $("img.lazy").lazyload({
            effect: "fadeIn"
        });
    });
</script>
<script type="text/javascript" src="{{ $http_url('frontend/theme_2/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ $http_url('frontend/theme_2/js/bootstrap-select.js') }}"></script>
<script type="text/javascript" src="{{ $http_url('frontend/theme_2/js/jquery.smooth-scroll.min.js') }}"></script>
<script type="text/javascript" src="{{ $http_url('frontend/theme_2/js/jquery.lazyload.js') }}"></script>
<script type="text/javascript" src="{{ $http_url('frontend/theme_2/js/jquery.hover-intent.js') }}"></script>
<script type="text/javascript" src="{{ $http_url('frontend/theme_2/js/jquery.cluetip.min.js') }}"></script>
<script type="text/javascript" src="{{ $http_url('frontend/theme_2/js/perfect-scrollbar.jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ $http_url('frontend/theme_2/js/detectmobilebrowser.js') }}"></script>
<script type="text/javascript" src="{{ $http_url('frontend/theme_2/js/slide.min.js') }}"></script>
<script type="text/javascript" src="{{ $http_url('frontend/theme_2/js/myscript.js') }}"></script>
</body>
</html>
