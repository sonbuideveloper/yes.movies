<?php
$http_url = env('HTTP_URL');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title class="meta-title">{{ $meta['title'] }}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="content-language" content="en"/>
    <meta name="robots" content="noodp,noydir,index,follow "/>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1"/>
    <link rel="shortcut icon" href="https://gomovies.to/assets/images/gomovies-favicon.png" type="image/x-icon"/>
    <meta http-equiv="content-language" content="en"/>
    <meta class="meta-description" name="description" content="{!! $meta['description'] !!}">
    <meta class="meta-keywords" name="keywords" content="{!! $meta['keywords'] !!}">
    <meta class="meta-title" name="title" content="{{ $meta['title'] }}">
    <meta property="fb:app_id" content="{!! $setting->app_id_face !!}">
    <meta class="meta-title" property="og:title" content="{{ $meta['title'] }}"/>
    <meta class="meta-images" property="og:image" content="{{ $meta['images'] }}"/>
    <meta property="og:site_name" content="{{ $setting->name }}"/>
    <meta class="meta-url" property="og:url" content="{{ $http_url()->current() }}"/>
    <meta class="meta-keywords" property="og:keywords" content="{{ $meta['keywords'] }}"/>
    <meta class="meta-description" property="og:description" content="{!! $meta['description'] !!}"/>
    <meta name="twitter:card" content="summary_large_image"/>
    <meta class="meta-description" name="twitter:description" content="{!! $meta['description'] !!}"/>
    <meta class="meta-title" name="twitter:title" content="{!! $meta['title'] !!}"/>
    <meta name="twitter:site" content="{{ $http_url()->current() }}"/>
    <meta class="meta-images" name="twitter:image" content="{{ $meta['images'] }}"/>

    <link rel="apple-touch-icon" href="{{ $http_url(Storage::url($setting->favicon)) }}">
    <link rel="shortcut icon" href="{{ $http_url(Storage::url($setting->favicon)) }}">
    <link rel="author" href="https://plus.google.com/{{ $setting->google_url }}"/>
    <link rel="canonical" href="{{ $http_url()->current() }}"/>
    <link rel="stylesheet" href="{{ $http_url('frontend/theme_1/css/bootstrap.min.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ $http_url('frontend/theme_1/css/styles.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ $http_url('frontend/theme_1/css/jquery.cluetip.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ $http_url('frontend/theme_1/css/custom.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ $http_url('frontend/theme_1/css/slide.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ $http_url('frontend/theme_1/css/psbar.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ $http_url('frontend/theme_1/css/jquery-confirm.css') }}"/>
    <script type="text/javascript" src="{{ $http_url('frontend/theme_1/js/jquery-1.9.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ $http_url('frontend/theme_1/js/jquery.lazyload.js') }}"></script>
    <script type="text/javascript" src="{{ $http_url('frontend/theme_1/js/jquery.cluetip.min.js') }}"></script>
    <script type="text/javascript" src="{{ $http_url('frontend/theme_1/plugins/jquery.cookie.min.js') }}"></script>
    <script type="text/javascript" src="{{ $http_url('frontend/theme_1/js/jwplayer/jwplayer.js') }}"></script>
    <script type="text/javascript">jwplayer.key = '{{ $setting->key_jwplayer }}';</script>
    <script type="text/javascript" src="{{ $http_url('frontend/theme_1/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ $http_url('frontend/theme_1/js/jwplayer.js') }}"></script>
    @if($page=='watch')
        <script language="javascript" type="text/javascript">
            var isClose = false;
            document.onkeydown = checkKeycode
            function checkKeycode(e) {
                var keycode;
                if (window.event)
                    keycode = window.event.keyCode;
                else if (e)
                    keycode = e.which;
                if (keycode == 116) {
                    isClose = true;
                }
            }
            function somefunction() {
                isClose = true;
            }
            function doUnload() {
//                if (!isClose) {
                    addviewfilm({{$film->id}}, {!! json_encode(Session::get('subtitle_upload')) !!});
//                }
            }
        </script>
    @endif

    {{--Create Adsense--}}
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-5537233573134398",
            enable_page_level_ads: true
        });
    </script>

</head>
<style>
    .tn-movies .img {
        float: left;
        width: 100px;
        height: 65px;
        overflow: hidden;
        margin-right: 10px;
    }

    .tn-movies .img img {
        width: 100%;
        margin-top: -25%;
    }

    .tn-movies p {
        margin: 0;
    }

    .tn-movies p.view {
        font-size: 12px;
    }
</style>
<body onbeforeunload="doUnload();" onmousedown="somefunction();">
{{--Google analytci--}}
@if($setting->google_analytic)
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{ $setting->google_analytic }}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', '{{ $setting->google_analytic }}');
    </script>
@endif

{{--<div id="fb-root"></div>--}}
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.9&appId={{ $setting->app_id_face }}";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
{{--CHange mode--}}
<div id="switch-mode">
    <div class="sm-icon"><i class="fa fa-moon-o"></i></div>
    <div class="sm-text">Night mode</div>
    <div class="sm-button"><span></span></div>
</div>
<script type="text/javascript">
    if ($.cookie("night-mode")) {
        $("#switch-mode").addClass("active");
        $("head").append("<link href='{{ $http_url('frontend/theme_1/css/main-dark.css') }}' type='text/css' rel='stylesheet' />");
    } else {
        $("#switch-mode").removeClass("active");
    }
    $('#switch-mode').click(function () {
        if ($.cookie("night-mode")) {
            $.removeCookie("night-mode", {path: '/'});
        } else {
            $.cookie("night-mode", 1, {expires: 365, path: '/'});
        }
        location.reload();
    });
</script>

<!--header-->
@include('frontend.theme_1.block.header')
<!--/header-->

<!-- main -->
@include('frontend.theme_1.block.error')
@yield('content')
<!--/main -->

<!--footer-->
@include('frontend.theme_1.block.footer')
<!--/footet-->

<!--modal-->
@include('frontend.theme_1.block.modal')
<!--/ modal -->
<script>
    $("img.lazy").lazyload({effect: "fadeIn"});
</script>
<script type="text/javascript" src="{{ $http_url('frontend/theme_1/js/star-rating.min.js') }}"></script>
<script type="text/javascript" src="{{ $http_url('frontend/theme_1/js/ipmovies.min.js') }}"></script>
<script type="text/javascript" src="{{ $http_url('frontend/theme_1/js/auth.min.js') }}"></script>
<script type="text/javascript" src="{{ $http_url('frontend/theme_1/js/slide.min.js') }}"></script>
<script type="text/javascript" src="{{ $http_url('frontend/theme_1/js/psbar.jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ $http_url('frontend/theme_1/js/bootstrap-star-rating.js') }}"></script>
<script type="text/javascript" src="{{ $http_url('js/myscript.js') }}"></script>

<script type="text/javascript">
    rating();
    var swiper = new Swiper('#slider', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        loop: true,
        autoplay: 4000
    });
    $(function () {
        $('.tn-news, .tn-notice').perfectScrollbar();
    });
</script>
</body>

</html>