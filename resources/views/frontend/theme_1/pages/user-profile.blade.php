@extends('frontend.theme_1.master')
@section('content')

    <div id="main">
        <div class="container">
            <div class="pad"></div>
            <div class="main-content main-category">
                <div class="movies-list-wrap mlw-category">
                    <div class="ml-title"><span class="pull-left">{{ $title }}</span>
                        <ul role="tablist" class="nav nav-tabs">
                            <li class="{{ $sub_active=='profile'?'active':'' }}">
                                <a href="{{ url('users/profile') }}">Profile</a>
                            </li>
                            <li class="{{ $sub_active=='favorite'?'active':'' }}">
                                <a href="{{ url('users/favorite') }}">Favorite</a>
                            </li>
                            <li>
                                <a href="{{ url('users/logout') }}">Logout</a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="movies-list movies-list-full">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="right-account-content">
                            <form id="submitForm" class="form-horizontal" method="post" enctype="multipart/form-data">
                                {!! csrf_field() !!}
                                <div class="form-group">
                                    <label class="control-label avatar-label col-sm-3" for="account_fname">Avatar:</label>
                                    <div class="col-sm-9">
                                        <img class="avatar-holder" src="{{ Auth::user()->images?url(Storage::url(Auth::user()->images)):url('public/frontend/img/no-image.jpg') }}">
                                    </div>
                                </div>

                                <div class="account-info-input">
                                    <div class="form-group">
                                        <label class="control-label col-sm-3" for="account_fname" style="text-transform: uppercase;">First Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" id="account_fname" name="firstname" value="{{ Auth::user()->firstname }}" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-3" for="account_lname" style="text-transform: uppercase;">Last Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" id="account_lname" name="lastname" value="{{ Auth::user()->lastname }}" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-3" for="account_email">Email</label>
                                        <div class="col-sm-9">
                                            <input type="email" class="form-control" name="email" id="account_email" value="{{ Auth::user()->email }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-sm-3" for="account_newpassword"></label>
                                        <div class="col-sm-9">
                                            <button class="btn btn-primary" type="submit">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- account right content end -->
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/main -->
@stop
