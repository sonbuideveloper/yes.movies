@extends('frontend.theme_1.master')
@section('content')

    <div id="main">
        <div class="container">
            <div class="pad"></div>
            @if($page_list_top_page && $page_list_top_page->content)
                <div class="banner-content">
                    {!! stripslashes($page_list_top_page->content) !!}
                </div>
            @endif
            <div class="main-content main-category">
                <div class="movies-list-wrap mlw-category">
                    <div class="ml-title ml-title-page"><span>{{ $title }}</span>
                        {{--<div class="filter-toggle"><i class="fa fa-sort mr5"></i>Filter</div>--}}
                        <div class="clearfix"></div>
                    </div>
                    <div class="movies-list movies-list-full">
                        @if(count($results)>0)
                            @foreach($results as $item)
                            <div class="ml-item">
                                <a href="{{ url('playlist/'.$item->slug.'-'.$item->id) }}" title="{{ $item->name }}">
                                    <img data-original="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif"
                                         src="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif"
                                         class="lazy thumb mli-thumb" alt="{{ $item->name }}"> <span class="mli-info"><h2>{{ $item->name }}</h2></span>
                                </a>
                            </div>
                            @endforeach
                        @endif
                        <div class="clearfix"></div>
                    </div>
                    <br>
                    @if($page_list_bottom_page && $page_list_bottom_page->content)
                        <div class="banner-content">
                            {!! stripslashes($page_list_bottom_page->content) !!}
                        </div>
                    @endif
                    <br>
                    <div class="text-center">
                        {!! $results->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/main -->
@stop
