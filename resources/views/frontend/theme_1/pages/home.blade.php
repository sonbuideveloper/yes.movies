@extends('frontend.theme_1.master')
@section('content')
<style>
    .imgThumb {
        width: 25%;
        display: inline-block;
        float: left;
    }
    .blockContInfo {
        width: 70%;
        display: inline-block;
        float: left;
    }
</style>
<div id="main">
    <div class="container">
        <div class="top-content">
            <!-- slider -->
            @include('frontend.theme_1.block.slider')
            <!--/slider -->

            <!--top news-->
            <div id="top-news">
                <div class="top-news">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#tn-comingsoon" role="tab" data-toggle="tab">COMING SOON</a></li>
                        <li><a href="#tn-fanpage" role="tab" data-toggle="tab">FAN PAGE</a></li>
                    </ul>
                    <div class="top-news-content">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade" id="tn-fanpage">
                                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2F{{ $setting->facebook_url }}%2F&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId={{ $setting->app_id_face }}"
                                        width="340" height="500" style="border:none;overflow:hidden" scrolling="no"
                                        frameborder="0" allowTransparency="true"></iframe>
                            </div>
                            <div role="tabpanel" class="tab-pane in fade active" id="tn-comingsoon">
                                <ul class="tn-news tn-movies">
                                    @if(isset($articles) && count($articles)>0)
                                        @foreach($articles as $item)
                                            <li>
                                                <a class="imgThumb" href="{{ url('coming-soon/'.$item->slug) }}">
                                                    <img src="{{ url(Storage::url($item->images)) }}" alt="" width="65" height="65">
                                                </a>
                                                <div class="blockContInfo">
                                                    <a href="{{ url('coming-soon/'.$item->slug) }}">{{ $item->name }}</a>
                                                </div>
                                            </li>
                                            <div class="clearfix"></div>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/top news-->
            <div class="clearfix"></div>
        </div>

        <!--social home-->
        <div class="social-home">
            <div class="addthis_native_toolbox">
                <div class="fb-like" data-href="{{ url('/') }}" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
                <div class="fb-send" data-href="{{ url('/') }}"></div>
                <span class="sh-text">Like and Share our website to support us.</span>
            </div>
            <div class="clearfix"></div>
        </div>
        <!--/social home-->
        <div style="text-align: center; margin: 20px 0;"></div>
        <div class="main-content">
            @if(isset($home_top_episode) && $home_top_episode->content)
            <div class="banner-content">
                {!! stripslashes($home_top_episode->content) !!}
            </div>
            @endif
            <div class="movies-list-wrap mlw-topview">
                <div class="ml-title"><span class="pull-left">New Episode<i class="fa fa-chevron-right ml10"></i></span>
                    <a href="{{ url('tv-series') }}" class="pull-right cat-more">View more »</a>
                    <div class="clearfix"></div>
                </div>
                <div class="tab-content">
                    <div id="latest-tv-all" class="movies-list movies-list-full tab-pane in fade active">
                        @if(isset($film_series) && count($film_series)>0)
                            @foreach($film_series as $item)
                                <div class="ml-item">
                                    <a href="{{ url('watch/'.$item->slug.'-Episode-'.count($item->film_detail).'-'.$item->id.'?p='.count($item->film_detail)) }}" data-id="{{ $item->id }}" data-url="{{ url('/') }}" data-index="2" data-jtip="#f-movies-2-{{ $item->id }}" class="ml-mask jt film-detail-short" title="{{ $item->name }}">
                                        <img data-original="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif" src="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif" class="lazy thumb mli-thumb" alt="{{ $item->name }}">
                                        <span class="mli-info">
                                            <h2>{{ $item->name }}</h2>
                                        </span>
                                        <span class="mli-eps">Eps<i>{{ count($item->film_detail) }}</i></span>
                                    </a>
                                </div>
                            @endforeach
                        @endif

                        <div class="clearfix"></div>
                    </div>
                    @if(count($country)>3)
                        @for($i=0; $i<3; $i++)
                            <div id="latest-{{ $country[$i]->slug }}" class="movies-list movies-list-full tab-pane in fade"></div>
                        @endfor
                    @endif
                </div>
            </div>
            @if($home_top_suggestion && $home_top_suggestion->content)
                <div class="banner-content">
                    {!! stripslashes($home_top_suggestion->content) !!}
                </div>
            @endif
            <div class="movies-list-wrap mlw-topview">
                <div class="ml-title">
                    <span class="pull-left">Suggestion<i class="fa fa-chevron-right ml10"></i></span>
                    <a href="{{ url('featured') }}" class="pull-right cat-more">View more »</a>
                    <ul role="tablist" class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" role="tab" href="#movies-featured" aria-expanded="false">Featured Movies</a></li>
                        <li onclick="movies_by_top('today')"><a data-toggle="tab" role="tab" href="#movies-today" aria-expanded="false">Top Today</a></li>
                        <li onclick="movies_by_top('imdb')"><a data-toggle="tab" role="tab" href="#movies-imdb" aria-expanded="false">Top IMDb</a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="tab-content">
                    <div id="movies-featured" class="movies-list movies-list-full tab-pane in fade active">
                        @if(count($featured)>0)
                            @foreach($featured as$item)
                            <div class="ml-item">
                                <a href="{{ url('watch/'.$item->slug.'-'.$item->id) }}" data-id="{{ $item->id }}" data-url="{{ url('/') }}" data-index="0" data-jtip="#f-movies-0-{{ $item->id }}" class="ml-mask jt film-detail-short" title="{{ $item->name }}">
                                    @if(!$item->movie_series)
                                        <span class="mli-quality" style="text-transform: uppercase">{{ $item->quality }}</span>
                                    @endif
                                    <img data-original="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif" src="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif" class="lazy thumb mli-thumb" alt="{{ $item->name }}">
                                    <span class="mli-info">
                                        <h2>{{ $item->name }}</h2>
                                    </span>
                                    @if($item->movie_series)
                                        <span class="mli-eps">Eps<i>{{ count($item->film_detail) }}</i></span>
                                    @endif
                                </a>
                            </div>
                            @endforeach
                        @endif
                        <div class="clearfix"></div>
                    </div>
                    <div id="movies-today" class="movies-list movies-list-full tab-pane in fade"></div>
                    <div id="movies-imdb" class="movies-list movies-list-full tab-pane in fade"></div>
                </div>
            </div>
            <!--#list movies -->
            @if($home_top_movies && $home_top_movies->content)
                <div class="banner-content">
                    {!! stripslashes($home_top_movies->content) !!}
                </div>
            @endif
            <div class="movies-list-wrap mlw-topview">
                <div class="ml-title"><span class="pull-left">Movies<i class="fa fa-chevron-right ml10"></i></span> <a href="{{ url('movies') }}" class="pull-right cat-more">View more »</a>
                    <ul role="tablist" class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" role="tab" href="#latest-all" aria-expanded="true">All</a>
                        </li>
                        @if(count($genres)>3)
                            @for($i=0; $i<3; $i++)
                                <li onclick="movies_by_genre('{{ $genres[$i]->slug }}')">
                                    <a data-toggle="tab" role="tab" href="#latest-{{ $genres[$i]->slug }}" aria-expanded="false">{{ $genres[$i]->name }}</a>
                                </li>
                            @endfor
                        @endif
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="tab-content">
                    <div id="latest-all" class="movies-list movies-list-full tab-pane in fade active">
                        @if(count($movies)>0)
                            @foreach($movies as$item)
                                <div class="ml-item">
                                    <a href="{{ url('watch/'.$item->slug.'-'.$item->id) }}" data-id="{{ $item->id }}" data-url="{{ url('/') }}" data-index="1" data-jtip="#f-movies-1-{{ $item->id }}" class="ml-mask jt film-detail-short" title="{{ $item->name }}">
                                        @if(!$item->movie_series)
                                            <span class="mli-quality" style="text-transform: uppercase">{{ $item->quality }}</span>
                                        @endif
                                        <img data-original="@if($item->images){{url(Storage::url($item->images))}}@elseif($item->images_link){{url($item->images_link)}}@endif" src="@if($item->images){{url(Storage::url($item->images))}}@elseif($item->images_link){{url($item->images_link)}}@endif" class="lazy thumb mli-thumb" alt="{{ $item->name }}">
                                        <span class="mli-info">
                                        <h2>{{ $item->name }}</h2>
                                    </span>
                                    </a>
                                </div>
                            @endforeach
                        @endif
                        <div class="clearfix"></div>
                    </div>
                    @if(count($genres)>3)
                        @for($i=0; $i<3; $i++)
                            <div id="latest-{{ $genres[$i]->slug }}" class="movies-list movies-list-full tab-pane in fade"></div>
                        @endfor
                    @endif
                </div>
            </div>
            <!--#list movies -->
            @if($home_top_tv_series && $home_top_tv_series->content)
                <div class="banner-content">
                    {!! stripslashes($home_top_tv_series->content) !!}
                </div>
            @endif
            <div class="movies-list-wrap mlw-topview">
                <div class="ml-title"><span class="pull-left">TV series<i class="fa fa-chevron-right ml10"></i></span>
                    <a href="{{ url('tv-series') }}" class="pull-right cat-more">View more »</a>
                    <ul role="tablist" class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" role="tab" href="#latest-tv-all" aria-expanded="true">All</a>
                        </li>
                        @if(count($country)>3)
                            @for($i=0; $i<3; $i++)
                                <li onclick="movies_by_country('{{ $country[$i]->slug }}')">
                                    <a data-toggle="tab" role="tab" href="#latest-{{ $country[$i]->slug }}" aria-expanded="false">{{ $country[$i]->name }}</a>
                                </li>
                            @endfor
                        @endif
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="tab-content">
                    <div id="latest-tv-all" class="movies-list movies-list-full tab-pane in fade active">
                        @if(count($film_series)>0)
                            @foreach($film_series as$item)
                                <div class="ml-item">
                                    <a href="{{ url('watch/'.$item->slug.'-'.$item->id) }}" data-id="{{ $item->id }}" data-url="{{ url('/') }}" data-index="2" data-jtip="#f-movies-2-{{ $item->id }}" class="ml-mask jt film-detail-short" title="{{ $item->name }}">
                                        <img data-original="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif" src="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif" class="lazy thumb mli-thumb" alt="{{ $item->name }}">
                                        <span class="mli-info">
                                            <h2>{{ $item->name }}</h2>
                                        </span>
                                        <span class="mli-eps">Eps<i>{{ count($item->film_detail) }}</i></span>
                                    </a>
                                </div>
                            @endforeach
                        @endif

                        <div class="clearfix"></div>
                    </div>
                    @if(count($country)>3)
                        @for($i=0; $i<3; $i++)
                            <div id="latest-{{ $country[$i]->slug }}" class="movies-list movies-list-full tab-pane in fade"></div>
                        @endfor
                    @endif
                </div>
            </div>
            <!--#list tv series -->
        </div>
    </div>
</div>

@stop
