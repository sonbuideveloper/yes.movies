@extends('frontend.theme_1.master')
@section('content')
<style>
    .movies-letter a.btn-letter{
        width: 9%;
    }
    #year_filter{
        height: 30px;
        background-color: #ffffff;
        border: none;
        padding: 0px 10px;
    }
    @media screen and (max-width: 799px){
        .movies-letter a.btn-letter{
            width: 30%;
        }
    }
</style>
<div id="main" class="">
    <div class="container">
        <div class="pad"></div>
        @if($page_list_top_page && $page_list_top_page->content)
            <div class="banner-content">
                {!! stripslashes($page_list_top_page->content) !!}
            </div>
        @endif
        <div class="main-content main-category">
            <div class="movies-list-wrap mlw-category">
                <div class="ml-title ml-title-page"><span>{{ $title }}</span>
                    <div class="clearfix"></div>
                </div>
                <div class="ml-alphabet">
                    <div class="movies-letter">
                        <a class="btn btn-letter {{ $q=='top-views'?'active':'' }}" title="Top Views" href="{{ url('ranking-film?q=top-views') }}">Top Views</a>
                        <a class="btn btn-letter {{ $q=='spring'?'active':'' }}" title="Spring" href="{{ url('ranking-film?q=spring') }}">Spring</a>
                        <a class="btn btn-letter {{ $q=='summer'?'active':'' }}" title="Summer" href="{{ url('ranking-film?q=summer') }}">Summer</a>
                        <a class="btn btn-letter {{ $q=='fall'?'active':'' }}" title="Fall" href="{{ url('ranking-film?q=fall') }}">Fall</a>
                        <a class="btn btn-letter {{ $q=='winter'?'active':'' }}" title="Winter" href="{{ url('ranking-film?q=winter') }}">Winter</a>
                        <a class="btn btn-letter {{ $q=='year'?'active':'' }}" title="Year" href="{{ url('ranking-film?q=year') }}">Year</a>
                        <a class="btn btn-letter {{ $q=='favorite'?'active':'' }}" title="Favorite" href="{{ url('ranking-film?q=favorite') }}">Favorite</a>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="letter-movies-lits">
                    @if($y)
                    <form id="filter-year-form" class="form-horizontal" method="get" action="">
                        <div class="form-group">
                            <input type="hidden" name="q" value="{{ $q }}">
                            <div class="col-md-3 col-xs-12">
                                <select id="year_filter" name="y" style="width: 100%;">
                                    <option value="">Release Year</option>
                                    @for($i=2017; $i>1970; $i--)
                                        <option {{ (!empty($y) && $y==$i)?'selected':'' }} value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </form>
                    @endif
                    <table class="table table-striped">
                        <tbody>
                        <tr class="mlnew-head">
                            <td class="mlnh-1">#</td>
                            <td colspan="2" class="mlnh-letter">{{ count($results) }} results</td>
                            <td class="mlnh-3">Day release</td>
                            <td class="mlnh-3">Status</td>
                            <td class="mlnh-5">Country</td>
                            <td class="mlnh-4">Genre</td>
                            @if($q=='favorite')
                                <td class="mlnh-6">Favorite</td>
                            @else
                                <td class="mlnh-6">View</td>
                            @endif
                        </tr>
                        @if(count($results)>0)
                            <?php $i=0; ?>
                            @foreach($results as $item)
                                <?php $i++; ?>
                                <tr class="mlnew">
                                    <td class="mlnh-1">{{ $i }}</td>
                                    <td class="mlnh-thumb">
                                        <a class="thumb" title="{{ $item->name }}" href="{{ url('watch/'.$item->slug.'-'.$item->id) }}">
                                            <img alt="{{ $item->name }}" title="{{ $item->name }}" src="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif">
                                        </a>
                                    </td>
                                    <td class="mlnh-2">
                                        <h2><a title="{{ $item->name }}" href="{{ url('watch/'.$item->slug.'-'.$item->id) }}">{{ $item->name }}</a></h2>
                                    </td>
                                    <td>{{ $item->day_release }}</td>
                                    <td class="mlnh-3">{{ strtoupper($item->quality) }}</td>
                                    <td class="mlnh-4"><a href="{{ url('country/'.$item->country->slug) }}" title="{{ $item->country->name }}">{{ $item->country->name }}</a></td>
                                    <td class="mlnh-5">
                                        @if(count($item->genres)>0)
                                            <?php $i=0; ?>
                                            @foreach($item->genres as $element)
                                                <a href="{{ url('genres/'.$element->slug) }}" title="{{ $element->name }}">{{ $element->name }}</a>{{ ($i==count($item->genres)-1)?'':',' }}
                                                <?php $i++; ?>
                                            @endforeach
                                        @endif
                                    </td>
                                    @if($q=='favorite')
                                        <td class="mlnh-6">{{ $item->favorite_total }}</td>
                                    @else
                                        <td class="mlnh-6">{{ $item->view }}</td>
                                    @endif
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                <br>
                @if($page_list_bottom_page && $page_list_bottom_page->content)
                    <div class="banner-content">
                        {!! stripslashes($page_list_bottom_page->content) !!}
                    </div>
                @endif
                <br>
                <div class="text-center">
                    {!! $results->appends(['q' => $q])->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@stop