@extends('frontend.theme_1.master')
@section('content')

    <div id="main">
        <div class="container">
            <div class="pad"></div>
            @if($page_list_top_page && $page_list_top_page->content)
                <div class="banner-content">
                    {!! stripslashes($page_list_top_page->content) !!}
                </div>
            @endif
            <div class="main-content main-category">
                <div class="movies-list-wrap mlw-category">
                    <div class="ml-title ml-title-page"><span>{{ $title }}</span>
                        <div class="filter-toggle"><i class="fa fa-sort mr5"></i>Filter</div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="filter">
                        <form id="form_filter" method="get">
                            Order:
                            <select name="order"  class="selectpicker">
                                <option {{ $order=='0'?'selected':'' }} value="0">Default</option>
                                <option {{ $order=='new'?'selected':'' }} value="new">New Update</option>
                                <option {{ $order=='view'?'selected':'' }} value="view">Most Viewed</option>
                                <option {{ $order=='year'?'selected':'' }} value="year">Release Year</option>
                                <option {{ $order=='name'?'selected':'' }} value="name">Movies name</option>
                                <option {{ $order=='imdb'?'selected':'' }} value="imdb">IMDB</option>
                            </select>
                            @if($page!='genres')
                            Genre:
                            <select name="categoryid"  class="selectpicker">
                                <option value="0">All</option>
                                @foreach($genres as $item)
                                <option {{ $categoryid==$item->id?'selected':'' }} value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                            @endif
                            @if($page!='country')
                            Country:
                            <select name="countryid"  class="selectpicker">
                                <option value="0">All</option>
                                @foreach($country as $item)
                                    <option {{ $countryid==$item->id?'selected':'' }} value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                            @endif
                            Season:
                            <select name="season"  class="selectpicker">
                                <option value="0">All</option>
                                <option {{ (!empty($season) && $season=='spring')?'selected':'' }} value="spring">Spring</option>
                                <option {{ (!empty($season) && $season=='summer')?'selected':'' }} value="summer">Summer</option>
                                <option {{ (!empty($season) && $season=='fall')?'selected':'' }} value="fall">Fall</option>
                                <option {{ (!empty($season) && $season=='winter')?'selected':'' }} value="winter">Winter</option>
                            </select>
                            Years:
                            <select name="year" class="selectpicker">
                                <option value="0">All</option>
                                @for($i=2017; $i>1990; $i--)
                                    <option {{ (!empty($year) && $year==$i)?'selected':'' }} value="{{ $i }}">{{ $i }}</option>
                                @endfor
                            </select>
                            <div class="mt10">
                                <button type="submit" class="btn btn-success btn-block">Show</button>
                            </div>
                        </form>
                    </div>

                    <div class="movies-list movies-list-full">
                        @if(count($results)>0)
                            @foreach($results as $item)
                            <div class="ml-item">
                                <a href="{{ url('watch/'.$item->slug.'-'.$item->id) }}" data-jtip="#f-movies-0-{{ $item->id }}" class="ml-mask jt" title="{{ $item->name }}">
                                    @if(!$item->movie_series)
                                        <span class="mli-quality" style="text-transform: uppercase">{{ $item->quality }}</span>
                                    @endif
                                    <img data-original="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif"
                                         src="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif"
                                         class="lazy thumb mli-thumb" alt="{{ $item->name }}"> <span class="mli-info"><h2>{{ $item->name }}</h2></span>
                                    @if($item->movie_series)
                                        <span class="mli-eps">Eps<i>{{ count($item->film_detail) }}</i></span>
                                    @endif
                                </a>
                                <div id="f-movies-0-{{ $item->id }}" style="display: none; position: relative;">
                                    <div class="jtip-top">
                                        <div class="jt-info jt-imdb">IMDb: {{ $item->IMDB }}</div>
                                        <div class="jt-info">{{ $item->release }}</div>
                                        <div class="jt-info">{{ $item->duration }} min</div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <p class="f-desc">{!! stripslashes($item->content) !!}</p>
                                    <div class="block">
                                        Country: <a href="{{ url('country/'.$item->country->slug) }}" title="{{ $item->country->name }} Movies">{{ $item->country->name }}</a>
                                    </div>
                                    <div class="block">Actor:
                                        @if(count($item->actors)>0)
                                            <?php $i=0; ?>
                                            @foreach($item->actors as $element)
                                                <a href="{{ url('actors/'.$element->slug) }}" title="{{ $element->name }}">{{ $element->name }}</a>{{ ($i==count($item->actors)-1)?'':',' }}
                                                <?php $i++; ?>
                                            @endforeach
                                        @endif
                                    </div>
                                    <?php
                                    $favorite = 0;
                                    if(count($item->favorite)>0 && Auth::user()){
                                        foreach ($item->favorite as $element){
                                            if($element->user_id == Auth::user()->id){
                                                $favorite = 1;
                                                break;
                                            }
                                        }
                                    }
                                    ?>
                                    <div class="jtip-bottom">
                                        <a href="{{ url('watch/'.$item->slug.'-'.$item->id) }}" class="btn btn-block btn-success"><i class="fa fa-play-circle mr10"></i>Watch movie</a>
                                        <button onclick="favorite_add({{ $item->id }},this,0,{{ Auth::user()?1:0 }})" class="btn btn-block btn-default mt10 btn-favorite-{{ $item->id }} add-favorite" data-favorite="{{ $favorite }}" data-url="{{ url('/') }}">
                                            @if($favorite)
                                                <i class="fa fa-remove mr10"></i>Remove Favorite
                                            @else
                                                <i class="fa fa-heart mr10"></i>Add to favorite
                                            @endif
                                        </button>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        @else
                            <div style="font-size: 18px;border: 1px solid #dfdfdf; background-color: #fff;text-align: center;margin-top: 10px;line-height: 50px;color:#333">Not found.</div>
                        @endif
                        <div class="clearfix"></div>
                    </div>
                    <br><br>
                </div>
            </div>
            @if($page_list_bottom_page && $page_list_bottom_page->content)
                <div class="banner-content">
                    {!! stripslashes($page_list_bottom_page->content) !!}
                </div>
            @endif
        </div>
    </div>
    <!--/main -->
@stop
