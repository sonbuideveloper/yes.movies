@extends('frontend.theme_1.master')
@section('content')
    <style>
        .mvi-content h3{
            color: #d4272b;
        }
        .mvic-desc img{
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 100%;
            height:auto;
        }
        .mvic-desc .mvic-siderbar{
            width: 28%;
            float: left;
        }

        .tn-news.tn-movies{
            list-style: none;
            padding: 0;
            margin: 0;
        }
        .blockContInfo {
            width: 70%;
            display: inline-block;
            padding: 10px;
            vertical-align: middle;
        }
        .blockContInfo a{
            color: #0b0b0b;
        }
        li{
            margin-bottom: 10px;
            margin-top: 10px;
            display: inline-block;
        }
    </style>
    <div id="main">
        <div class="container">
            <div class="main-content main-detail">
                <div id="mv-info">
                    <div class="mvi-content">
                        <div class="mvic-desc">
                            <h3>{!! $result->name !!}</h3>
                            <p><i class="fa fa-clock-o" aria-hidden="true"></i> {{ gmdate('d/m/Y', strtotime($result->created_at)) }}</p>
                            <br>
                            <div class="mvic-info">
                                {!! stripslashes ($result->content) !!}
                            </div>
                        </div>
                        <div class="mvic-siderbar">
                            <h3>You May Also Like</h3>
                            <br>
                            <ul class="tn-news tn-movies">
                            @if(count($article)>0)
                                @foreach($article as $item)
                                    <li>
                                        <a class="imgThumb" href="{{ url('coming-soon/'.$item->slug) }}">
                                            <img src="{{ url(Storage::url($item->images)) }}" alt="" width="65" height="65">
                                        </a>
                                        <div class="blockContInfo">
                                            <a href="{{ url('coming-soon/'.$item->slug) }}">{{ $item->name }}</a>
                                        </div>
                                    </li>
                                @endforeach
                            @endif
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
