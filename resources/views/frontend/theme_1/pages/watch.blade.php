@extends('frontend.theme_1.master')
@section('content')
<style>
    .player_iframe {
        position: relative!important;
        padding-bottom: 56.25%!important; /* 16:9 */
        height: 0!important;
    }

    .player_iframe iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    .player_iframe{
        display: none;
    }

    .jwplayer.jw-flag-aspect-mode{
        height: 100%!important;
    }

    .jconfirm-content, .jconfirm-title{
        color: #333333;
    }
</style>
    <!-- main -->
    <div id="main">
        <div class="container">
            <div class="pad"></div>
            @if($watching_top_player && $watching_top_player->content)
                <div class="banner-content">
                    {!! stripslashes($watching_top_player->content) !!}
                </div>
            @endif
            <div class="main-content main-detail">
                <div id="bread">
                    @if($film->movie_series)
                        <ol class="breadcrumb">
                            <li><a href="{{ url('/') }}" title="Home">Home</a></li>
                            <li><a href="{{ url('tv-series') }}" title="TV series">TV series</a></li>
                            <li><a href="{{ url('watch/'.$film->slug.'-'.$film->id) }}" title="{{ $film->name }}">{{ $film->name }}</a></li>
                            <li class="active">
                                <a id="top-url-episode" href="{{ url('watch/'.$film->slug.'-Episode-'.$p.'-'.$film->id.'?p='.$p) }}" title="{{ $film->name }} Episode {{ $p }}">Episode {{ $p }}</a>
                            </li>
                        </ol>
                    @else
                        <ol class="breadcrumb">
                            <li><a href="{{ url('/') }}" title="Home">Home</a></li>
                            <li><a href="{{ url('movies') }}" title="Movies">Movies</a></li>
                            <li class="active">
                                <a href="{{ url('watch/'.$film->slug.'-'.$film->id) }}" title="{{ $film->name }}">{{ $film->name }}</a>
                            </li>
                        </ol>
                    @endif
                </div>

                <div id="mv-info">
                    <div class="content_player">
                        <div class="content_media">
                            <div class="content_swf">
                                <div class="player">
                                    <div class="mediaplayer" id="player"></div>
                                </div>
                                <div class="player_iframe">
                                    <iframe id="iframe-link" src="" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                            <?php
                            $favorite = 0;
                            if(count($film->favorite)>0 && Auth::user()){
                                foreach ($film->favorite as $element){
                                    if($element->user_id == Auth::user()->id){
                                        $favorite = 1;
                                        break;
                                    }
                                }
                            }
                            ?>
                            <div id="bar-player">
                                <a class="btn bp-btn-light"><i class="fa fa-lightbulb-o"></i><span></span></a>
                                <a class="btn bp-btn-like btn-favorite-{{ $film->id }}" onclick="favorite_add({{ $film->id }},this,1,{{ Auth::user()?1:0 }})"  data-favorite="{{ $favorite }}" data-url="{{ url('/') }}">
                                    @if($favorite)
                                        <i class="fa fa-remove mr10"></i>Remove Favorite
                                    @else
                                        <i class="fa fa-heart mr10"></i>Add to favorite
                                    @endif
                                </a>
                                <a class="btn bp-btn-report-popup" data-toggle="modal" data-target="#pop-report" style="color: #fff000; display: none"><i class="fa fa-exclamation-triangle"></i><span>Report</span></a>
                                <a class="btn bp-btn-report-button" data-url="{{ url('/') }}" data-film="{{ $film->id }}" data-link="" data-name="{{ $film->name }}" data-user="{{ Auth::user()?1:0 }}" style="color: #fff000;"><i class="fa fa-exclamation-triangle"></i><span>Report</span></a>
                                <a class="btn btn-report-modal" data-toggle="modal" data-target="#pop-report" style="color: #fff000; display: none;"><i class="fa fa-exclamation-triangle"></i><span>Report</span></a>
                                <a class="btn btn-download" data-url="{{ url('/') }}" data-film="{{ $film->id }}" data-link=""><i class="fa fa-download"></i>Download</a>
                                <a class="btn btn-upload" data-toggle="modal" data-target="#load-subtitle"><i class="fa fa-upload"></i>Load Subtitle</a>
                                <span class="bp-view"><i class="fa fa-eye mr10"></i><span>{{ $film->view }}</span></span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="server_plugins"><a class="btn-server">Playing on <span class="playing-on"></span></a>
                            <div id="servers-list" data-id="1">
                                <ul id="ip_server">
                                </ul>
                            </div>
                        </div>
                    </div>
                    @if($film->movie_series==1)
                        <div id="list-eps">
                            <div class="le-server">
                                <div class="les-title"><i class="fa fa-server mr5"></i><strong>Episode</strong></div>
                                <div id="ip_episode" class="les-content">
                                    @if(count($film->film_detail)>0)
                                        <?php $i=1; ?>
                                        @foreach($film->film_detail as $element)
                                            <a class="episode_{{ $i }} btn-eps first-ep {{ $i==$p?'active':'' }} film-eposide-item" data-id="{{ $film->id }}" data-name="{{ $i }}" data-url="{{ url('watch/'.$film->slug.'-Episode-'.$i.'-'.$film->id) }}" data-server="" title="Watch Episode {{ $i }}">{{ $i }}</a>
                                        <?php $i++; ?>
                                        @endforeach
                                    @endif
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <input type="hidden" id="episode_index" value="{{ $p }}">
                    @endif
                    @if($watching_under_player && $watching_under_player->content)
                        <div class="banner-content">
                            {!! stripslashes($watching_under_player->content) !!}
                        </div>
                    @endif
                    <input type="hidden" id="movie_series" value="{{ $film->movie_series }}">
                    <div class="mvi-content">
                        <div class="mvic-btn">
                            @if($watching_right_info && $watching_right_info->content)
                                <div class="banner-content">
                                    {!! stripslashes($watching_right_info->content) !!}
                                </div>
                            @endif
                        </div>
                        <a href="{{ url('watch/'.$film->slug.'-'.$film->id) }}" title="{{ $film->name }}">
                            <div style="background-image: url(@if($film->images) {{ url(Storage::url($film->images)) }} @elseif($film->images_link) {{ url($film->images_link) }} @endif);"
                                 title="{{ $film->name }}" class="thumb mvic-thumb">
                            </div>
                        </a>
                        <?php
                            $rate = 0;
                            if(count($film->rate)>0 && Auth::user()){
                                foreach ($film->rate as $element){
                                    if($element->user_id == Auth::user()->id){
                                        $rate = $element->rate;
                                        break;
                                    }
                                }
                            }
                        ?>
                        <div class="mvic-desc">
                            <div id="stars-green" data-rating="{{ $rate }}" data-film="{{ $film->id }}" data-user="{{ Auth::user()?1:0 }}"></div>
                            <h3>
                                <a href="{{ url('watch/'.$film->slug.'-'.$film->id) }}" title="{{ $film->name }}">{{ $film->name }}</a>
                            </h3>
                            <div class="block-social">
                                <div class="fb-like" data-href="{{ url('watch/'.$film->slug.'-'.$film->id.($p?'?p='.$p:'')) }}" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
                                <div class="fb-send" data-href="{{ url('watch/'.$film->slug.'-'.$film->id.($p?'?p='.$p:'')) }}"></div>
                            </div>
                            <div class="desc">{!! stripslashes($film->content) !!}</div>
                            <div class="mvic-info">
                                <div class="mvici-left">
                                    <p><strong>Genres: </strong>
                                        @if(count($film->genres)>0)
                                            <?php $i=0; ?>
                                            @foreach($film->genres as $element)
                                                <a href="{{ url('genres/'.$element->slug) }}" title="{{ $element->name }}">{{ $element->name }}</a>{{ ($i==count($film->genres)-1)?'':',' }}
                                                <?php $i++; ?>
                                            @endforeach
                                        @endif
                                    </p>
                                    <p><strong>Actor: </strong>
                                        @if(count($film->actors)>0)
                                            @if(count($film->actors)>30)
                                                <div class="actor_div_sort">
                                                    <?php $i=0; ?>
                                                    @foreach($film->actors as $element)
                                                        <a href="{{ url('actors/'.$element->slug) }}" title="{{ $element->name }}">{{ $element->name }}</a>{{ ($i==count($film->actors)-1)?'':',' }}
                                                        <?php
                                                            $i++;
                                                            if($i==30) break;
                                                        ?>
                                                    @endforeach
                                                        ...
                                                    <br>
                                                    <a href="javascript:void(0)" class="load-more-button pull-right">Load More</a>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="actor_div_large" style="display: none;">
                                                    <?php $i=0; ?>
                                                    @foreach($film->actors as $element)
                                                        <a href="{{ url('actors/'.$element->slug) }}" title="{{ $element->name }}">{{ $element->name }}</a>{{ ($i==count($film->actors)-1)?'':',' }}
                                                        <?php $i++; ?>
                                                    @endforeach
                                                </div>
                                            @else
                                                <div class="actor_div_large">
                                                    <?php $i=0; ?>
                                                    @foreach($film->actors as $element)
                                                        <a href="{{ url('actors/'.$element->slug) }}" title="{{ $element->name }}">{{ $element->name }}</a>{{ ($i==count($film->actors)-1)?'':',' }}
                                                        <?php $i++; ?>
                                                    @endforeach
                                                </div>
                                            @endif
                                        @endif
                                    </p>
                                    <p><strong>Director: </strong>
                                        @if(count($film->directors)>0)
                                            <?php $i=0; ?>
                                            @foreach($film->directors as $element)
                                                <a href="{{ url('directors/'.$element->slug) }}" title="{{ $element->name }}">{{ $element->name }}</a>{{ ($i==count($film->directors)-1)?'':',' }}
                                                <?php $i++; ?>
                                            @endforeach
                                        @endif
                                    </p>
                                    <p><strong>Producer: </strong>
                                        @if(isset($film->producer))
                                            <a href="{{ url('producers/'.$film->producer->slug) }}" title="{{ $film->producer->name }}">{{ $film->producer->name }}</a>
                                        @endif
                                    </p>
                                    <p><strong>Country: </strong>
                                        <a href="{{ url('country/'.$film->country->slug) }}" title="{{ $film->country->name }} Movies">{{ $film->country->name }}</a>
                                    </p>
                                </div>
                                <div class="mvici-right">
                                    <p><strong>Duration:</strong> {{ $film->duration }} min</p>
                                    <p><strong>Release:</strong> {{ $film->release }}</p>
                                    <p><img src="{{ url('frontend/images/imdb.png') }}"/><span style="vertical-align: middle;font-size:18px"> {{ $film->IMDB }}</span></p>
                                </div>
                                <div class="clearfix"></div>
                                @if($film->movie_series===1)
                                    <div class="fb-comments" data-href="{{ url('watch/'.$film->slug.'-'.$film->id.'-'.$p) }}" data-width="100%" data-numposts="10"></div>
                                @else
                                    <div class="fb-comments" data-href="{{ url('watch/'.$film->slug.'-'.$film->id) }}" data-width="100%" data-numposts="10"></div>
                                @endif
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div id="mv-keywords">
                    <strong class="mr10">Keywords:</strong>
                    <?php
                        if($film->keywords){
                            $keywords = explode(',', $film->keywords);
                            if(count($keywords)>0){
                                foreach ($keywords as $element){
                                    echo "<a href=\"".url('tag/'.trim($element))."\" title=\"".trim($element)."\"><h5>".trim($element)."</h5></a>";
                                }
                            }
                        }
                    ?>
                </div>
                @if($watching_under_info && $watching_under_info->content)
                    <div class="banner-content">
                        {!! stripslashes($watching_under_info->content) !!}
                    </div>
                @endif
                <div class="movies-list-wrap mlw-related">
                    <div class="ml-title ml-title-page"><span>You May Also Like</span></div>
                    <div id="movies-related" class="movies-list movies-list-full">
                        @foreach($film_random as $item)
                        <div class="ml-item">
                            <a href="{{ url('watch/'.$item->slug.'-'.$item->id) }}" data-id="{{ $item->id }}" data-url="{{ url('/') }}" data-jtip="#f-movies-0-{{ $item->id }}" class="ml-mask jt film-detail-short" title="{{ $item->name }}">
                                @if(!$item->movie_series)
                                    <span class="mli-quality" style="text-transform: uppercase">{{ $item->quality }}</span>
                                @endif
                                <img data-original="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif"
                                     src="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif"
                                     class="lazy thumb mli-thumb" alt="{{ $item->name }}">
                                <span class="mli-info"><h2>{{ $item->name }}</h2></span>
                                @if($item->movie_series)
                                    <span class="mli-eps">Eps<i>{{ count($item->film_detail) }}</i></span>
                                @endif
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <style>
                #mediaplayer_caption span {
                    border-radius: 5px;
                }
            </style>
        </div>
    </div>

<input type="hidden" value="{{ $subtitle_user }}" id="input_subtitle_user">
<input type="hidden" value="0" id="link_id">
<input type="hidden" class="token" name="_token" value="{{ csrf_token() }}">
<script>
    var movie_series = $('#movie_series').val();
    var subtitle_user = $('#input_subtitle_user').val();
    if(subtitle_user==0){
        var subtitle_status = 0;
    }else{
        var subtitle_status = 1;
    }
    if(movie_series == 1){
        var episode = $('#episode_index').val();
        request_meta_data({{$film->id}}, episode);
        request_video_jwplayer_detail({{$film->id}}, episode, {{ $s }}, subtitle_status);
    }else{
        request_video_jwplayer({{$film->id}},{{ $s }}, subtitle_status);
    }
</script>

@stop