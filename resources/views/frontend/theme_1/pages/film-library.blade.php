@extends('frontend.theme_1.master')
@section('content')

<div id="main" class="">
    <div class="container">
        <div class="pad"></div>
        @if($page_list_top_page && $page_list_top_page->content)
            <div class="banner-content">
                {!! stripslashes($page_list_top_page->content) !!}
            </div>
        @endif
        <div class="main-content main-category">
            <div class="movies-list-wrap mlw-category">
                <div class="ml-title ml-title-page"><span>{{ $title }}</span>
                    <div class="clearfix"></div>
                </div>
                <div class="ml-alphabet">
                    <div class="movies-letter">
                        <a class="btn btn-letter {{ $q=='0-9'?'active':'' }}" title="0-9" href="{{ url('library-film?q=0-9') }}">0-9</a>
                        <a class="btn btn-letter {{ $q=='A'?'active':'' }}" title="A" href="{{ url('library-film?q=A') }}">A</a>
                        <a class="btn btn-letter {{ $q=='B'?'active':'' }}" title="B" href="{{ url('library-film?q=B') }}">B</a>
                        <a class="btn btn-letter {{ $q=='C'?'active':'' }}" title="C" href="{{ url('library-film?q=C') }}">C</a>
                        <a class="btn btn-letter {{ $q=='D'?'active':'' }}" title="D" href="{{ url('library-film?q=D') }}">D</a>
                        <a class="btn btn-letter {{ $q=='E'?'active':'' }}" title="E" href="{{ url('library-film?q=E') }}">E</a>
                        <a class="btn btn-letter {{ $q=='F'?'active':'' }}" title="F" href="{{ url('library-film?q=F') }}">F</a>
                        <a class="btn btn-letter {{ $q=='G'?'active':'' }}" title="G" href="{{ url('library-film?q=G') }}">G</a>
                        <a class="btn btn-letter {{ $q=='H'?'active':'' }}" title="H" href="{{ url('library-film?q=H') }}">H</a>
                        <a class="btn btn-letter {{ $q=='I'?'active':'' }}" title="I" href="{{ url('library-film?q=I') }}">I</a>
                        <a class="btn btn-letter {{ $q=='J'?'active':'' }}" title="J" href="{{ url('library-film?q=J') }}">J</a>
                        <a class="btn btn-letter {{ $q=='K'?'active':'' }}" title="K" href="{{ url('library-film?q=K') }}">K</a>
                        <a class="btn btn-letter {{ $q=='L'?'active':'' }}" title="L" href="{{ url('library-film?q=L') }}">L</a>
                        <a class="btn btn-letter {{ $q=='M'?'active':'' }}" title="M" href="{{ url('library-film?q=M') }}">M</a>
                        <a class="btn btn-letter {{ $q=='N'?'active':'' }}" title="N" href="{{ url('library-film?q=N') }}">N</a>
                        <a class="btn btn-letter {{ $q=='O'?'active':'' }}" title="O" href="{{ url('library-film?q=O') }}">O</a>
                        <a class="btn btn-letter {{ $q=='P'?'active':'' }}" title="P" href="{{ url('library-film?q=P') }}">P</a>
                        <a class="btn btn-letter {{ $q=='Q'?'active':'' }}" title="Q" href="{{ url('library-film?q=Q') }}">Q</a>
                        <a class="btn btn-letter {{ $q=='R'?'active':'' }}" title="R" href="{{ url('library-film?q=R') }}">R</a>
                        <a class="btn btn-letter {{ $q=='S'?'active':'' }}" title="S" href="{{ url('library-film?q=S') }}">S</a>
                        <a class="btn btn-letter {{ $q=='T'?'active':'' }}" title="T" href="{{ url('library-film?q=T') }}">T</a>
                        <a class="btn btn-letter {{ $q=='U'?'active':'' }}" title="U" href="{{ url('library-film?q=U') }}">U</a>
                        <a class="btn btn-letter {{ $q=='V'?'active':'' }}" title="V" href="{{ url('library-film?q=V') }}">V</a>
                        <a class="btn btn-letter {{ $q=='W'?'active':'' }}" title="W" href="{{ url('library-film?q=W') }}">W</a>
                        <a class="btn btn-letter {{ $q=='X'?'active':'' }}" title="X" href="{{ url('library-film?q=X') }}">X</a>
                        <a class="btn btn-letter {{ $q=='Y'?'active':'' }}" title="Y" href="{{ url('library-film?q=Y') }}">Y</a>
                        <a class="btn btn-letter {{ $q=='Z'?'active':'' }}" title="Z" href="{{ url('library-film?q=Z') }}">Z</a>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="letter-movies-lits">
                    <table class="table table-striped">
                        <tbody>
                        <tr class="mlnew-head">
                            <td class="mlnh-1">#</td>
                            <td colspan="2" class="mlnh-letter">{{ count($results) }} results</td>
                            <td class="mlnh-3">Year</td>
                            <td class="mlnh-3">Status</td>
                            <td class="mlnh-5">Country</td>
                            <td class="mlnh-4">Genre</td>
                            <td class="mlnh-6">IMDb</td>
                        </tr>
                        @if(count($results)>0)
                            <?php $i=0; ?>
                            @foreach($results as $item)
                                <?php $i++; ?>
                                <tr class="mlnew">
                                    <td class="mlnh-1">{{ $i }}</td>
                                    <td class="mlnh-thumb">
                                        <a class="thumb" title="{{ $item->name }}" href="{{ url('watch/'.$item->slug.'-'.$item->id) }}">
                                            <img alt="{{ $item->name }}" title="{{ $item->name }}" src="@if($item->images){{ url(Storage::url($item->images)) }}@elseif($item->images_link){{ url($item->images_link) }}@endif">
                                        </a>
                                    </td>
                                    <td class="mlnh-2">
                                        <h2><a title="{{ $item->name }}" href="{{ url('watch/'.$item->slug.'-'.$item->id) }}">{{ $item->name }}</a></h2>
                                    </td>
                                    <td>{{ $item->release }}</td>
                                    <td class="mlnh-3">{{ strtoupper($item->quality) }}</td>
                                    <td class="mlnh-4"><a href="{{ url('country/'.$item->country->slug) }}" title="{{ $item->country->name }}">{{ $item->country->name }}</a></td>
                                    <td class="mlnh-5">
                                        @if(count($item->genres)>0)
                                            <?php $i=0; ?>
                                            @foreach($item->genres as $element)
                                                <a href="{{ url('genres/'.$element->slug) }}" title="{{ $element->name }}">{{ $element->name }}</a>{{ ($i==count($item->genres)-1)?'':',' }}
                                                <?php $i++; ?>
                                            @endforeach
                                        @endif
                                    </td>
                                    <td class="mlnh-6"><span class="label label-warning">{{ $item->IMDB?$item->IMDB:0 }}</span></td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                <br>
                @if($page_list_bottom_page && $page_list_bottom_page->content)
                    <div class="banner-content">
                        {!! stripslashes($page_list_bottom_page->content) !!}
                    </div>
                @endif
                <br>
                <div class="text-center">
                    {!! $results->appends(['q' => $q])->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@stop