
<div class="modal fade modal-cuz" id="pop-login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-close"></i>
                </button>
                <h4 class="modal-title" id="myModalLabel">MEMBER LOGIN AREA</h4>
            </div>
            <div class="modal-body">
                <p class="desc">Watch HD Movies Online For Free and Download the latest movies. For everybody,
                    everywhere, everydevice, and everything ;)</p>
                <form id="login-form" method="POST">
                    {{--<div class="csrf-token">--}}
                        {{--<input type="hidden" name="ipos_login" value="1"/>--}}
                    {{--</div>--}}
                    {{--<div class="block">--}}
                        {{--<input required type="text" class="form-control" name="username" id="username"--}}
                               {{--placeholder="Username">--}}
                    {{--</div>--}}
                    {{--<div class="block mt10">--}}
                        {{--<input required type="password" class="form-control" name="password" id="password"--}}
                               {{--placeholder="Password">--}}
                    {{--</div>--}}
                    {{--<div style="display: none;" id="error-message" class="alert alert-danger"></div>--}}
                    {{--<div class="block mt10 small">--}}
                        {{--<label><input type="checkbox" style="vertical-align: sub; margin-right: 3px;"> Remember--}}
                            {{--me</label>--}}
                        {{--<div class="pull-right">--}}
                            {{--<a id="open-forgot" data-dismiss="modal" data-target="#pop-forgot" data-toggle="modal"--}}
                               {{--title="Forgot password?">Forgot password?</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<button id="login-submit" type="submit" class="btn btn-block btn-success btn-approve mt10">--}}
                        {{--<i class="fa fa-user mr10"></i>Login--}}
                    {{--</button>--}}
                    <p id="login-loading" style="display: none; text-align: center;">
                        <img class="mt10" height="35px" src="{{ $http_url('frontend/theme_1/images/ajax-loading.gif') }}"></p>
                    <a href="{{ url('facebook/redirect') }}" class="btn btn-block btn-facebook mt10">
                        <i class="fa fa-facebook mr10"></i>
                        Login in with facebook
                    </a>
                    <a href="{{ url('google/redirect') }}" class="btn btn-block btn-google mt10">
                        <i class="fa fa-google mr10"></i>
                        Login in with google
                    </a>
                </form>
            </div>
            {{--<div class="modal-footer text-center"> Not a member ?--}}
                {{--<a id="open-register" data-dismiss="modal" data-target="#pop-register" data-toggle="modal" title="">Join--}}
                    {{--now!</a>--}}
            {{--</div>--}}
        </div>
    </div>
</div>
<div class="modal fade modal-cuz" id="pop-register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-close"></i>
                </button>
                <h4 class="modal-title" id="myModalLabel">You are welcome</h4>
            </div>
            <div class="modal-body">
                <p class="desc">When becoming members of the site, you could use the full range of functions and enjoy
                    the most exciting films.</p>
                <form id="register-form" method="POST">
                    <div class="csrf-token">
                        <input type="hidden" name="ipos_reg" value="1"/>
                    </div>
                    <div class="block">
                        <input required type="text" class="form-control" name="full_name" id="full_name"
                               placeholder="Full name">
                    </div>
                    <div class="block mt10">
                        <input required type="text" class="form-control" name="username" id="username"
                               placeholder="Username">
                    </div>
                    <div class="block mt10">
                        <input required type="password" class="form-control" name="password" id="password"
                               placeholder="Password">
                    </div>
                    <div class="block mt10">
                        <input required type="password" class="form-control" name="confirm_password"
                               id="confirm_password" placeholder="Confirm Password">
                    </div>
                    <div class="block mt10">
                        <input required type="email" class="form-control" name="email" id="email" placeholder="Email">
                    </div>
                    <div style="display: none;" id="error-reg-message" class="alert alert-danger"></div>
                    <button id="register-submit" type="submit" class="btn btn-block btn-success btn-approve mt20">
                        Register
                    </button>
                    <p id="register-loading" style="display: none; text-align: center;">
                        <img class="mt10" height="35px" src="{{ $http_url('frontend/theme_1/images/ajax-loading.gif') }}">
                    </p>
                </form>
            </div>
            <div class="modal-footer text-center">
                <a id="open-register" style="color: #888" data-dismiss="modal" data-target="#pop-login"
                   data-toggle="modal" title="">
                    <i class="fa fa-chevron-left mr10"></i>
                    Back to login
                </a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-cuz" id="pop-forgot" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-close"></i>
                </button>
                <h4 class="modal-title" id="myModalLabel">Forgot Password</h4>
            </div>
            <div class="modal-body">
                <p class="desc">We will send a new password to your email. Please fill your email to form below.</p>
                <form id="forgot-form">
                    <div class="block mt10">
                        <input type="email" class="form-control" name="email" id="email" placeholder="Your email"
                               required>
                    </div>
                    <div class="block mt10" style="position: relative;">
                        <input type="text" class="form-control" name="captcha_forgot" id="captcha_forgot"
                               placeholder="Enter code" required>
                        {{-- <img style="position: absolute; right: 5px; top: 5px;" class="captchaimg"
                             src="/captcha.php?n=captcha_forgot&t=1491775461"> --}}
                    </div>
                    <div style="display: none;" id="forgot-success-message" class="alert alert-success">
                    </div>
                    <div style="display: none;" id="forgot-error-message" class="alert alert-danger"></div>
                    <button type="submit" class="btn btn-block btn-success btn-approve mt20">Submit</button>
                </form>
            </div>
            <div class="modal-footer text-center">
                <a id="open-register" style="color: #888" data-dismiss="modal" data-target="#pop-login"
                   data-toggle="modal" title="">
                    <i class="fa fa-chevron-left mr10"></i> Back to login
                </a>
            </div>
        </div>
    </div>
</div>
@if($page == 'watch')
<div class="modal fade modal-cuz" id="pop-report" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-close"></i></button>
                <h4 class="modal-title" id="report-label"></h4>
            </div>
            <div class="modal-body">
                <p class="desc" id="report_text">Please enter the captcha</p>
                <form id="report-form" method="POST" action="{{ url('report-link') }}">
                    {!! csrf_field() !!}
                    <?php $publickey = env('RECAPTCHA_SITE_KEY'); ?>
                    {!! recaptcha_get_html($publickey) !!}
                    <input type="hidden" name="film_id" value="{{ !empty($film)?$film->id:0 }}">
                    <input type="hidden" name="link_id" id="link_id_input" value="">
                    <button type="submit" class="btn btn-block btn-success btn-approve mt20">Submit</button>
                </form>
            </div>
            <div class="modal-footer text-center"></div>
        </div>
    </div>
</div>
@endif
<div class="modal fade modal-cuz" id="load-subtitle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-close"></i></button>
                <h4 class="modal-title" id="report-label">Load subtitle</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="{{ url('load-subtitle') }}" method="post" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <input required type="file" class="form-control" name="file">
                    <input type="hidden" name="film_id" value="{{ !empty($film)?$film->id:0 }}">
                    <input type="hidden" name="episode" value="{{ !empty($p)?$p:0 }}">
                    <br>
                    <button type="submit" class="btn btn-success">Begin Upload</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-cuz" id="modal-download" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="top: 35%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-close"></i></button>
                <h4 class="modal-title">Download</h4>
            </div>
            <div class="modal-body" style="text-align: center">
            </div>
        </div>
    </div>
</div>