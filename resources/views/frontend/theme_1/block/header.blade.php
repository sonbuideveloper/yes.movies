<header>
    <div class="container">
        <div class="header-logo">
            <h1>
                <a title="{{ $setting->name }}" href="{{ $http_url('/') }}" id="logo" style="background-image: url({{ $http_url(Storage::url($setting->images)) }});">
                </a>
            </h1>
        </div>
        <div class="mobile-menu"><i class="fa fa-reorder"></i></div>
        <div class="mobile-search"><i class="fa fa-search"></i></div>
        <div id="menu">
            <ul class="top-menu">
                <li {{ $page=='home'?'class=active':'' }}><a href="{{ $http_url('/') }}" title="Home">Home</a></li>
                <li {{ $page=='genres'?'class=active':'' }}>
                    <a href="#" title="Genres">Genres</a>
                    <div class="sub-container" style="display: none">
                        <ul class="sub-menu">
                            @foreach($genres as $item)
                                <li><a title="{{ $item->name }}" href="{{ $http_url('genres/'.$item->slug) }}">{{ $item->name }}</a></li>
                            @endforeach
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </li>
                <li {{ $page=='country'?'class=active':'' }}>
                    <a href="#" title="Country">Country</a>
                    <div class="sub-container" style="display: none">
                        <ul class="sub-menu">
                            @foreach($country as $item)
                                <li><a title="{{ $item->name }}" href="{{ $http_url('country/'.$item->slug) }}">{{ $item->name }}</a></li>
                            @endforeach
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </li>
                <li {{ $page=='featured'?'class=active':'' }}><a href="{{ $http_url('featured') }}" title="Featured Movies">Featured</a></li>
                <li {{ $page=='type'?'class=active':'' }}>
                    <a href="#" title="Type">List Films</a>
                    <div class="sub-container" style="display: none">
                        <ul class="sub-menu">
                            <li><a href="{{ $http_url('movies') }}" title="Movies">Movies</a></li>
                            <li><a href="{{ $http_url('tv-series') }}" title="TV-Series">TV-Series</a></li>
                            @foreach($type as $item)
                                <li><a title="{{ $item->name }}" href="{{ $http_url('type/'.$item->slug) }}">{{ $item->name }}</a></li>
                            @endforeach
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </li>
                <li {{ $page=='playlist'?'class=active':'' }}><a href="{{ $http_url('playlist-film') }}" title="Playlist">Playlist</a></li>
                <li {{ $page=='top-imdb'?'class=active':'' }}><a href="{{ $http_url('top-imdb/all') }}" title="Top IMDb">Top IMDb</a></li>
                <li {{ $page=='library'?'class=active':'' }}><a href="{{ $http_url('library-film') }}" title="A-Z List">A-Z List</a></li>
                <li {{ $page=='ranking'?'class=active':'' }}><a href="{{ $http_url('ranking-film') }}" title="Ranking">Ranking</a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
        @if(!Auth::user())
        <div id="top-user">
            <div class="top-user-content guest">
                <a href="#" class="btn btn-success btn-login" title="Login" data-target="#pop-login" data-toggle="modal">LOGIN</a>
            </div>
        </div>
        @else
        <div id="top-user">
            <div class="top-user-content guest">
                <a href="{{ url('users/favorite') }}" class="btn btn-success" title="Profile">PROFILE</a>
            </div>
        </div>
        @endif
        <div id="search">
            <div class="search-content">
                <input data-url="{{ url('/') }}" name="keyword" type="text" class="form-control search-input" placeholder="Type to search..."/>
                <a onclick="searchMovie()" class="search-submit" href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a>
                <div class="search-suggest" style="display: none;"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</header>