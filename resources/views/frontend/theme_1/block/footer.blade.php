<footer>
    <div id="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-block-1">
                    <div class="footer-link">
                        <h3 class="footer-link-head">{{ $setting->name }}</h3>
                        <p><a title="Movies" href="{{ url('type/movies') }}">Movies</a></p>
                        <p><a title="Movies" href="{{ url('type/tv-series') }}">Tv-series</a></p>
                        <p><a title="Top IMDb" href="{{ url('top-imdb/all') }}">Top IMDb</a></p>
                    </div>
                    <div class="footer-link">
                        <h3 class="footer-link-head">Genres</h3>
                        @for($i=0; $i < count($genres); $i++)
                            <p><a title="{{ $genres[$i]->name }}" href="{{ $http_url('genres/'.$genres[$i]->slug) }}">{{ $genres[$i]->name }}</a></p>
                            @if($i==4) @break @endif
                        @endfor
                    </div>
                    <div class="footer-link">
                        <h3 class="footer-link-head">Country</h3>
                        @for($i=0; $i < count($country); $i++)
                            <p><a title="{{ $country[$i]->name }}" href="{{ $http_url('country/'.$country[$i]->slug) }}">{{ $country[$i]->name }}</a></p>
                            @if($i==4) @break @endif
                        @endfor
                    </div>
                    <div class="clearfix"></div>
                </div>
                {{--<div class="col-lg-4 col-block-2">--}}
                    {{--<div class="xc-subs-content">--}}
                        {{--<h3 class="footer-link-head">Subscribe</h3>--}}
                        {{--<p class="desc text-left">Subscribe to the {{ $setting->name }} mailing list to receive updates on movies, tv-series and news of top movies.</p>--}}
                        {{--<form id="subscribe-form" action="https://feedburner.google.com/fb/a/mailverify" method="post" target="popupwindow" onsubmit="window.open('https://feedburner.google.com/fb/a/mailverify?uri=', 'popupwindow', 'scrollbars=yes,width=550,height=520');return true">--}}
                            {{--<div class="ubc-input">--}}
                                {{--<input type="email" class="form-control" name="email" id="subscribe-email" required--}}
                                       {{--placeholder="Enter your email">--}}
                                {{--<span style="display: none;" id="success-subscribe" class="help-block">Thank you for subscribing!</span>--}}
                            {{--</div>--}}
                            {{--<input type="hidden" value="" name="uri">--}}
                            {{--<input type="hidden" name="loc" value="en_US">--}}
                            {{--<button id="subscribe-submit" type="submit" class="btn btn-block btn-success btn-approve">--}}
                                {{--Subscribe--}}
                            {{--</button>--}}
                            {{--<div class="clearfix"></div>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="col-lg-6 col-block-3">
                    <p id="footer-logo"><img border="0" src="{{ url(Storage::url($setting->images)) }}" width="170" class="mv-ft-logo"></p>
                    <p>Copyright &copy; {{ url('/') }}. All Rights Reserved</p>
                    <p>Contact:
                        <a href="mailto:{{ $setting->email }}">{{ $setting->email }}</a>
                    </p>
                    <p style="font-size: 11px; line-height: 14px; color: rgba(255,255,255,0.4)"></p>
                </div>
                <div id="divCheckbox" style="visibility: hidden">
                    <script id="_waucbf">
                        var _wau = _wau || [];
                        _wau.push(["classic", "123moviesfree", "cbf"]);
                        (function () {
                            var s = document.createElement("script");
                            s.async = true;
                            s.src = "//widgets.amung.us/classic.js";
                            document.getElementsByTagName("head")[0].appendChild(s);
                        })();
                    </script>
                    <div id="histats_counter"></div>
                    <noscript>
                        <a href="/" target="_blank">
                            <img src="//sstatic1.histats.com/0.gif?3663567&101" alt="" border="0"></a>
                    </noscript>
                </div>

                <div class="clearfix"></div>
                <div class="footer-tags">
                    {!! $footer?stripslashes($footer->content):'' !!}
                </div>
            </div>
        </div>
    </div>
</footer>
<div id="overlay"></div>