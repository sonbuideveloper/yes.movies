<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::group(['middleware' => 'admin', 'prefix' => 'admin', 'namespace' => 'Admin'], function () {

    Route::get('/', 'UsersController@getIndex');
    Route::get('dashboard', 'UsersController@getIndex');

    Route::group(['prefix' => 'language'], function () {
        Route::get('list', 'LanguageController@getList');
        Route::get('create', 'LanguageController@getCreate');
        Route::get('edit/{id}', 'LanguageController@getEdit');
        Route::get('delete/{id}', 'LanguageController@getDelete');
        Route::post('create', 'LanguageController@postCreate');
        Route::post('delete', 'LanguageController@postDelete');
    });

    Route::group(['prefix' => 'film'], function () {
        Route::get('list', 'FilmController@getIndex');
        Route::get('create', 'FilmController@getCreate');
        Route::get('edit/{id}', 'FilmController@getEdit');
        Route::get('delete/{id}', 'FilmController@getDelete');
        Route::get('check-link', 'FilmController@getCheckLink');
        Route::get('check-status/{type}/{id}', 'FilmController@getCheckStatus');
        Route::post('create', 'FilmController@postCreate');
        Route::post('delete', 'FilmController@postDelete');
        Route::post('delete-image', 'FilmController@postDeleteImage');
        Route::post('images', 'FilmController@postAvatar');
        Route::post('search-info', 'FilmController@postSearchInfo');
        Route::post('images-link', 'FilmController@postImagesLink');
        Route::get('delete-image-link/{id}', 'FilmController@getDeleteImageLink');
        Route::post('cover', 'FilmController@postCover');
        Route::post('delete-cover', 'FilmController@postDeleteCover');
        Route::post('cover-link', 'FilmController@postCoverLink');
        Route::get('delete-cover-link/{id}', 'FilmController@getDeleteCoverLink');
        Route::post('background', 'FilmController@postBackground');
        Route::post('delete-background', 'FilmController@postDeleteBackground');
        Route::post('background-link', 'FilmController@postBackgroundLink');
        Route::get('delete-background-link/{id}', 'FilmController@getDeleteBackgroundLink');
        Route::get('link-error', 'FilmController@getLinkError');
        Route::get('slider-home', 'FilmController@getSliderHome');
        Route::get('multi-link/{id}', 'FilmController@getMultiLink');
        Route::post('multi-link', 'FilmController@postMultiLink');
        Route::post('multi-link/get-server-film-data', 'FilmController@postServerFilmData');

        Route::post('ajax-get-country', 'FilmController@postAjaxCountry');
        Route::post('ajax-get-genres', 'FilmController@postAjaxGenres');
        Route::post('ajax-get-producer', 'FilmController@postAjaxProducer');

        Route::get('upload', 'FilmController@getUpload');
        Route::post('upload', 'FilmController@postUpload');
    });

    Route::group(['prefix' => 'film-series'], function () {
        Route::get('list', 'FilmDetailController@getIndex');
        Route::get('create', 'FilmDetailController@getCreate');
        Route::get('edit/{id}', 'FilmDetailController@getEdit');
        Route::get('delete/{id}', 'FilmDetailController@getDelete');
        Route::get('check-link', 'FilmDetailController@getCheckLink');
        Route::get('check-status/{type}/{id}', 'FilmDetailController@getCheckStatus');
        Route::post('create', 'FilmDetailController@postCreate');
        Route::post('delete', 'FilmDetailController@postDelete');
        Route::post('delete-image', 'FilmDetailController@postDeleteImage');
        Route::post('images', 'FilmDetailController@postAvatar');
        Route::post('search-info', 'FilmDetailController@postSearchInfo');
        Route::post('images-link', 'FilmDetailController@postImagesLink');
        Route::get('delete-image-link/{id}', 'FilmDetailController@getDeleteImageLink');
        Route::post('cover', 'FilmDetailController@postCover');
        Route::post('delete-cover', 'FilmDetailController@postDeleteCover');
        Route::post('cover-link', 'FilmDetailController@postCoverLink');
        Route::get('delete-cover-link/{id}', 'FilmDetailController@getDeleteCoverLink');
        Route::post('background', 'FilmDetailController@postBackground');
        Route::post('delete-background', 'FilmDetailController@postDeleteBackground');
        Route::post('background-link', 'FilmDetailController@postBackgroundLink');
        Route::get('delete-background-link/{id}', 'FilmDetailController@getDeleteBackgroundLink');
        Route::get('add-episode/{id}', 'FilmDetailController@getCreateEposide');
        Route::post('add-episode/{id}', 'FilmDetailController@postCreateEposide');
        Route::get('list-episode/{id}', 'FilmDetailController@getListEposide');
        Route::get('multi-link/{id}', 'FilmDetailController@getMultiLink');
        Route::post('multi-link', 'FilmDetailController@postMultiLink');
        Route::post('list-episode/delete', 'FilmDetailController@postListEpisodeDelete');
        Route::post('multi-link/get-server-film-data', 'FilmDetailController@postServerFilmData');
        Route::get('link-error', 'FilmDetailController@getLinkError');
    });

    Route::group(['prefix' => 'link-film'], function () {
        Route::post('create', 'LinkFilmController@postCreate');
        Route::post('ajax-create', 'LinkFilmController@postAjaxCreate');
        Route::post('ajax-edit', 'LinkFilmController@postAjaxEdit');
        Route::get('delete/{id}', 'LinkFilmController@getDelete');
        Route::post('find-link-film', 'LinkFilmController@postFindLinkFilm');
        Route::post('link-film-error/edit', 'LinkFilmController@postEditFilmError');
        Route::get('check-status/{id}', 'LinkFilmController@getCheckStatus');
        Route::post('delete', 'LinkFilmController@postDelete');
        Route::post('active', 'LinkFilmController@postActive');
        Route::post('ajax-delete', 'LinkFilmController@postAjaxDelete');
        Route::post('ajax-active', 'LinkFilmController@postAjaxActive');
    });

    Route::group(['prefix' => 'link-film-detail'], function () {
        Route::post('create', 'LinkFilmDetailController@postCreate');
        Route::get('delete/{id}', 'LinkFilmDetailController@getDelete');
        Route::post('find-link-film', 'LinkFilmDetailController@postFindLinkFilm');
        Route::post('link-film-error/edit', 'LinkFilmDetailController@postEditFilmError');
        Route::get('check-status/{id}', 'LinkFilmDetailController@getCheckStatus');
        Route::post('delete', 'LinkFilmDetailController@postDelete');
        Route::post('active', 'LinkFilmDetailController@postActive');
    });

    Route::group(['prefix' => 'subtitle'], function () {
        Route::post('create', 'SubTitleController@postCreate');
        Route::get('delete/{id}', 'SubTitleController@getDelete');
        Route::get('multi/{slug}', 'SubTitleController@getMulti');
        Route::post('multi/{slug}', 'SubTitleController@postMulti');
    });

    Route::group(['prefix' => 'subtitle-detail'], function () {
        Route::post('create', 'SubTitleDetailController@postCreate');
        Route::get('delete/{id}', 'SubTitleDetailController@getDelete');
        Route::get('multi/{slug}', 'SubTitleDetailController@getMulti');
        Route::post('multi/{slug}', 'SubTitleDetailController@postMulti');
    });

    Route::group(['prefix' => 'user'], function () {
        Route::get('logout', 'UsersController@getLogout');
        Route::get('list', 'UsersController@getListUser');
        Route::get('change-group/{id}', 'UsersController@getChangeGroup');
        Route::get('create', 'UsersController@getCreate');
        Route::get('edit/{id}', 'UsersController@getCreate');
        Route::get('delete/{id}', 'UsersController@getDelete');
        Route::get('export-pdf', 'UsersController@exportPDF');
        Route::get('export-xls', 'UsersController@exportXLS');
        Route::post('create', 'UsersController@postCreate');
        Route::post('edit-profile', 'UsersController@postEditProfile');
        Route::post('login', 'UsersController@postLogin');
        Route::post('delete', 'UsersController@postDelete');
        Route::post('images', 'UsersController@postAvatar');
        Route::post('test-image', 'UsersController@postTestImage');
        Route::post('delete-image', 'UsersController@postDeleteImage');
        Route::get('check-active/{id}', 'UsersController@getCheckActive');
    });

    Route::group(['prefix' => 'setting'], function () {
        Route::get('edit', 'SettingController@getIndex');
        Route::post('avatar', 'SettingController@postAvatar');
        Route::post('meta-data', 'SettingController@postMetaData');
        Route::post('content', 'SettingController@postContent');
        Route::post('delete-image', 'SettingController@postDeleteImage');
        Route::post('meta-images', 'SettingController@postMetaImages');
        Route::post('delete-meta-image', 'SettingController@postDeleteMetaImage');
        Route::get('clear-cache', 'SettingController@getClearCache');
    });

    Route::group(['prefix' => 'menu'], function () {
        Route::get('/', 'MenuController@getIndex');
        Route::get('create', 'MenuController@getCreate');
        Route::get('edit/{id}', 'MenuController@getEdit');
        Route::get('update-order/{id}', 'MenuController@getUpdateOrder');
        Route::get('delete/{id}', 'MenuController@getDelete');
        Route::get('check-link', 'MenuController@getCheckLink');
        Route::get('check-status/{id}', 'MenuController@getCheckStatus');
        Route::post('create', 'MenuController@postCreate');
        Route::post('meta-data', 'MenuController@postMetaData');
        Route::post('delete', 'MenuController@postDelete');
    });

    Route::group(['prefix' => 'server-film'], function () {
        Route::get('list', 'ServerPlayFilmController@getIndex');
        Route::get('create', 'ServerPlayFilmController@getCreate');
        Route::get('edit/{id}', 'ServerPlayFilmController@getEdit');
        Route::get('delete/{id}', 'ServerPlayFilmController@getDelete');
        Route::get('check-link', 'ServerPlayFilmController@getCheckLink');
        Route::get('check-status/{id}', 'ServerPlayFilmController@getCheckStatus');
        Route::post('create', 'ServerPlayFilmController@postCreate');
        Route::post('meta-data', 'ServerPlayFilmController@postMetaData');
        Route::post('delete', 'ServerPlayFilmController@postDelete');
    });

    Route::group(['prefix' => 'film-country'], function () {
        Route::get('list', 'CountryFilmController@getIndex');
        Route::get('create', 'CountryFilmController@getCreate');
        Route::get('edit/{id}', 'CountryFilmController@getEdit');
        Route::get('delete/{id}', 'CountryFilmController@getDelete');
        Route::get('check-link', 'CountryFilmController@getCheckLink');
        Route::get('check-status/{id}', 'CountryFilmController@getCheckStatus');
        Route::post('create', 'CountryFilmController@postCreate');
        Route::post('meta-data', 'CountryFilmController@postMetaData');
        Route::post('delete', 'CountryFilmController@postDelete');
    });

    Route::group(['prefix' => 'genre'], function () {
        Route::get('list', 'CategoryFilmController@getIndex');
        Route::get('create', 'CategoryFilmController@getCreate');
        Route::get('edit/{id}', 'CategoryFilmController@getEdit');
        Route::get('delete/{id}', 'CategoryFilmController@getDelete');
        Route::get('check-link', 'CategoryFilmController@getCheckLink');
        Route::get('check-status/{id}', 'CategoryFilmController@getCheckStatus');
        Route::post('create', 'CategoryFilmController@postCreate');
        Route::post('meta-data', 'CategoryFilmController@postMetaData');
        Route::post('delete', 'CategoryFilmController@postDelete');
    });

    Route::group(['prefix' => 'type'], function () {
        Route::get('list', 'TypeFilmController@getIndex');
        Route::get('create', 'TypeFilmController@getCreate');
        Route::get('edit/{id}', 'TypeFilmController@getEdit');
        Route::get('delete/{id}', 'TypeFilmController@getDelete');
        Route::get('check-link', 'TypeFilmController@getCheckLink');
        Route::get('check-status/{id}', 'TypeFilmController@getCheckStatus');
        Route::post('create', 'TypeFilmController@postCreate');
        Route::post('meta-data', 'TypeFilmController@postMetaData');
        Route::post('delete', 'TypeFilmController@postDelete');
    });

    Route::group(['prefix' => 'playlist'], function () {
        Route::get('list', 'PlayListController@getIndex');
        Route::get('create', 'PlayListController@getCreate');
        Route::get('edit/{id}', 'PlayListController@getEdit');
        Route::get('delete/{id}', 'PlayListController@getDelete');
        Route::get('check-link', 'PlayListController@getCheckLink');
        Route::get('check-status/{id}', 'PlayListController@getCheckStatus');
        Route::post('create', 'PlayListController@postCreate');
        Route::post('delete', 'PlayListController@postDelete');
        Route::post('delete-image', 'PlayListController@postDeleteImage');
        Route::post('images', 'PlayListController@postAvatar');
        Route::post('search-info', 'PlayListController@postSearchInfo');
        Route::post('images-link', 'PlayListController@postImagesLink');
        Route::get('delete-image-link/{id}', 'PlayListController@getDeleteImageLink');
        Route::post('cover', 'PlayListController@postCover');
        Route::post('delete-cover', 'PlayListController@postDeleteCover');
        Route::post('cover-link', 'PlayListController@postCoverLink');
        Route::get('delete-cover-link/{id}', 'PlayListController@getDeleteCoverLink');
        Route::post('background', 'PlayListController@postBackground');
        Route::post('delete-background', 'PlayListController@postDeleteBackground');
        Route::post('background-link', 'PlayListController@postBackgroundLink');
        Route::get('delete-background-link/{id}', 'PlayListController@getDeleteBackgroundLink');
    });

    Route::group(['prefix' => 'chanel'], function () {
        Route::get('list', 'ChanelController@getIndex');
        Route::get('create', 'ChanelController@getCreate');
        Route::get('edit/{id}', 'ChanelController@getEdit');
        Route::get('delete/{id}', 'ChanelController@getDelete');
        Route::get('check-link', 'ChanelController@getCheckLink');
        Route::get('check-status/{id}', 'ChanelController@getCheckStatus');
        Route::post('create', 'ChanelController@postCreate');
        Route::post('delete', 'ChanelController@postDelete');
        Route::post('delete-image', 'ChanelController@postDeleteImage');
        Route::post('images', 'ChanelController@postAvatar');
        Route::post('search-info', 'ChanelController@postSearchInfo');
        Route::post('images-link', 'ChanelController@postImagesLink');
        Route::get('delete-image-link/{id}', 'ChanelController@getDeleteImageLink');
        Route::post('cover', 'ChanelController@postCover');
        Route::post('delete-cover', 'ChanelController@postDeleteCover');
        Route::post('cover-link', 'ChanelController@postCoverLink');
        Route::get('delete-cover-link/{id}', 'ChanelController@getDeleteCoverLink');
        Route::post('background', 'ChanelController@postBackground');
        Route::post('delete-background', 'ChanelController@postDeleteBackground');
        Route::post('background-link', 'ChanelController@postBackgroundLink');
        Route::get('delete-background-link/{id}', 'ChanelController@getDeleteBackgroundLink');
    });

    Route::group(['prefix' => 'producer'], function () {
        Route::get('list', 'ProducerController@getIndex');
        Route::get('create', 'ProducerController@getCreate');
        Route::get('edit/{id}', 'ProducerController@getEdit');
        Route::get('delete/{id}', 'ProducerController@getDelete');
        Route::get('check-link', 'ProducerController@getCheckLink');
        Route::get('check-status/{id}', 'ProducerController@getCheckStatus');
        Route::post('create', 'ProducerController@postCreate');
        Route::post('delete', 'ProducerController@postDelete');
        Route::post('delete-image', 'ProducerController@postDeleteImage');
        Route::post('images', 'ProducerController@postAvatar');
        Route::post('search-info', 'ProducerController@postSearchInfo');
        Route::post('images-link', 'ProducerController@postImagesLink');
        Route::get('delete-image-link/{id}', 'ProducerController@getDeleteImageLink');
        Route::post('cover', 'ProducerController@postCover');
        Route::post('delete-cover', 'ProducerController@postDeleteCover');
        Route::post('cover-link', 'ProducerController@postCoverLink');
        Route::get('delete-cover-link/{id}', 'ProducerController@getDeleteCoverLink');
        Route::post('background', 'ProducerController@postBackground');
        Route::post('delete-background', 'ProducerController@postDeleteBackground');
        Route::post('background-link', 'ProducerController@postBackgroundLink');
        Route::get('delete-background-link/{id}', 'ProducerController@getDeleteBackgroundLink');
    });

    Route::group(['prefix' => 'article'], function () {
        Route::get('list', 'ArticleController@getIndex');
        Route::get('create', 'ArticleController@getCreate');
        Route::get('edit/{id}', 'ArticleController@getEdit');
        Route::get('delete/{id}', 'ArticleController@getDelete');
        Route::get('check-link', 'ArticleController@getCheckLink');
        Route::get('check-status/{type}/{id}', 'ArticleController@getCheckStatus');
        Route::get('update-order/{id}', 'ArticleController@getUpdateOrder');
        Route::post('create', 'ArticleController@postCreate');
        Route::post('meta-data', 'ArticleController@postMetaData');
        Route::post('delete', 'ArticleController@postDelete');
        Route::post('delete-image', 'ArticleController@postDeleteImage');
        Route::post('images', 'ArticleController@postAvatar');
        Route::post('multiple-images', 'ArticleController@postMultipleImages');
    });

    Route::group(['prefix' => 'ads'], function () {
        Route::get('list', 'AdsController@getIndex');
        Route::get('create', 'AdsController@getCreate');
        Route::post('create', 'AdsController@postCreate');
        Route::get('edit/{id}', 'AdsController@getEdit');
        Route::get('delete/{id}', 'AdsController@getDelete');
        Route::get('check-link', 'AdsController@getCheckLink');
        Route::get('check-status/{type}/{id}', 'AdsController@getCheckStatus');
        Route::get('update-order/{id}', 'AdsController@getUpdateOrder');
        Route::post('delete', 'AdsController@postDelete');
    });

    Route::get('footer/edit', 'AdsController@getFooter');
    Route::post('footer/edit', 'AdsController@postFooter');
    Route::get('login', 'UsersController@getLogin');
    Route::post('sign-in', 'UsersController@postLogin');
    Route::post('forgot-password', 'UsersController@postForgotPassword');
    Route::post('register', 'UsersController@postRegister');
});

Route::get('facebook/redirect', 'Admin\SocialController@redirectToProvider');
Route::get('facebook/callback', 'Admin\SocialController@handleProviderCallback');
Route::get('google/redirect', 'Admin\SocialController@redirectToProviderGoogle');
Route::get('google/callback', 'Admin\SocialController@handleProviderCallbackGoogle');

Route::group(['prefix' => 'users'], function () {
    Route::get('active', 'UsersController@getActive');
    Route::get('login', 'UsersController@getLogin');
    Route::get('logout', 'UsersController@getLogout');
    Route::post('login', 'UsersController@postLogin');
    Route::get('profile', 'UsersController@getProfile');
    Route::get('favorite', 'UsersController@getFavorite');
});

//**************Demon Dragon******************//
Route::get('/', 'HomeController@index');
Route::get('site', 'HomeController@getSite');
Route::get('genres/{slug}', 'HomeController@getGenres');
Route::get('country/{slug}', 'HomeController@getCountry');
Route::get('featured', 'HomeController@getFeatured');
Route::get('movies', 'HomeController@getMovies');
Route::get('tv-series', 'HomeController@getTVSeries');
Route::get('type/{slug}', 'HomeController@getListType');
Route::get('top-imdb/{slug}', 'HomeController@getTopIMDB');
Route::get('movie/{slug}', 'HomeController@getMovie');
Route::get('watch/{slug}', 'HomeController@getWatch');
Route::get('actors/{slug}', 'HomeController@getActors');
Route::get('directors/{slug}', 'HomeController@getDirectors');
Route::get('playlist-film', 'HomeController@getPlaylistFilm');
Route::get('playlist/{slug}', 'HomeController@getPlaylistDetail');
Route::get('producers/{slug}', 'HomeController@getProducers');
Route::get('library-film', 'HomeController@getFilmLibrary');
Route::get('ranking-film', 'HomeController@getRankingFilm');
Route::post('load-subtitle', 'HomeController@postLoadSubtitle');
Route::get('tag/{slug}', 'HomeController@getTag');
Route::post('report-link', 'HomeController@postReportLink');
Route::get('news', 'HomeController@getNews');
Route::get('news/{slug}', 'HomeController@getComingSoon');
Route::get('coming-soon/{slug}', 'HomeController@getComingSoon');
Route::get('search', 'HomeController@getSearch');
//Ajax
Route::post('top-today', 'HomeController@postTopToday');
Route::post('movie-genres', 'HomeController@postMovieGenres');
Route::post('movie-country', 'HomeController@postMovieCountry');
Route::post('watch/request-api', 'HomeController@postRequestAPI');
Route::post('watch/request-api-detail', 'HomeController@postRequestAPIDetail');
Route::post('watch/add-view-film', 'HomeController@postAddViewFilm');
Route::post('get-info-film', 'HomeController@getInfoFilm');
Route::post('watch/get-meta-film', 'HomeController@postMetaFilm');
Route::post('watch/report-link-api-error', 'HomeController@postReportLinkAPIError');

Route::post('post-favorite', 'HomeController@postFavorite');
Route::post('watch/rate-film', 'HomeController@postRateFilm');
Route::post('movie_search', 'HomeController@postSearchMovie');
Route::post('get_server_play', 'HomeController@getServerPlay');
Route::post('report-film', 'HomeController@postReportFilm');
Route::post('request-download', 'HomeController@postDownload');
Route::post('clear_user_subtitle', 'HomeController@postClearUserSubtitle');
Route::get('sitemap.xml', 'SitemapsController@index');