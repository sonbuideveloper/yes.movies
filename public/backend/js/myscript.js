$(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
});

$("div.alert").delay(3000).slideUp();

function xacnhanxoa(msg){
	if(window.confirm(msg)){
		return true;
	}else{
		return false;
	}
}



$(document).ready(function(){
	$('.films_content').css("display","none");
	$('.casts_content').css("display","none");
	$('.directors_content').css("display","none");

	var id = $('input:radio[name=type_parent]:checked').val();
	switch(id){
		case '1':
			$('.films_content').css("display","block");
			$('.casts_content').css("display","none");
			$('.directors_content').css("display","none");
			break;
		case '2':
			$('.films_content').css("display","none");
			$('.casts_content').css("display","block");
			$('.directors_content').css("display","none");
			break;
		case '3':
			$('.films_content').css("display","none");
			$('.casts_content').css("display","none");
			$('.directors_content').css("display","block");
			break;
		case '0':
			$('.films_content').css("display","none");
			$('.casts_content').css("display","none");
			$('.directors_content').css("display","none");
			break;
	}

	$('.type_parent_check').change(function () {
		var id = $(this).val();
		// console.log(id);
		switch(id){
			case '1':
				$('.films_content').css("display","block");
				$('.casts_content').css("display","none");
				$('.directors_content').css("display","none");
				break;
			case '2':
				$('.films_content').css("display","none");
				$('.casts_content').css("display","block");
				$('.directors_content').css("display","none");
				break;
			case '3':
				$('.films_content').css("display","none");
				$('.casts_content').css("display","none");
				$('.directors_content').css("display","block");
				break;
			case '0':
				$('.films_content').css("display","none");
				$('.casts_content').css("display","none");
				$('.directors_content').css("display","none");
				break;
		}
	});
});

$(document).ready(function(){
	$('.search_info_film').click(function () {
		var key = $(this).parent().parent().parent().find('#url_film').val();
        $("#buttonloading").css('display','inline-block');
        $('.fa-search').css('display', 'none');
		if(key != ''){
			$.ajax({
				url		: 'search-info',
				type	: 'post',
				data	: {
					key: key
				},
				success	: function(data){
				    console.log(data);
					if(data != 'No Result'){
						var result = JSON.parse(data);
						console.log(result);
						if(result['data']){
                            $("#buttonloading").css('display','none');
                            $('.fa-search').css('display', 'inline-block');
                            $('#slug').val(result['slug']);
                            if(result['type']=='movie') {
                                $('#name').val(result['data']['title']);
                                $('#meta-title').val(result['data']['title']);
                            }else if(result['type']=='tv'){
                                $('#name').val(result['data']['name']);
                                $('#meta-title').val(result['data']['name']);
							}
							$('#meta-description').val(result['data']['overview'].substring(0,250));
							$('#day_release').val(result['data']['release_date']);
                            $('#duration').val(result['data']['runtime']);
                            $('#IMDB').val(result['data']['vote_average']);
                            if(result['data']['videos']['results'].length>0) {
                                $('#trailer').val('https://www.youtube.com/embed/' + result['data']['videos']['results'][0]['key']);
                            }
                            var overview = '<textarea id="content" name="content" class="form-control" rows="6" placeholder="Enter meta description..">'+result['data']['overview'];
							overview += '</textarea>';
							$('.text-content').html(overview);
							var profile = 'https://image.tmdb.org/t/p/w600_and_h900_bestv2'+result['data']['poster_path'];
							var multi_img = 'https://image.tmdb.org/t/p/w600_and_h900_bestv2'+result['data']['backdrop_path']
							$('.profile_image_div').css('display','block');
							$('#profile_image').attr('src',profile);
							$('#profile_image_input').attr('value',profile);
							$('.backdrops_image_div').css('display','block');
							$('#backdrops_image').attr('src',multi_img);
							$('#backdrops_image_input').attr('value',multi_img);
							$('#id_films').attr('value',result['data']['id']);
							var cast_list = '';
							var html_cast = '';
							// var id_cast = '';
							var director_list = '';
							var html_director = '';							
							if(result['relation']){								
								for (var i = 0, len = result['relation']['cast'].length; i < len; i++) {
									cast_list += result['relation']['cast'][i]['name']+',';	
									html_cast += '<li class="tagging_tag">'+result['relation']['cast'][i]['name']+'<span class="tag_delete">x</span></li>';
									// id_cast += result['relation']['cast'][i]['id']+',';
								}
								for (var i = 0, len = result['relation']['crew'].length; i < len; i++) {
									if(result['relation']['crew'][i]['department'] == 'Directing'){
										director_list += result['relation']['crew'][i]['name']+',';	
										html_director += '<li class="tagging_tag">'+result['relation']['crew'][i]['name']+'<span class="tag_delete">x</span></li>';				
									}									
								}
							}
							// $('#id_cast').val(id_cast);

							var keywords = '';
							if(result['keywords']){
								if(result['type']=='movie') {
                                    for (var i = 0, len = result['keywords']['keywords'].length; i < len; i++) {
                                        if (i < (len - 1)) {
                                            keywords += result['keywords']['keywords'][i]['name'] + ', ';
                                        } else {
                                            keywords += result['keywords']['keywords'][i]['name'];
                                        }
                                    }
                                }else if(result['type']=='tv'){
                                    for (var i = 0, len = result['keywords']['results'].length; i < len; i++) {
                                        if (i < (len - 1)) {
                                            keywords += result['keywords']['results'][i]['name'] + ', ';
                                        } else {
                                            keywords += result['keywords']['results'][i]['name'];
                                        }
                                    }
								}
								$('#meta-keywords').attr('value',keywords);		
							}							
							if(cast_list != ''){
								$('#cast').attr('value',cast_list);
								$('.content-cast .tagging_ul').html(html_cast);
							}
							if(director_list != ''){
								$('#director').attr('value',director_list);
								$('.content-director .tagging_ul').html(html_director);
							}

							if(result['data']['production_countries']){
								var country = result['data']['production_countries'][0]['name'];
								if(country){
                                    $.ajax({
                                        url		: 'ajax-get-country',
                                        type	: 'post',
                                        data	: {
                                            country_name: country
                                        },
                                        success	: function(data_country){
                                            if(data_country.status==200){
												$('#country_film_id').html(data_country.result);
                                            }
                                        }
                                    });
								}
							}

                            if(result['data']['genres']){
                                var genres = result['data']['genres'];
                                if(genres){
                                    $.ajax({
                                        url		: 'ajax-get-genres',
                                        type	: 'post',
                                        data	: {
                                            genres: genres
                                        },
                                        success	: function(data_genres){
                                            if(data_genres.status==200){
                                                $('#genre_film_checkbox').html(data_genres.result);
                                            }
                                        }
                                    });
                                }
                            }

                            if(result['data']['production_companies']){
                                var producer = result['data']['production_companies'][0]['name'];
                                if(producer){
                                    $.ajax({
                                        url		: 'ajax-get-producer',
                                        type	: 'post',
                                        data	: {
                                            producer_name: producer
                                        },
                                        success	: function(data_producer){
                                        	console.log(data_producer);
                                            if(data_producer.status==200){
                                                $('#producer_id').html(data_producer.result);
                                            }
                                        }
                                    });
                                }
                            }
						}else{
							alert('No result');	
						}
					}else{
						alert('No result');
					}
				}
			});
		}
	});
});


$('#tag-film').autoComplete({
    minChars: 1,
    source: function(term, suggest){
        term = term.toLowerCase();
        var choices = $('#tag_all').val().split(",");
        var suggestions = [];
        for (i=0;i<choices.length;i++)
            if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
        suggest(suggestions);
    }
});

$('.btn-edit').click(function() {
	var link_film_id = $(this).data('id');
	var url = $(this).data('url');
	if(link_film_id){
		$.ajax({
			url		: url+'/admin/link-film/find-link-film',
			type	: 'post',
			data	: {
				link_film_id: link_film_id
			},
			success	: function(data){
				if(data.status==200){
					$('#film_id_edit').val(data.result.film_id);
					$('#film_link_id_edit').val(data.result.id);
					$('#link_edit').val(data.result.link);
					$('#email_edit').val(data.result.email);
					$('#server_play_edit option[value='+data.result.server_play+']').attr('selected','selected');
					$('.btn-submit').click();
				}
			}
		});
	}
});

$('.btn-detail-edit').click(function(event) {
	var link_film_id = $(this).data('id');
	var url = $(this).data('url');
	if(link_film_id){
		$.ajax({
			url		: url+'/admin/link-film-detail/find-link-film',
			type	: 'post',
			data	: {
				link_film_id: link_film_id 
			},
			success	: function(data){
				// console.log(data);
				if(data.status==200){
					$('#film_id').val(data.result.film_id);
					$('#link_film_id').val(data.result.id);
					$('#link_film').val(data.result.link);
					$('#email').val(data.result.email);					
					$('#server_play option[value='+data.result.server_play+']').attr('selected','selected');
					$('.btn-submit').click();
				}
			}
		});
	}
});

$('.btn-link-film-error').click(function () {
    var link_film_id = $(this).data('id');
    var link_film = $(this).parent().parent().parent().find('.link_film').html();
    var email = $(this).parent().parent().parent().find('.email').html();
    $('#link_film').val(link_film);
    $('#link_film_id').val(link_film_id);
    $('#email').val(email);
    $('.btn-submit').click();
});

$('.btn-link-film-detail-error').click(function () {
    var link_film_id = $(this).data('id');
    var link_film = $(this).parent().parent().parent().find('.link_film').html();
    var email = $(this).parent().parent().parent().find('.email').html();
    $('#link_film_detail').val(link_film);
    $('#link_film_detail_id').val(link_film_id);
    $('#email_detail').val(email);
    $('.btn-detail-submit').click();
})

/**
 * Filter Film
 */
$('.filter_film').change(function () {
	$('#form_filter_film').submit();
})


$('.btn-addmore').click(function () {
	var episode = $(this).data('episode');
	var count_link = $(this).data('link');
	var url = $(this).data('url');
	var data = $('.tbody_'+episode).html();
    $.ajax({
        url		: url + '/get_server_play',
        type	: 'post',
        success	: function(result){
            if(result.status==200){
                var data_2 = "<tr>";
                data_2 += "<input type=\"hidden\" value=\""+episode+"\" name=\"episode[]\">";
                data_2 += "<td>Link</td>";
                data_2 += "<td style=\"width: 120px;\">"+result.result+"</td>";
                data_2 += "<td><input type=\"text\" class=\"form-control\" placeholder=\"Link\" name=\"link[]\"></td>";
                data_2 += "</tr>";
                $('.tbody_'+episode).html(data+data_2);
                $('.table_'+episode+' .btn-addmore').attr('data-link',(count_link+1));
            }
        }
    });
});

$('.select-server-play').on('change',function(){
	var serve_id = $(".select-server-play option:selected").val();
	var film_id = $('#film_id').val();
	$('#server_id').val(serve_id);
    if(serve_id && film_id){
        $.ajax({
            url		: 'get-server-film-data',
            type	: 'post',
			data	:{
            	serve_id: serve_id,
				film_id: film_id
			},
            success	: function(result){
            	if(result.status==200){
					$('#data-content').val(result.result);
				}
            }
        });
	}
});

$('.btn-submit-data').click(function(){
    var serve_id = $('#server_id').val();
    var film_id = $('#film_id').val();
    var content = $('#data-content').val();
    if(serve_id && film_id && content){
        $.ajax({
            url		: '/admin/film/multi-link',
            type	: 'post',
            data	:{
                serve_id: serve_id,
                film_id: film_id,
                textarea_data_content: content
            },
            success	: function(result){
				$.ajax({
					url		: 'get-server-film-data',
					type	: 'post',
					data	:{
						serve_id: serve_id,
						film_id: film_id
					},
					success	: function(data){
						if(data.status==200){
							$('#data-content').val(data.result);
						}
					}
				});
                if(result.status==200){
                    $.bootstrapGrowl('<h4>Success!</h4> <p>You already updated!</p>', {
                        type: "success",
                        delay: 2500,
                        allow_dismiss: true
                    });
                }else{
                    $.bootstrapGrowl('<h4>Error!</h4> <p>'+result.result+'</p>', {
                        type: "danger",
                        delay: 2500,
                        allow_dismiss: true
                    });
                }
            }
        });
    }
});

$('.select-server-play-multi').on('change',function(){
    var serve_id = $(".select-server-play-multi option:selected").val();
    var film_id = $('#film_id').val();
    $('#server_id').val(serve_id);
    if(serve_id && film_id){
        $.ajax({
            url		: 'get-server-film-data',
            type	: 'post',
            data	:{
                serve_id: serve_id,
                film_id: film_id
            },
            success	: function(result){
                if(result.status==200){
                    $('#data-content').val(result.result);
                }
            }
        });
    }
});

$('.btn-submit-data-multi').click(function(){
    var serve_id = $('#server_id').val();
    var film_id = $('#film_id').val();
    var content = $('#data-content').val();
    if(serve_id && film_id && content){
        $.ajax({
            url		: '/admin/film-series/multi-link',
            type	: 'post',
            data	:{
                serve_id: serve_id,
                film_id: film_id,
                textarea_data_content: content
            },
            success	: function(result){
                $.ajax({
                    url		: 'get-server-film-data',
                    type	: 'post',
                    data	:{
                        serve_id: serve_id,
                        film_id: film_id
                    },
                    success	: function(data){
                        if(data.status==200){
                            $('#data-content').val(data.result);
                        }
                    }
                });
                if(result.status==200){
                    $.bootstrapGrowl('<h4>Success!</h4> <p>You already updated!</p>', {
                        type: "success",
                        delay: 2500,
                        allow_dismiss: true
                    });
				}else{
                    $.bootstrapGrowl('<h4>Error!</h4> <p>'+result.result+'</p>', {
                        type: "danger",
                        delay: 2500,
                        allow_dismiss: true
                    });
				}
            }
        });
    }
});

//=======Ajax link film=========================//
//Create//
$('#btn-add-link').click(function(){
    var film_link_id = $('#film_link_id').val();
    var server_play = $("#server_play option:selected").val();
    var link = $('#link').val();
    var email = $('#email').val();
    if(server_play && link && film_link_id){
        $.ajax({
            url		: '/admin/link-film/ajax-create',
            type	: 'post',
            data	:{
                film_link_id: film_link_id,
                server_play: server_play,
                link: link,
                email: email
            },
            success	: function(result){
                console.log(result);
                if(result.status==200){
                    swal({
                        title: "Success!",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: 'btn-success btn-md waves-effect waves-light',
                        confirmButtonText: 'OK',
                        closeOnConfirm: false
                    },function(isConfirm){
                        location.reload();
                    });
                }else{
                    $.bootstrapGrowl('<h4>Error!</h4> <p>'+result.result+'</p>', {
                        type: "danger",
                        delay: 2500,
                        allow_dismiss: true
                    });
                }
            }
        });
    }else {
        $.bootstrapGrowl('<h4>Error!</h4> <p>Please enter Link</p>', {
            type: "danger",
            delay: 2500,
            allow_dismiss: true
        });
    }
});

//Edit
$('#btn-edit-link').click(function(){
    var film_link_id = $('#film_link_id_edit').val();
    var server_play = $("#server_play_edit option:selected").val();
    var link = $('#link_edit').val();
    var email = $('#email_edit').val();
    var film_id = $('#film_id_edit').val();

    if(server_play && link && film_link_id){
        $.ajax({
            url		: '/admin/link-film/ajax-edit',
            type	: 'post',
            data	:{
                film_link_id: film_link_id,
                server_play: server_play,
                link: link,
                email: email,
                film_id: film_id
            },
            success	: function(result){
                console.log(result);
                if(result.status==200){
                    $('#modalEdit').modal('hide');
                    swal({
                        title: "Success!",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: 'btn-success btn-md waves-effect waves-light',
                        confirmButtonText: 'OK',
                        closeOnConfirm: false
                    },function(isConfirm){
                        location.reload();
                    });
                }else{
                    $.bootstrapGrowl('<h4>Error!</h4> <p>'+result.result+'</p>', {
                        type: "danger",
                        delay: 2500,
                        allow_dismiss: true
                    });
                }
            }
        });
    }else {
        $.bootstrapGrowl('<h4>Error!</h4> <p>Please enter Link</p>', {
            type: "danger",
            delay: 2500,
            allow_dismiss: true
        });
    }
});

//Delete
$('.ajax-warning-alert').on('click', function (e) {
    e.preventDefault();
    var id = $(this).data('href');
    swal({
        title: "Are you sure?",
        type: "warning",
        showCancelButton: true,
        cancelButtonClass: 'btn-white btn-md waves-effect',
        confirmButtonClass: 'btn-warning btn-md waves-effect waves-light sa-success',
        confirmButtonText: 'OK',
        closeOnConfirm: false
    },function(isConfirm){
        if (isConfirm){
            $.ajax({
                url		: '/admin/link-film/ajax-delete',
                type	: 'post',
                data	:{
                    id: id
                },
                success	: function(result){
                    console.log(result);
                    if(result.status==200){
                        swal({
                            title: "Success!",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: 'btn-success btn-md waves-effect waves-light',
                            confirmButtonText: 'OK',
                            closeOnConfirm: false
                        },function(isConfirm){
                            location.reload();
                        });
                    }else{
                        $.bootstrapGrowl('<h4>Error!</h4> <p>Error</p>', {
                            type: "danger",
                            delay: 2500,
                            allow_dismiss: true
                        });
                    }
                }
            });
        }
    });
});

//Delete all
$('.form-submit-delete-all').on('click', function (e) {
    e.preventDefault();
    var url = $(this).data('href');
    swal({
        title: "Are you sure?",
        type: "warning",
        showCancelButton: true,
        cancelButtonClass: 'btn-white btn-md waves-effect',
        confirmButtonClass: 'btn-warning btn-md waves-effect waves-light sa-success',
        confirmButtonText: 'OK',
        closeOnConfirm: false
    },function(isConfirm){
        if (isConfirm){
            var id = new Array();
            var n = $(".checkbox_id:checked").length;
            if (n > 0){
                $(".checkbox_id:checked").each(function(){
                    id.push($(this).val());
                });
            }
            if(id.length>0){
                $.ajax({
                    url		: '/admin/link-film/ajax-delete',
                    type	: 'post',
                    data	:{
                        id: id
                    },
                    success	: function(result){
                        console.log(result);
                        if(result.status==200){
                            swal({
                                    title: "Success!",
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonClass: 'btn-success btn-md waves-effect waves-light',
                                    confirmButtonText: 'OK',
                                    closeOnConfirm: false
                                },function(isConfirm){
                                    location.reload();
                                });
                        }else{
                            $.bootstrapGrowl('<h4>Error!</h4> <p>Error</p>', {
                                type: "danger",
                                delay: 2500,
                                allow_dismiss: true
                            });
                        }
                    }
                });
            }else{
                swal("Error", "Please choose link to delete", "error");
            }
        }
    });
});

//==============Admin link film error===========//
$('.btn-all-link-submit').on('click', function (e) {
    e.preventDefault();
    var url = $(this).data('url');
    swal({
        title: "Are you sure?",
        type: "warning",
        showCancelButton: true,
        cancelButtonClass: 'btn-white btn-md waves-effect',
        confirmButtonClass: 'btn-warning btn-md waves-effect waves-light sa-success',
        confirmButtonText: 'OK',
        closeOnConfirm: false
    },function(isConfirm){
        if (isConfirm){
            $('#link-film-error-form').attr('action',url);
            $('#link-film-error-form').submit();
        }
    });
});

$('#select-order').on('change', function(){
    $(this).parent().parent().submit();
});
//==============Admin link film error===========//
$('.btn-all-link-detail-submit').on('click', function (e) {
    e.preventDefault();
    var url = $(this).data('url');
    swal({
        title: "Are you sure?",
        type: "warning",
        showCancelButton: true,
        cancelButtonClass: 'btn-white btn-md waves-effect',
        confirmButtonClass: 'btn-warning btn-md waves-effect waves-light sa-success',
        confirmButtonText: 'OK',
        closeOnConfirm: false
    },function(isConfirm){
        if (isConfirm){
            $('#link-film-detail-error-form').attr('action',url);
            $('#link-film-detail-error-form').submit();
        }
    });
});