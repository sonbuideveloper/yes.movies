$('div.alert').delay(5000).slideUp();

$('.smile-btn').click(function(){
    var data = $(this).attr('id');
    var id = $(this).attr('data');
    var url = $(this).attr('data-url');
    
    $.ajax({
        method : 'POST',
        url  :  url,
        data : {
            data: data,
            id: id
        },
        success :  function(data) {
            if(data == 'success'){
                location.reload();
            }          
        }
    });
});

$('.user-opinion-option').click(function(){
    var data = $(this).attr('id');
    var id = $(this).attr('data');
    var url = $(this).attr('data-url');

    $.ajax({
        method : 'POST',
        url  :  url,
        data : {
            data: data,
            id: id
        },
        success :  function(data) {
            if(data == 'success'){
                location.reload();
            }          
        }
    });
});

$('#year_filter').change(function () {
    $('#filter-year-form').submit();
});


if($(window).width()>768) {
    $(".ajax-film").cluetip();

    $('.film-detail-short').cluetip({
        ajaxProcess: function (data) {
            data = {title: "Another title", body: "Blah blah blah blah"};
            $(this).attr("title", data.title);
            return data.body;
        },
        onShow: function (ct, c) {
            var id = $(this).data('id');
            var url = $(this).data('url');
            var index = $(this).data('index');
            if (id) {
                $.ajax({
                    method: 'post',
                    url: url + '/get-info-film',
                    data: {
                        id: id
                    },
                    success: function (data) {
                        if (data.status == 200) {
                            ct.find(".cluetip-inner.ui-widget-content.ui-cluetip-content").html(
                                data.result
                            );
                        }
                    }
                });
            }
        }
    });
}

function rating() {
    $("#stars-green").rating('create',{coloron:'#ffc600',onClick:function(){
        var rate = $(this).attr('data-rating');
        var film_id = $(this).data('film');
        var user_status = $(this).data('user');
        if(user_status){
            $.ajax({
                method: 'post',
                url: 'rate-film',
                data: {
                    rate : rate,
                    film_id: film_id
                },
                success: function (data) {

                }
            });
        }else{
            $('#pop-login').modal('show');
        }
    }});
}

$('#list-eps').on('click', '.film-eposide-item', function () {
    var id = $(this).data('id');
    var episode = $(this).data('name');
    var url = $(this).data('url');
    $('#iframe-link').attr('src','');
    jwplayer("player").pause(true);
    if(id && episode && url){
        $('.iframe-div').css('display', 'none');
        $('.player').css('display', 'none');
        $(this).parent().find('.film-eposide-item').removeClass('active');
        $(this).addClass('active');
        window.history.pushState('string', '', url+'?p='+episode);
        $('#top-url-episode').attr('href',url+'?p='+episode);
        $('#top-url-episode').attr('title','Episode '+episode);
        $('#top-url-episode').html('Episode '+episode);
        request_video_jwplayer_detail(id, episode);
    }
});

$('#ip_server').on('click', '.server_play_chosen', function () {
    var id = $(this).data('id');
    var url = $(this).data('url');
    var server = $(this).data('server');
    $('#iframe-link').attr('src','');
    jwplayer("player").pause(true);
    if(id && url && server){
        $('.iframe-div').css('display', 'none');
        $('.player').css('display', 'none');
        $(this).parent().parent().find('.server_play_chosen').removeClass('active');
        $(this).addClass('active');
        window.history.pushState('string', '', url);
        request_video_jwplayer(id, server);
    }
});

$('#ip_server').on('click', '.server_play_chosen_episode', function () {
    var id = $(this).data('id');
    var url = $(this).data('url');
    var server = $(this).data('server');
    var ep = $(this).data('ep');
    $('#iframe-link').attr('src','');
    jwplayer("player").pause(true);
    if(id && url && server && ep){
        $('.iframe-div').css('display', 'none');
        $('.player').css('display', 'none');
        $(this).parent().parent().find('.server_play_chosen_episode').removeClass('active');
        $(this).addClass('active');
        window.history.pushState('string', '', url);
        request_video_jwplayer_detail(id, ep, server);
    }
});

$('.bp-btn-report-button').click(function () {
    var url = $(this).data('url');
    var link = $(this).data('link');
    var film = $(this).data('film');
    var name = $(this).data('name');
    var user = $(this).data('user');

    $('#report-label').html('Report: '+name);
    $('#link_id_input').val(link);
    $('.btn-report-modal').click();
});

$('#mv-info').on('click', '.btn-download', function () {
    var film_id = $(this).data('film');
    var link_id = $('#link_id').val();
    var url = $(this).data('url');
    var token = $('.token').val();
    $.ajax({
        method: 'post',
        url: url+'/request-download',
        data: {
            film_id: film_id,
            link_id: link_id,
            _token: token
        },
        success: function (data) {
            if(data.status == 200){
                $('#modal-download .modal-body').html(data.result);
                $('#modal-download').modal('show');
            }
        }
    });
});

$('.load-more-button').click(function () {
    $('.actor_div_sort').css('display','none');
    $('.actor_div_large').css('display','inline-block');
});









