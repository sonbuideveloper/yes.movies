(function ($) {
    $.fn.ipplayer = function (options) {
        var defaults = {
            dir: '/ip.file/swf/ipplayer/',
            about: null,
            poster: null,
            files: null,
            autotime: false,
            captions: [],
            moviesinfo: {title: '', desc: '', next: ''},
            fix: 0,
            autostart: false,
            width: 900,
            height: 560
        };
        options = $.extend(defaults, options);
        var $ipplayer = $(this);
        $ipplayer.css({
            width: options.width,
            height: options.height,
            'background-color': '#dfdfdf',
            'position': 'relative'
        }).html('<div class="ipplayerjs-overlay-loading"><span></span></div>');
        $.getJSON(options.dir + "ipplayer.php", {
            u: options.files,
            w: options.width,
            h: options.height,
            s: $('input[name="server"]').val(),
            n: options.fix
        }).done(function (json) {
            if (!$.trim(json.data)) {
                if (json.next > 0) {
                    ip_build_player($('input[name="phimid"]').val(), $('input[name="server"]').val(), $('input[name="epname"]').val(), json.next, 'next');
                    return false;
                } else {
                    var movies_fis_id = $('input[name="phimid"]').val();
                    var movies_eps_id = $('input[name="movies_eps_id"]').val();
                    var movies_eps = $('input[name="movies_eps"]').val();
                    var movies_url = $('input[name="movies_url"]').val();
                    if (server_next_eps > 0) {
                        var server_next = server_next_eps;
                    } else {
                        var server_next = server_next_all;
                    }
                    var ref_movie = movies_url + '?p=' + movies_eps + '&s=' + server_next;
                    $ipplayer.css({'background-color': '#000000'}).find('.ipplayerjs-overlay-loading').remove();
                    $.post("/ajax.php", {
                        post: 'report',
                        movies_eps_id: movies_eps_id,
                        movies_fis_id: movies_fis_id
                    }, function (data, status) {
                        next_check_server = false;
                        next_check = false;
                        $("#servers-list a").each(function () {
                            if (next_check_server == true) {
                                next_check = true;
                                window.location = movies_url + '?p=' + movies_eps + '&s=' + $(this).data('server');
                                return false;
                            }
                            if ($(this).data('server') == $('input[name="server"]').val()) {
                                next_check_server = true;
                            }
                        });
                        if (next_check == false) {
                            $ipplayer.append('<div class=\"ipplayer_popup\">' + '<h1>Notification</h1>' + '<div class=\"ipplayer_content\">' + '<p>Episode die!</p>' + '</div>' + '</div>');
                            var ipplayer_h1 = $ipplayer.height();
                            var ipplayer_h2 = $('div.ipplayer_popup').height();
                            $('div.ipplayer_popup').css({'top': (ipplayer_h1 - ipplayer_h2) / 2});
                        }
                        return false;
                    });
                    return false;
                }
                return false;
            } else {
                if (json.type == 1) {
                    $ipplayer.html('<video id="ipplayer_video" width="' + options.width + '" height="' + options.height + '" preload="yes" controls="controls"></video>');
                    if (options.autostart == true) {
                        $ipplayer.find('video').attr('autoplay', 1);
                    }
                    $.each(json.data, function (j, data) {
                        $ipplayer.find('video').append('<source type="video/mp4" data-quality="' + data.quality + '" src="' + data.files + '">');
                    });
                    $ipplayer.find('video').append('<track kind="subtitles" src="" srclang="iploadsubtitle" />');
                    $.each(options.captions, function (i, sub) {
                        $ipplayer.find('video').append('<track kind="subtitles" src="' + sub.file + '" srclang="' + sub.lang + '" />');
                    });
                    $('#ipplayer_video').ipplayer_config({
                        poster: options.poster,
                        autotime: options.autotime,
                        ipplayer_dir: options.dir,
                        ipplayer_about: options.about,
                        moviesinfo: options.moviesinfo
                    });
                    iDownload();
                    function iDownload() {
                        $.post("/ajax.php", {
                            post: 'download',
                            phimid: $('input[name="phimid"]').val(),
                            epid: json.epi
                        }, function (getUrl) {
                            $('#mediaplayer').append('<a href="' + getUrl + '" target="_blank" id="getDownload" style="text-align: center; line-height: 40px;"><i class="fa fa-download"></i></a>');
                            $('#mediaplayer').hover(function () {
                                clearTimeout(download_time);
                                $('#getDownload').show();
                            }, function () {
                                download_time = setTimeout(function () {
                                    $('#getDownload').hide();
                                }, 1400);
                            });
                        });
                    }
                }
                else if (json.type == 2) {
                    $ipplayer.html('<video id="ipplayer_video" width="' + options.width + '" height="' + options.height + '" preload="yes" controls="controls"><source type="video/youtube" src="' + json.data + '"></video>');
                    $('#ipplayer_video').ipplayer_config({
                        poster: options.poster,
                        ipplayer_dir: options.dir,
                        ipplayer_about: options.about
                    });
                }
                else {
                    $ipplayer.html('<div id="ipplayer_wrapper"><iframe width="' + options.width + '" height="' + options.height + '" src="' + json.data + '" frameborder="0" scrolling="no" allowfullscreen></iframe></div>');
                }
            }
        });
    }
    $.fn.ipplayer.defaults = {};
})(jQuery);