﻿$(document).ready(function () {
    play_ichphien();
});
function refreshCaptcha() {
    $("body").find("img.captchaimg").attr('src', "/captcha.php?rand=" + Math.random() * 1000);
}
function ip_view(phimid) {
    $.ajax({
        url: '/ajax.php', type: "POST", data: 'post=view&phimid=' + phimid, success: function (view) {
            $('#ip_view').text(view);
        }
    });
}
function update_error() {
    var d = parseInt($("input[name='epid']").val());
    var f = parseInt($("input[name='phimid']").val());
    var captcha = $("input[name='code']").val();
    if (!captcha) {
        $("#report-error-message").show().text('Please enter code!');
    } else {
        $.post('/ajax.php', {post: 'error', epid: d, phimid: f, captcha: captcha}, function (a) {
            if (a == 'ok') {
                $("#report_text").hide();
                $("#report-form").html('<div class="alert alert-success" style="font-size:18px">Your report has been sent!</div>');
                refreshCaptcha();
                $("input[name='code']").val('').focus();
            } else {
                if (a == 'error') {
                    $("#report-error-message").show().text('Incorrect code!');
                } else {
                    $("#report_text").find('span').text(a);
                    refreshCaptcha();
                    $("input[name='code']").val('').focus();
                    $("#report-error-message").show().text('Please enter the code continues.');
                }
            }
        });
    }
    return false;
}
function play_ichphien() {
    $("#ip_episode a,#servers-list a").click(function (e) {
        $("html, body").animate({scrollTop: 0}, 400);
        var ip_film = $(this).data('film');
        var ip_server = $(this).data('server');
        var ip_name = $(this).data('name');
        ip_build_player(ip_film, ip_server, ip_name);
        $('input[name="movies_eps"]').val(ip_name);
        e.preventDefault();
        return false;
    });
}
function ip_build_server(phimid, keyurl, ip_server) {
    var post_data = {postid: 'server', phimid: phimid, keyurl: keyurl};
    $.post('/index.php', post_data, function (data) {
        if (data) {
            $('#servers-list').html(data);
            $('.server_' + ip_server).addClass('active');
            play_ichphien();
        }
    });
}
function ip_build_player(ip_film, ip_server, ip_name, fix, x) {
    var w = '100%';
    var h = 500;
    var post_data = {ipplugins: 1, ip_film: ip_film, ip_server: ip_server, ip_name: ip_name, fix: fix};
    var data_id = $('#servers-list').attr('data-id');
    $("#servers-list").removeClass('in');
    $("a.btn-server").find('span').html($('li.server_' + ip_server).html());
    $.post('/ip.file/swf/plugins/ipplugins.php', post_data, function (obj) {
        if (obj == null) {
            $("#mediaplayer").css({'line-height': 'normal'}).html('<p class="no-link">Faulty video links!</p>');
            $('#mediaplayer_ajax').remove();
        } else {
            server_play = ip_server;
            if (x != 'next') {
                $("#servers-list .active,#ip_episode .active").removeClass('active');
                var $episode = $('.episode_' + ip_name);
                $episode.addClass('active');
                if ($episode.find('a').data('next')) {
                    $('input[name="movies_next"]').val($('input[name="movies_url"]').val() + '?p=' + $episode.find('a').data('next') + '&s=' + $('input[name="server"]').val());
                } else {
                    $('input[name="movies_next"]').val('');
                }
                if (data_id > 0) {
                    $('.server_' + ip_server).addClass('active');
                    $('#servers-list').attr('data-id', 0);
                } else {
                    ip_build_server(ip_film, ip_name, ip_server);
                }
                $('span.playing-on').text($('.server_' + ip_server).text());
            }
            if (obj.p == 'ip') {
                $('input[name="server"]').val(ip_server);
                $('input[name="epname"]').val(ip_name);
                var captionz = [];
                if (obj.c) {
                    var captionz = [{file: obj.c, lang: 'en'}];
                    var split = obj.c.split("url=");
                    $('.btn-subtitles').show();
                    $('#subtitles_input').val(split[1]);
                    $('#subtitles_url').attr('href', split[1]);
                }
                var movies_series = $('input[name="movies_series"]').val();
                var movies_title = $('input[name="movies_title"]').val();
                var movies_cate = $('input[name="movies_cate"]').val();
                var movies_eps = $('input[name="movies_eps"]').val();
                var movies_next = $('input[name="movies_next"]').val();
                var autotime = false;
                if (movies_series > 0) {
                    movies_title = movies_title + ' Episode ' + movies_eps;
                }
                if ((ipplayer_server == ip_server) && (ipplayer_keyurl == ip_name)) {
                    autotime = true;
                }
                $('#mediaplayer').ipplayer({
                    about: 'HTML5 Player',
                    width: w,
                    height: h,
                    autostart: false,
                    autotime: autotime,
                    moviesinfo: {title: movies_title, desc: movies_cate, next: movies_next},
                    captions: captionz,
                    fix: fix,
                    files: obj.s,
                    poster: $('input[name="phimimg"]').val()
                });
                if ($('#player_banner').html()) {
                    $('#player_banner').css({
                        width: $('#player_banner').width() + 'px',
                        'margin-left': '-' + ($('#player_banner').width() / 2) + 'px',
                        'height': 'auto',
                        'display': 'block'
                    });
                    $('#player_popup').css({'display': 'block'});
                } else {
                    $('#player_popup').css({'display': 'block'});
                }
            } else {
                $("#mediaplayer").css({'line-height': 'normal'}).html('<p class="no-link">' + obj.s + '</p>');
                $('#player_popup,#player_banner').css({'display': 'none'});
            }
        }
    })
    return false;
}