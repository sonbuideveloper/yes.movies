function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function convert_time(time) {
    var h=0;
    var m=0;
    var s=0;
    if(time>60){
        m = Math.floor(time/60);
        s = time%60;
        if(m>60){
            h = Math.floor(m/60);
            m = m%60;
        }
    }else{
        s = time;
    }
    return h+':'+m+':'+s;
}

var Base64 = {
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    encode: function(input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;
        input = Base64._utf8_encode(input);
        while (i < input.length) {
            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);
            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;
            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }
            output = output + this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) + this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
        }
        return output;
    },


    decode: function(input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;
        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
        while (i < input.length) {
            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));
            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;
            output = output + String.fromCharCode(chr1);
            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }
        }
        output = Base64._utf8_decode(output);
        return output;
    },

    _utf8_encode: function(string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";
        for (var n = 0; n < string.length; n++) {
            var c = string.charCodeAt(n);
            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
        }
        return utftext;
    },

    _utf8_decode: function(utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;
        while (i < utftext.length) {
            c = utftext.charCodeAt(i);
            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if ((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i + 1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i + 1);
                c3 = utftext.charCodeAt(i + 2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }
        }
        return string;
    }
};

function request_video_jwplayer(id, s, subtitle_user) {
    var token = $('.token').val();
    $.ajax({
        method: 'post',
        url: 'request-api',
        data: {
            id: id,
            s: s,
            _token: token
        },
        success: function (data) {
            var result = JSON.parse(Base64.decode(data.result));

            if(data.server_id){
                window.history.pushState('string', '', data.server_id);
            }
            if(data.status == 'api'){
                $('.player_iframe').css('display', 'none');
                $('#ip_server').html(data.server);
                $('.player').css('display', 'block');
                $('.btn-upload').css('display', 'inline-block');
                $('.bp-btn-report-button').attr('data-link',data.link_id);
                $('#link_id').val(data.link_id);
                if(result.status == 'ok'){
                    if(subtitle_user){
                        var subtitle = JSON.parse($('#input_subtitle_user').val());
                    }else{
                        var subtitle = data.subtitle;
                    }
                    var playerInstance = jwplayer("player");
                    playerInstance.setup({
                        image: "",
                        stagevideo: false,
                        stretching: "uniform",
                        volume: "80",
                        width: "100%",
                        height: "100%",
                        autostart: false,
                        allowfullscreen: true,
                        allowscriptaccess: "always",
                        sources: result.data, tracks: subtitle,
                        captions: {
                            color: '#FFFFFF',
                            fontsize: 20,
                            fontOpacity: 75,
                            backgroundOpacity: 2,
                            backgroundColor: '#00FF00',
                            edgeStyle:'uniform',
                            windowColor: '#0000FF',
                            windowOpacity: 30
                        },
                        aspectratio: "16:9",
                        events: {
                            onReady: function() {
                                var seek_time= getCookie('seek_time_'+id);
                                if(seek_time>0){
                                    var time = convert_time(seek_time);
                                    $.confirm({
                                        title: 'Notification!',
                                        content: 'The system recognizes you left off last time at: '+time+', Do you want to see next?',
                                        buttons: {
                                            confirm: function () {
                                                jwplayer("player").seek(seek_time);
                                            },
                                            cancel: function () {
                                                jwplayer("player").seek(0);
                                            }
                                        }
                                    });
                                }else{
                                    jwplayer("player").seek(0);
                                }
                            },
                            onError: function(){
                                setCookie('seek_time_'+id, 0, 1);
                                var server_error = getCookie('server_error_'+id);
                                if(server_error){
                                    setCookie('server_error_'+id, parseInt(server_error)+1, 1);
                                    if(server_error<5){
                                        request_video_jwplayer(id, s+1, subtitle_user);
                                    }
                                }else{
                                    setCookie('server_error_'+id, 1, 1);
                                    request_video_jwplayer(id, s+1, subtitle_user)
                                }
                            },
                            onTime: function(event){
                                var time = Math.floor(event.position),
                                    counter = 0;
                                if (time == counter) {
                                    counter += 5;
                                }
                                setCookie('seek_time_'+id, time, 1);
                            }
                        }
                    });
                }
            }else if(data.status == 'embed'){
                $('.player_iframe').css('display', 'block');
                $('.player').css('display', 'none');
                $('.btn-upload').css('display', 'none');
                $('#ip_server').html(data.server);
                $('#iframe-link').attr('src',result);
                $('.bp-btn-report-button').attr('data-link',data.link_id);
                $('#link_id').val(0);
            }else {
                $('#ip_server').html(data.server);
                $('.player_iframe').css('display', 'block');
                $('.player').css('display', 'none');
                $('.btn-upload').css('display', 'none');
                $('#iframe-link').attr('src','https://www.youtube.com/embed/abc?autoplay=1&rel=0&nologo=1&iv_load_policy=0&cc_load_policy=1');
                $('.bp-btn-report-button').attr('data-link',data.link_id);
                $('#link_id').val(0);
            }
        }
    });
}

function request_video_jwplayer_detail(id, ep, s, subtitle_user){
    var token = $('.token').val();
    $.ajax({
        method: 'post',
        url: 'request-api-detail',
        data: {
            id: id,
            ep: ep,
            s: s,
            _token: token
        },
        success: function (data) {
            var result = JSON.parse(Base64.decode(data.result));

            if(data.server_id){
                window.history.pushState('string', '', data.server_id);
            }
            if(data.status == 'api'){
                $('.player_iframe').css('display', 'none');
                $('.player').css('display', 'block');
                $('.btn-upload').css('display', 'inline-block');
                $('#ip_server').html(data.server);
                $('.bp-btn-report-button').attr('data-link',data.link_id);
                $('#link_id').val(data.link_id);
                if(result.status == 'ok'){
                    if(subtitle_user){
                        var subtitle = JSON.parse($('#input_subtitle_user').val());
                    }else{
                        var subtitle = data.subtitle;
                    }
                    var playerInstance = jwplayer("player");
                    playerInstance.setup({
                        image: "",
                        stagevideo: false,
                        stretching: "uniform",
                        volume: "80",
                        width: "100%",
                        height: "100%",
                        autostart: false,
                        allowfullscreen: true,
                        allowscriptaccess: "always",
                        sources: result.data, tracks: subtitle,
                        captions: {
                            color: '#FFFFFF',
                            fontsize: 20,
                            fontOpacity: 75,
                            backgroundOpacity: 2,
                            backgroundColor: '#00FF00',
                            edgeStyle:'uniform',
                            windowColor: '#0000FF',
                            windowOpacity: 30
                        },
                        aspectratio: "16:9",
                        events: {
                            onReady: function() {
                                var seek_time= getCookie('seek_time_'+id+'_'+ep);
                                if(seek_time>0){
                                    var time = convert_time(seek_time);
                                    $.confirm({
                                        title: 'Notification!',
                                        content: 'The system recognizes you left off last time at: '+time+', Do you want to see next?',
                                        buttons: {
                                            confirm: function () {
                                                jwplayer("player").seek(seek_time);
                                            },
                                            cancel: function () {
                                                jwplayer("player").seek(0);
                                            }
                                        }
                                    });
                                }else{
                                    jwplayer("player").seek(0);
                                }
                            },
                            onComplete: function() {
                                $('.episode_'+(parseInt(ep)+1)+'.film-eposide-item').click();
                            },
                            onError: function(){
                                setCookie('seek_time_'+id+'_'+ep, 0, 1);
                                var server_error = getCookie('server_error_'+id+'_'+ep);
                                if(server_error){
                                    setCookie('server_error_'+id+'_'+ep, parseInt(server_error)+1, 1);
                                    if(server_error<5){
                                        request_video_jwplayer_detail(id, ep, s+1, subtitle_user);
                                    }
                                }else{
                                    setCookie('server_error_'+id+'_'+ep, 1, 1);
                                    request_video_jwplayer_detail(id, ep, s+1, subtitle_user)
                                }
                            },
                            onTime: function(event){
                                var time = Math.floor(event.position),
                                    counter = 0;
                                if (time == counter) {
                                    counter += 5;
                                }
                                setCookie('seek_time_'+id+'_'+ep, time, 1);
                            }
                        }
                    });
                }
            }else if(data.status == 'embed'){
                $('.player_iframe').css('display', 'block');
                $('.player').css('display', 'none');
                $('.btn-upload').css('display', 'none');
                $('#ip_server').html(data.server);
                $('.bp-btn-report-button').attr('data-link',data.link_id);
                $('#iframe-link').attr('src',result);
                $('#link_id').val(0);
            }else {
                $('#ip_server').html(data.server);
                $('.bp-btn-report-button').attr('data-link',data.link_id);
                $('.player_iframe').css('display', 'block');
                $('.player').css('display', 'none');
                $('.btn-upload').css('display', 'none');
                $('#link_id').val(0);
                $('#iframe-link').attr('src','https://www.youtube.com/embed/abc?autoplay=1&rel=0&nologo=1&iv_load_policy=0&cc_load_policy=1');
            }
        }
    });
}

function addviewfilm(id, subtitle){
    console.log(id);
    console.log(subtitle);
    if(id){
        $.ajax({
            method: 'post',
            url: 'add-view-film',
            data: {
                id: id,
                subtitle: subtitle
            },
            success: function (data) {
                console.log(data);
            }
        });
    }
}

function request_meta_data(film_id, episode) {
    if(film_id, episode){
        $.ajax({
            method: 'post',
            url: 'get-meta-film',
            data: {
                film_id: film_id,
                episode: episode
            },
            success: function (data) {
                // console.log(data);
                $('.meta-title').html(data.result.title);
            }
        });
    }
}