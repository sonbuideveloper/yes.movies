<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Models\Contact;
use Models\Setting;

class RegisterUser extends Mailable
{
    use Queueable, SerializesModels;

    public $token;
    protected  $setting;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($token, Setting $setting)
    {
        $this->setting = $setting;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->setting->email, $this->setting->name)
            ->view('emails.password',['token' => $this->token]);
    }
}
