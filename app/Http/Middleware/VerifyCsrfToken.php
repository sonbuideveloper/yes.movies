<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'language',
        'search',
        'films/rate',
        'films/like',
        'ajaxdata',
        'top-today',
        'movie-genres',
        'movie-country',
        'post-favorite',
        'watch/rate-film',
        'watch/add-view-film',
        'get-info-film',
        'movie_search',
        'get_server_play',
        'report-film',
        'watch/get-meta-film',
        'clear_user_subtitle',
        'watch/report-link-api-error',

        'admin/film/cast/search-info',
        'admin/film/director/search-info',
        'admin/film/search-info',
        'admin/film/ajax-get-country',
        'admin/film/ajax-get-genres',
        'admin/film/ajax-get-producer',
        'admin/film/multi-link/get-server-film-data',
        'admin/film/multi-link',

        'admin/film-series/multi-link/get-server-film-data',
        'admin/film-series/multi-link',

        'admin/link-film/find-link-film',
        'admin/link-film-detail/find-link-film',
        'admin/link-film/ajax-create',
        'admin/link-film/ajax-edit',
        'admin/link-film/ajax-delete'
    ];
}
