<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Models\Languages;

class Language
{
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $lang = App::getLocale();

        if($lang == 'en') {
            Session::set('lang_id', 0);
        }else{
            $language = Languages::where('language',$lang)->first();
            Session::set('lang_id', $language->id);
        }
        return $next($request);

    }
}
