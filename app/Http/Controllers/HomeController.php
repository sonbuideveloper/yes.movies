<?php

namespace App\Http\Controllers;

use App\Models\Actor;
use App\Models\ActorRelation;
use App\Models\Ads;
use App\Models\CategoryFilm;
use App\Models\CountryFilm;
use App\Models\Director;
use App\Models\DirectorRelation;
use App\Models\FavoriteFilm;
use App\Models\LinkFilm;
use App\Models\LinkFilmDetail;
use App\Models\Playlist;
use App\Models\Producer;
use App\Models\RateFilm;
use App\Models\ServerPlayFilm;
use App\Models\TagFilmRelation;
use App\Models\TypeFilm;
use Illuminate\Support\Facades\App;
use Models\Article;
use Models\Languages;
use Roumen\Sitemap\Sitemap;
use Validator, Storage, Session, Response, Auth, Cache, DB, URL;
use Models\Setting;
use Illuminate\Http\Request;
use App\Models\CategoryFilmRelation;
use App\Models\Film;

class HomeController extends Controller
{
    protected $str_encode = '7eb98170dc6ff1e84165bf0d9ebea0a7';

    public function __construct()
    {
        $this->middleware('language');
    }

    public function index(){
        $setting = Setting::first();
        if($setting->theme == 1){
            $data_cache = Cache::get('site_page');
            if($data_cache){
                return view('frontend.theme_'.$setting->theme.'.pages.site')->with($data_cache);
            }else {
                $lang = App::getLocale();
                $lang_id = Session::get('lang_id');
                $language_list = Languages::get();
                $meta = array(
                    'title' => $setting->title,
                    'description' => $setting->description,
                    'keywords' => $setting->keywords,
                    'images' => url(Storage::url($setting->meta_images))
                );
                $genres = CategoryFilm::where('publish', 1)->orderby('name')->get();
                $country = CountryFilm::where('publish', 1)->orderby('name')->get();
                $type = TypeFilm::where('publish', 1)->orderby('name')->get();
                $sliders = Film::where('publish', 1)->where('slider', 1)->orderby('created_at', 'desc')->take(8)->get();
                $featured = Film::where('publish', 1)->where('featured', 1)->with('country')->with('genres')->with('actors')->orderby('created_at', 'desc')->take($setting->film_home)->get();
                $movies = Film::where('publish', 1)->where('movie_series', 0)->with('country')->with('genres')->with('actors')->with('favorite')->orderby('created_at', 'desc')->take($setting->film_home)->get();
                $film_series = Film::where('publish', 1)->where('movie_series', 1)->with('country')->with('genres')->with('actors')->with('film_detail')->orderby('created_at', 'desc')->take($setting->film_home)->get();
                $articles = Article::where('publish', 1)->where('type', 'article')->orderby('created_at', 'desc')->take(10)->get();
                $footer = Ads::where('type', 'footer')->first();
                $home_top_episode = Ads::where('slug', 'home-top-episode')->where('publish', 1)->where('type', 'ads')->first();
                $home_top_suggestion = Ads::where('slug', 'home-top-suggestion')->where('publish', 1)->where('type', 'ads')->first();
                $home_top_movies = Ads::where('slug', 'home-top-movies')->where('publish', 1)->where('type', 'ads')->first();
                $home_top_tv_series = Ads::where('slug', 'home-top-tv-series')->where('publish', 1)->where('type', 'ads')->first();

                $data_cache = [
                    'page' => 'site',
                    'setting' => $setting,
                    'meta' => $meta,
                    'lang' => $lang,
                    'lang_id' => $lang_id,
                    'language_list' => $language_list,
                    'genres' => $genres,
                    'country' => $country,
                    'type' => $type,
                    'sliders' => $sliders,
                    'featured' => $featured,
                    'movies' => $movies,
                    'film_series' => $film_series,
                    'articles' => $articles,
                    'footer' => (!empty($footer) ? $footer : null),
                    'home_top_episode' => (!empty($home_top_episode) ? $home_top_episode : null),
                    'home_top_suggestion' => (!empty($home_top_suggestion) ? $home_top_suggestion : null),
                    'home_top_movies' => (!empty($home_top_movies) ? $home_top_movies : null),
                    'home_top_tv_series' => (!empty($home_top_tv_series) ? $home_top_tv_series : null)
                ];
                Cache::put('site_page', $data_cache, $setting->cache);
                return view('frontend.theme_'.$setting->theme.'.pages.site')->with($data_cache);
            }
        }
        $data_cache = Cache::get('index');
        if($data_cache){
            return view('frontend.theme_'.$setting->theme.'.pages.home')->with($data_cache);
        }else {
            $lang = App::getLocale();
            $lang_id = Session::get('lang_id');
            $language_list = Languages::get();
            $meta = array(
                'title' => $setting->title,
                'description' => $setting->description,
                'keywords' => $setting->keywords,
                'images' => url(Storage::url($setting->meta_images))
            );

            $data_cache = [
                'page' => 'index',
                'setting' => $setting,
                'meta' => $meta,
                'lang' => $lang,
                'lang_id' => $lang_id,
                'language_list' => $language_list
            ];
            Cache::put('index', $data_cache, $setting->cache);
            return view('frontend.theme_'.$setting->theme.'.pages.home')->with($data_cache);
        }
    }

    public function getSite()
    {
        $setting = Setting::first();
        $data_cache = Cache::get('site_page');
        if($data_cache){
            return view('frontend.theme_'.$setting->theme.'.pages.site')->with($data_cache);
        }else {
            $lang = App::getLocale();
            $lang_id = Session::get('lang_id');
            $language_list = Languages::get();
            $meta = array(
                'title' => $setting->title,
                'description' => $setting->description,
                'keywords' => $setting->keywords,
                'images' => url(Storage::url($setting->meta_images))
            );
            $genres = CategoryFilm::where('publish', 1)->orderby('name')->get();
            $country = CountryFilm::where('publish', 1)->orderby('name')->get();
            $type = TypeFilm::where('publish', 1)->orderby('name')->get();
            $sliders = Film::where('publish', 1)->where('slider', 1)->orderby('created_at', 'desc')->take(8)->get();
            $featured = Film::where('publish', 1)->where('featured', 1)->with('country')->with('genres')->with('actors')->orderby('created_at', 'desc')->take($setting->film_home)->get();
            $movies = Film::where('publish', 1)->where('movie_series', 0)->with('country')->with('genres')->with('actors')->with('favorite')->orderby('created_at', 'desc')->take($setting->film_home)->get();
            $film_series = Film::where('publish', 1)->where('movie_series', 1)->with('country')->with('genres')->with('actors')->with('film_detail')->orderby('created_at', 'desc')->take($setting->film_home)->get();
            $articles = Article::where('publish', 1)->where('type', 'article')->orderby('created_at', 'desc')->take(10)->get();
            $footer = Ads::where('type', 'footer')->first();
            $home_top_episode = Ads::where('slug', 'home-top-episode')->where('publish', 1)->where('type', 'ads')->first();
            $home_top_suggestion = Ads::where('slug', 'home-top-suggestion')->where('publish', 1)->where('type', 'ads')->first();
            $home_top_movies = Ads::where('slug', 'home-top-movies')->where('publish', 1)->where('type', 'ads')->first();
            $home_top_tv_series = Ads::where('slug', 'home-top-tv-series')->where('publish', 1)->where('type', 'ads')->first();

            $data_cache = [
                'page' => 'site',
                'setting' => $setting,
                'meta' => $meta,
                'lang' => $lang,
                'lang_id' => $lang_id,
                'language_list' => $language_list,
                'genres' => $genres,
                'country' => $country,
                'type' => $type,
                'sliders' => $sliders,
                'featured' => $featured,
                'movies' => $movies,
                'film_series' => $film_series,
                'articles' => $articles,
                'footer' => (!empty($footer) ? $footer : null),
                'home_top_episode' => (!empty($home_top_episode) ? $home_top_episode : null),
                'home_top_suggestion' => (!empty($home_top_suggestion) ? $home_top_suggestion : null),
                'home_top_movies' => (!empty($home_top_movies) ? $home_top_movies : null),
                'home_top_tv_series' => (!empty($home_top_tv_series) ? $home_top_tv_series : null)
            ];
            Cache::put('site_page', $data_cache, $setting->cache);
            return view('frontend.theme_'.$setting->theme.'.pages.site')->with($data_cache);
        }
    }

    public function postTopToday(Request $request)
    {
        $name = $request->name;
        $data_cache = Cache::get('top_today_'.$name);
        if($data_cache){
            return Response::json(['status' => 200, 'result' => $data_cache]);
        }else {
            $setting = Setting::first();
            if ($name) {
                if ($name == 'today') {
                    $films = Film::where('publish', 1)->orderby('view', 'desc')->take($setting->film_home)->get();
                } else {
                    $films = Film::where('publish', 1)->orderby($name, 'desc')->take($setting->film_home)->get();
                }
                $data = "";
                if (count($films) > 0) {
                    ob_start();
                    foreach ($films as $item) { ?>
                        <div class="ml-item">
                            <a href="<?php echo url('movie/' . $item->slug . '-' . $item->id); ?>"
                               data-id="<?php echo $item->id; ?>" data-url="<?php echo url('/'); ?>" data-index="0"
                               data-jtip="#f-movies-0-<?php echo $item->id; ?>" class="ml-mask ajax-film"
                               title="<?php echo $item->name; ?>">
                                <?php if (!$item->movie_series) { ?>
                                    <span class="mli-quality"
                                          style="text-transform: uppercase"><?php echo $item->quality; ?></span>
                                <?php } ?>
                                <img data-original="<?php if ($item->images) echo url(Storage::url($item->images)); elseif ($item->images_link) echo url($item->images_link); ?>"
                                     src="<?php if ($item->images) echo url(Storage::url($item->images)); elseif ($item->images_link) echo url($item->images_link); ?>"
                                     class="lazy thumb mli-thumb" alt="<?php echo $item->name; ?>">
                                <span class="mli-info">
                                    <h2><?php echo $item->name; ?></h2>
                                </span>
                                <?php if ($item->movie_series == 1) { ?>
                                    <span class="mli-eps">Eps<i><?php echo count($item->film_detail); ?></i></span>
                                <?php } ?>
                            </a>
                            <div id="f-movies-0-<?php echo $item->id ?>" style="display: none; position: relative;">
                                <div class="jtip-top">
                                    <div class="jt-info jt-imdb">IMDb: <?php echo $item->IMDB; ?></div>
                                    <div class="jt-info"><?php echo $item->release; ?></div>
                                    <div class="jt-info"><?php echo $item->duration; ?> min</div>
                                    <div class="clearfix"></div>
                                </div>
                                <p class="f-desc"><?php echo stripslashes($item->content); ?></p>
                                <div class="block">
                                    Country: <a href="<?php echo url('country/' . $item->country->slug); ?>"
                                                title="<?php echo $item->country->name; ?> Movies"><?php echo $item->country->name; ?></a>
                                </div>
                                <div class="block">Genres:
                                    <?php
                                    if (count($item->genres) > 0) {
                                        $i = 0;
                                        foreach ($item->genres as $element) { ?>
                                            <a href="<?php echo url('genres/' . $element->slug); ?>"
                                               title="<?php echo $element->name; ?>"><?php echo $element->name; ?></a><?php echo ($i == count($item->genres) - 1) ? '' : ','; ?>
                                            <?php $i++;
                                        }
                                    } ?>
                                </div>
                                <div class="block">Actor:
                                    <?php
                                    if (count($item->actors) > 0) {
                                        $i = 0;
                                        foreach ($item->actors as $element) { ?>
                                            <a href="<?php echo url('actors/' . $element->slug); ?>"
                                               title="<?php echo $element->name; ?>"><?php echo $element->name; ?></a><?php echo ($i == count($item->actors) - 1 || $i == 9) ? '' : ','; ?>
                                            <?php
                                            $i++;
                                            if ($i == 10) {
                                                break;
                                            }
                                        }
                                    } ?>
                                </div>
                                <?php
                                $favorite = 0;
                                if (count($item->favorite) > 0 && Auth::user()) {
                                    foreach ($item->favorite as $element) {
                                        if ($element->user_id == Auth::user()->id) {
                                            $favorite = 1;
                                            break;
                                        }
                                    }
                                }
                                ?>
                                <div class="jtip-bottom">
                                    <a href="<?php echo url('movie/' . $item->slug . '-' . $item->id); ?>"
                                       class="btn btn-block btn-success"><i class="fa fa-play-circle mr10"></i>Watch
                                        movie</a>
                                    <?php if ($item->movie_series == 1) { ?>
                                        <a href="<?php echo url('movie/' . $item->slug . '-Episode-' . count($item->film_detail) . '-' . $item->id . '?p=' . count($item->film_detail)); ?>"
                                           class="btn btn-block btn-success"><i class="fa fa-play-circle mr10"></i>Watch
                                            Episode <?php count($item->film_detail); ?></a>;
                                    <?php } ?>
                                    <button onclick="favorite_add(<?php echo $item->id; ?>,this,0,<?php echo Auth::user() ? 1 : 0; ?>)"
                                            class="btn btn-block btn-default mt10 btn-favorite-<?php echo $item->id; ?> add-favorite"
                                            data-favorite="<?php echo $favorite; ?>" data-url="<?php echo url('/'); ?>">
                                        <?php if ($favorite) { ?>
                                            <i class="fa fa-remove mr10"></i>Remove Favorite
                                        <?php } else { ?>
                                            <i class="fa fa-heart mr10"></i>Add to favorite
                                        <?php } ?>
                                    </button>
                                </div>
                            </div>
                        </div>
                    <?php }
                    $data = ob_get_clean();
                }
                Cache::put('top_today_'.$name, $data, $setting->cache);
                return Response::json(['status' => 200, 'result' => $data]);
            } else {
                return Response::json(['status' => 'error']);
            }
        }
    }

    public function postMovieGenres(Request $request)
    {
        $name = $request->name;
        $data_cache = Cache::get('movie_genres_'.$name);
        if($data_cache){
            return Response::json(['status' => 200, 'result' => $data_cache]);
        }else {
            $setting = Setting::first();
            if ($name) {
                $genre = CategoryFilm::where('slug', $name)->first();
                $films = CategoryFilmRelation::where('category_film_id', $genre->id)->with('films')->take($setting->film_home)->get();
                $data = "";
                if (count($films) > 0) {
                    ob_start();
                    foreach ($films as $item) {
                        if ($item->films->publish == 1) {
                            ?>
                            <div class="ml-item">
                                <a href="<?php echo url('movie/' . $item->films->slug . '-' . $item->films->id); ?>"
                                   data-id="<?php echo $item->films->id; ?>" data-url="<?php echo url('/'); ?>"
                                   data-index="1" data-jtip="#f-movies-1-<?php echo $item->films->id; ?>"
                                   class="ml-mask ajax-film" title="<?php echo $item->films->name; ?>">
                                    <?php if (!$item->films->movie_series) { ?>
                                        <span class="mli-quality"
                                              style="text-transform: uppercase"><?php echo $item->films->quality; ?></span>
                                    <?php } ?>
                                    <img data-original="<?php if ($item->films->images) echo url(Storage::url($item->films->images)); elseif ($item->films->images_link) echo url($item->films->images_link); ?>"
                                         src="<?php if ($item->films->images) echo url(Storage::url($item->films->images)); elseif ($item->films->images_link) echo url($item->films->images_link); ?>"
                                         class="lazy thumb mli-thumb" alt="<?php echo $item->films->name; ?>">
                                    <span class="mli-info">
                                    <h2><?php echo $item->films->name; ?></h2>
                                </span>
                                    <?php if ($item->films->movie_series == 1) { ?>
                                        <span class="mli-eps">Eps<i><?php echo count($item->films->film_detail); ?></i></span>
                                    <?php } ?>
                                </a>
                                <div id="f-movies-1-<?php echo $item->films->id ?>"
                                     style="display: none; position: relative;">
                                    <div class="jtip-top">
                                        <div class="jt-info jt-imdb">IMDb: <?php echo $item->films->IMDB; ?></div>
                                        <div class="jt-info"><?php echo $item->films->release; ?></div>
                                        <div class="jt-info"><?php echo $item->films->duration; ?> min</div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <p class="f-desc"><?php echo stripslashes($item->films->content); ?></p>
                                    <div class="block">
                                        Country: <a href="<?php echo url('country/' . $item->films->country->slug); ?>"
                                                    title="<?php echo $item->films->country->name; ?> Movies"><?php echo $item->films->country->name; ?></a>
                                    </div>
                                    <div class="block">Genres:
                                        <?php
                                        if (count($item->films->genres) > 0) {
                                            $i = 0;
                                            foreach ($item->films->genres as $element) { ?>
                                                <a href="<?php echo url('genres/' . $element->slug); ?>"
                                                   title="<?php echo $element->name; ?>"><?php echo $element->name; ?></a><?php echo ($i == count($item->films->genres) - 1) ? '' : ','; ?>
                                                <?php $i++;
                                            }
                                        } ?>
                                    </div>
                                    <div class="block">Actor:
                                        <?php
                                        if (count($item->films->actors) > 0) {
                                            $i = 0;
                                            foreach ($item->films->actors as $element) { ?>
                                                <a href="<?php echo url('actors/' . $element->slug); ?>"
                                                   title="<?php echo $element->name; ?>"><?php echo $element->name; ?></a><?php echo ($i == count($item->films->actors) - 1 || $i == 9) ? '' : ','; ?>
                                                <?php $i++;
                                                if ($i == 10) {
                                                    break;
                                                }
                                            }
                                        } ?>
                                    </div>
                                    <?php
                                    $favorite = 0;
                                    if (count($item->films->favorite) > 0 && Auth::user()) {
                                        foreach ($item->films->favorite as $element) {
                                            if ($element->user_id == Auth::user()->id) {
                                                $favorite = 1;
                                                break;
                                            }
                                        }
                                    }
                                    ?>
                                    <div class="jtip-bottom">
                                        <a href="<?php echo url('movie/' . $item->films->slug . '-' . $item->films->id); ?>"
                                           class="btn btn-block btn-success"><i class="fa fa-play-circle mr10"></i>Watch
                                            movie</a>
                                        <button onclick="favorite_add(<?php echo $item->films->id; ?>,this,0,<?php echo Auth::user() ? 1 : 0; ?>)"
                                                class="btn btn-block btn-default mt10 btn-favorite-<?php echo $item->films->id; ?> add-favorite"
                                                data-favorite="<?php echo $favorite; ?>"
                                                data-url="<?php echo url('/'); ?>">
                                            <?php if ($favorite) { ?>
                                                <i class="fa fa-remove mr10"></i>Remove Favorite
                                            <?php } else { ?>
                                                <i class="fa fa-heart mr10"></i>Add to favorite
                                            <?php } ?>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        <?php }
                    }
                    $data = ob_get_clean();
                }
                Cache::put('movie_genres_'.$name, $data, $setting->cache);
                return Response::json(['status' => 200, 'result' => $data]);
            } else {
                return Response::json(['status' => 'error']);
            }
        }
    }

    public function postMovieCountry(Request $request)
    {
        $name = $request->name;
        $data_cache = Cache::get('movie_country_'.$name);
        if($data_cache){
            return Response::json(['status' => 200, 'result' => $data_cache]);
        }else {
            $setting = Setting::first();
            if ($name) {
                $country_item = CountryFilm::where('slug', $name)->first();
                $films = Film::where('publish', 1)->where('country_film_id', $country_item->id)->where('movie_series', 1)->orderby('created_at', 'desc')->take($setting->film_home)->get();
                $data = "";
                if (count($films) > 0) {
                    ob_start();
                    foreach ($films as $item) { ?>
                        <div class="ml-item">
                            <a href="<?php echo url('movie/' . $item->slug . '-' . $item->id); ?>"
                               data-id="<?php echo $item->id; ?>" data-url="<?php echo url('/'); ?>" data-index="2"
                               data-jtip="#f-movies-2-<?php echo $item->id; ?>" class="ml-mask jt ajax-film"
                               title="<?php echo $item->name; ?>">
                                <img data-original="<?php if ($item->images) echo url(Storage::url($item->images)); elseif ($item->images_link) echo url($item->images_link); ?>"
                                     src="<?php if ($item->images) echo url(Storage::url($item->images)); elseif ($item->images_link) echo url($item->images_link); ?>"
                                     class="lazy thumb mli-thumb" alt="<?php echo $item->name; ?>">
                                <span class="mli-info">
                                    <h2><?php echo $item->name; ?></h2>
                                </span>
                                <?php if ($item->movie_series == 1) { ?>
                                    <span class="mli-eps">Eps<i><?php echo count($item->film_detail); ?></i></span>
                                <?php } ?>
                            </a>
                            <div id="f-movies-2-<?php echo $item->id ?>" style="display: none; position: relative;">
                                <div class="jtip-top">
                                    <div class="jt-info jt-imdb">IMDb: <?php echo $item->IMDB; ?></div>
                                    <div class="jt-info"><?php echo $item->release; ?></div>
                                    <div class="jt-info"><?php echo $item->duration; ?> min</div>
                                    <div class="clearfix"></div>
                                </div>
                                <p class="f-desc"><?php echo stripslashes($item->content); ?></p>
                                <div class="block">
                                    Country: <a href="<?php echo url('country/' . $item->country->slug); ?>"
                                                title="<?php echo $item->country->name; ?> Movies"><?php echo $item->country->name; ?></a>
                                </div>
                                <div class="block">Genres:
                                    <?php
                                    if (count($item->genres) > 0) {
                                        $i = 0;
                                        foreach ($item->genres as $element) { ?>
                                            <a href="<?php echo url('genres/' . $element->slug); ?>"
                                               title="<?php echo $element->name; ?>"><?php echo $element->name; ?></a><?php echo ($i == count($item->genres) - 1) ? '' : ','; ?>
                                            <?php $i++;
                                        }
                                    } ?>
                                </div>
                                <div class="block">Actor:
                                    <?php
                                    if (count($item->actors) > 0) {
                                        $i = 0;
                                        foreach ($item->actors as $element) { ?>
                                            <a href="<?php echo url('actors/' . $element->slug); ?>"
                                               title="<?php echo $element->name; ?>"><?php echo $element->name; ?></a><?php echo ($i == count($item->actors) - 1 || $i == 9) ? '' : ','; ?>
                                            <?php $i++;
                                            if ($i == 10) {
                                                break;
                                            }
                                        }
                                    } ?>
                                </div>
                                <?php
                                $favorite = 0;
                                if (count($item->favorite) > 0 && Auth::user()) {
                                    foreach ($item->favorite as $element) {
                                        if ($element->user_id == Auth::user()->id) {
                                            $favorite = 1;
                                            break;
                                        }
                                    }
                                }
                                ?>
                                <div class="jtip-bottom">
                                    <a href="<?php echo url('movie/' . $item->slug . '-' . $item->id); ?>"
                                       class="btn btn-block btn-success"><i class="fa fa-play-circle mr10"></i>Watch
                                        movie</a>
                                    <button onclick="favorite_add(<?php echo $item->id; ?>,this,0,<?php echo Auth::user() ? 1 : 0; ?>)"
                                            class="btn btn-block btn-default mt10 btn-favorite-<?php echo $item->id; ?> add-favorite"
                                            data-favorite="<?php echo $favorite; ?>" data-url="<?php echo url('/'); ?>">
                                        <?php if ($favorite) { ?>
                                            <i class="fa fa-remove mr10"></i>Remove Favorite
                                        <?php } else { ?>
                                            <i class="fa fa-heart mr10"></i>Add to favorite
                                        <?php } ?>
                                    </button>
                                </div>
                            </div>
                        </div>
                    <?php }
                    $data = ob_get_clean();
                }
                Cache::put('movie_country_'.$name, $data, $setting->cache);
                return Response::json(['status' => 200, 'result' => $data]);
            } else {
                return Response::json(['status' => 'error']);
            }
        }
    }

    public function getGenres($slug, Request $request)
    {
        $genre = CategoryFilm::where('slug', $slug)->first();
        $lang = App::getLocale();
        $lang_id = Session::get('lang_id');
        $language_list = Languages::get();
        $setting = Setting::first();
        $meta = array(
            'title' => ($genre->title ? $genre->title : $setting->title),
            'description' => ($genre->descriptions ? $genre->descriptions : $setting->description),
            'keywords' => ($genre->keywords ? $genre->keywords : $setting->keywords),
            'images' => url(Storage::url($setting->meta_images))
        );
        $genres = CategoryFilm::where('publish', 1)->orderby('name')->get();
        $country = CountryFilm::where('publish', 1)->orderby('name')->get();
        $type = TypeFilm::where('publish', 1)->orderby('name')->get();
        $footer = Ads::where('type', 'footer')->first();
        $page_list_top_page = Ads::where('slug', 'page-list-top-page')->where('publish', 1)->where('type', 'ads')->first();
        $page_list_bottom_page = Ads::where('slug', 'page-list-bottom-page')->where('publish', 1)->where('type', 'ads')->first();

        $order = $request->order;
        $countryid = $request->countryid;
        $year = $request->year;
        $season = $request->season;
        if(!$order) $order = 0;
        if (!$order && !$countryid && !$year && !$season) {
            $data_cache = Cache::get('genres_'.$genre->id.'?page='.$request->page);
            if($data_cache){
                return view('frontend.theme_'.$setting->theme.'.pages.film-country')->with($data_cache);
            }else {
                $films = $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                    ->where('category_film_relations.category_film_id', $genre->id)
                    ->where('films.publish', 1)
                    ->select('films.*')
                    ->orderby('films.created_at', 'desc')
                    ->paginate($setting->film_page);
                $data_cache = [
                    'page' => 'genres',
                    'setting' => $setting,
                    'meta' => $meta,
                    'lang' => $lang,
                    'lang_id' => $lang_id,
                    'language_list' => $language_list,
                    'genres' => $genres,
                    'country' => $country,
                    'type' => $type,
                    'results' => $films,
                    'title' => $genre->name . ' Movies',
                    'footer' => (!empty($footer) ? $footer : null),
                    'page_list_top_page' => (!empty($page_list_top_page) ? $page_list_top_page : null),
                    'page_list_bottom_page' => (!empty($page_list_bottom_page) ? $page_list_bottom_page : null)
                ];
                Cache::put('genres_'.$genre->id.'?page='.$request->page, $data_cache, $setting->cache);
                return view('frontend.theme_'.$setting->theme.'.pages.film-country')->with($data_cache);
            }
        } else {
            $where = array();
            $where['films.publish'] = 1;
            if ($year) {
                $where['films.release'] = $year;
            }
            if ($countryid) {
                $where['films.country_film_id'] = $countryid;
            }

            switch ($order) {
                case 'new':
                    $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                        ->where('category_film_relations.category_film_id', $genre->id)
                        ->where($where)
                        ->select('films.*')
                        ->orderby('created_at', 'desc')
                        ->get();
                    break;
                case 'year':
                    $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                        ->where('category_film_relations.category_film_id', $genre->id)
                        ->where($where)
                        ->select('films.*')
                        ->orderby('release', 'desc')
                        ->get();
                    break;
                case 'name':
                    $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                        ->where('category_film_relations.category_film_id', $genre->id)
                        ->where($where)
                        ->select('films.*')
                        ->orderby('name', 'asc')
                        ->get();
                    break;
                case 'imdb':
                    $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                        ->where('category_film_relations.category_film_id', $genre->id)
                        ->where($where)
                        ->select('films.*')
                        ->orderby('IMDB', 'desc')
                        ->get();
                    break;
                case 'view':
                    $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                        ->where('category_film_relations.category_film_id', $genre->id)
                        ->where($where)
                        ->select('films.*')
                        ->orderby('view', 'desc')
                        ->get();
                    break;
                case '0':
                    $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                        ->where('category_film_relations.category_film_id', $genre->id)
                        ->where($where)
                        ->select('films.*')
                        ->orderby('created_at', 'asc')
                        ->get();
                    break;
            }

            if ($season) {
                $films_final = array();
                if (isset($films) && count($films) > 0) {
                    foreach ($films as $item) {
                        if ($item->day_release) {
                            $str_month = substr($item->day_release, 5, 2);
                            if ($str_month) {
                                switch ($season) {
                                    case 'spring':
                                        if ($str_month == '01' || $str_month == '02' || $str_month == '03') {
                                            $films_final[] = $item;
                                        }
                                        break;
                                    case 'summer':
                                        if ($str_month == '04' || $str_month == '05' || $str_month == '06') {
                                            $films_final[] = $item;
                                        }
                                        break;
                                    case 'fall':
                                        if ($str_month == '07' || $str_month == '08' || $str_month == '09') {
                                            $films_final[] = $item;
                                        }
                                        break;
                                    case 'winter':
                                        if ($str_month == '10' || $str_month == '11' || $str_month == '12') {
                                            $films_final[] = $item;
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }

            return view('frontend.theme_'.$setting->theme.'.pages.film-filter')->with([
                'page' => 'genres',
                'setting' => $setting,
                'meta' => $meta,
                'lang' => $lang,
                'lang_id' => $lang_id,
                'language_list' => $language_list,
                'genres' => $genres,
                'country' => $country,
                'type' => $type,
                'results' => (isset($films_final) ? $films_final : (isset($films) ? $films : null)),
                'title' => $genre->name . ' Movies',
                'footer' => (!empty($footer) ? $footer : null),
                'order' => $order,
                'countryid' => $countryid,
                'year' => $year,
                'categoryid' => 0,
                'season' => $season,
                'page_list_top_page' => (!empty($page_list_top_page) ? $page_list_top_page : null),
                'page_list_bottom_page' => (!empty($page_list_bottom_page) ? $page_list_bottom_page : null)
            ]);
        }
    }

    public function getCountry($slug, Request $request)
    {
        $country_item = CountryFilm::where('slug', $slug)->first();
        $lang = App::getLocale();
        $lang_id = Session::get('lang_id');
        $language_list = Languages::get();
        $setting = Setting::first();
        $meta = array(
            'title' => ($country_item->title ? $country_item->title : $setting->title),
            'description' => ($country_item->descriptions ? $country_item->descriptions : $setting->description),
            'keywords' => ($country_item->keywords ? $country_item->keywords : $setting->keywords),
            'images' => url(Storage::url($setting->meta_images))
        );
        $genres = CategoryFilm::where('publish', 1)->orderby('name')->get();
        $country = CountryFilm::where('publish', 1)->orderby('name')->get();
        $type = TypeFilm::where('publish', 1)->orderby('name')->get();
        $footer = Ads::where('type', 'footer')->first();
        $page_list_top_page = Ads::where('slug', 'page-list-top-page')->where('publish', 1)->where('type', 'ads')->first();
        $page_list_bottom_page = Ads::where('slug', 'page-list-bottom-page')->where('publish', 1)->where('type', 'ads')->first();

        $order = $request->order;
        $categoryid = $request->categoryid;
        $year = $request->year;
        $season = $request->season;
        if (!$order && !$categoryid && !$year && !$season) {
            $data_cache = Cache::get('country_'.$country_item->id.'?page='.$request->page);
            if($data_cache){
                return view('frontend.theme_'.$setting->theme.'.pages.film-country')->with($data_cache);
            }else {
                $films = Film::where('publish', 1)->where('country_film_id', $country_item->id)->orderby('created_at', 'desc')->paginate($setting->film_page);
                $data_cache = [
                    'page' => 'country',
                    'setting' => $setting,
                    'meta' => $meta,
                    'lang' => $lang,
                    'lang_id' => $lang_id,
                    'language_list' => $language_list,
                    'genres' => $genres,
                    'country' => $country,
                    'type' => $type,
                    'results' => $films,
                    'title' => $country_item->name . ' Movies',
                    'footer' => (!empty($footer) ? $footer : null),
                    'page_list_top_page' => (!empty($page_list_top_page) ? $page_list_top_page : null),
                    'page_list_bottom_page' => (!empty($page_list_bottom_page) ? $page_list_bottom_page : null)
                ];
                Cache::put('country_'.$country_item->id.'?page='.$request->page, $data_cache, $setting->cache);
                return view('frontend.theme_'.$setting->theme.'.pages.film-country')->with($data_cache);
            }
        } else {
            $where = array();
            $where['films.publish'] = 1;
            if ($year) {
                $where['films.release'] = $year;
            }

            switch ($order) {
                case 'new':
                    if ($categoryid) {
                        $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                            ->where('category_film_relations.category_film_id', $categoryid)
                            ->where('films.country_film_id', $country_item->id)
                            ->where($where)
                            ->select('films.*')
                            ->orderby('created_at', 'desc')
                            ->get();
                    } else {
                        $films = Film::where('country_film_id', $country_item->id)
                            ->where($where)
                            ->orderby('created_at', 'desc')
                            ->get();
                    }
                    break;
                case 'year':
                    if ($categoryid) {
                        $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                            ->where('category_film_relations.category_film_id', $categoryid)
                            ->where('films.country_film_id', $country_item->id)
                            ->where($where)
                            ->select('films.*')
                            ->orderby('release', 'desc')
                            ->get();
                    } else {
                        $films = Film::where('country_film_id', $country_item->id)
                            ->where($where)
                            ->orderby('release', 'desc')
                            ->get();
                    }
                    break;
                case 'name':
                    if ($categoryid) {
                        $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                            ->where('category_film_relations.category_film_id', $categoryid)
                            ->where('films.country_film_id', $country_item->id)
                            ->where($where)
                            ->select('films.*')
                            ->orderby('name', 'asc')
                            ->get();
                    } else {
                        $films = Film::where('country_film_id', $country_item->id)
                            ->where($where)
                            ->orderby('name', 'asc')
                            ->get();
                    }
                    break;
                case 'imdb':
                    if ($categoryid) {
                        $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                            ->where('category_film_relations.category_film_id', $categoryid)
                            ->where('films.country_film_id', $country_item->id)
                            ->where($where)
                            ->select('films.*')
                            ->orderby('IMDB', 'desc')
                            ->get();
                    } else {
                        $films = Film::where('country_film_id', $country_item->id)
                            ->where($where)
                            ->orderby('IMDB', 'desc')
                            ->get();
                    }
                    break;
                case 'view':
                    if ($categoryid) {
                        $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                            ->where('category_film_relations.category_film_id', $categoryid)
                            ->where('films.country_film_id', $country_item->id)
                            ->where($where)
                            ->select('films.*')
                            ->orderby('view', 'desc')
                            ->get();
                    } else {
                        $films = Film::where('country_film_id', $country_item->id)
                            ->where($where)
                            ->orderby('view', 'desc')
                            ->get();
                    }
                    break;
                case '0':
                    if ($categoryid) {
                        $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                            ->where('category_film_relations.category_film_id', $categoryid)
                            ->where('films.country_film_id', $country_item->id)
                            ->where($where)
                            ->select('films.*')
                            ->orderby('created_at', 'asc')
                            ->get();
                    } else {
                        $films = Film::where('country_film_id', $country_item->id)
                            ->where($where)
                            ->orderby('created_at', 'asc')
                            ->get();
                    }
                    break;
            }

            if ($season) {
                $films_final = array();
                if (isset($films) && count($films) > 0) {
                    foreach ($films as $item) {
                        if ($item->day_release) {
                            $str_month = substr($item->day_release, 5, 2);
                            if ($str_month) {
                                switch ($season) {
                                    case 'spring':
                                        if ($str_month == '01' || $str_month == '02' || $str_month == '03') {
                                            $films_final[] = $item;
                                        }
                                        break;
                                    case 'summer':
                                        if ($str_month == '04' || $str_month == '05' || $str_month == '06') {
                                            $films_final[] = $item;
                                        }
                                        break;
                                    case 'fall':
                                        if ($str_month == '07' || $str_month == '08' || $str_month == '09') {
                                            $films_final[] = $item;
                                        }
                                        break;
                                    case 'winter':
                                        if ($str_month == '10' || $str_month == '11' || $str_month == '12') {
                                            $films_final[] = $item;
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }

            return view('frontend.theme_'.$setting->theme.'.pages.film-filter')->with([
                'page' => 'country',
                'setting' => $setting,
                'meta' => $meta,
                'lang' => $lang,
                'lang_id' => $lang_id,
                'language_list' => $language_list,
                'genres' => $genres,
                'country' => $country,
                'type' => $type,
                'results' => (isset($season) ? $films_final : (isset($films) ? $films : null)),
                'title' => $country_item->name . ' Movies',
                'footer' => (!empty($footer) ? $footer : null),
                'order' => $order,
                'countryid' => 0,
                'year' => $year,
                'categoryid' => $categoryid,
                'season' => $season,
                'page_list_top_page' => (!empty($page_list_top_page) ? $page_list_top_page : null),
                'page_list_bottom_page' => (!empty($page_list_bottom_page) ? $page_list_bottom_page : null)
            ]);
        }
    }

    public function getTag($slug)
    {
        $setting = Setting::first();
        $data_cache = Cache::get('tag_'.$slug);
        if($data_cache){
            return view('frontend.theme_'.$setting->theme.'.pages.film-country')->with($data_cache);
        }else {
            $lang = App::getLocale();
            $lang_id = Session::get('lang_id');
            $language_list = Languages::get();
            $meta = array(
                'title' => $setting->title,
                'description' => $setting->description,
                'keywords' => $setting->keywords,
                'images' => url(Storage::url($setting->meta_images))
            );
            $genres = CategoryFilm::where('publish', 1)->orderby('name')->get();
            $country = CountryFilm::where('publish', 1)->orderby('name')->get();
            $type = TypeFilm::where('publish', 1)->orderby('name')->get();
            $films = Film::where('keywords', 'like', "%$slug%")->where('publish', 1)->orderby('created_at', 'desc')->paginate($setting->film_page);
            $footer = Ads::where('type', 'footer')->first();
            $page_list_top_page = Ads::where('slug', 'page-list-top-page')->where('publish', 1)->where('type', 'ads')->first();
            $page_list_bottom_page = Ads::where('slug', 'page-list-bottom-page')->where('publish', 1)->where('type', 'ads')->first();

            $data_cache = [
                'page' => '',
                'setting' => $setting,
                'meta' => $meta,
                'lang' => $lang,
                'lang_id' => $lang_id,
                'language_list' => $language_list,
                'genres' => $genres,
                'country' => $country,
                'type' => $type,
                'results' => $films,
                'title' => $slug,
                'footer' => (!empty($footer) ? $footer : null),
                'page_list_top_page' => (!empty($page_list_top_page) ? $page_list_top_page : null),
                'page_list_bottom_page' => (!empty($page_list_bottom_page) ? $page_list_bottom_page : null)
            ];
            Cache::put('tag_'.$slug, $data_cache, $setting->cache);
            return view('frontend.theme_'.$setting->theme.'.pages.film-country')->with($data_cache);
        }
    }

    public function getFeatured(Request $request)
    {
        $lang = App::getLocale();
        $lang_id = Session::get('lang_id');
        $language_list = Languages::get();
        $setting = Setting::first();
        $meta = array(
            'title' => 'Featured | good Featured movies | Featured movies on top',
            'description' => 'Top Featured movies 2017, wathing Featured movies top ' . str_replace('https://', '', str_replace('http://', '', url('/'))),
            'keywords' => 'Featured, Featured movies, Featured 2017, top Featured movies',
            'images' => url(Storage::url($setting->meta_images))
        );
        $genres = CategoryFilm::where('publish', 1)->orderby('name')->get();
        $country = CountryFilm::where('publish', 1)->orderby('name')->get();
        $type = TypeFilm::where('publish', 1)->orderby('name')->get();
        $footer = Ads::where('type', 'footer')->first();
        $page_list_top_page = Ads::where('slug', 'page-list-top-page')->where('publish', 1)->where('type', 'ads')->first();
        $page_list_bottom_page = Ads::where('slug', 'page-list-bottom-page')->where('publish', 1)->where('type', 'ads')->first();

        $order = $request->order;
        $categoryid = $request->categoryid;
        $countryid = $request->countryid;
        $year = $request->year;
        $season = $request->season;
        if (!$order && !$categoryid && !$year && !$countryid && !$season) {
            $data_cache = Cache::get('featured?page='.$request->page);
            if($data_cache){
                return view('frontend.theme_'.$setting->theme.'.pages.film-country')->with($data_cache);
            }else {
                $films = Film::where('publish', 1)->where('featured', 1)->orderby('created_at', 'desc')->paginate($setting->film_page);
                $data_cache = [
                    'page' => 'featured',
                    'setting' => $setting,
                    'meta' => $meta,
                    'lang' => $lang,
                    'lang_id' => $lang_id,
                    'language_list' => $language_list,
                    'genres' => $genres,
                    'country' => $country,
                    'type' => $type,
                    'results' => $films,
                    'title' => 'Featured Movies',
                    'footer' => (!empty($footer) ? $footer : null),
                    'page_list_top_page' => (!empty($page_list_top_page) ? $page_list_top_page : null),
                    'page_list_bottom_page' => (!empty($page_list_bottom_page) ? $page_list_bottom_page : null)
                ];
                Cache::put('featured?page='.$request->page, $data_cache, $setting->cache);
                return view('frontend.theme_'.$setting->theme.'.pages.film-country')->with($data_cache);
            }
        } else {
            $where = array();
            $where['films.featured'] = 1;
            $where['films.publish'] = 1;
            if ($year) {
                $where['films.release'] = $year;
            }
            if ($countryid) {
                $where['films.country_film_id'] = $countryid;
            }

            switch ($order) {
                case 'new':
                    if ($categoryid) {
                        $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                            ->where('category_film_relations.category_film_id', $categoryid)
                            ->where($where)
                            ->select('films.*')
                            ->orderby('created_at', 'desc')
                            ->get();
                    } else {
                        $films = Film::where($where)
                            ->orderby('created_at', 'desc')
                            ->get();
                    }
                    break;
                case 'year':
                    if ($categoryid) {
                        $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                            ->where('category_film_relations.category_film_id', $categoryid)
                            ->where($where)
                            ->select('films.*')
                            ->orderby('release', 'desc')
                            ->get();
                    } else {
                        $films = Film::where($where)
                            ->orderby('release', 'desc')
                            ->get();
                    }
                    break;
                case 'name':
                    if ($categoryid) {
                        $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                            ->where('category_film_relations.category_film_id', $categoryid)
                            ->where($where)
                            ->select('films.*')
                            ->orderby('name', 'asc')
                            ->get();
                    } else {
                        $films = Film::where($where)
                            ->orderby('name', 'asc')
                            ->get();
                    }
                    break;
                case 'imdb':
                    if ($categoryid) {
                        $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                            ->where('category_film_relations.category_film_id', $categoryid)
                            ->where($where)
                            ->select('films.*')
                            ->orderby('IMDB', 'desc')
                            ->get();
                    } else {
                        $films = Film::where($where)
                            ->orderby('IMDB', 'desc')
                            ->get();
                    }
                    break;
                case 'view':
                    if ($categoryid) {
                        $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                            ->where('category_film_relations.category_film_id', $categoryid)
                            ->where($where)
                            ->select('films.*')
                            ->orderby('view', 'desc')
                            ->get();
                    } else {
                        $films = Film::where($where)
                            ->orderby('view', 'desc')
                            ->get();
                    }
                    break;
                case '0':
                    if ($categoryid) {
                        $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                            ->where('category_film_relations.category_film_id', $categoryid)
                            ->where($where)
                            ->select('films.*')
                            ->orderby('created_at', 'asc')
                            ->get();
                    } else {
                        $films = Film::where($where)
                            ->orderby('created_at', 'asc')
                            ->get();
                    }
                    break;
            }

            if ($season) {
                $films_final = array();
                if (count($films) > 0) {
                    foreach ($films as $item) {
                        if ($item->day_release) {
                            $str_month = substr($item->day_release, 5, 2);
                            if ($str_month) {
                                switch ($season) {
                                    case 'spring':
                                        if ($str_month == '01' || $str_month == '02' || $str_month == '03') {
                                            $films_final[] = $item;
                                        }
                                        break;
                                    case 'summer':
                                        if ($str_month == '04' || $str_month == '05' || $str_month == '06') {
                                            $films_final[] = $item;
                                        }
                                        break;
                                    case 'fall':
                                        if ($str_month == '07' || $str_month == '08' || $str_month == '09') {
                                            $films_final[] = $item;
                                        }
                                        break;
                                    case 'winter':
                                        if ($str_month == '10' || $str_month == '11' || $str_month == '12') {
                                            $films_final[] = $item;
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }

            return view('frontend.theme_'.$setting->theme.'.pages.film-filter')->with([
                'page' => 'featured',
                'setting' => $setting,
                'meta' => $meta,
                'lang' => $lang,
                'lang_id' => $lang_id,
                'language_list' => $language_list,
                'genres' => $genres,
                'country' => $country,
                'type' => $type,
                'results' => (!empty($season) ? $films_final : $films),
                'title' => 'Featured Movies',
                'footer' => (!empty($footer) ? $footer : null),
                'order' => $order,
                'countryid' => $countryid,
                'year' => $year,
                'categoryid' => $categoryid,
                'season' => $season,
                'page_list_top_page' => (!empty($page_list_top_page) ? $page_list_top_page : null),
                'page_list_bottom_page' => (!empty($page_list_bottom_page) ? $page_list_bottom_page : null)
            ]);
        }
    }

    public function getMovies(Request $request)
    {
        $lang = App::getLocale();
        $lang_id = Session::get('lang_id');
        $language_list = Languages::get();
        $setting = Setting::first();
        $meta = array(
            'title' => 'Movies | cinema movies good | cinema movies top + 2017',
            'description' => 'Top cinema movies 2017, wathing cinema movies top on ' . str_replace('https://', '', str_replace('http://', '', url('/'))),
            'keywords' => 'Movies, cinema movies, movies 2017, top cinema movies',
            'images' => url(Storage::url($setting->meta_images))
        );
        $genres = CategoryFilm::where('publish', 1)->orderby('name')->get();
        $country = CountryFilm::where('publish', 1)->orderby('name')->get();
        $type = TypeFilm::where('publish', 1)->orderby('name')->get();
        $footer = Ads::where('type', 'footer')->first();
        $page_list_top_page = Ads::where('slug', 'page-list-top-page')->where('publish', 1)->where('type', 'ads')->first();
        $page_list_bottom_page = Ads::where('slug', 'page-list-bottom-page')->where('publish', 1)->where('type', 'ads')->first();

        $order = $request->order;
        $categoryid = $request->categoryid;
        $countryid = $request->countryid;
        $year = $request->year;
        $season = $request->season;
        if (!$order && !$categoryid && !$year && !$countryid && !$season) {
            $data_cache = Cache::get('movies?page='.$request->page);
            if($data_cache){
                return view('frontend.theme_'.$setting->theme.'.pages.film-country')->with($data_cache);
            }else {
                $films = Film::where('publish', 1)->where('movie_series', 0)->orderby('created_at', 'desc')->paginate($setting->film_page);
                $data_cache = [
                    'page' => 'type',
                    'setting' => $setting,
                    'meta' => $meta,
                    'lang' => $lang,
                    'lang_id' => $lang_id,
                    'language_list' => $language_list,
                    'genres' => $genres,
                    'country' => $country,
                    'type' => $type,
                    'results' => $films,
                    'title' => 'All Movies',
                    'footer' => (!empty($footer) ? $footer : null),
                    'page_list_top_page' => (!empty($page_list_top_page) ? $page_list_top_page : null),
                    'page_list_bottom_page' => (!empty($page_list_bottom_page) ? $page_list_bottom_page : null)
                ];
                Cache::put('movies?page='.$request->page, $data_cache, $setting->cache);
                return view('frontend.theme_'.$setting->theme.'.pages.film-country')->with($data_cache);
            }
        } else {
            $where = array();
            $where['films.movie_series'] = 0;
            $where['films.publish'] = 1;
            if ($year) {
                $where['films.release'] = $year;
            }
            if ($countryid) {
                $where['films.country_film_id'] = $countryid;
            }

            switch ($order) {
                case 'new':
                    if ($categoryid) {
                        $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                            ->where('category_film_relations.category_film_id', $categoryid)
                            ->where($where)
                            ->select('films.*')
                            ->orderby('created_at', 'desc')
                            ->get();
                    } else {
                        $films = Film::where($where)
                            ->orderby('created_at', 'desc')
                            ->get();
                    }
                    break;
                case 'year':
                    if ($categoryid) {
                        $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                            ->where('category_film_relations.category_film_id', $categoryid)
                            ->where($where)
                            ->select('films.*')
                            ->orderby('release', 'desc')
                            ->get();
                    } else {
                        $films = Film::where($where)
                            ->orderby('release', 'desc')
                            ->get();
                    }
                    break;
                case 'name':
                    if ($categoryid) {
                        $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                            ->where('category_film_relations.category_film_id', $categoryid)
                            ->where($where)
                            ->select('films.*')
                            ->orderby('name', 'asc')
                            ->get();
                    } else {
                        $films = Film::where($where)
                            ->orderby('name', 'asc')
                            ->get();
                    }
                    break;
                case 'imdb':
                    if ($categoryid) {
                        $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                            ->where('category_film_relations.category_film_id', $categoryid)
                            ->where($where)
                            ->select('films.*')
                            ->orderby('IMDB', 'desc')
                            ->get();
                    } else {
                        $films = Film::where($where)
                            ->orderby('IMDB', 'desc')
                            ->get();
                    }
                    break;
                case 'view':
                    if ($categoryid) {
                        $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                            ->where('category_film_relations.category_film_id', $categoryid)
                            ->where($where)
                            ->select('films.*')
                            ->orderby('view', 'desc')
                            ->get();
                    } else {
                        $films = Film::where($where)
                            ->orderby('view', 'desc')
                            ->get();
                    }
                    break;
                case '0':
                    if ($categoryid) {
                        $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                            ->where('category_film_relations.category_film_id', $categoryid)
                            ->where($where)
                            ->select('films.*')
                            ->orderby('created_at', 'asc')
                            ->get();
                    } else {
                        $films = Film::where($where)
                            ->orderby('created_at', 'asc')
                            ->get();
                    }
                    break;
            }

            if ($season) {
                $films_final = array();
                if (count($films) > 0) {
                    foreach ($films as $item) {
                        if ($item->day_release) {
                            $str_month = substr($item->day_release, 5, 2);
                            if ($str_month) {
                                switch ($season) {
                                    case 'spring':
                                        if ($str_month == '01' || $str_month == '02' || $str_month == '03') {
                                            $films_final[] = $item;
                                        }
                                        break;
                                    case 'summer':
                                        if ($str_month == '04' || $str_month == '05' || $str_month == '06') {
                                            $films_final[] = $item;
                                        }
                                        break;
                                    case 'fall':
                                        if ($str_month == '07' || $str_month == '08' || $str_month == '09') {
                                            $films_final[] = $item;
                                        }
                                        break;
                                    case 'winter':
                                        if ($str_month == '10' || $str_month == '11' || $str_month == '12') {
                                            $films_final[] = $item;
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }

            return view('frontend.theme_'.$setting->theme.'.pages.film-filter')->with([
                'page' => 'type',
                'setting' => $setting,
                'meta' => $meta,
                'lang' => $lang,
                'lang_id' => $lang_id,
                'language_list' => $language_list,
                'genres' => $genres,
                'country' => $country,
                'type' => $type,
                'results' => (!empty($season) ? $films_final : $films),
                'title' => 'Movies',
                'footer' => (!empty($footer) ? $footer : null),
                'order' => $order,
                'countryid' => $countryid,
                'year' => $year,
                'categoryid' => $categoryid,
                'season' => $season,
                'page_list_top_page' => (!empty($page_list_top_page) ? $page_list_top_page : null),
                'page_list_bottom_page' => (!empty($page_list_bottom_page) ? $page_list_bottom_page : null)
            ]);
        }
    }

    public function getTVSeries(Request $request)
    {
        $lang = App::getLocale();
        $lang_id = Session::get('lang_id');
        $language_list = Languages::get();
        $setting = Setting::first();
        $meta = array(
            'title' => 'Tv-series films | good Tv-series films | Tv-series films top + 2017',
            'description' => 'Top Tv-series films 2017, wathing Tv-series films top on ' . str_replace('https://', '', str_replace('http://', '', url('/'))),
            'keywords' => 'Tv-series, Tv-series films, Tv-series films 2017, top Tv-series',
            'images' => url(Storage::url($setting->meta_images))
        );
        $genres = CategoryFilm::where('publish', 1)->orderby('name')->get();
        $country = CountryFilm::where('publish', 1)->orderby('name')->get();
        $type = TypeFilm::where('publish', 1)->orderby('name')->get();
        $footer = Ads::where('type', 'footer')->first();
        $page_list_top_page = Ads::where('slug', 'page-list-top-page')->where('publish', 1)->where('type', 'ads')->first();
        $page_list_bottom_page = Ads::where('slug', 'page-list-bottom-page')->where('publish', 1)->where('type', 'ads')->first();

        $order = $request->order;
        $categoryid = $request->categoryid;
        $countryid = $request->countryid;
        $year = $request->year;
        $season = $request->season;
        if (!$order && !$categoryid && !$year && !$countryid && !$season) {
            $data_cache = Cache::get('tv_series?page='.$request->page);
            if($data_cache){
                return view('frontend.theme_'.$setting->theme.'.pages.film-country')->with($data_cache);
            }else {
                $films = Film::where('publish', 1)->where('movie_series', 1)->orderby('created_at', 'desc')->paginate($setting->film_page);
                $data_cache = [
                    'page' => 'type',
                    'setting' => $setting,
                    'meta' => $meta,
                    'lang' => $lang,
                    'lang_id' => $lang_id,
                    'language_list' => $language_list,
                    'genres' => $genres,
                    'country' => $country,
                    'type' => $type,
                    'results' => $films,
                    'title' => 'Tv series',
                    'footer' => (!empty($footer) ? $footer : null),
                    'page_list_top_page' => (!empty($page_list_top_page) ? $page_list_top_page : null),
                    'page_list_bottom_page' => (!empty($page_list_bottom_page) ? $page_list_bottom_page : null)
                ];
                Cache::put('tv_series?page='.$request->page, $data_cache, $setting->cache);
                return view('frontend.theme_'.$setting->theme.'.pages.film-country')->with($data_cache);
            }
        } else {
            $where = array();
            $where['films.movie_series'] = 1;
            $where['films.publish'] = 1;
            if ($year) {
                $where['films.release'] = $year;
            }
            if ($countryid) {
                $where['films.country_film_id'] = $countryid;
            }

            switch ($order) {
                case 'new':
                    if ($categoryid) {
                        $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                            ->where('category_film_relations.category_film_id', $categoryid)
                            ->where($where)
                            ->select('films.*')
                            ->orderby('created_at', 'desc')
                            ->get();
                    } else {
                        $films = Film::where($where)
                            ->orderby('created_at', 'desc')
                            ->get();
                    }
                    break;
                case 'year':
                    if ($categoryid) {
                        $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                            ->where('category_film_relations.category_film_id', $categoryid)
                            ->where($where)
                            ->select('films.*')
                            ->orderby('release', 'desc')
                            ->get();
                    } else {
                        $films = Film::where($where)
                            ->orderby('release', 'desc')
                            ->get();
                    }
                    break;
                case 'name':
                    if ($categoryid) {
                        $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                            ->where('category_film_relations.category_film_id', $categoryid)
                            ->where($where)
                            ->select('films.*')
                            ->orderby('name', 'asc')
                            ->get();
                    } else {
                        $films = Film::where($where)
                            ->orderby('name', 'asc')
                            ->get();
                    }
                    break;
                case 'imdb':
                    if ($categoryid) {
                        $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                            ->where('category_film_relations.category_film_id', $categoryid)
                            ->where($where)
                            ->select('films.*')
                            ->orderby('IMDB', 'desc')
                            ->get();
                    } else {
                        $films = Film::where($where)
                            ->orderby('IMDB', 'desc')
                            ->get();
                    }
                    break;
                case 'view':
                    if ($categoryid) {
                        $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                            ->where('category_film_relations.category_film_id', $categoryid)
                            ->where($where)
                            ->select('films.*')
                            ->orderby('view', 'desc')
                            ->get();
                    } else {
                        $films = Film::where($where)
                            ->orderby('view', 'desc')
                            ->get();
                    }
                    break;
                case '0':
                    if ($categoryid) {
                        $films = Film::join('category_film_relations', 'category_film_relations.film_id', 'films.id')
                            ->where('category_film_relations.category_film_id', $categoryid)
                            ->where($where)
                            ->select('films.*')
                            ->orderby('created_at', 'asc')
                            ->get();
                    } else {
                        $films = Film::where($where)
                            ->orderby('created_at', 'asc')
                            ->get();
                    }
                    break;
            }

            if ($season) {
                $films_final = array();
                if (count($films) > 0) {
                    foreach ($films as $item) {
                        if ($item->day_release) {
                            $str_month = substr($item->day_release, 5, 2);
                            if ($str_month) {
                                switch ($season) {
                                    case 'spring':
                                        if ($str_month == '01' || $str_month == '02' || $str_month == '03') {
                                            $films_final[] = $item;
                                        }
                                        break;
                                    case 'summer':
                                        if ($str_month == '04' || $str_month == '05' || $str_month == '06') {
                                            $films_final[] = $item;
                                        }
                                        break;
                                    case 'fall':
                                        if ($str_month == '07' || $str_month == '08' || $str_month == '09') {
                                            $films_final[] = $item;
                                        }
                                        break;
                                    case 'winter':
                                        if ($str_month == '10' || $str_month == '11' || $str_month == '12') {
                                            $films_final[] = $item;
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }

            return view('frontend.theme_'.$setting->theme.'.pages.film-filter')->with([
                'page' => 'type',
                'setting' => $setting,
                'meta' => $meta,
                'lang' => $lang,
                'lang_id' => $lang_id,
                'language_list' => $language_list,
                'genres' => $genres,
                'country' => $country,
                'type' => $type,
                'results' => (!empty($season) ? $films_final : $films),
                'title' => 'Tv series',
                'footer' => (!empty($footer) ? $footer : null),
                'order' => $order,
                'countryid' => $countryid,
                'year' => $year,
                'categoryid' => $categoryid,
                'season' => $season,
                'page_list_top_page' => (!empty($page_list_top_page) ? $page_list_top_page : null),
                'page_list_bottom_page' => (!empty($page_list_bottom_page) ? $page_list_bottom_page : null)
            ]);
        }
    }

    public function getListType($slug, Request $request)
    {
        $type_film = TypeFilm::where('slug', $slug)->first();
        $lang = App::getLocale();
        $lang_id = Session::get('lang_id');
        $language_list = Languages::get();
        $setting = Setting::first();
        $meta = array(
            'title' => ($type_film->title ? $type_film->title : $setting->title),
            'description' => ($type_film->descriptions ? $type_film->descriptions : $setting->description),
            'keywords' => ($type_film->keywords ? $type_film->keywords : $setting->keywords),
            'images' => url(Storage::url($setting->meta_images))
        );
        $genres = CategoryFilm::where('publish', 1)->orderby('name')->get();
        $country = CountryFilm::where('publish', 1)->orderby('name')->get();
        $type = TypeFilm::where('publish', 1)->orderby('name')->get();
        $footer = Ads::where('type', 'footer')->first();
        $page_list_top_page = Ads::where('slug', 'page-list-top-page')->where('publish', 1)->where('type', 'ads')->first();
        $page_list_bottom_page = Ads::where('slug', 'page-list-bottom-page')->where('publish', 1)->where('type', 'ads')->first();

        $order = $request->order;
        $countryid = $request->countryid;
        $year = $request->year;
        $season = $request->season;
        if (!$order && !$countryid && !$year && !$season) {
            $data_cache = Cache::get('type_'.$type_film->id.'?page='.$request->page);
            if($data_cache){
                return view('frontend.theme_'.$setting->theme.'.pages.film-country')->with($data_cache);
            }else {
                $films = Film::join('type_film_relations', 'type_film_relations.film_id', 'films.id')
                    ->where('type_film_relations.type_film_id', $type_film->id)
                    ->where('films.publish', 1)
                    ->select('films.*')
                    ->orderby('films.created_at', 'desc')
                    ->paginate($setting->film_page);
                $data_cache = [
                    'page' => 'type',
                    'setting' => $setting,
                    'meta' => $meta,
                    'lang' => $lang,
                    'lang_id' => $lang_id,
                    'language_list' => $language_list,
                    'genres' => $genres,
                    'country' => $country,
                    'type' => $type,
                    'results' => $films,
                    'title' => 'Type ' . $type_film->name,
                    'footer' => (!empty($footer) ? $footer : null),
                    'page_list_top_page' => (!empty($page_list_top_page) ? $page_list_top_page : null),
                    'page_list_bottom_page' => (!empty($page_list_bottom_page) ? $page_list_bottom_page : null)
                ];
                Cache::put('type_'.$type_film->id.'?page='.$request->page, $data_cache, $setting->id);
                return view('frontend.theme_'.$setting->theme.'.pages.film-country')->with($data_cache);
            }
        } else {
            $where = array();
            $where['films.publish'] = 1;
            if ($year) {
                $where['films.release'] = $year;
            }
            if ($countryid) {
                $where['films.country_film_id'] = $countryid;
            }

            switch ($order) {
                case 'new':
                    $films = Film::join('type_film_relations', 'type_film_relations.film_id', 'films.id')
                        ->where('type_film_relations.type_film_id', $type_film->id)
                        ->where($where)
                        ->select('films.*')
                        ->orderby('created_at', 'desc')
                        ->get();
                    break;
                case 'year':
                    $films = Film::join('type_film_relations', 'type_film_relations.film_id', 'films.id')
                        ->where('type_film_relations.type_film_id', $type_film->id)
                        ->where($where)
                        ->select('films.*')
                        ->orderby('release', 'desc')
                        ->get();
                    break;
                case 'name':
                    $films = Film::join('type_film_relations', 'type_film_relations.film_id', 'films.id')
                        ->where('type_film_relations.type_film_id', $type_film->id)
                        ->where($where)
                        ->select('films.*')
                        ->orderby('name', 'asc')
                        ->get();
                    break;
                case 'imdb':
                    $films = Film::join('type_film_relations', 'type_film_relations.film_id', 'films.id')
                        ->where('type_film_relations.type_film_id', $type_film->id)
                        ->where($where)
                        ->select('films.*')
                        ->orderby('IMDB', 'desc')
                        ->get();
                    break;
                case 'view':
                    $films = Film::join('type_film_relations', 'type_film_relations.film_id', 'films.id')
                        ->where('type_film_relations.type_film_id', $type_film->id)
                        ->where($where)
                        ->select('films.*')
                        ->orderby('view', 'desc')
                        ->get();
                    break;
                case '0':
                    $films = Film::join('type_film_relations', 'type_film_relations.film_id', 'films.id')
                        ->where('type_film_relations.type_film_id', $type_film->id)
                        ->where($where)
                        ->select('films.*')
                        ->orderby('created_at', 'asc')
                        ->get();
                    break;
            }

            if ($season) {
                $films_final = array();
                if (count($films) > 0) {
                    foreach ($films as $item) {
                        if ($item->day_release) {
                            $str_month = substr($item->day_release, 5, 2);
                            if ($str_month) {
                                switch ($season) {
                                    case 'spring':
                                        if ($str_month == '01' || $str_month == '02' || $str_month == '03') {
                                            $films_final[] = $item;
                                        }
                                        break;
                                    case 'summer':
                                        if ($str_month == '04' || $str_month == '05' || $str_month == '06') {
                                            $films_final[] = $item;
                                        }
                                        break;
                                    case 'fall':
                                        if ($str_month == '07' || $str_month == '08' || $str_month == '09') {
                                            $films_final[] = $item;
                                        }
                                        break;
                                    case 'winter':
                                        if ($str_month == '10' || $str_month == '11' || $str_month == '12') {
                                            $films_final[] = $item;
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }

            return view('frontend.theme_'.$setting->theme.'.pages.film-filter')->with([
                'page' => 'type',
                'setting' => $setting,
                'meta' => $meta,
                'lang' => $lang,
                'lang_id' => $lang_id,
                'language_list' => $language_list,
                'genres' => $genres,
                'country' => $country,
                'type' => $type,
                'results' => (!empty($season) ? $films_final : $films),
                'title' => 'Type ' . $type_film->name,
                'footer' => (!empty($footer) ? $footer : null),
                'order' => $order,
                'countryid' => $countryid,
                'year' => $year,
                'categoryid' => 0,
                'season' => $season,
                'page_list_top_page' => (!empty($page_list_top_page) ? $page_list_top_page : null),
                'page_list_bottom_page' => (!empty($page_list_bottom_page) ? $page_list_bottom_page : null)
            ]);
        }
    }

    public function getTopIMDB($slug, Request $request)
    {
        $setting = Setting::first();
        $data_cache = Cache::get('topimdb_'.$slug.'?page='.($request->has('page')?$request->page:''));
        if($data_cache){
            return view('frontend.theme_'.$setting->theme.'.pages.film-top-imdb')->with($data_cache);
        }else {
            $lang = App::getLocale();
            $lang_id = Session::get('lang_id');
            $language_list = Languages::get();
            $meta = array(
                'title' => 'Top IMDB | Top films imdb | Top cinema movie on IMDB in 2017',
                'description' => 'Top imdb, Top films imdb, Top cinema on imdb, top tv-series on IDMB',
                'keywords' => 'Top imdb, Top films imdb, Top cinema on imdb, top tv-series on IDMB',
                'images' => url(Storage::url($setting->meta_images))
            );
            $genres = CategoryFilm::where('publish', 1)->orderby('name')->get();
            $country = CountryFilm::where('publish', 1)->orderby('name')->get();
            $type = TypeFilm::where('publish', 1)->orderby('name')->get();
            if ($slug == 'all') {
                $films = Film::where('publish', 1)->orderby('IMDB', 'desc')->paginate($setting->film_page);
            } elseif ($slug == 'movies') {
                $films = Film::where('publish', 1)->where('movie_series', 0)->orderby('IMDB', 'desc')->paginate($setting->film_page);
            } elseif ($slug == 'tv-series') {
                $films = Film::where('publish', 1)->where('movie_series', 1)->orderby('IMDB', 'desc')->paginate($setting->film_page);
            }
            $footer = Ads::where('type', 'footer')->first();
            $page_list_top_page = Ads::where('slug', 'page-list-top-page')->where('publish', 1)->where('type', 'ads')->first();
            $page_list_bottom_page = Ads::where('slug', 'page-list-bottom-page')->where('publish', 1)->where('type', 'ads')->first();
            $data_cache = [
                'page' => 'top-imdb',
                'sub_active' => $slug,
                'setting' => $setting,
                'meta' => $meta,
                'lang' => $lang,
                'lang_id' => $lang_id,
                'language_list' => $language_list,
                'genres' => $genres,
                'country' => $country,
                'type' => $type,
                'results' => $films,
                'title' => 'Top IMDB',
                'footer' => (!empty($footer) ? $footer : null),
                'page_list_top_page' => (!empty($page_list_top_page) ? $page_list_top_page : null),
                'page_list_bottom_page' => (!empty($page_list_bottom_page) ? $page_list_bottom_page : null)
            ];
            Cache::put('topimdb_'.$slug.'?page='.($request->has('page')?$request->page:''), $data_cache, $setting->cache);
            return view('frontend.theme_'.$setting->theme.'.pages.film-top-imdb')->with($data_cache);
        }
    }

    public function getActors($slug)
    {
        $setting = Setting::first();
        $data_cache = Cache::get('actor_'.$slug);
        if($data_cache){
            return view('frontend.theme_'.$setting->theme.'.pages.film-genres')->with($data_cache);
        }else {
            $actor = Actor::where('slug', $slug)->first();
            $lang = App::getLocale();
            $lang_id = Session::get('lang_id');
            $language_list = Languages::get();
            $meta = array(
                'title' => $setting->title,
                'description' => $setting->description,
                'keywords' => $setting->keywords,
                'images' => url(Storage::url($setting->meta_images))
            );
            $genres = CategoryFilm::where('publish', 1)->orderby('name')->get();
            $country = CountryFilm::where('publish', 1)->orderby('name')->get();
            $type = TypeFilm::where('publish', 1)->orderby('name')->get();
            $films = ActorRelation::where('actor_id', $actor->id)->with('films')->paginate($setting->film_page);
            $footer = Ads::where('type', 'footer')->first();
            $page_list_top_page = Ads::where('slug', 'page-list-top-page')->where('publish', 1)->where('type', 'ads')->first();
            $page_list_bottom_page = Ads::where('slug', 'page-list-bottom-page')->where('publish', 1)->where('type', 'ads')->first();
            $data_cache = [
                'page' => 'home',
                'setting' => $setting,
                'meta' => $meta,
                'lang' => $lang,
                'lang_id' => $lang_id,
                'language_list' => $language_list,
                'genres' => $genres,
                'country' => $country,
                'type' => $type,
                'results' => $films,
                'title' => $actor->name,
                'footer' => (!empty($footer) ? $footer : null),
                'page_list_top_page' => (!empty($page_list_top_page) ? $page_list_top_page : null),
                'page_list_bottom_page' => (!empty($page_list_bottom_page) ? $page_list_bottom_page : null)
            ];
            Cache::put('actor_'.$slug, $data_cache, $setting->cache);
            return view('frontend.theme_'.$setting->theme.'.pages.film-genres')->with($data_cache);
        }
    }

    public function getDirectors($slug)
    {
        $setting = Setting::first();
        $data_cache = Cache::get('director_'.$slug);
        if($data_cache){
            return view('frontend.theme_'.$setting->theme.'.pages.film-genres')->with($data_cache);
        }else {
            $director = Director::where('slug', $slug)->first();
            $lang = App::getLocale();
            $lang_id = Session::get('lang_id');
            $language_list = Languages::get();
            $meta = array(
                'title' => $setting->title,
                'description' => $setting->description,
                'keywords' => $setting->keywords,
                'images' => url(Storage::url($setting->meta_images))
            );
            $genres = CategoryFilm::where('publish', 1)->orderby('name')->get();
            $country = CountryFilm::where('publish', 1)->orderby('name')->get();
            $type = TypeFilm::where('publish', 1)->orderby('name')->get();
            $films = DirectorRelation::where('director_id', $director->id)->with('films')->paginate($setting->film_page);
            $footer = Ads::where('type', 'footer')->first();
            $page_list_top_page = Ads::where('slug', 'page-list-top-page')->where('publish', 1)->where('type', 'ads')->first();
            $page_list_bottom_page = Ads::where('slug', 'page-list-bottom-page')->where('publish', 1)->where('type', 'ads')->first();
            $data_cache = [
                'page' => 'home',
                'setting' => $setting,
                'meta' => $meta,
                'lang' => $lang,
                'lang_id' => $lang_id,
                'language_list' => $language_list,
                'genres' => $genres,
                'country' => $country,
                'type' => $type,
                'results' => $films,
                'title' => $director->name,
                'footer' => (!empty($footer) ? $footer : null),
                'page_list_top_page' => (!empty($page_list_top_page) ? $page_list_top_page : null),
                'page_list_bottom_page' => (!empty($page_list_bottom_page) ? $page_list_bottom_page : null)
            ];
            Cache::put('director_'.$slug, $data_cache, $setting->cache);
            return view('frontend.theme_'.$setting->theme.'.pages.film-genres')->with($data_cache);
        }
    }

    public function getPlaylistFilm()
    {
        $setting = Setting::first();
        $data_cache = Cache::get('playlist');
        if($data_cache){
            return view('frontend.theme_'.$setting->theme.'.pages.film-playlist')->with($data_cache);
        }else {
            $lang = App::getLocale();
            $lang_id = Session::get('lang_id');
            $language_list = Languages::get();
            $meta = array(
                'title' => $setting->title,
                'description' => $setting->description,
                'keywords' => $setting->keywords,
                'images' => url(Storage::url($setting->meta_images))
            );
            $genres = CategoryFilm::where('publish', 1)->orderby('name')->get();
            $country = CountryFilm::where('publish', 1)->orderby('name')->get();
            $type = TypeFilm::where('publish', 1)->orderby('name')->get();
            $playlist = Playlist::where('publish', 1)->paginate($setting->film_page);
            $footer = Ads::where('type', 'footer')->first();
            $page_list_top_page = Ads::where('slug', 'page-list-top-page')->where('publish', 1)->where('type', 'ads')->first();
            $page_list_bottom_page = Ads::where('slug', 'page-list-bottom-page')->where('publish', 1)->where('type', 'ads')->first();
            $data_cache = [
                'page' => 'playlist',
                'setting' => $setting,
                'meta' => $meta,
                'lang' => $lang,
                'lang_id' => $lang_id,
                'language_list' => $language_list,
                'genres' => $genres,
                'country' => $country,
                'type' => $type,
                'results' => $playlist,
                'title' => 'Playlist Film',
                'footer' => (!empty($footer) ? $footer : null),
                'page_list_top_page' => (!empty($page_list_top_page) ? $page_list_top_page : null),
                'page_list_bottom_page' => (!empty($page_list_bottom_page) ? $page_list_bottom_page : null)
            ];
            Cache::put('playlist', $data_cache, $setting->cache);
            return view('frontend.theme_'.$setting->theme.'.pages.film-playlist')->with($data_cache);
        }
    }

    public function getPlaylistDetail($slug)
    {
        $setting = Setting::first();
        $data_cache = Cache::get('playlist_'.$slug);
        if($data_cache){
            return view('frontend.theme_'.$setting->theme.'.pages.film-genres')->with($data_cache);
        }else {
            $lang = App::getLocale();
            $lang_id = Session::get('lang_id');
            $language_list = Languages::get();
            $genres = CategoryFilm::where('publish', 1)->orderby('name')->get();
            $country = CountryFilm::where('publish', 1)->orderby('name')->get();
            $type = TypeFilm::where('publish', 1)->orderby('name')->get();

            $id = substr($slug, strrpos($slug, '-') + 1);
            $playlist_item = Playlist::find($id);
            $meta = array(
                'title' => $playlist_item->title,
                'description' => $playlist_item->descriptions,
                'keywords' => $playlist_item->keywords,
                'images' => ($playlist_item->images ? url(Storage::url($playlist_item->images)) : $playlist_item->images_link)
            );
            $tag_films_relation = TagFilmRelation::where('film_id', $id)->where('type', 'playlist')->first();
            if ($tag_films_relation) {
                $films = TagFilmRelation::where('tag_id', $tag_films_relation->tag_id)->where('type', 'tag_film')->paginate($setting->film_page);
            }
            $footer = Ads::where('type', 'footer')->first();
            $page_list_top_page = Ads::where('slug', 'page-list-top-page')->where('publish', 1)->where('type', 'ads')->first();
            $page_list_bottom_page = Ads::where('slug', 'page-list-bottom-page')->where('publish', 1)->where('type', 'ads')->first();
            $data_cache = [
                'page' => 'playlist',
                'setting' => $setting,
                'meta' => $meta,
                'lang' => $lang,
                'lang_id' => $lang_id,
                'language_list' => $language_list,
                'genres' => $genres,
                'country' => $country,
                'type' => $type,
                'results' => (!empty($films) ? $films : ''),
                'title' => 'Playlist ' . $playlist_item->name,
                'footer' => (!empty($footer) ? $footer : null),
                'page_list_top_page' => (!empty($page_list_top_page) ? $page_list_top_page : null),
                'page_list_bottom_page' => (!empty($page_list_bottom_page) ? $page_list_bottom_page : null)
            ];
            Cache::put('playlist_'.$slug, $data_cache, $setting->cache);
            return view('frontend.theme_'.$setting->theme.'.pages.film-genres')->with($data_cache);
        }
    }

    public function getProducers($slug)
    {
        $setting = Setting::first();
        $data_cache = Cache::get('producer_'.$slug);
        if($data_cache){
            return view('frontend.theme_'.$setting->theme.'.pages.film-country')->with($data_cache);
        }else {
            $lang = App::getLocale();
            $lang_id = Session::get('lang_id');
            $language_list = Languages::get();
            $meta = array(
                'title' => $setting->title,
                'description' => $setting->description,
                'keywords' => $setting->keywords,
                'images' => url(Storage::url($setting->meta_images))
            );
            $genres = CategoryFilm::where('publish', 1)->orderby('name')->get();
            $country = CountryFilm::where('publish', 1)->orderby('name')->get();
            $type = TypeFilm::where('publish', 1)->orderby('name')->get();
            $id = substr($slug, strrpos($slug, '-') + 1);
            $films = Producer::join('films', 'films.producer_id', '=', 'producers.id')
                ->where('producers.id', $id)
                ->select('films.*', 'producers.name as producer_name')
                ->paginate($setting->film_page);

            $footer = Ads::where('type', 'footer')->first();
            $page_list_top_page = Ads::where('slug', 'page-list-top-page')->where('publish', 1)->where('type', 'ads')->first();
            $page_list_bottom_page = Ads::where('slug', 'page-list-bottom-page')->where('publish', 1)->where('type', 'ads')->first();
            $data_cache = [
                'page' => '',
                'setting' => $setting,
                'meta' => $meta,
                'lang' => $lang,
                'lang_id' => $lang_id,
                'language_list' => $language_list,
                'genres' => $genres,
                'country' => $country,
                'type' => $type,
                'results' => (!empty($films) ? $films : ''),
                'title' => (!empty($films) ? 'Producer ' : 'Producer'),
                'footer' => (!empty($footer) ? $footer : null),
                'page_list_top_page' => (!empty($page_list_top_page) ? $page_list_top_page : null),
                'page_list_bottom_page' => (!empty($page_list_bottom_page) ? $page_list_bottom_page : null)
            ];
            Cache::put('producer_'.$slug, $data_cache, $setting->cache);
            return view('frontend.theme_'.$setting->theme.'.pages.film-country')->with($data_cache);
        }
    }

    public function getFilmLibrary(Request $request)
    {
        $setting = Setting::first();
        $q = $request->q;
        $data_cache = Cache::get('film_library_'.$q);
        if($data_cache){
            return view('frontend.theme_'.$setting->theme.'.pages.film-library')->with($data_cache);
        }else {
            $lang = App::getLocale();
            $lang_id = Session::get('lang_id');
            $language_list = Languages::get();
            $meta = array(
                'title' => 'List best A-Z movies free online, hot A-Z list movies 2017, top views A-Z list colection',
                'description' => 'Watch HD Movies Online For Free and Download the latest movies without Registration at ' . str_replace('https://', '', str_replace('http://', '', url('/'))),
                'keywords' => 'watch movies online, free movies online, free hd movies, full hd movies, best site for movies, watch free movies online, streaming free movies, full hd movies, free movies, cinema movies, movies in theaters now, free tv series, free anime series, putlocker, megashare9, megashare, hdmovie14, project free tv, 123movies, primewire, letmewatchthis, sockshare',
                'images' => url(Storage::url($setting->meta_images))
            );
            $genres = CategoryFilm::where('publish', 1)->orderby('name')->get();
            $country = CountryFilm::where('publish', 1)->orderby('name')->get();
            $type = TypeFilm::where('publish', 1)->orderby('name')->get();
            $footer = Ads::where('type', 'footer')->first();
            $page_list_top_page = Ads::where('slug', 'page-list-top-page')->where('publish', 1)->where('type', 'ads')->first();
            $page_list_bottom_page = Ads::where('slug', 'page-list-bottom-page')->where('publish', 1)->where('type', 'ads')->first();
            if ($q && $q != '0-9') {
                $films = Film::where('name', 'like', "$q%")->where('publish', 1)->orderby('name')->paginate($setting->film_page);
            } else {
                $films = Film::where('name', 'like', '0%')
                    ->orWhere('name', 'like', '1%')
                    ->orWhere('name', 'like', '2%')
                    ->orWhere('name', 'like', '3%')
                    ->orWhere('name', 'like', '4%')
                    ->orWhere('name', 'like', '5%')
                    ->orWhere('name', 'like', '6%')
                    ->orWhere('name', 'like', '7%')
                    ->orWhere('name', 'like', '8%')
                    ->orWhere('name', 'like', '9%')
                    ->orderby('name')
                    ->paginate($setting->film_page);
            }
            $data_cache = [
                'page' => 'library',
                'setting' => $setting,
                'meta' => $meta,
                'lang' => $lang,
                'lang_id' => $lang_id,
                'language_list' => $language_list,
                'genres' => $genres,
                'country' => $country,
                'type' => $type,
                'results' => $films,
                'title' => 'Movies By Letter',
                'q' => ($q ? $q : '0-9'),
                'footer' => (!empty($footer) ? $footer : null),
                'page_list_top_page' => (!empty($page_list_top_page) ? $page_list_top_page : null),
                'page_list_bottom_page' => (!empty($page_list_bottom_page) ? $page_list_bottom_page : null)
            ];
            Cache::put('film_library_'.$q, $data_cache, $setting->cache);
            return view('frontend.theme_'.$setting->theme.'.pages.film-library')->with($data_cache);
        }
    }

    public function getRankingFilm(Request $request)
    {
        $setting = Setting::first();
        $q = $request->q;
        $data_cache = Cache::get('film_ranking_'.$q.'?page='.$request->page);
        if($data_cache){
            return view('frontend.theme_'.$setting->theme.'.pages.film-ranking')->with($data_cache);
        }else {
            $lang = App::getLocale();
            $lang_id = Session::get('lang_id');
            $language_list = Languages::get();
            $meta = array(
                'title' => 'List best Ranking movies free online, hot Ranking movies 2017, top views Ranking movies colection',
                'description' => 'Watch HD Movies Online For Free and Download the latest movies without Registration at ' . str_replace('https://', '', str_replace('http://', '', url('/'))),
                'keywords' => 'Ranking movie online, free movies online, free hd movies, full hd movies, best site for movies, watch free movies online, gomovies,streaming free movies, full hd movies, free movies, cinema movies, movies in theaters now, free tv series, free anime series, putlocker, megashare9, megashare, hdmovie14, project free tv, 123movies, primewire, letmewatchthis, sockshare',
                'images' => url(Storage::url($setting->meta_images))
            );
            $genres = CategoryFilm::where('publish', 1)->orderby('name')->get();
            $country = CountryFilm::where('publish', 1)->orderby('name')->get();
            $type = TypeFilm::where('publish', 1)->orderby('name')->get();
            $footer = Ads::where('type', 'footer')->first();
            $page_list_top_page = Ads::where('slug', 'page-list-top-page')->where('publish', 1)->where('type', 'ads')->first();
            $page_list_bottom_page = Ads::where('slug', 'page-list-bottom-page')->where('publish', 1)->where('type', 'ads')->first();

            if (!$q) {
                $q = 'top-views';
            }
            switch ($q) {
                case 'top-views':
                    $films = Film::where('publish', 1)->orderby('view', 'desc')->paginate($setting->film_page);
                    break;
                case 'spring':
                    $y = $request->y;
                    if (!$y) {
                        $y = 2017;
                    }
                    $films = Film::where([
                        ['day_release', 'like', '%-03-%'],
                        ['release', '=', $y]
                    ])
                        ->orWhere([
                            ['day_release', 'like', '%-02-%'],
                            ['release', '=', $y]
                        ])
                        ->orWhere([
                            ['day_release', 'like', '%-01-%'],
                            ['release', '=', $y]
                        ])
                        ->orderby('view', 'desc')
                        ->paginate($setting->film_page);
                    break;
                case 'summer':
                    $y = $request->y;
                    if (!$y) {
                        $y = 2017;
                    }
                    $films = Film::where([
                        ['day_release', 'like', '%-04-%'],
                        ['release', '=', $y]
                    ])
                        ->orWhere([
                            ['day_release', 'like', '%-05-%'],
                            ['release', '=', $y]
                        ])
                        ->orWhere([
                            ['day_release', 'like', '%-06-%'],
                            ['release', '=', $y]
                        ])
                        ->orderby('view', 'desc')
                        ->paginate($setting->film_page);
                    break;
                case 'fall':
                    $y = $request->y;
                    if (!$y) {
                        $y = 2017;
                    }
                    $films = Film::where([
                        ['day_release', 'like', '%-07-%'],
                        ['release', '=', $y]
                    ])
                        ->orWhere([
                            ['day_release', 'like', '%-08-%'],
                            ['release', '=', $y]
                        ])
                        ->orWhere([
                            ['day_release', 'like', '%-08-%'],
                            ['release', '=', $y]
                        ])
                        ->orderby('view', 'desc')
                        ->paginate($setting->film_page);
                    break;
                case 'winter':
                    $y = $request->y;
                    if (!$y) {
                        $y = 2017;
                    }
                    $films = Film::where([
                        ['day_release', 'like', '%-10-%'],
                        ['release', '=', $y]
                    ])
                        ->orWhere([
                            ['day_release', 'like', '%-11-%'],
                            ['release', '=', $y]
                        ])
                        ->orWhere([
                            ['day_release', 'like', '%-12-%'],
                            ['release', '=', $y]
                        ])
                        ->orderby('view', 'desc')
                        ->paginate($setting->film_page);
                    break;
                case 'favorite':
                    $films_favorite = FavoriteFilm::select('film_id')->distinct('film_id')->get();
                    $films_list_id = array();
                    foreach ($films_favorite as $item) {
                        $films_list_id[] = $item->film_id;
                    }
                    $films = Film::whereIn('id', $films_list_id)->orderby('favorite_total', 'desc')->paginate($setting->film_page);
                    break;
                case 'year':
                    $y = $request->y;
                    if (!$y) {
                        $y = 2017;
                    }
                    $films = Film::where('release', $y)->orderby('view', 'desc')->paginate($setting->film_page);
            }
            $data_cache = [
                'page' => 'ranking',
                'setting' => $setting,
                'meta' => $meta,
                'lang' => $lang,
                'lang_id' => $lang_id,
                'language_list' => $language_list,
                'genres' => $genres,
                'country' => $country,
                'type' => $type,
                'results' => $films,
                'title' => 'Ranking Films',
                'q' => $q,
                'y' => (!empty($y) ? $y : 0),
                'footer' => (!empty($footer) ? $footer : null),
                'page_list_top_page' => (!empty($page_list_top_page) ? $page_list_top_page : null),
                'page_list_bottom_page' => (!empty($page_list_bottom_page) ? $page_list_bottom_page : null)
            ];
            Cache::put('film_ranking_'.$q.'?page='.$request->page, $data_cache, $setting->cache);
            return view('frontend.theme_'.$setting->theme.'.pages.film-ranking')->with($data_cache);
        }
    }

    public function getMovie($slug, Request $request){
        $lang = App::getLocale();
        $lang_id = Session::get('lang_id');
        $language_list = Languages::get();
        $setting = Setting::first();
        $genres = CategoryFilm::where('publish', 1)->orderby('name')->get();
        $country = CountryFilm::where('publish', 1)->orderby('name')->get();
        $type = TypeFilm::where('publish', 1)->orderby('name')->get();
        $id = substr($slug, strrpos($slug, '-') + 1);
        $film = Film::where('id', $id)->first();

        $meta = array(
            'title' => 'Watching ' . $film->name . ' on ' . str_replace('https://', '', str_replace('http://', '', url('/'))),
            'description' => str_replace('"', '&quot;', stripslashes($film->descriptions)),
            'keywords' => $film->keywords,
            'images' => ($film->images ? url(Storage::url($film->images)) : $film->images_link)
        );

        $films_random = array();
        $hagtag_check = TagFilmRelation::where('type', 'tag_film')->where('film_id', $film->id)->first();
        if ($hagtag_check) {
            $film_relation = TagFilmRelation::where('type', 'tag_film')->where('tag_id', $hagtag_check->tag_id)->where('id', '<>', $hagtag_check->id)->with('films')->get();
            if (count($film_relation) > 0) {
                $film_id = array();
                $film_id[] = $film->id;
                foreach ($film_relation as $item) {
                    $films_random[] = $item->films;
                    $film_id[] = $item->films->id;
                }
                $tmp = $setting->film_home - count($film_relation);
                $films_after = Film::whereNotIn('id', $film_id)->where('publish', 1)->inRandomOrder()->take($tmp)->get();
                foreach ($films_after as $item) {
                    $films_random[] = $item;
                }
            } else {
                $films_random = Film::where('id', '<>', $id)->where('publish', 1)->inRandomOrder()->take($setting->film_home)->get();
            }
        } else {
            $films_random = Film::where('id', '<>', $id)->where('publish', 1)->inRandomOrder()->take($setting->film_home)->get();
        }

        $watching_top_player = Ads::where('slug', 'watching-top-player')->where('publish', 1)->where('type', 'ads')->first();
        $watching_under_player = Ads::where('slug', 'watching-under-player')->where('publish', 1)->where('type', 'ads')->first();
        $watching_right_info = Ads::where('slug', 'watching-right-info')->where('publish', 1)->where('type', 'ads')->first();
        $watching_under_info = Ads::where('slug', 'watching-under-info')->where('publish', 1)->where('type', 'ads')->first();

        return view('frontend.theme_'.$setting->theme.'.pages.movie')->with([
            'page' => 'watch',
            'sub_active' => $slug,
            'setting' => $setting,
            'meta' => $meta,
            'lang' => $lang,
            'lang_id' => $lang_id,
            'language_list' => $language_list,
            'genres' => $genres,
            'country' => $country,
            'type' => $type,
            'film' => $film,
            'film_random' => $films_random,
            'p' => (!empty($p) ? $p : 1),
            's' => (!empty($s) ? $s : 0),
            'footer' => (!empty($footer) ? $footer : null),
            'watching_top_player' => (!empty($watching_top_player) ? $watching_top_player : null),
            'watching_under_player' => (!empty($watching_under_player) ? $watching_under_player : null),
            'watching_right_info' => (!empty($watching_right_info) ? $watching_right_info : null),
            'watching_under_info' => (!empty($watching_under_info) ? $watching_under_info : null)
        ]);
    }

    public function getWatch($slug, Request $request)
    {
        $lang = App::getLocale();
        $lang_id = Session::get('lang_id');
        $language_list = Languages::get();
        $setting = Setting::first();
        $genres = CategoryFilm::where('publish', 1)->orderby('name')->get();
        $country = CountryFilm::where('publish', 1)->orderby('name')->get();
        $type = TypeFilm::where('publish', 1)->orderby('name')->get();

        $id = substr($slug, strrpos($slug, '-') + 1);

        $film = Film::where('id', $id)->first();
        if ($film->movie_series) {
            $p = $request->p;
        }
        $meta = array(
            'title' => 'Watching ' . $film->name . ' on ' . str_replace('https://', '', str_replace('http://', '', url('/'))),
            'description' => str_replace('"', '&quot;', stripslashes($film->descriptions)),
            'keywords' => $film->keywords,
            'images' => ($film->images ? url(Storage::url($film->images)) : $film->images_link)
        );

        $s = $request->s;
        if ($s) {
            $server = ServerPlayFilm::find($s);
            if (!$server) {
                return redirect('/')->with(['flash_level' => 'danger', 'flash_message' => 'Server not found']);
            }
        }

        $footer = Ads::where('type', 'footer')->first();
        $subtitle = Session::get('subtitle_upload');

        if (isset($subtitle) && $subtitle['film_id'] == $id) {
            $subtitle_user = array();
            $subtitle_user[0]['file'] = url(Storage::url('user_subtitle/' . $subtitle['name']));
            $subtitle_user[0]['label'] = $subtitle['language'];
            $subtitle_user[0]['kind'] = 'captions';
            $subtitle_user[0]['default'] = true;
        } else {
            $subtitle_user = 0;
        }

        $films_random = array();
        $hagtag_check = TagFilmRelation::where('type', 'tag_film')->where('film_id', $film->id)->first();
        if ($hagtag_check) {
            $film_relation = TagFilmRelation::where('type', 'tag_film')->where('tag_id', $hagtag_check->tag_id)->where('id', '<>', $hagtag_check->id)->with('films')->get();
            if (count($film_relation) > 0) {
                $film_id = array();
                $film_id[] = $film->id;
                foreach ($film_relation as $item) {
                    $films_random[] = $item->films;
                    $film_id[] = $item->films->id;
                }
                $tmp = $setting->film_home - count($film_relation);
                $films_after = Film::whereNotIn('id', $film_id)->where('publish', 1)->inRandomOrder()->take($tmp)->get();
                foreach ($films_after as $item) {
                    $films_random[] = $item;
                }
            } else {
                $films_random = Film::where('id', '<>', $id)->where('publish', 1)->inRandomOrder()->take($setting->film_home)->get();
            }
        } else {
            $films_random = Film::where('id', '<>', $id)->where('publish', 1)->inRandomOrder()->take($setting->film_home)->get();
        }

        $watching_top_player = Ads::where('slug', 'watching-top-player')->where('publish', 1)->where('type', 'ads')->first();
        $watching_under_player = Ads::where('slug', 'watching-under-player')->where('publish', 1)->where('type', 'ads')->first();
        $watching_right_info = Ads::where('slug', 'watching-right-info')->where('publish', 1)->where('type', 'ads')->first();
        $watching_under_info = Ads::where('slug', 'watching-under-info')->where('publish', 1)->where('type', 'ads')->first();

        return view('frontend.theme_'.$setting->theme.'.pages.watch')->with([
            'page' => 'watch',
            'sub_active' => $slug,
            'setting' => $setting,
            'meta' => $meta,
            'lang' => $lang,
            'lang_id' => $lang_id,
            'language_list' => $language_list,
            'genres' => $genres,
            'country' => $country,
            'type' => $type,
            'film' => $film,
            'film_random' => $films_random,
            'p' => (!empty($p) ? $p : 1),
            's' => (!empty($s) ? $s : 0),
            'footer' => (!empty($footer) ? $footer : null),
            'subtitle_user' => json_encode($subtitle_user),
            'watching_top_player' => (!empty($watching_top_player) ? $watching_top_player : null),
            'watching_under_player' => (!empty($watching_under_player) ? $watching_under_player : null),
            'watching_right_info' => (!empty($watching_right_info) ? $watching_right_info : null),
            'watching_under_info' => (!empty($watching_under_info) ? $watching_under_info : null)
        ]);
    }

    public function getComingSoon($slug)
    {
        $setting = Setting::first();
//        $data_cache = Cache::get('coming_soon'.$slug);
//        if($data_cache){
//            return view('frontend.theme_'.$setting->theme.'.pages.coming-soon')->with($data_cache);
//        }else {
            $lang = App::getLocale();
            $lang_id = Session::get('lang_id');
            $language_list = Languages::get();

            $genres = CategoryFilm::where('publish', 1)->orderby('name')->get();
            $country = CountryFilm::where('publish', 1)->orderby('name')->get();
            $type = TypeFilm::where('publish', 1)->orderby('name')->get();

            $footer = Ads::where('type', 'footer')->first();
            $result = Article::where('slug', $slug)->first();

            if (!$result) {
                return redirect()->to('/')->with(['flash_level' => 'danger', 'flash_message' => 'Error']);
            }
            $article = Article::where('type', 'article')->where('slug', '<>', $slug)->orderby('created_at', 'desc')->take($setting->film_home)->get();
            $meta = array(
                'title' => $result->name ? $result->name : $setting->title,
                'description' => $result->descriptions ? $result->descriptions : $setting->decription,
                'keywords' => $result->keywords ? $result->keywords : $setting->keywords,
                'images' => $result->images ? url(Storage::url($result->images)) : url(Storage::url($setting->meta_images))
            );
            $data_cache = [
                'page' => 'site',
                'sub_active' => $slug,
                'setting' => $setting,
                'meta' => $meta,
                'lang' => $lang,
                'lang_id' => $lang_id,
                'language_list' => $language_list,
                'genres' => $genres,
                'country' => $country,
                'type' => $type,
                'footer' => (!empty($footer) ? $footer : null),
                'result' => $result,
                'news' => $result,
                'article' => $article
            ];
            Cache::put('coming_soon'.$slug, $data_cache, $setting->cache);
            return view('frontend.theme_'.$setting->theme.'.pages.coming-soon')->with($data_cache);
//        }
    }

    public function getSearch(Request $request)
    {
        $key = $request->key;
        if ($key) {
            $setting = Setting::first();
            $data_cache = Cache::get('search_'.$key.'?page='.$request->page);
            if($data_cache){
                return view('frontend.theme_'.$setting->theme.'.pages.film-country')->with($data_cache);
            }else {
                $lang = App::getLocale();
                $lang_id = Session::get('lang_id');
                $language_list = Languages::get();
                $meta = array(
                    'title' => 'Looking for ' . $key . ' on ' . str_replace('https://', '', str_replace('http://', '', url('/'))),
                    'description' => $setting->description,
                    'keywords' => $setting->keywords,
                    'images' => url(Storage::url($setting->meta_images))
                );
                $genres = CategoryFilm::where('publish', 1)->orderby('name')->get();
                $country = CountryFilm::where('publish', 1)->orderby('name')->get();
                $type = TypeFilm::where('publish', 1)->orderby('name')->get();
                $footer = Ads::where('type', 'footer')->first();
                $page_list_top_page = Ads::where('slug', 'page-list-top-page')->where('publish', 1)->where('type', 'ads')->first();
                $page_list_bottom_page = Ads::where('slug', 'page-list-bottom-page')->where('publish', 1)->where('type', 'ads')->first();
                $films = Film::where('name', 'like', "%$key%")->where('publish', 1)->paginate($setting->film_page);
                $data_cache = [
                    'page' => 'search',
                    'setting' => $setting,
                    'meta' => $meta,
                    'lang' => $lang,
                    'lang_id' => $lang_id,
                    'language_list' => $language_list,
                    'genres' => $genres,
                    'country' => $country,
                    'type' => $type,
                    'results' => $films,
                    'title' => 'Search "' . $key . '"',
                    'footer' => (!empty($footer) ? $footer : null),
                    'page_list_top_page' => (!empty($page_list_top_page) ? $page_list_top_page : null),
                    'page_list_bottom_page' => (!empty($page_list_bottom_page) ? $page_list_bottom_page : null)
                ];
                Cache::put('search_'.$key.'?page='.$request->page, $data_cache, $setting->cache);
                return view('frontend.theme_'.$setting->theme.'.pages.film-country')->with($data_cache);
            }
        } else {
            return redirect('/');
        }
    }


    public function postRequestAPI(Request $request)
    {
        $id = $request->id;
        $s = $request->s;
        if (!$id) {
            return Response::json(['status' => 'error']);
        }
        $film = Film::where('id', $id)->select('id', 'movie_series', 'slug')->first();
        if (!$film) {
            return Response::json(['status' => 'error']);
        }
        $server = ServerPlayFilm::find($s);
        if (!$server && $s != 0) {
            return redirect('/')->with(['flash_level' => 'danger', 'flash_message' => 'Server not found']);
        }
        $setting = Setting::first();
        $data_cache = Cache::get('data_request_api_' . $id . '_' . $s);
        if ($data_cache) {
            return Response::json($data_cache);
        } else {
            $server_active = null;
            if ($s == 0) {
                if ($film->link_play && count($film->link_play) > 0) {
                    $subtitle = $film->subtitle;
                    $subtitle_data = array();
                    if (count($subtitle) > 0) {
                        $i = 0;
                        foreach ($subtitle as $element) {
                            $subtitle_data[$i]['file'] = url(Storage::url('subtitle/' . $element->name));
                            $subtitle_data[$i]['label'] = $element->language;
                            $subtitle_data[$i]['kind'] = 'captions';
                            if ($i == 0) {
                                $subtitle_data[$i]['default'] = true;
                            }
                            $i++;
                        }
                    }

                    for ($i = 0; $i < count($film->link_play); $i++) {
                        if (isset($film->link_play[$i]) && $film->link_play[$i]->server->embedded == 1) {
                            if ($film->link_play[$i]->error == 1) {
                                continue;
                            }
                            $data = \stdClass::class;
                            $data = $film->link_play[$i]->link;
//                            if (strpos($data, 'http') === false) {
//                                continue;
//                            }
                            $server_html = "";
                            $server = LinkFilm::where('film_id', $id)->select('server_play')->distinct('server_play')->get();

                            if (count($server) > 0) {
                                foreach ($server as $item) {
                                    $server_check = ServerPlayFilm::where('id', $item->server->id)->where('publish', 1)->first();
                                    if ($server_check) {
                                        $server_data = LinkFilm::where('film_id', $id)->distinct('server_play')->with('server')->get();
                                        $server_html .= "<li><a href='javascript:void(0)' class=\"server_play_chosen server_" . $item->server->id . ($item->server->id == $film->link_play[$i]->server->id ? ' active' : '') . "\"  data-url=\"" . url('watch/' . $film->slug . '-' . $film->id . '?s=' . $item->server->id) . "\" data-id='" . $id . "' data-server='" . $item->server->id . "'>" . $item->server->name . "</a></li>";
                                        if($item->server->id == $film->link_play[$i]->server->id){
                                            $server_active = $item->server->name;
                                        }
                                    }
                                }
                            }
                            Cache::put('data_request_api_' . $id . '_' . $s, ['status' => 'embed', 'result' => base64_encode(json_encode($data)), 'link_id' => $film->link_play[$i]->id, 'subtitle' => $subtitle_data, 'server' => $server_html, 'server_name' => $server_active], $setting->cache);
                            return Response::json(['status' => 'embed', 'result' => base64_encode(json_encode($data)), 'link_id' => $film->link_play[$i]->id, 'subtitle' => $subtitle_data, 'server' => $server_html, 'server_name' => $server_active]);
                        } else {
                            if ($film->link_play[$i]->error == 1) {
                                continue;
                            }

                            $data = getDataAPI($setting->api_player . $film->link_play[$i]->link . '&email=' . $film->link_play[$i]->email);

                            if ($data) {
                                $data = json_decode($data);
                            }

                            if ($data && $data->status == 'ok') {
                                $server_html = "";

                                $server = LinkFilm::where('film_id', $id)->select('server_play')->distinct('server_play')->get();
                                if (count($server) > 0) {
                                    foreach ($server as $item) {
                                        $server_check = ServerPlayFilm::where('id', $item->server->id)->where('publish', 1)->first();
                                        if ($server_check) {
                                            $server_html .= "<li><a  href='javascript:void(0)' class=\"server_play_chosen server_" . $item->server->id . ($item->server->id == $film->link_play[$i]->server->id ? ' active' : '') . "\" data-url=\"" . url('watch/' . $film->slug . '-' . $film->id . '?s=' . $item->server->id) . "\" data-id='" . $id . "' data-server='" . $item->server->id . "'>" . $item->server->name . "</a></li>";
                                            if($item->server->id == $film->link_play[$i]->server->id){
                                                $server_active = $item->server->name;
                                            }
                                        }
                                    }
                                }
                                Cache::put('data_request_api_' . $id . '_' . $s, ['status' => 'api', 'result' => base64_encode(json_encode($data)), 'link_id' => $film->link_play[$i]->id, 'subtitle' => $subtitle_data, 'server' => $server_html, 'server_name' => $server_active], $setting->cache);
                                return Response::json(['status' => 'api', 'result' => base64_encode(json_encode($data)), 'link_id' => $film->link_play[$i]->id, 'subtitle' => $subtitle_data, 'server' => $server_html, 'server_name' => $server_active]);
                            } else {
                                $check = LinkFilm::find($film->link_play[$i]->id);
                                if ($check) {
                                    $check->error = 1;
                                    $check->save();
                                }
                            }
                        }
                    }
                }
            } else {
                if ($film->link_play && count($film->link_play) > 0) {
                    $subtitle = $film->subtitle;
                    $subtitle_data = array();
                    if (count($subtitle) > 0) {
                        $i = 0;
                        foreach ($subtitle as $element) {
                            $subtitle_data[$i]['file'] = url(Storage::url('subtitle/' . $element->name));
                            $subtitle_data[$i]['label'] = $element->language;
                            $subtitle_data[$i]['kind'] = 'captions';
                            if ($i == 0) {
                                $subtitle_data[$i]['default'] = true;
                            }
                            $i++;
                        }
                    }

                    for ($i = 0; $i < count($film->link_play); $i++) {
                        if ($film->link_play[$i]->server_play == $s) {
                            if (isset($film->link_play[$i]) && $film->link_play[$i]->server->embedded == 1) {
                                if ($film->link_play[$i]->error == 1) {
                                    continue;
                                }
                                $data = \stdClass::class;
                                $data = $film->link_play[$i]->link;
//                                if (strpos($data, 'http') === false) {
//                                    continue;
//                                }
                                $server_html = "";
                                $server = LinkFilm::where('film_id', $id)->select('server_play')->distinct('server_play')->get();
                                if (count($server) > 0) {
                                    foreach ($server as $item) {
                                        $server_check = ServerPlayFilm::where('id', $item->server->id)->where('publish', 1)->first();
                                        if ($server_check) {
                                            $server_html .= "<li><a href='javascript:void(0)' class=\"server_play_chosen server_" . $item->server->id . ($item->server->id == $film->link_play[$i]->server->id ? ' active' : '') . "\" data-url=\"" . url('watch/' . $film->slug . '-' . $film->id . '?s=' . $item->server->id) . "\" data-id='" . $id . "' data-server='" . $item->server->id . "'>" . $item->server->name . "</a></li>";
                                            if($item->server->id == $film->link_play[$i]->server->id){
                                                $server_active = $item->server->name;
                                            }
                                        }
                                    }
                                }
                                Cache::put('data_request_api_' . $id . '_' . $s, ['status' => 'embed', 'result' => base64_encode(json_encode($data)), 'link_id' => $film->link_play[$i]->id, 'subtitle' => $subtitle_data, 'server' => $server_html, 'server_name' => $server_active], $setting->cache);
                                return Response::json(['status' => 'embed', 'result' => base64_encode(json_encode($data)), 'link_id' => $film->link_play[$i]->id, 'subtitle' => $subtitle_data, 'server' => $server_html, 'server_name' => $server_active]);
                            } else {
                                if ($film->link_play[$i]->error == 1) {
                                    continue;
                                }
                                $data = getDataAPI($setting->api_player . $film->link_play[$i]->link . '&email=' . $film->link_play[$i]->email);
                                if ($data) {
                                    $data = json_decode($data);
                                }

                                if ($data && $data->status == 'ok') {
                                    $server_html = "";

                                    $server = LinkFilm::where('film_id', $id)->select('server_play')->distinct('server_play')->get();
                                    if (count($server) > 0) {
                                        foreach ($server as $item) {
                                            $server_check = ServerPlayFilm::where('id', $item->server->id)->where('publish', 1)->first();
                                            if ($server_check) {
                                                $server_html .= "<li><a  href='javascript:void(0)' class=\"server_play_chosen server_" . $item->server->id . ($item->server->id == $film->link_play[$i]->server->id ? ' active' : '') . "\" data-url=\"" . url('watch/' . $film->slug . '-' . $film->id . '?s=' . $item->server->id) . "\" data-id='" . $id . "' data-server='" . $item->server->id . "'>" . $item->server->name . "</a></li>";
                                                if($item->server->id == $film->link_play[$i]->server->id){
                                                    $server_active = $item->server->name;
                                                }
                                            }
                                        }
                                    }
                                    Cache::put('data_request_api_' . $id . '_' . $s, ['status' => 'api', 'result' => base64_encode(json_encode($data)), 'link_id' => $film->link_play[$i]->id, 'subtitle' => $subtitle_data, 'server' => $server_html, 'server_name' => $server_active], $setting->cache);
                                    return Response::json(['status' => 'api', 'result' => base64_encode(json_encode($data)), 'link_id' => $film->link_play[$i]->id, 'subtitle' => $subtitle_data, 'server' => $server_html, 'server_name' => $server_active]);
                                } else {
                                    $check = LinkFilm::find($film->link_play[$i]->id);
                                    if ($check) {
                                        $check->error = 1;
                                        $check->save();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //If all link 1 server error
            if ($film->link_play && count($film->link_play) > 0) {
                $subtitle = $film->subtitle;
                $subtitle_data = array();
                if (count($subtitle) > 0) {
                    $i = 0;
                    foreach ($subtitle as $element) {
                        $subtitle_data[$i]['file'] = url(Storage::url('subtitle/' . $element->name));
                        $subtitle_data[$i]['label'] = $element->language;
                        $subtitle_data[$i]['kind'] = 'captions';
                        if ($i == 0) {
                            $subtitle_data[$i]['default'] = true;
                        }
                        $i++;
                    }
                }

                for ($i = 0; $i < count($film->link_play); $i++) {
                    if (isset($film->link_play[$i]) && $film->link_play[$i]->server->embedded == 1) {
                        if ($film->link_play[$i]->error == 1) {
                            continue;
                        }
                        $data = \stdClass::class;
                        $data = $film->link_play[$i]->link;
//                        if (strpos($data, 'http') === false) {
//                            continue;
//                        }
                        $server_html = "";
                        $server = LinkFilm::where('film_id', $id)->select('server_play')->distinct('server_play')->get();

                        if (count($server) > 0) {
                            foreach ($server as $item) {
                                $server_check = ServerPlayFilm::where('id', $item->server->id)->where('publish', 1)->first();
                                if ($server_check) {
                                    $server_data = LinkFilm::where('film_id', $id)->distinct('server_play')->with('server')->get();
                                    $server_html .= "<li><a href='javascript:void(0)' class=\"server_play_chosen server_" . $item->server->id . ($item->server->id == $film->link_play[$i]->server->id ? ' active' : '') . "\"  data-url=\"" . url('watch/' . $film->slug . '-' . $film->id . '?s=' . $item->server->id) . "\" data-id='" . $id . "' data-server='" . $item->server->id . "'>" . $item->server->name . "</a></li>";
                                    if($item->server->id == $film->link_play[$i]->server->id){
                                        $server_active = $item->server->name;
                                    }
                                }
                            }
                        }
                        Cache::put('data_request_api_' . $id . '_' . $s, ['status' => 'embed', 'result' => base64_encode(json_encode($data)), 'link_id' => $film->link_play[$i]->id, 'subtitle' => $subtitle_data, 'server' => $server_html, 'server_id' => url('watch/' . $film->slug . '-' . $film->id . '?s=' . $film->link_play[$i]->server->id), 'server_name' => $server_active], $setting->cache);
                        return Response::json(['status' => 'embed', 'result' => base64_encode(json_encode($data)), 'link_id' => $film->link_play[$i]->id, 'subtitle' => $subtitle_data, 'server' => $server_html, 'server_id' => url('watch/' . $film->slug . '-' . $film->id . '?s=' . $film->link_play[$i]->server->id), 'server_name' => $server_active]);
                    } else {
                        if ($film->link_play[$i]->error == 1) {
                            continue;
                        }
                        $data = getDataAPI($setting->api_player . $film->link_play[$i]->link . '&email=' . $film->link_play[$i]->email);
                        if ($data) {
                            $data = json_decode($data);
                        }

                        if ($data && $data->status == 'ok') {
                            $server_html = "";

                            $server = LinkFilm::where('film_id', $id)->select('server_play')->distinct('server_play')->get();
                            if (count($server) > 0) {
                                foreach ($server as $item) {
                                    $server_check = ServerPlayFilm::where('id', $item->server->id)->where('publish', 1)->first();
                                    if ($server_check) {
                                        $server_html .= "<li><a  href='javascript:void(0)' class=\"server_play_chosen server_" . $item->server->id . ($item->server->id == $film->link_play[$i]->server->id ? ' active' : '') . "\" data-url=\"" . url('watch/' . $film->slug . '-' . $film->id . '?s=' . $item->server->id) . "\" data-id='" . $id . "' data-server='" . $item->server->id . "'>" . $item->server->name . "</a></li>";
                                        if($item->server->id == $film->link_play[$i]->server->id){
                                            $server_active = $item->server->name;
                                        }
                                    }
                                }
                            }
                            Cache::put('data_request_api_' . $id . '_' . $s, ['status' => 'api', 'result' => base64_encode(json_encode($data)), 'link_id' => $film->link_play[$i]->id, 'subtitle' => $subtitle_data, 'server' => $server_html, 'server_id' => url('watch/' . $film->slug . '-' . $film->id . '?s=' . $film->link_play[$i]->server->id), 'server_name' => $server_active], $setting->cache);
                            return Response::json(['status' => 'api', 'result' => base64_encode(json_encode($data)), 'link_id' => $film->link_play[$i]->id, 'subtitle' => $subtitle_data, 'server' => $server_html, 'server_id' => url('watch/' . $film->slug . '-' . $film->id . '?s=' . $film->link_play[$i]->server->id), 'server_name' => $server_active]);
                        } else {
                            $check = LinkFilm::find($film->link_play[$i]->id);
                            if ($check) {
                                $check->error = 1;
                                $check->save();
                            }
                        }
                    }
                }
            }

            $server_html = "";
            $server = LinkFilm::where('film_id', $id)->select('server_play')->distinct('server_play')->get();
            if (count($server) > 0) {
                foreach ($server as $item) {
                    $server_check = ServerPlayFilm::where('id', $item->server->id)->where('publish', 1)->first();
                    if ($server_check) {
                        if ($s) {
                            $server_html .= "<li><a href='javascript:void(0)' class=\"server_play_chosen server_" . $item->server->id . ($item->server->id == $s ? ' active' : '') . "\" data-url=\"" . url('watch/' . $film->slug . '-' . $film->id . '?s=' . $item->server->id) . "\" data-id='" . $id . "' data-server='" . $item->server->id . "'>" . $item->server->name . "</a></li>";
                        } else {
                            $server_html .= "<li><a href='javascript:void(0)' class=\"server_play_chosen server_" . $item->server->id . "\" data-url=\"" . url('watch/' . $film->slug . '-' . $film->id . '?s=' . $item->server->id) . "\" data-id='" . $id . "' data-server='" . $item->server->id . "'>" . $item->server->name . "</a></li>";
                        }
                        if($item->server->id == $s){
                            $server_active = $item->server->name;
                        }
                    }
                }
            }
            Cache::put('data_request_api_' . $id . '_' . $s, ['status' => 'error', 'server' => $server_html, 'link_id' => 0, 'server_name' => $server_active], $setting->cache);
            return Response::json(['status' => 'error', 'server' => $server_html, 'link_id' => 0, 'server_name' => $server_active]);
        }
    }

    public function postRequestAPIDetail(Request $request)
    {
        $id = $request->id;
        $ep = $request->ep;
        $s = $request->s;
        if (!$id) {
            return Response::json(['status' => 'error']);
        }
        $film = Film::find($id);
        if (!$film) {
            return Response::json(['status' => 'error']);
        }
        $server = ServerPlayFilm::where('id', $s)->where('publish', 1)->get();
        if (!$server && $s != 0) {
            return redirect('/')->with(['flash_level' => 'danger', 'flash_message' => 'Server not found']);
        }
        $setting = Setting::first();
        $data_cache = Cache::get('data_request_api_detail_' . $id . '_' . $ep . '_' . $s);
        if ($data_cache) {
            return Response::json($data_cache);
        } else {
            $server_active = null;
            if ($s == 0) {
                if ($film->film_detail && isset($film->film_detail[$ep - 1])) {
                    $result = $film->film_detail[$ep - 1];
                    $id_episode = $film->film_detail[$ep - 1];

                    if ($result->link_play && count($result->link_play) > 0) {
                        $subtitle = $result->subtitle;
                        $subtitle_data = array();
                        if (count($subtitle) > 0) {
                            $i = 0;
                            foreach ($subtitle as $element) {
                                $subtitle_data[$i]['file'] = url(Storage::url('subtitle/' . $element->name));
                                $subtitle_data[$i]['label'] = $element->language;
                                $subtitle_data[$i]['kind'] = 'captions';
                                if ($i == 0) {
                                    $subtitle_data[$i]['default'] = true;
                                }
                                $i++;
                            }
                        }
                        for ($i = 0; $i < count($result->link_play); $i++) {
                            if (isset($result->link_play[$i]) && $result->link_play[$i]->server->publish == 1 && $result->link_play[$i]->server->embedded == 1) {
                                if ($result->link_play[$i]->error == 1) {
                                    continue;
                                }
                                $data = \stdClass::class;
                                $data = $result->link_play[$i]->link;
//                                if (strpos($data, 'http') === false) {
//                                    continue;
//                                }
                                $server_html = "";
                                $server = LinkFilmDetail::where('film_detail_id', $id_episode->id)->select('server_play')->distinct('server_play')->get();
                                if (count($server) > 0) {
                                    foreach ($server as $item) {
                                        $server_check = ServerPlayFilm::where('id', $item->server->id)->where('publish', 1)->first();
                                        if ($server_check) {
                                            $server_html .= "<li><a href='javascript:void(0)' class=\"server_play_chosen_episode server_" . $item->server->id . ($item->server->id == $result->link_play[$i]->server->id ? ' active' : '') . "\" data-url=\"" . url('watch/' . $film->slug . '-Episode-' . $ep . '-' . $film->id . '?p=' . $ep . '&s=' . $item->server->id) . "\" data-id='" . $id . "' data-server='" . $item->server->id . "' data-ep='" . $ep . "'>" . $item->server->name . "</a></li>";
                                            if($item->server->id == $result->link_play[$i]->server->id){
                                                $server_active = $item->server->name;
                                            }
                                        }
                                    }
                                }
                                Cache::put('data_request_api_detail_' . $id . '_' . $ep . '_' . $s, ['status' => 'embed', 'result' => base64_encode(json_encode($data)), 'link_id' => $result->link_play[$i]->id, 'server' => $server_html, 'server_name' => $server_active], $setting->cache);
                                return Response::json(['status' => 'embed', 'result' => base64_encode(json_encode($data)), 'link_id' => $result->link_play[$i]->id, 'server' => $server_html, 'server_name' => $server_active]);
                            } else {
                                if ($result->link_play[$i]->error == 1) {
                                    continue;
                                }
                                $data = getDataAPI($setting->api_player . $result->link_play[$i]->link . '&email=' . $result->link_play[$i]->email);
                                if ($data) {
                                    $data = json_decode($data);
                                }
                                if ($data && $data->status == 'ok') {

                                    $server_html = "";
                                    $server = LinkFilmDetail::where('film_detail_id', $id_episode->id)->select('server_play')->distinct('server_play')->get();
                                    if (count($server) > 0) {
                                        foreach ($server as $item) {
                                            $server_check = ServerPlayFilm::where('id', $item->server->id)->where('publish', 1)->first();
                                            if ($server_check) {
                                                $server_html .= "<li><a href='javascript:void(0)' class=\"server_play_chosen_episode server_" . $item->server->id . ($item->server->id == $result->link_play[$i]->server->id ? ' active' : '') . "\" data-url=\"" . url('watch/' . $film->slug . '-Episode-' . $ep . '-' . $film->id . '?p=' . $ep . '&s=' . $item->server->id) . "\" data-id='" . $id . "' data-server='" . $item->server->id . "' data-ep='" . $ep . "'>" . $item->server->name . "</a></li>";
                                                if($item->server->id == $result->link_play[$i]->server->id){
                                                    $server_active = $item->server->name;
                                                }
                                            }
                                        }
                                    }
                                    Cache::put('data_request_api_detail_' . $id . '_' . $ep . '_' . $s, ['status' => 'api', 'result' => base64_encode(json_encode($data)), 'link_id' => $result->link_play[$i]->id, 'subtitle' => $subtitle_data, 'server' => $server_html, 'server_name' => $server_active], $setting->cache);
                                    return Response::json(['status' => 'api', 'result' => base64_encode(json_encode($data)), 'link_id' => $result->link_play[$i]->id, 'subtitle' => $subtitle_data, 'server' => $server_html, 'server_name' => $server_active]);
                                } else {
                                    $check = LinkFilmDetail::find($result->link_play[$i]->id);
                                    if ($check) {
                                        $check->error = 1;
                                        $check->save();
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                if ($film->film_detail && isset($film->film_detail[$ep - 1])) {
                    $result = $film->film_detail[$ep - 1];
                    $id_episode = $film->film_detail[$ep - 1];

                    if ($result->link_play && count($result->link_play) > 0) {
                        $subtitle = $result->subtitle;
                        $subtitle_data = array();
                        if (count($subtitle) > 0) {
                            $i = 0;
                            foreach ($subtitle as $element) {
                                $subtitle_data[$i]['file'] = url(Storage::url('subtitle/' . $element->name));
                                $subtitle_data[$i]['label'] = $element->language;
                                $subtitle_data[$i]['kind'] = 'captions';
                                if ($i == 0) {
                                    $subtitle_data[$i]['default'] = true;
                                }
                                $i++;
                            }
                        }
                        for ($i = 0; $i < count($result->link_play); $i++) {
                            if ($result->link_play[$i]->server_play == $s) {
                                if (isset($result->link_play[$i]) && $result->link_play[$i]->server->embedded == 1) {
                                    if ($result->link_play[$i]->error == 1) {
                                        continue;
                                    }
                                    $data = \stdClass::class;
                                    $data = $result->link_play[$i]->link;
//                                    if (strpos($data, 'http') === false) {
//                                        continue;
//                                    }
                                    $server_html = "";
                                    $server = LinkFilmDetail::where('film_detail_id', $id_episode->id)->select('server_play')->distinct('server_play')->get();
                                    if (count($server) > 0) {
                                        foreach ($server as $item) {
                                            $server_check = ServerPlayFilm::where('id', $item->server->id)->where('publish', 1)->first();
                                            if ($server_check) {
                                                $server_html .= "<li><a href='javascript:void(0)' class=\"server_play_chosen_episode server_" . $item->server->id . ($item->server->id == $result->link_play[$i]->server->id ? ' active' : '') . "\" data-url=\"" . url('watch/' . $film->slug . '-Episode-' . $ep . '-' . $film->id . '?p=' . $ep . '&s=' . $item->server->id) . "\" data-id='" . $id . "' data-server='" . $item->server->id . "' data-ep='" . $ep . "'>" . $item->server->name . "</a></li>";
                                                if($item->server->id == $result->link_play[$i]->server->id){
                                                    $server_active = $result->server->name;
                                                }
                                            }
                                        }
                                    }
                                    Cache::put('data_request_api_detail_' . $id . '_' . $ep . '_' . $s, ['status' => 'embed', 'result' => base64_encode(json_encode($data)), 'link_id' => $result->link_play[$i]->id, 'server' => $server_html, 'server_name' => $server_active], $setting->cache);
                                    return Response::json(['status' => 'embed', 'result' => base64_encode(json_encode($data)), 'link_id' => $result->link_play[$i]->id, 'server' => $server_html, 'server_name' => $server_active]);
                                } else {
                                    if ($result->link_play[$i]->error == 1) {
                                        continue;
                                    }
                                    $data = getDataAPI($setting->api_player . $result->link_play[$i]->link . '&email=' . $result->link_play[$i]->email);
                                    if ($data) {
                                        $data = json_decode($data);
                                    }
                                    if ($data && $data->status == 'ok') {

                                        $server_html = "";
                                        $server = LinkFilmDetail::where('film_detail_id', $id_episode->id)->select('server_play')->distinct('server_play')->get();
                                        if (count($server) > 0) {
                                            foreach ($server as $item) {
                                                $server_check = ServerPlayFilm::where('id', $item->server->id)->where('publish', 1)->first();
                                                if ($server_check) {
                                                    $server_html .= "<li><a href='javascript:void(0)' class=\"server_play_chosen_episode server_" . $item->server->id . ($item->server->id == $result->link_play[$i]->server->id ? ' active' : '') . "\" data-url=\"" . url('watch/' . $film->slug . '-Episode-' . $ep . '-' . $film->id . '?p=' . $ep . '&s=' . $item->server->id) . "\" data-id='" . $id . "' data-server='" . $item->server->id . "' data-ep='" . $ep . "'>" . $item->server->name . "</a></li>";
                                                    if($item->server->id == $result->link_play[$i]->server->id){
                                                        $server_active = $item->server->name;
                                                    }
                                                }
                                            }
                                        }
                                        Cache::put('data_request_api_detail_' . $id . '_' . $ep . '_' . $s, ['status' => 'api', 'result' => base64_encode(json_encode($data)), 'link_id' => $result->link_play[$i]->id, 'subtitle' => $subtitle_data, 'server' => $server_html, 'server_name' => $server_active], $setting->cache);
                                        return Response::json(['status' => 'api', 'result' => base64_encode(json_encode($data)), 'link_id' => $result->link_play[$i]->id, 'subtitle' => $subtitle_data, 'server' => $server_html, 'server_name' => $server_active]);
                                    } else {
                                        $check = LinkFilmDetail::find($result->link_play[$i]->id);
                                        if ($check) {
                                            $check->error = 1;
                                            $check->save();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //All link 1 server die
            if ($film->film_detail && isset($film->film_detail[$ep - 1])) {
                $result = $film->film_detail[$ep - 1];
                $id_episode = $film->film_detail[$ep - 1];

                if ($result->link_play && count($result->link_play) > 0) {
                    $subtitle = $result->subtitle;
                    $subtitle_data = array();
                    if (count($subtitle) > 0) {
                        $i = 0;
                        foreach ($subtitle as $element) {
                            $subtitle_data[$i]['file'] = url(Storage::url('subtitle/' . $element->name));
                            $subtitle_data[$i]['label'] = $element->language;
                            $subtitle_data[$i]['kind'] = 'captions';
                            if ($i == 0) {
                                $subtitle_data[$i]['default'] = true;
                            }
                            $i++;
                        }
                    }
                    for ($i = 0; $i < count($result->link_play); $i++) {
                        if (isset($result->link_play[$i]) && $result->link_play[$i]->server->embedded == 1) {
                            if ($result->link_play[$i]->error == 1) {
                                continue;
                            }
                            $data = \stdClass::class;
                            $data = $result->link_play[$i]->link;
//                            if (strpos($data, 'http') === false) {
//                                continue;
//                            }
                            $server_html = "";
                            $server = LinkFilmDetail::where('film_detail_id', $id_episode->id)->select('server_play')->distinct('server_play')->get();
                            if (count($server) > 0) {
                                foreach ($server as $item) {
                                    $server_check = ServerPlayFilm::where('id', $item->server->id)->where('publish', 1)->first();
                                    if ($server_check) {
                                        $server_html .= "<li><a href='javascript:void(0)' class=\"server_play_chosen_episode server_" . $item->server->id . ($item->server->id == $result->link_play[$i]->server->id ? ' active' : '') . "\" data-url=\"" . url('watch/' . $film->slug . '-Episode-' . $ep . '-' . $film->id . '?p=' . $ep . '&s=' . $item->server->id) . "\" data-id='" . $id . "' data-server='" . $item->server->id . "' data-ep='" . $ep . "'>" . $item->server->name . "</a></li>";
                                        if($item->server->id == $result->link_play[$i]->server->id){
                                            $server_active = $item->server->name;
                                        }
                                    }
                                }
                            }
                            Cache::put('data_request_api_detail_' . $id . '_' . $ep . '_' . $s, ['status' => 'embed', 'result' => base64_encode(json_encode($data)), 'link_id' => $result->link_play[$i]->id, 'server' => $server_html, 'server_id' => url('watch/' . $film->slug . '-Episode-' . $ep . '-' . $film->id . '?p=' . $ep . '&s=' . $result->link_play[$i]->server->id), 'server_name' => $server_active], $setting->cache);
                            return Response::json(['status' => 'embed', 'result' => base64_encode(json_encode($data)), 'link_id' => $result->link_play[$i]->id, 'server' => $server_html, 'server_id' => url('watch/' . $film->slug . '-Episode-' . $ep . '-' . $film->id . '?p=' . $ep . '&s=' . $result->link_play[$i]->server->id), 'server_name' => $server_active]);
                        } else {
                            if ($result->link_play[$i]->error == 1) {
                                continue;
                            }
                            $data = getDataAPI($setting->api_player . $result->link_play[$i]->link . '&email=' . $result->link_play[$i]->email);
                            if ($data) {
                                $data = json_decode($data);
                            }
                            if ($data && $data->status == 'ok') {

                                $server_html = "";
                                $server = LinkFilmDetail::where('film_detail_id', $id_episode->id)->select('server_play')->distinct('server_play')->get();
                                if (count($server) > 0) {
                                    foreach ($server as $item) {
                                        $server_check = ServerPlayFilm::where('id', $item->server->id)->where('publish', 1)->first();
                                        if ($server_check) {
                                            $server_html .= "<li><a href='javascript:void(0)' class=\"server_play_chosen_episode server_" . $item->server->id . ($item->server->id == $result->link_play[$i]->server->id ? ' active' : '') . "\" data-url=\"" . url('watch/' . $film->slug . '-Episode-' . $ep . '-' . $film->id . '?p=' . $ep . '&s=' . $item->server->id) . "\" data-id='" . $id . "' data-server='" . $item->server->id . "' data-ep='" . $ep . "'>" . $item->server->name . "</a></li>";
                                            if($item->server->id == $result->link_play[$i]->server->id){
                                                $server_active = $item->server->name;
                                            }
                                        }
                                    }
                                }
                                Cache::put('data_request_api_detail_' . $id . '_' . $ep . '_' . $s, ['status' => 'api', 'result' => base64_encode(json_encode($data)), 'link_id' => $result->link_play[$i]->id, 'subtitle' => $subtitle_data, 'server' => $server_html, 'server_id' => url('watch/' . $film->slug . '-Episode-' . $ep . '-' . $film->id . '?p=' . $ep . '&s=' . $result->link_play[$i]->server->id), 'server_name' => $server_active], $setting->cache);
                                return Response::json(['status' => 'api', 'result' => base64_encode(json_encode($data)), 'link_id' => $result->link_play[$i]->id, 'subtitle' => $subtitle_data, 'server' => $server_html, 'server_id' => url('watch/' . $film->slug . '-Episode-' . $ep . '-' . $film->id . '?p=' . $ep . '&s=' . $result->link_play[$i]->server->id), 'server_name' => $server_active]);
                            } else {
                                $check = LinkFilmDetail::find($result->link_play[$i]->id);
                                if ($check) {
                                    $check->error = 1;
                                    $check->save();
                                }
                            }
                        }
                    }
                }
            }

            $server_html = "";
            $server = LinkFilmDetail::where('film_detail_id', $film->film_detail[$ep - 1]->id)->select('server_play')->distinct('server_play')->get();
            if (count($server) > 0) {
                foreach ($server as $item) {
                    $server_check = ServerPlayFilm::where('id', $item->server->id)->where('publish', 1)->first();
                    if ($server_check) {
                        if ($s) {
                            $server_html .= "<li><a href='javascript:void(0)' class=\"server_play_chosen_episode server_" . $item->server->id . ($item->server->id == $s ? ' active' : '') . "\" data-url=\"" . url('watch/' . $film->slug . '-Episode-' . $ep . '-' . $film->id . '?p=' . $ep . '&s=' . $item->server->id) . "\" data-id='" . $id . "' data-server='" . $item->server->id . "' data-ep='" . $ep . "'>" . $item->server->name . "</a></li>";
                        } else {
                            $server_html .= "<li><a href='javascript:void(0)' class=\"server_play_chosen_episode server_" . $item->server->id . "\" data-url=\"" . url('watch/' . $film->slug . '-Episode-' . $ep . '-' . $film->id . '?p=' . $ep . '&s=' . $item->server->id) . "\" data-id='" . $id . "' data-server='" . $item->server->id . "' data-ep='" . $ep . "'>" . $item->server->name . "</a></li>";
                        }
                        if($item->server->id == $s){
                            $server_active = $item->server->name;
                        }
                    }
                }
            }
            Cache::put('data_request_api_detail_' . $id . '_' . $ep . '_' . $s, ['status' => 'error', 'server' => $server_html, 'link_id' => 0, 'server_name' => $server_active], $setting->cache);
            return Response::json(['status' => 'error', 'server' => $server_html, 'link_id' => 0, 'server_name' => $server_active]);
        }
    }

    public function postAddViewFilm(Request $request)
    {
        $id = $request->id;
        if ($id) {
            $check = Film::find($id);
            if ($check) {
                $check->view = $check->view + 1;
                $check->save();
            }
        }
        $subtitle = $request->subtitle;
        if ($subtitle) {
            deleteSubtitle('public/user_subtitle/' . $subtitle['name']);
            Session::forget('subtitle_upload');
        }
        return Response::json(['status' => 200]);
    }

    public function postFavorite(Request $request)
    {
        $data = $request->data;
        $user = Auth::user();
        if ($data && $user) {
            $check = FavoriteFilm::where('user_id', $user->id)->where('film_id', $data['id'])->first();
            $film = Film::find($data['id']);
            if ($check) {
                $check->delete();
                if ($film->favorite_total > 0) {
                    $film->favorite_total = $film->favorite_total - 1;
                    $film->save();
                }
                return Response::json(['status' => 200]);
            }
            $favorite = new FavoriteFilm;
            $favorite->film_id = $data['id'];
            $favorite->user_id = $user->id;
            $favorite->type = 'favorite';
            $favorite->created_at = gmdate('Y-m-d H:i:s', time());
            $favorite->updated_at = gmdate('Y-m-d H:i:s', time());
            $favorite->save();
            $film->favorite_total = $film->favorite_total + 1;
            $film->save();
            return Response::json(['status' => 200]);
        }
        return Response::json(['status' => 'error']);
    }

    public function postRateFilm(Request $request)
    {
        $film_id = $request->film_id;
        $rate = $request->rate;
        $user = Auth::user();
        if ($film_id && $rate && $user) {
            $check = RateFilm::where('user_id', $user->id)->where('film_id', $film_id)->first();
            if ($check) {
                $check->rate = $rate;
                $check->updated_at = gmdate('Y-m-d H:i:s', time());
                $check->save();
            } else {
                $rating = new RateFilm;
                $rating->film_id = $film_id;
                $rating->user_id = $user->id;
                $rating->type = 'rate';
                $rating->rate = $rate;
                $rating->created_at = gmdate('Y-m-d H:i:s', time());
                $rating->updated_at = gmdate('Y-m-d H:i:s', time());
                $rating->save();
            }
            return Response::json(['status' => 200]);
        } else {
            return Response::json(['status' => 'error']);
        }
    }

    public function getInfoFilm(Request $request)
    {
        $id = $request->id;
        if ($id) {
            $setting = Setting::first();
            $data_cache = Cache::get('cache_info_film_' . $id);
            if ($data_cache) {
                return Response::json($data_cache);
            } else {
                $film = Film::find($id);
                if ($film) {
                    $result = "";
                    $result .= "<div class=\"jtip-top\">";
                    $result .= "<div class=\"jt-info jt-imdb\">IMDb: " . $film->IMDB . "</div>";
                    $result .= "<div class=\"jt-info\">" . $film->release . "</div>";
                    $result .= "<div class=\"jt-info\">" . $film->duration . " min</div>";
                    $result .= "<div class=\"clearfix\"></div>";
                    $result .= "</div>";
                    $result .= "<p class=\"f-desc\">" . (strlen($film->content)>170?substr(stripslashes($film->content),0,170).'...':stripslashes($film->content)) . "</p>";
                    $result .= "<div class=\"block\">Country: <a href=" . url('country/' . $film->country->slug) . " title=" . $film->country->name . " Movies>" . $film->country->name . "</a></div>";
                    $result .= "<div class=\"block\">Genre: ";
                    if (count($film->genres) > 0) {
                        $i = 0;
                        foreach ($film->genres as $element) {
                            $result .= "<a href=" . url('genres/' . $element->slug) . " title=" . $element->name . ">" . $element->name . "</a>" . (($i == count($film->genres) - 1) ? '' : ', ');
                            $i++;
                        }
                    }
                    $result .= "</div>";
                    $result .= "<div class=\"block\">Actor: ";
                    if (count($film->actors) > 0) {
                        $i = 0;
                        foreach ($film->actors as $element) {
                            $result .= "<a href=" . url('actors/' . $element->slug) . " title=" . $element->name . ">" . $element->name . "</a>" . (($i == count($film->actors) - 1) || $i == 9 ? '' : ', ');
                            $i++;
                            if ($i == 10) {
                                break;
                            }
                        }
                    }
                    $result .= "</div>";

                    $favorite = 0;
                    if (count($film->favorite) > 0 && Auth::user()) {
                        foreach ($film->favorite as $element) {
                            if ($element->user_id == Auth::user()->id) {
                                $favorite = 1;
                                break;
                            }
                        }
                    }

                    $result .= "<div class=\"jtip-bottom\">";
                    $result .= "<a href=\"" . url('movie/' . $film->slug . '-' . $film->id) . "\" class=\"btn btn-block btn-success\"><i class=\"fa fa-play-circle mr10\"></i>Watch movie</a>";
                    if ($film->movie_series == 1) {
                        $result .= "<a href=\"" . url('movie/' . $film->slug . '-Episode-' . count($film->film_detail) . '-' . $film->id . '?p=' . count($film->film_detail)) . "\" class=\"btn btn-block btn-success\"><i class=\"fa fa-play-circle mr10\"></i>Watch Episode " . count($film->film_detail) . "</a>";
                    }

                    $result .= "<button onclick=\"favorite_add(" . $film->id . ",this,0," . (Auth::user() ? 1 : 0) . ")\" class=\"btn btn-block btn-default mt10 btn-favorite-" . $film->id . " add-favorite\" data-favorite=\"" . $favorite . "\" data-url=\"" . url('/') . "\">";
                    if ($favorite == 1) {
                        $result .= "<i class=\"fa fa-remove mr10\"></i>Remove Favorite";
                    } else {
                        $result .= "<i class=\"fa fa-heart mr10\"></i>Add to favorite";
                    }
                    $result .= "</button>";
                    $result .= "</div>";
                    Cache::put('cache_info_film_' . $id, ['status' => 200, 'result' => $result], $setting->cache);
                    return Response::json(['status' => 200, 'result' => $result]);
                }
            }
        }
        return Response::json(['status' => 'error']);
    }

    public function postSearchMovie(Request $request)
    {
        $key = $request->key;
        if ($key) {
            $films = Film::where('name', 'like', "%$key%")->where('publish', 1)->take(4)->get();
            if (count($films) > 0) {
                ob_start(); ?>
                <ul style="margin-bottom: 0;">
                    <?php
                    foreach ($films as $item) { ?>
                        <li>
                            <a style="background-image: url(<?php if ($item->images) echo url(Storage::url($item->images)); elseif ($item->images_link) echo url($item->images_link); ?>)"
                               class="thumb"
                               href="<?php echo url('movie/' . $item->slug . '-' . $item->id); ?>"></a>
                            <div class="ss-info">
                                <a href="<?php echo url('movie/' . $item->slug . '-' . $item->id); ?>"
                                   class="ss-title"><?php echo $item->name ?></a>
                                <p><?php echo $item->duration; ?> min</p>
                                <p><?php
                                    if(count($item->genres)>3){
                                        for($i=0; $i<3; $i++){
                                            echo $item->genres[$i]->name;
                                            if($i<2){
                                                echo ', ';
                                            }
                                        }
                                    }else{
                                        $i=0;
                                        foreach ($item->genres as $value){
                                            echo $value->name;
                                            if($i < count($item->genres) - 1){
                                                echo ', ';
                                            }
                                            $i++;
                                        }
                                    }
                                    ?></p>
                            </div>
                            <div class="clearfix"></div>
                        </li>
                    <?php } ?>
                    <li class="ss-bottom" style="padding: 0; border-bottom: none;"><a
                                href="<?php echo url('search?key=' . $key) ?>" id="suggest-all">View all</a></li>
                </ul>
                <?php
                $result = ob_get_clean();
            } else {
                return Response::json(['status' => 'error']);
            }
            return Response::json(['status' => 200, 'result' => $result]);
        } else {
            return Response::json(['status' => 'error']);
        }
    }

    public function getServerPlay()
    {
        $server_play = ServerPlayFilm::get();
        if (count($server_play) > 0) {
            $data = "<select class=\"form-control\" name=\"server_play[]\">";
            foreach ($server_play as $value) {
                $data .= "<option value=\"" . $value->id . "\">" . $value->name . "</option>";
            }
            return Response::json(['status' => 200, 'result' => $data]);
        }
        return Response::json(['status' => 'error']);
    }

    public function postReportFilm(Request $request)
    {
        $film_id = $request->film;
        $link_id = $request->link;
        if ($film_id && $link_id) {
            $film = Film::find($film_id);
            if ($film && $film->movie_series == 1) {
                $link = LinkFilmDetail::find($link_id);
                if ($link->server->embedded == 1) {
                    $link->error = 1;
                    $link->save();
                }
                return Response::json(['status' => 200, 'link' => $link]);
            } elseif ($film && $film->movie_series == 0) {
                $link = LinkFilm::find($link_id);
                if ($link->server->embedded == 1) {
                    $link->error = 1;
                    $link->save();
                }
                return Response::json(['status' => 200, 'link' => $link]);
            }
            return Response::json(['status' => 'error']);
        } else {
            return Response::json(['status' => 'error']);
        }
    }

    public function postLoadSubtitle(Request $request)
    {
        $film_id = $request->film_id;
        if ($film_id) {
            $film = Film::find($film_id);
            $file = $request->file('file');
            if ($file->isValid()) {
                $name = $file->getClientOriginalName();
                $name = getNameImage($name);
                $extension = $file->getClientOriginalExtension();
                $array_extension = array('srt', 'vtt', 'ass');
                $extension_check = 0;
                foreach ($array_extension as $value) {
                    if ($extension == $value) {
                        $extension_check = 1;
                    }
                }
                if ($extension_check == 0) {
                    return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Wrong subtitles format']);
                }
                $fileName = $name . '-' . time() . '.' . $extension;
                $path = putUploadSubtitleUser($file, $fileName);

                $data = array(
                    'name' => $fileName,
                    'language' => 'sub upload',
                    'film_id' => $film_id,
                    'created_at' => gmdate('Y-m-d H:i:s', time()),
                    'updated_at' => gmdate('Y-m-d H:i:s', time())
                );

                $data_bd = array(
                    'link' => $fileName,
                    'created_at' => gmdate('Y-m-d H:i:s', time()),
                    'updated_at' => gmdate('Y-m-d H:i:s', time())
                );

                $check = Session::get('subtitle_upload');
                if ($check) {
                    deleteSubtitle('public/user_subtitle/' . $check['name']);
                    Session::forget('subtitle_upload');
                    Session::put('subtitle_upload', $data);
                } else {
                    Session::put('subtitle_upload', $data);
                }

                return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Success']);
            }
        }
        return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Error']);
    }

    public function postReportLink(Request $request)
    {
        $privatekey = env('RECAPTCHA_SECRECT_KEY');
        $resp = recaptcha_check_answer(
            $privatekey,
            $_SERVER["REMOTE_ADDR"],
            $_POST["recaptcha_challenge_field"],
            $_POST["recaptcha_response_field"]
        );
        if (!$resp->is_valid) {
            return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'The captcha wasn\'t entered correctly']);
        } else {
            $film_id = $request->film_id;
            $link_id = $request->link_id;

            if ($film_id && $link_id) {
                $film = Film::find($film_id);
                if ($film && $film->movie_series == 1) {
                    $link = LinkFilmDetail::find($link_id);
                    if ($link->server->embedded == 1) {
                        $link->error = 1;
                        $link->updated_at = gmdate('Y-m-d H:i:s', time());
                        $link->save();
                    }
                    Cache::flush();
                    return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Success']);
                } elseif ($film && $film->movie_series == 0) {
                    $link = LinkFilm::find($link_id);
                    if ($link->server->embedded == 1) {
                        $link->error = 1;
                        $link->updated_at = gmdate('Y-m-d H:i:s', time());
                        $link->save();
                    }
                    Cache::flush();
                    return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Success']);
                } else {
                    return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Error']);
                }
            } else {
                return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Error']);
            }
        }
    }

    public function postDownload(Request $request)
    {
        $film_id = $request->film_id;
        $link_id = $request->link_id;
        if ($film_id && $link_id) {
            $setting = Setting::first();
            $data_cache = Cache::get('cache_download_' . $film_id . '_' . $link_id);
            if ($data_cache) {
                return Response::json($data_cache);
            } else {
                $film = Film::find($film_id);
                if ($film && $film->movie_series == 1) {
                    $link = LinkFilmDetail::find($link_id);
                } else if ($film) {
                    $link = LinkFilm::find($link_id);
                }
                if ($link) {
                    $setting = Setting::first();
                    $data = getDataAPI($setting->api_download . $link->link . '&email=' . $link->email);
                    Cache::put('cache_download_' . $film_id . '_' . $link_id, ['status' => 200, 'result' => $data], $setting->cache);
                    return Response::json(['status' => 200, 'result' => $data]);
                }
            }
        }
        return Response::json(['status' => 'error']);
    }

    public function postMetaFilm(Request $request)
    {
        $film_id = $request->film_id;
        $episode = $request->episode;
        if ($film_id && $episode) {
            $result = array();
            $film = Film::find($film_id);
            if ($film) {
                $result['title'] = 'Watching ' . $film->name . ' ' . $film->film_detail[$episode - 1]->name . ' on ' . str_replace('https://', '', str_replace('http://', '', url('/')));
            }
            return Response::json(['status' => 200, 'result' => $result]);
        } else {
            return Response::json(['status' => 'error']);
        }
    }

    public function postClearUserSubtitle(Request $request)
    {
        $check = json_decode($request->subtitle);
        if ($check) {
            deleteSubtitle('public/user_subtitle/' . $check['name']);
            Session::forget('subtitle_upload');
        }
    }

    public function postReportLinkAPIError(Request $request)
    {
        $film_id = $request->film_id;
        $link_id = $request->link_id;
        if ($film_id) {
            $film = Film::find($film_id);
            if ($film) {
                if ($film->movie_series) {
                    $link_film = LinkFilmDetail::find($link_id);
                    if ($link_film && $link_film->error == 0) {
                        $link_film->error = 1;
                        $link_film->save();
                        Cache::flush();
                        return Response::json(['status' => 200]);
                    }
                } else {
                    $link_film = LinkFilm::find($link_id);
                    if ($link_film && $link_film->error == 0) {
                        $link_film->error = 1;
                        $link_film->save();
                        Cache::flush();
                        return Response::json(['status' => 200]);
                    }
                }
            }
        }
        return Response::json(['status' => 'error']);
    }

    public function getNews(){
        $setting = Setting::first();
        $data_cache = Cache::get('news_page');
        if($data_cache){
            return view('frontend.theme_'.$setting->theme.'.pages.news')->with($data_cache);
        }else {
            $lang = App::getLocale();
            $lang_id = Session::get('lang_id');
            $language_list = Languages::get();
            $meta = array(
                'title' => $setting->title,
                'description' => $setting->description,
                'keywords' => $setting->keywords,
                'images' => url(Storage::url($setting->meta_images))
            );
            $genres = CategoryFilm::where('publish', 1)->orderby('name')->get();
            $country = CountryFilm::where('publish', 1)->orderby('name')->get();
            $type = TypeFilm::where('publish', 1)->orderby('name')->get();
            $news = Article::where('publish',1)->where('slug','<>','footer')->orderby('created_at', 'desc')->paginate(12);
            $footer = Ads::where('type', 'footer')->first();
            $data_cache = [
                'page' => 'news',
                'setting' => $setting,
                'meta' => $meta,
                'lang' => $lang,
                'lang_id' => $lang_id,
                'language_list' => $language_list,
                'genres' => $genres,
                'country' => $country,
                'type' => $type,
                'news'  => $news,
                'footer' => (!empty($footer) ? $footer : null)
            ];
            Cache::put('news_page', $data_cache, $setting->cache);
            return view('frontend.theme_'.$setting->theme.'.pages.news')->with($data_cache);
        }
    }

}
