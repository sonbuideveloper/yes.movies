<?php

namespace App\Http\Controllers\Admin;

use App\Models\FilmDetail;
use App\Models\ServerPlayFilm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth, Redirect, Validator, Session, Response, Cache;
use Models\MasterModel;
use Models\Setting;
use App\Models\SubtitleFilmDetail;

class SubTitleDetailController extends Controller
{
        protected $table = "subtitle_film_details";

    public function postCreate(Request $request)
    {
    	$setting = Setting::getSetting();
        $id = $request->get('id');
        $file = $request->file('subtitle');
        if ($file->isValid()) {
            $name = $file->getClientOriginalName();
            $name = getNameImage($name);
            $extension = $file->getClientOriginalExtension();
            $array_extension = array('srt', 'vtt', 'ass');
            $extension_check = 0;
            foreach ($array_extension as $value) {
                if ($extension == $value) {
                    $extension_check = 1;
                }
            }
            if ($extension_check == 0) {
                Session::flash('error', 'Wrong subtitles format');
                return Redirect::to('admin/film-series/edit/' . $request->film_id);
            }
            $fileName = $name . '-' . time() . '.' . $extension;
            $path = putUploadSubtitle($file, $fileName);

            $data = array(
                'name' => $fileName,
                'language' => $request->language,
                'film_id' => $request->film_id,
                'created_at' => gmdate('Y-m-d H:i:s', time()),
                'updated_at' => gmdate('Y-m-d H:i:s', time())
            );

            $check = SubtitleFilmDetail::where(['name' => $path, 'language' => $request->language])->first();
            if ($check == null) {
                $id = MasterModel::CreateContent($this->table, $data);
            }
            $serve = ServerPlayFilm::get();
            $film = FilmDetail::find($request->film_id);
            foreach ($serve as $item) {
                Cache::forget('data_request_api_detail_' . $film->film->id . '_' . $request->film_id . '_' . $item->id);
            }
        }

        Session::flash('success','success');
        return Redirect::to('admin/film-series/edit/'.$request->film_id);
    }

    public function getDelete($id)
    {
        if(!Auth::check() || Auth::user()->type != 1)
        {
            return Redirect::to('admin');
        }
        $check = SubtitleFilmDetail::find($id);
        if(count($check) > 0) {
        	deleteImage('public/subtitle/'.$check->name);
            $check->delete();
        }
        Session::flash('success','success');
        return Redirect::back();
    }

    public function getMulti($id){
        $episode = FilmDetail::where('film_id',$id)->orderby('name')->with('subtitle')->get();
        return view("admin.subtitle_detail.edit", [
            'user'      => Auth::user(),
            'active'    => '',
            'id'        => $id,
            'episode'   => $episode
        ]);
    }

    public function postMulti(Request $request){
        $setting = Setting::getSetting();
        $id = $request->get('id');
        $episode_id = $request->get('episode_id');
        $file = $request->file('subtitle');
        if ($file->isValid()) {
            $name = $file->getClientOriginalName();
            $name = getNameImage($name);
            $extension = $file->getClientOriginalExtension();
            $array_extension = array('srt', 'vtt', 'ass');
            $extension_check = 0;
            foreach ($array_extension as $value) {
                if ($extension == $value) {
                    $extension_check = 1;
                }
            }
            if ($extension_check == 0) {
                Session::flash('error', 'Wrong subtitles format');
                return redirect()->back();
            }
            $fileName = $name . '-' . time() . '.' . $extension;
            $path = putUploadSubtitle($file, $fileName);

            $data = array(
                'name' => $fileName,
                'language' => $request->language,
                'film_id' => $episode_id,
                'created_at' => gmdate('Y-m-d H:i:s', time()),
                'updated_at' => gmdate('Y-m-d H:i:s', time())
            );

            $check = SubtitleFilmDetail::where(['name' => $path, 'language' => $request->language])->first();
            if ($check == null) {
                $id = MasterModel::CreateContent($this->table, $data);
            }
            $serve = ServerPlayFilm::get();
            $film = FilmDetail::find($episode_id);
            foreach ($serve as $item) {
                Cache::forget('data_request_api_detail_' . $film->film->id . '_' . $request->film_id . '_' . $item->id);
            }
        }

        Session::flash('success','success');
        return redirect()->back();
    }
}
