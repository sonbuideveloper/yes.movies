<?php

namespace App\Http\Controllers\Admin;

use App\Models\ServerPlayFilm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth, Redirect, Validator, Session, Response, Cache;
use Models\MasterModel;
use Models\Setting;
use App\Models\SubtitleFilm;

class SubTitleController extends Controller
{
    protected $table = "subtitle_films";

    public function postCreate(Request $request)
    {
    	$setting = Setting::getSetting();
        $id = $request->get('id');
        $file = $request->file('subtitle');
        if ($file->isValid()) {
            $name = $file->getClientOriginalName();
            $name = getNameImage($name);
            $extension = $file->getClientOriginalExtension();
            $array_extension = array('srt', 'vtt', 'ass');
            $extension_check = 0;
            foreach ($array_extension as $value) {
            	if($extension == $value){
            		$extension_check = 1;
            	}
            }
            if($extension_check==0){
            	Session::flash('error','Wrong subtitles format');
        		return Redirect::to('admin/film/edit/'.$request->film_id);
            }
            $fileName = $name.'-'.time().'.'.$extension;
            $path = putUploadSubtitle($file, $fileName);
            
            $data = array(
	            'name'              => $fileName,
	            'language'			=> $request->language,
	            'film_id'           => $request->film_id,
	            'created_at'        => gmdate('Y-m-d H:i:s', time()),
	            'updated_at'        => gmdate('Y-m-d H:i:s', time())
	        );

	        $check = SubtitleFilm::where(['name' => $path, 'language' => $request->language])->first();
            if($check == null) {
                $id = MasterModel::CreateContent($this->table, $data);
            }
            $serve = ServerPlayFilm::get();
            foreach ($serve as $item){
                Cache::forget('data_request_api_'.$request->film_id.'_'.$item->id);
            }
        }

        Session::flash('success','success');
        return Redirect::to('admin/film/edit/'.$request->film_id);
    }

    public function getDelete($id)
    {
        if(!Auth::check() || Auth::user()->type != 1)
        {
            return Redirect::to('admin');
        }
        $check = SubtitleFilm::find($id);
        if(count($check) > 0) {
        	deleteImage('public/subtitle/'.$check->name);
            $check->delete();
        }
        Session::flash('success','success');
        return Redirect::back();
    }

    public function getMulti($id){
        $subtitle = SubtitleFilm::where('film_id',$id)->orderby('language')->get();
        return view("admin.subtitle.edit", [
            'user'      => Auth::user(),
            'active'    => '',
            'id'        => $id,
            'subtitle'  => $subtitle
        ]);
    }

    public function postMulti(Request $request){
        $setting = Setting::getSetting();
        $film_id = $request->get('id');
        $file = $request->file('subtitle');
        if ($file->isValid()) {
            $name = $file->getClientOriginalName();
            $name = getNameImage($name);
            $extension = $file->getClientOriginalExtension();
            $array_extension = array('srt', 'vtt', 'ass');
            $extension_check = 0;
            foreach ($array_extension as $value) {
                if($extension == $value){
                    $extension_check = 1;
                }
            }
            if($extension_check==0){
                Session::flash('error','Wrong subtitles format');
                return redirect()->back();
            }
            $fileName = $name.'-'.time().'.'.$extension;
            $path = putUploadSubtitle($file, $fileName);

            $data = array(
                'name'              => $fileName,
                'language'			=> $request->language,
                'film_id'           => $film_id,
                'created_at'        => gmdate('Y-m-d H:i:s', time()),
                'updated_at'        => gmdate('Y-m-d H:i:s', time())
            );

            $check = SubtitleFilm::where(['name' => $path, 'language' => $request->language])->first();
            if($check == null) {
                MasterModel::CreateContent($this->table, $data);
            }
            $serve = ServerPlayFilm::get();
            foreach ($serve as $item){
                Cache::forget('data_request_api_'.$request->film_id.'_'.$item->id);
            }
        }

        Session::flash('success','success');
        return redirect()->back();
    }
}
