<?php

namespace App\Http\Controllers\Admin;

use App\Models\ServerPlayFilm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth, Session, Redirect, Validator;
use Models\MasterModel;
use Models\Setting;

class ServerPlayFilmController extends Controller
{
    protected $table = "server_play_films";

    public function getIndex()
    {
        return view("admin.server_film.index", [
            'user' => Auth::user(),
            'active' => 'server_film',
            'result' => ServerPlayFilm::RecursiveIndexAdmin()
        ]);
    }

    public function getCreate()
    {
        return view("admin.server_film.edit", [
            'user' => Auth::user(),
            'active' => 'server_film',
            'action' => 'Create',
            'setting' => Setting::first()
        ]);
    }

    public function getEdit($id)
    {
        $result = ServerPlayFilm::getById($id);
        return view("admin.server_film.edit", [
            'user' => Auth::user(),
            'active' => 'server_film',
            'action' => 'Edit',
            'setting' => Setting::first(),
            'result' => $result,
        ]);
    }

    public function getDelete($id)
    {
        if(!Auth::check() || Auth::user()->type != 1)
        {
            return Redirect::to('admin');
        }
        $check = ServerPlayFilm::find($id);
        if(count($check) > 0) {
            ServerPlayFilm::DeleteById($id);
        }
        Session::flash('delete','success');
        return Redirect::to('admin/server-film/list');
    }

    public function postDelete(Request $request)
    {
        if(!Auth::check() || Auth::user()->type != 1)
        {
            return Redirect::to('admin');
        }
        $id = $request->get('id');
        if(!empty($id)) {
            foreach ($id as $item)
            {
                ServerPlayFilm::DeleteById($item);
            }
        }
        Session::flash('delete','success');
        return Redirect::to('admin/server-film/list');
    }

    public function postCreate(Request $request)
    {
        $rules = [
            'name' => 'required'
        ];
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $setting = Setting::getSetting();
        $id = $request->get('id');
        $data = array(
            'name' => $request->get('name'),
            'slug' => $request->get('slug'),
            'embedded'   => ($request->get('embedded') == 'on')?1:0,
            'publish' => ($request->get('publish') == 'on')?1:0,
            'created_at' => gmdate('Y-m-d H:i:s', time()),
            'updated_at' => gmdate('Y-m-d H:i:s', time())
        );
        if($id == 0) {
            $check = ServerPlayFilm::where(['name' => $request->get('name'), 'slug' => $request->get('slug')])->first();
            if($check == null) {
                $id = MasterModel::CreateContent($this->table, $data);
            }
        } else {
            MasterModel::CreateContent($this->table, $data, ['id' => $id]);
        }
        Session::flash('success','success');
        return Redirect::to('admin/server-film/edit/'.$id);
    }

    public function getCheckStatus($id)
    {
        $check = ServerPlayFilm::find($id);
        if($check != null)
        {
            if ($check->publish == 1)
            {
                MasterModel::CreateContent($this->table,['publish' => 0], ['id' => $id]);
                return response()->json(['status' => 200, 'result' => 'hidden']);
            } else {
                MasterModel::CreateContent($this->table,['publish' => 1], ['id' => $id]);
                return response()->json(['status' => 200, 'result' => 'publish']);
            }
        }
        return response()->json(['status' => 'error']);
    }
}
