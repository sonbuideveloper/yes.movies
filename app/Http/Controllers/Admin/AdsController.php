<?php

namespace App\Http\Controllers\Admin;

use App\Models\Ads;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth, Session, Redirect, Validator, Response;
use Models\MasterModel;
use Models\Setting;

class AdsController extends Controller
{
    private $table = "ads";

    private $media = "media";

    public function getIndex()
    {
        return view("admin.ads.index", [
            'user' => Auth::user(),
            'active' => 'ads',
            'result' => Ads::RecursiveIndexAdmin('ads')
        ]);
    }

    public function getCreate()
    {
        return view("admin.ads.edit", [
            'user' => Auth::user(),
            'active' => 'ads',
            'action' => 'Create',
            'setting' => Setting::first()
        ]);
    }

    public function postCreate(Request $request)
    {
        $rules = [
            'name' => 'required'
        ];
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $setting = Setting::getSetting();
        $id = $request->get('id');
        $data = array(
            'name' => $request->get('name'),
            'slug' => $request->get('slug'),
            'content' => addslashes($request->get('content')),
            'publish' => ($request->get('publish') == 'on')?1:0,
            'type'  => 'ads'
        );
        if($id == 0) {
            $data['created_at'] = gmdate('Y-m-d H:i:s', time());
            $check = Ads::where(['name' => $request->name, 'slug' => $request->slug])->first();
            if($check == null) {
                $id = MasterModel::CreateContent($this->table, $data);
            }else{
                Session::flash('error','Ads Exist');
                return redirect()->back();
            }
        } else {
            $data['updated_at'] = gmdate('Y-m-d H:i:s', time());
            MasterModel::CreateContent($this->table, $data, ['id' => $id]);
        }
        Session::flash('success','success');
        return redirect('admin/ads/edit/'.$id);
    }

    public function getEdit($id)
    {
        $result = Ads::getById($id);

        return view("admin.ads.edit", [
            'user' => Auth::user(),
            'active' => 'ads',
            'action' => 'Edit',
            'setting' => Setting::first(),
            'result' => $result
        ]);
    }

    public function getCheckStatus($type,$id)
    {
        $check = Ads::find($id);
        if($check != null)
        {
            $hidden = "hidden";
            if($type == 'featured'){
                $hidden = "normal";
            }
            if ($check->$type == 1)
            {
                MasterModel::CreateContent($this->table,[$type => 0], ['id' => $id]);
                return Response::json(['status' => 200, 'result' => $hidden]);
            } else {
                MasterModel::CreateContent($this->table,[$type => 1], ['id' => $id]);
                return Response::json(['status' => 200, 'result' => $type]);
            }
        }
        return Response::json(['status' => 'error']);
    }

    public function getFooter(){
        $result = Ads::where('type', 'footer')->first();

        return view("admin.article.preview.footer", [
            'user' => Auth::user(),
            'active' => 'footer',
            'action' => 'Edit',
            'setting' => Setting::first(),
            'result' => $result
        ]);
    }

    public function postFooter(Request $request){
        $rules = [
            'name' => 'required'
        ];
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $id = $request->get('id');
        $data = array(
            'name' => $request->get('name'),
            'slug' => $request->get('slug'),
            'content' => addslashes($request->get('content')),
            'description' => addslashes($request->get('description')),
            'publish' => ($request->get('publish') == 'on')?1:0,
            'type'  => 'footer',
            'created_at' => gmdate('Y-m-d H:i:s', time()),
            'updated_at' => gmdate('Y-m-d H:i:s', time())
        );
        if($id == 0) {
            $check = Ads::where(['name' => $request->get('name'), 'slug' => $request->get('slug')])->first();
            if($check == null) {
                $id = MasterModel::CreateContent($this->table, $data);
            }
        } else {
            MasterModel::CreateContent($this->table, $data, ['id' => $id]);
        }
        Session::flash('success','success');
        return redirect()->back();
    }
}
