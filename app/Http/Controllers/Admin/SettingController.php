<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth,Redirect,Validator,Response, Cache;
use Illuminate\Support\Facades\Storage;
use Models\Contact;
use Models\Contents;
use Models\Languages;
use Models\MasterModel;
use Models\Media;
use Models\Setting;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @var string
     */
    private $table = "setting";

    private $media = "media";

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex() {
        return view("admin.setting.edit", [
            'user' => Auth::user(),
            'country' => Languages::get(),
            'active' => 'setting',
            'result' => Setting::getSetting()
        ]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function postContent(Request $request) {
        $validation = Validator::make($request->all(), ['name' => 'required']);
        if($validation->fails()){
            return Redirect::back()->withErrors( $validation )->withInput();
        }
        $data = array(
            'name' => $request->get('name'),
            'language' => ($request->get('language'))?implode(',',$request->get('language')):null,
            'address' => $request->get('address'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),        
            'title' => $request->get('title'),
            'keywords' => $request->get('keywords'),
            'description' => $request->get('description'),
            'google_url' => $request->get('google_url'),
            'facebook_url' => $request->get('facebook_url'),
            'twitter_url' => $request->get('twitter-page'),
            'google_analytic' => $request->get('google_analytic'),
            'api_youtube'   => $request->api_youtube,
            'app_id_face'   => $request->app_id_face,
            'api_player'    => $request->api_player,
            'api_download'    => $request->api_download,
            'cache'         => $request->cache,
            'film_home'     => $request->film_home,
            'film_page'     => $request->film_page,
            'key_jwplayer'  => $request->key_jwplayer,
            'theme'         => $request->theme
        );        
        $check = Setting::first();
        if($check == null){
            MasterModel::CreateContent($this->table, $data);
            return Redirect::back();
        } else {
            MasterModel::CreateContent($this->table, $data, ['id' => $check->id]);                
            return Redirect::back();
        }
    }

    public function postAvatar(Request $request) {
        $file = $request->file('images');
        if ($file->isValid()) {
            $name = $file->getClientOriginalName();
            $name = getNameImage($name);
            $extension = $file->getClientOriginalExtension();
            $fileName = $name.'-'.time().'.'.$extension;

            $check = Setting::first();
            if($check == null) {
                $path = putUploadImage($request->file('images'), $fileName);
                MasterModel::CreateContent($this->table,['images' => $path]);
            } else {
                if($check->images != null){
                    deleteImage($check->images);
                }
                $path = putUploadImage($request->file('images'), $fileName);
                MasterModel::CreateContent($this->table,['images' => $path], ['id' => $check->id]);
            }
        }

    }

    public function postDeleteImage(Request $request) {
        $check = Setting::find($request->get('id'));
        if($check != null){
            if($check->images != null) {
                deleteImage($check->images);
                MasterModel::CreateContent($this->table,['images' => null], ['id' => $check->id]);
            }
        }
        return Response::json(['status' => 200]);
    }

    public function postMetaImages(Request $request) {
        $file = $request->file('images');
        if ($file->isValid()) {
            $name = $file->getClientOriginalName();
            $name = getNameImage($name);
            $extension = $file->getClientOriginalExtension();
            $fileName = $name.'-'.time().'.'.$extension;

            $check = Setting::first();
            if($check == null) {
                $path = putUploadImage($request->file('images'), $fileName);
                MasterModel::CreateContent($this->table,['meta_images' => $path]);
            } else {
                if($check->meta_images != null){
                    deleteImage($check->meta_images);
                }
                $path = putUploadImage($request->file('images'), $fileName);
                MasterModel::CreateContent($this->table,['meta_images' => $path], ['id' => $check->id]);
            }
        }
    }

    public function postDeleteMetaImage(Request $request) {
        $check = Setting::find($request->get('id'));
        if($check != null){
            if($check->meta_images != null) {
                deleteImage($check->meta_images);
                MasterModel::CreateContent($this->table,['meta_images' => null], ['id' => $check->id]);
            }
        }
        return Response::json(['status' => 200]);
    }

    public function postDeleteMediaImage(Request $request)
    {
        $check = Media::find($request->get('id'));
        if($check != null){
            if($check->images != null) {
                deleteImage($check->images);
                Media::find($request->get('id'))->delete();
            }
        }
        return Response::json(['status' => 200]);
    }

    public function postUpdateNameMedia(Request $request)
    {
        $check = Media::find($request->get('id'));
        if($check != null)
        {
            MasterModel::CreateContent($this->media,['name' => $request->get('name')], ['id' => $check->id]);
        }
        return Response::json(['status' => 200]);
    }

    public function postUpdateOrderMedia(Request $request)
    {
        $check = Media::find($request->get('id'));
        if($check != null)
        {
            MasterModel::CreateContent($this->media,['order_by' => $request->get('order_by')], ['id' => $check->id]);
        }
        return Response::json(['status' => 200]);
    }

    public function getClearCache(){
        Cache::flush();
        return redirect()->back();
    }
}
