<?php

namespace App\Http\Controllers\Admin;

use App\Models\ColorProduct;
use App\Models\TypeProduct;
use App\Models\TypeProductRelation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Models\Article;
use Models\Category;
use Models\CategoryProducts;
use Models\Contact;
use Models\Contents;
use Models\MapTags;
use Models\MasterModel;
use Models\Products;
use Models\Setting;
use Models\Languages;
use Models\Tags;
use Response, Redirect, Session, Validator, DB, Auth;

class GalleryController extends Controller
{
    private $content = "contents";

    private $table = "articles";

    private $media = "media";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        return view("admin.gallery.preview.index", [
            'user' => Auth::user(),
            'active' => 'gallery',
            'sub_action' => 'gallery',
            'message' => Contact::where('publish', 0)->count(),
            'result' => Article::RecursiveIndexAdmin('gallery')
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
        $films = Products::orderby('name')->get();
        $casts = CategoryProducts::orderby('name')->get();
        $directors = ColorProduct::orderby('name')->get();
        $category_type = TypeProduct::RecursiveCategory(0, 0, 0, '', 'gallery');

        $extra_tag_data = Tags::where('type','extraTag')->get();
        $extra_tag_all = '';
        foreach ($extra_tag_data as $item) {
            $extra_tag_all .= ','.$item->name;
        }
        $topic_tag_data = Tags::where('type','topicTag')->get();
        $topic_tag_all = '';
        foreach ($topic_tag_data as $item) {
            $topic_tag_all .= ','.$item->name;
        }
        $manual_tag_data = Tags::where('type','manualTag')->get();
        $manual_tag_all = '';
        foreach ($manual_tag_data as $item) {
            $manual_tag_all .= ','.$item->name;
        }
        $random_tag_data = Tags::where('type','randomTag')->get();
        $random_tag_all = '';
        foreach ($random_tag_data as $item) {
            $random_tag_all .= ','.$item->name;
        }

        return view("admin.gallery.preview.edit", [
            'user' => Auth::user(),
            'active' => 'gallery',
            'sub_action' => 'gallery',
            'action' => 'Create',
            'message' => Contact::where('publish', 0)->count(),
            'setting' => Setting::first(),            
            'type'  => $category_type,
            'films' => $films,
            'casts' => $casts,
            'directors' => $directors,
            'films_select'  => '',
            'cast_select'   => '',
            'director_select'   => '',
            'extra_tag_all'     => $extra_tag_all,
            'topic_tag_all'     => $topic_tag_all,
            'manual_tag_all'    => $manual_tag_all,
            'random_tag_all'    => $random_tag_all
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($id)
    {
        $result = Article::getById($id);
        $extra_tag = "";
        $topic_tag = "";
        $manual_tag = "";
        $random_tag = "";
        if(!empty($result) && count($result->mapTags) > 0)
        {
            foreach ($result->mapTags as $item)
            {
                if($item->tags->type == 'extraTag'){
                    $extra_tag .= $item->tags->name.",";
                }
                if($item->tags->type == 'topicTag'){
                    $topic_tag .= $item->tags->name.",";
                }
                if($item->tags->type == 'manualTag'){
                    $manual_tag .= $item->tags->name.",";
                }
                if($item->tags->type == 'randomTag'){
                    $random_tag .= $item->tags->name.",";
                }
            }
            $extra_tag = rtrim($extra_tag, ",");
            $topic_tag = rtrim($topic_tag, ",");
            $manual_tag = rtrim($manual_tag, ",");
            $random_tag = rtrim($random_tag, ",");
        }
        
        $type = TypeProductRelation::where('product_id',$id)->get()->toArray();
        
        if($result->film_id){
            $films_select = Products::whereIn('id', explode(',', $result->film_id))->get();
            $films = Products::whereNotIn('id', explode(',', $result->film_id))->orderby('name')->get();
        }else{
            $films_select = '';
            $films = Products::orderby('name')->get();
        }

        if($result->cast_id){
            $cast_select = CategoryProducts::whereIn('id', explode(',', $result->cast_id))->get();
            $casts = CategoryProducts::whereNotIn('id', explode(',', $result->cast_id))->orderby('name')->get();
        }else{
            $cast_select = '';
            $casts = CategoryProducts::orderby('name')->get();
        }

        if($result->director_id){
            $director_select = ColorProduct::whereIn('id', explode(',', $result->director_id))->get();
            $directors = ColorProduct::whereNotIn('id', explode(',', $result->director_id))->orderby('name')->get();
        }else{
            $director_select = '';
            $directors = ColorProduct::orderby('name')->get();
        }

        $extra_tag_data = Tags::where('type','extraTag')->get();
        $extra_tag_all = '';
        foreach ($extra_tag_data as $item) {
            $extra_tag_all .= ','.$item->name;
        }
        $topic_tag_data = Tags::where('type','topicTag')->get();
        $topic_tag_all = '';
        foreach ($topic_tag_data as $item) {
            $topic_tag_all .= ','.$item->name;
        }
        $manual_tag_data = Tags::where('type','manualTag')->get();
        $manual_tag_all = '';
        foreach ($manual_tag_data as $item) {
            $manual_tag_all .= ','.$item->name;
        }
        $random_tag_data = Tags::where('type','randomTag')->get();
        $random_tag_all = '';
        foreach ($random_tag_data as $item) {
            $random_tag_all .= ','.$item->name;
        }
        
        return view("admin.gallery.preview.edit", [
            'user' => Auth::user(),
            'active' => 'gallery',
            'sub_action' => 'gallery',
            'action' => 'Edit',
            'message' => Contact::where('publish', 0)->count(),
            'setting' => Setting::first(),
            'result' => $result,
            'type'  => TypeProduct::RecursiveCategory(0, 0, $type, '', 'gallery'),
            'films' => $films,
            'casts' => $casts,
            'directors' => $directors,
            'films_select'  => $films_select,
            'cast_select'   => $cast_select,
            'director_select'   => $director_select,
            'extra_tag'     => $extra_tag,
            'topic_tag'     => $topic_tag,
            'manual_tag'    => $manual_tag,
            'random_tag'    => $random_tag,
            'extra_tag_all' => $extra_tag_all,
            'topic_tag_all' => $topic_tag_all,
            'manual_tag_all'=> $manual_tag_all,
            'random_tag_all'=> $random_tag_all
        ]);
    }

    /**
     * Delete item by id using method get
     * @param $id
     * @return mixed
     */
    public function getDelete($id)
    {
        if(!Auth::check() || Auth::user()->type != 1)
        {
            return Redirect::to('admin');
        }
        $check = Article::find($id);
        if(count($check) > 0) {
            Article::DeleteById($id);
        }
        Session::flash('delete','success');
        return Redirect::to('admin/gallery/list');
    }

    /**
     * edit status public or hidden
     * @param $id
     * @return mixed
     */
    public function getCheckStatus($type,$id)
    {
        $check = Article::find($id);
        if($check != null)
        {
            $hidden = "hidden";
            if($type == 'featured'){
                $hidden = "normal";
            }
            if ($check->$type == 1)
            {
                MasterModel::CreateContent($this->table,[$type => 0], ['id' => $id]);
                return Response::json(['status' => 200, 'result' => $hidden]);
            } else {
                MasterModel::CreateContent($this->table,[$type => 1], ['id' => $id]);
                return Response::json(['status' => 200, 'result' => $type]);
            }
        }
        return Response::json(['status' => 'error']);
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     * Update order
     */
    public function getUpdateOrder(Request $request, $id)
    {
        $val = $request->get('val');
        $check = Article::find($id);
        if($check != null)
        {
            MasterModel::CreateContent($this->table,['order_by' => $val], ['id' => $id]);
            return Response::json(['status' => 200,'result' => $val]);
        }
        return Response::json(['status' => 'error']);
    }

    /**
     * Delete all item using checkbox, method post
     * @param Request $request
     * @return mixed
     */
    public function postDelete(Request $request)
    {
        if(!Auth::check() || Auth::user()->type != 1)
        {
            return Redirect::to('admin');
        }
        $id = $request->get('id');
        if(!empty($id)) {
            foreach ($id as $item)
            {
                Article::DeleteById($item);
            }
        }
        Session::flash('delete','success');
        return Redirect::to('admin/gallery/list');
    }

    /**
     * check detect links
     * @param Request $request
     * @return mixed
     */
    public function getCheckLink(Request $request)
    {
        if($request->get('lang') == 'general') {
            $check = Article::where('slug',$request->get('slug'))->where('id','<>',$request->get('id'))->first();

        } else {
            $language_id = Languages::where('language',$request->get('lang'))->pluck('id');
            $check = Contents::where(['language_id' => $language_id, 'type' => 'article', 'slug' => $request->get('slug')])->where('category_id','<>',$request->get('id'))->first();
        }
        if($check == null){
            $slug = $request->get('slug');
        } else {
            $slug = Article::count()."-".$request->get('slug');
        }
        return Response::json(['status' => 200, 'result' => $slug]);
    }

    /**
     * Create or edit one item
     * @param Request $request
     * @return mixed
     */
    public function postCreate(Request $request)
    {
        $rules = [
            'name' => 'required',
        ];
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }
        
        $setting = Setting::getSetting();
        $id = $request->get('id');
        $data = array(
            'name' => $request->get('name'),
            'slug' => $request->get('slug'),
            'content' => addslashes($request->get('content')),
            'description' => addslashes($request->get('description')),
            'order_by' => (!empty($request->get('order_by'))?$request->get('order_by'):0),
            'film_id' => (!empty($request->get('parent_id_film'))?implode($request->get('parent_id_film'),','):''),
            'cast_id' => (!empty($request->get('parent_id_cast'))?implode($request->get('parent_id_cast'),','):''),
            'director_id' => (!empty($request->get('parent_id_director'))?implode($request->get('parent_id_director'),','):0),
            'publish' => ($request->get('publish') == 'on')?1:0,
            'featured' => ($request->get('featured') == 'on')?1:0,
            'type'      => 'gallery',
            'title' => (!empty($request->get('meta-title'))?$request->get('meta-title'):$request->get('name')),
            'descriptions' => $request->get('meta-description'),
            'keywords' => $request->get('meta-keywords'),
            'created_at' => gmdate('Y-m-d H:i:s', time()),
            'updated_at' => gmdate('Y-m-d H:i:s', time())
        );
        
        $type_id = $request->get('type');
        if($id == 0) {
            $check = Article::where(['name' => $request->get('name'), 'slug' => $request->get('slug')])->first();
            if($check == null) {
                $id = MasterModel::CreateContent($this->table, $data);
                if(count($type_id) > 0) {
                    foreach ($type_id as $item) {
                        MasterModel::CreateContent('type_product_relations', array('product_id' => $id, 'category_id' => $item));
                    }
                }
            }
        } else {
            MasterModel::CreateContent($this->table, $data, ['id' => $id]);
            MasterModel::DeleteContent('type_product_relations', ['product_id'=>$id]);
            if(count($type_id) > 0) {
                foreach ($type_id as $item) {
                    MasterModel::CreateContent('type_product_relations', array('product_id' => $id, 'category_id' => $item));
                }
            }
        }
        
        if($request->get('extraTag') != "") {
            $tags = explode(",", $request->get('extraTag'));
            $tags_arr = array();
            foreach ($tags as $tag)
            {
                $data_tags = array(
                    'name' => $tag,
                    'type' => 'extraTag',
                    'slug' => clearUnicode($tag)
                );
                $tags_id = Tags::CreateTags($data_tags, $id, 'extraTag');
                array_push($tags_arr, $tags_id->id);
            }
            MapTags::DeleteMapTagsNotInTagsId($id, $tags_arr, 'extraTag');
        } else {
            MapTags::DeleteMapTags($id, 'extraTag');
        }

        if($request->get('topicTag') != "") {
            $tags = explode(",", $request->get('topicTag'));
            $tags_arr = array();
            foreach ($tags as $tag)
            {
                $data_tags = array(
                    'name' => $tag,
                    'type' => 'topicTag',
                    'slug' => clearUnicode($tag)
                );
                $tags_id = Tags::CreateTags($data_tags, $id, 'topicTag');
                array_push($tags_arr, $tags_id->id);
            }
            MapTags::DeleteMapTagsNotInTagsId($id, $tags_arr, 'topicTag');
        } else {
            MapTags::DeleteMapTags($id, 'topicTag');
        }

        if($request->get('manualTag') != "") {
            $tags = explode(",", $request->get('manualTag'));
            $tags_arr = array();
            foreach ($tags as $tag)
            {
                $data_tags = array(
                    'name' => $tag,
                    'type' => 'manualTag',
                    'slug' => clearUnicode($tag)
                );
                $tags_id = Tags::CreateTags($data_tags, $id, 'manualTag');
                array_push($tags_arr, $tags_id->id);
            }
            MapTags::DeleteMapTagsNotInTagsId($id, $tags_arr, 'manualTag');
        } else {
            MapTags::DeleteMapTags($id, 'manualTag');
        }

        if($request->get('randomTag') != "") {
            $tags = explode(",", $request->get('randomTag'));
            $tags_arr = array();
            foreach ($tags as $tag)
            {
                $data_tags = array(
                    'name' => $tag,
                    'type' => 'randomTag',
                    'slug' => clearUnicode($tag)
                );
                $tags_id = Tags::CreateTags($data_tags, $id, 'randomTag');
                array_push($tags_arr, $tags_id->id);
            }
            MapTags::DeleteMapTagsNotInTagsId($id, $tags_arr, 'randomTag');
        } else {
            MapTags::DeleteMapTags($id, 'randomTag');
        }

        Session::flash('success','success');
        return Redirect::to('admin/gallery/edit/'.$id);
    }

    /**
     * Create or edit google meta
     * @param Request $request
     * @return mixed
     */
    public function postMetaData(Request $request)
    {
        $setting = Setting::getSetting();
        $id = $request->get('id');
        $data = array(
            'title' => $request->get('meta-title'),
            'descriptions' => $request->get('meta-description'),
            'keywords' => $request->get('meta-keywords')
        );
        if($id == 0) {
            $check = Article::where(['name' => $request->get('name'), 'slug' => $request->get('slug')])->first();
            if(count($check) == 0) {
                $id = MasterModel::CreateContent($this->table, $data);
            }
        } else {
            MasterModel::CreateContent($this->table, $data, ['id' => $id]);
        }
        if(count($setting)>0 && $setting->language != null){
            $language = explode(',',$setting->language);
            foreach ($language as $language_id){
                $check_lang = Languages::find($language_id);
                if($check_lang != null){
                    if($request->get('title'.$check_lang->language) != "" || $request->get('description'.$check_lang->language)  != "" || $request->get('keywords'.$check_lang->language) != "") {
                        $data_content = array(
                            'title' => $request->get('title'.$check_lang->language),
                            'descriptions' => $request->get('description'.$check_lang->language),
                            'keywords' => $request->get('keywords'.$check_lang->language),
                            'language_id' => $language_id,
                            'article_id' => $id,
                            'type' => 'article'
                        );
                        $check_contents = Contents::where(['article_id' => $id, 'language_id' => $language_id])->first();
                        if($check_contents == null){
                            MasterModel::CreateContent($this->content, $data_content);
                        } else {
                            MasterModel::CreateContent($this->content, $data_content, ['article_id' => $id, 'language_id' => $language_id]);
                        }
                    }
                }
            }
        }
        Session::flash('success','success');
        return Redirect::to('admin/gallery/edit/'.$id);
    }

    /**
     * upload images
     * @param Request $request
     */
    public function postAvatar(Request $request) {
        $id = $request->get('id');
        $file = $request->file('images');
        if ($file->isValid()) {
            $name = $file->getClientOriginalName();
            $name = getNameImage($name);
            $extension = $file->getClientOriginalExtension();
            $fileName = $name.'-'.time().'.'.$extension;

            $check = Article::find($id);
            if($check == null) {
                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->table,['images' => $path]);
            } else {
                if($check->images != null){
                    deleteImage($check->images);
                }
                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->table,['images' => $path], ['id' => $check->id]);
            }
        }

    }

    public function postMultipleImages(Request $request) {
        $id = $request->get('id');
        $files = $request->file('images');
        foreach ($files as $file){
            if ($file->isValid()) {
                $name = $file->getClientOriginalName();
                $name = getNameImage($name);
                $extension = $file->getClientOriginalExtension();
                $fileName = $name.'-'.time().'.'.$extension;

                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->media,['images' => $path, 'type' => 'article', 'article_id' => $id]);
            }
        }

    }

    /**
     * Delete image
     * @param Request $request
     * @return mixed
     */
    public function postDeleteImage(Request $request) {
        $check = Article::find($request->get('id'));
        if($check != null){
            if($check->images != null) {
                deleteImage($check->images);
                MasterModel::CreateContent($this->table,['images' => null], ['id' => $check->id]);
            }
        }
        return Response::json(['status' => 200]);
    }

    public function postCategory(Request $request){
        $id_parent = $request->get('id');
        if($id_parent == 0){
            return 'fail';
        }else if($id_parent == 1){
            $query = Products::orderby('name')->get();
            return Article::RecursiveCategory(0,0,0,$query);
        }else if($id_parent == 2){
            $query = CategoryProducts::orderby('name')->get();
            return Article::RecursiveCategory(0,0,0,$query);
        }else if($id_parent == 3){
            $query = ColorProduct::orderby('name')->get();
            return Article::RecursiveCategory(0,0,0,$query);
        }
    }
}
