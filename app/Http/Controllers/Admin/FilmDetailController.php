<?php

namespace App\Http\Controllers\Admin;

use App\Models\Film;
use App\Models\FilmDetail;
use App\Models\LinkFilm;
use App\Models\LinkFilmDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth, Session, Redirect, Response, Validator;
use Models\MasterModel;
use Models\Setting;
use App\Models\ServerPlayFilm;

class FilmDetailController extends Controller
{
    private $table = "film_details";

    private $media = "media";

    public function getIndex(Request $request)
    {
        $key = $request->key;
        return view("admin.film_series.index", [
            'user' => Auth::user(),
            'active' => 'film-series',
            'sub_action' => 'films',
            'result' => FilmDetail::RecursiveIndexAdmin($key),
            'key'   => $key
        ]);
    }

    public function getCreate()
    {
        $film = Film::where('movie_series',1)->orderby('name')->get();
        $server_play = ServerPlayFilm::where('publish',1)->orderby('name')->get();

        return view("admin.film_series.edit", [
            'user' => Auth::user(),
            'active' => 'film-series',
            'action' => 'Create',
            'setting' => Setting::first(),
            'film'    => $film,
            'server_play'   => $server_play
        ]);
    }

    public function getEdit($id)
    {
        $result = FilmDetail::getById($id);
        $film = Film::where('movie_series',1)->orderby('name')->get();
        $server_play = ServerPlayFilm::where('publish',1)->orderby('name')->get();

        return view("admin.film_series.edit", [
            'user' => Auth::user(),
            'active' => 'film-series',
            'action' => 'Edit',
            'setting' => Setting::first(),
            'result' => $result,
            'cast_all'  => (!empty($cast_all) ? implode($cast_all, ',') : ''),
            'director_all'  => (!empty($director_all) ? implode($director_all, ',') : ''),
            'tag_film_all'  => (!empty($tag_film) ? implode($tag_film, ',') : ''),
            'film'          => $film,
            'server_play'   => $server_play
        ]);
    }

    public function getListEposide($id){
        $film = Film::find($id);
        return view("admin.film_series.index-episode", [
            'user' => Auth::user(),
            'active' => 'film-series',
            'sub_action' => 'films',
            'result' => FilmDetail::RecursiveIndexAdminEposide($id),
            'film'  => $film
        ]);
    }

    public function getCreateEposide($id){
        $server_play = ServerPlayFilm::where('publish',1)->orderby('name')->get();
        $film = Film::find($id);

        return view("admin.film_series.edit-episode", [
            'user' => Auth::user(),
            'active' => 'film-series',
            'action' => 'Create',
            'setting' => Setting::first(),
            'film_id'   => $id,
            'server_play'   => $server_play,
            'film'  => $film
        ]);
    }

    public function postCreateEposide(Request $request){
        $id = $request->get('id');
        if($request->get('meta-description') == ''){
            $description = substr(str_replace('<p>', '', str_replace('</p>', '', addslashes($request->get('content')))), 0, 250);
        }else{
            $description = $request->get('meta-description');
        }
        $data = array(
            'name'              => $request->get('name'),
            'slug'              => $request->get('slug'),
            'duration'          => (!empty($request->duration)?$request->duration:0),
            'quality'           => $request->quality,
            'release'           => $request->release,
            'day_release'       => $request->day_release,
            'end_day'           => $request->end_day,
            'content'           => addslashes($request->get('content')),
            'publish'           => ($request->get('publish') == 'on')?1:0,
            'film_id'           => $request->film_id,
            'created_at'        => gmdate('Y-m-d H:i:s', time()),
            'updated_at'        => gmdate('Y-m-d H:i:s', time())
        );

        if($id == 0) {
            $check = FilmDetail::where(['name' => $request->get('name'), 'slug' => $request->get('slug')])->first();
            if($check == null) {
                $id = MasterModel::CreateContent($this->table, $data);
            }else{
                Session::flash('error','Name exist');
                return Redirect::back();
            }
        } else {
            MasterModel::CreateContent($this->table, $data, ['id' => $id]);
        }


        Session::flash('success','success');
        return Redirect::to('admin/film-series/edit/'.$id);
    }

    public function getDelete($id)
    {
        $check = FilmDetail::find($id);
        if(count($check) > 0) {
            FilmDetail::DeleteById($id);
        }
        Session::flash('delete','success');
        return Redirect::back();
    }

    /**
     * edit status public or hidden
     * @param $id
     * @return mixed
     */
    public function getCheckStatus($type,$id)
    {
        $check = FilmDetail::find($id);
        if($check != null)
        {
            $hidden = "hidden";
            if($type == 'featured'){
                $hidden = "normal";
            }
            if ($check->$type == 1)
            {
                MasterModel::CreateContent($this->table,[$type => 0], ['id' => $id]);
                return Response::json(['status' => 200, 'result' => $hidden]);
            } else {
                MasterModel::CreateContent($this->table,[$type => 1], ['id' => $id]);
                return Response::json(['status' => 200, 'result' => $type]);
            }
        }
        return Response::json(['status' => 'error']);
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     * Update order
     */
    public function getUpdateOrder(Request $request, $id)
    {
        $val = $request->get('val');
        $check = Products::find($id);
        if($check != null)
        {
            MasterModel::CreateContent($this->table,['order_by' => $val], ['id' => $id]);
            return Response::json(['status' => 200,'result' => $val]);
        }
        return Response::json(['status' => 'error']);
    }

    /**
     * Delete all item using checkbox, method post
     * @param Request $request
     * @return mixed
     */
    public function postDelete(Request $request)
    {
        if(!Auth::check() || Auth::user()->type != 1)
        {
            return Redirect::to('admin');
        }
        $id = $request->get('id');
        if(!empty($id)) {
            foreach ($id as $item)
            {
                FilmDetail::DeleteById($item);
            }
        }
        Session::flash('delete','success');
        return Redirect::back();
    }

    /**
     * check detect links
     * @param Request $request
     * @return mixed
     */
    public function getCheckLink(Request $request)
    {
        if($request->get('lang') == 'general') {
            $check = Products::where('slug',$request->get('slug'))->where('id','<>',$request->get('id'))->first();

        } else {
            $language_id = Languages::where('language',$request->get('lang'))->pluck('id');
            $check = Contents::where(['language_id' => $language_id, 'type' => 'films', 'slug' => $request->get('slug')])->where('product_id','<>',$request->get('id'))->first();
        }
        if($check == null){
            $slug = $request->get('slug');
        } else {
            $slug = Products::count()."-".$request->get('slug');
        }
        return Response::json(['status' => 200, 'result' => $slug]);
    }

    public function postCreate(Request $request)
    {
        $rules = [
            'name' => 'required',
        ];
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $setting = Setting::getSetting();
        $id = $request->get('id');
        if($request->get('meta-description') == ''){
            $description = substr(str_replace('<p>', '', str_replace('</p>', '', addslashes($request->get('content')))), 0, 250);
        }else{
            $description = $request->get('meta-description');
        }
        $data = array(
            'name'              => $request->get('name'),
            'slug'              => clearUnicode($request->get('slug')),
            'duration'          => (!empty($request->duration)?$request->duration:0),
            'quality'           => $request->quality,
            'release'           => $request->release,
            'day_release'       => $request->day_release,
            'end_day'           => $request->end_day,
            'content'           => addslashes($request->get('content')),
            'publish'           => ($request->get('publish') == 'on')?1:0,
            'film_id'           => $request->film_id,
            'created_at'        => gmdate('Y-m-d H:i:s', time()),
            'updated_at'        => gmdate('Y-m-d H:i:s', time())
        );

        if($id == 0) {
            $check = FilmDetail::where(['name' => $request->get('name'), 'slug' => $request->get('slug')])->first();
            if($check == null) {
                $id = MasterModel::CreateContent($this->table, $data);
            }
        } else {
            MasterModel::CreateContent($this->table, $data, ['id' => $id]);
        }


        Session::flash('success','success');
        return Redirect::to('admin/film-series/edit/'.$id);
    }

    public function postAvatar(Request $request) {
        $id = $request->get('id');
        $file = $request->file('images');
        if ($file->isValid()) {
            $name = $file->getClientOriginalName();
            $name = getNameImage($name);
            $extension = $file->getClientOriginalExtension();
            $fileName = $name.'-'.time().'.'.$extension;

            $check = FilmDetail::find($id);
            if($check == null) {
                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->table,['images' => $path, 'images_link' => null]);
            } else {
                if($check->images != null){
                    deleteImage($check->images);
                }
                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->table,['images' => $path, 'images_link' => null], ['id' => $check->id]);
            }
        }
    }

    public function postImagesLink(Request $request){
        $id = $request->get('id');
        $images_link = $request->images_link;
        if($images_link){
            $check = FilmDetail::find($id);
            if($check){
                if($check->images){
                    deleteImage($check->images);
                    $check->images = '';
                }
                $check->images_link = $images_link;
                $check->save();
            }
        }
        Session::flash('success','success');
        return Redirect::to('admin/film-series/edit/'.$id);
    }

    public function postMultipleImages(Request $request) {
        $id = $request->get('id');
        $files = $request->file('images');
        foreach ($files as $file){
            if ($file->isValid()) {
                $name = $file->getClientOriginalName();
                $name = getNameImage($name);
                $extension = $file->getClientOriginalExtension();
                $fileName = $name.'-'.time().'.'.$extension;

                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->media,['images' => $path, 'type' => 'product', 'product_id' => $id]);
            }
        }

    }

    public function postDeleteImage(Request $request) {
        $check = FilmDetail::find($request->get('id'));
        if($check != null){
            if($check->images != null) {
                deleteImage($check->images);
                MasterModel::CreateContent($this->table,['images' => null], ['id' => $check->id]);
            }
        }
        return Response::json(['status' => 200]);
    }

    public function getDeleteImageLink($id){
        $check = FilmDetail::find($id);
        if($check != null){
            $check->images_link = '';
            $check->save();
        }
        Session::flash('success','success');
        return Redirect::to('admin/film-series/edit/'.$id);
    }

    public function postSearchInfo(Request $request){
        $key_url = $request->get('key');
        $key_arr = explode('/', $key_url);
        if(isset($key_arr[4])){
            $key = substr($key_arr[4], 0, strpos($key_arr[4], '-'));
        }else{
            return 'No Result';
        }

        if(isset($key)) {
            $data = array();
            $detail_data = file_get_contents('https://api.themoviedb.org/3/movie/'.$key.'?api_key=14e3baafa454ddf1dfc40627b16016d3');
            $cast_data = file_get_contents('https://api.themoviedb.org/3/movie/'.$key.'/credits?api_key=14e3baafa454ddf1dfc40627b16016d3');
            $keywords_data = file_get_contents('https://api.themoviedb.org/3/movie/'.$key.'/keywords?api_key=14e3baafa454ddf1dfc40627b16016d3');
            if(isset($detail_data)){
                $detail_arr = json_decode($detail_data);
                if($detail_arr){
                    $data['data'] = $detail_arr;
                    $data['slug'] = clearUnicode($detail_arr->title);
                }
            }
            if(isset($cast_data)){
                $cast_arr = json_decode($cast_data);
                if($cast_arr){
                    $data['relation'] = $cast_arr;
                }
            }
            if(isset($keywords_data)){
                $keywords_arr = json_decode($keywords_data);
                if($keywords_data){
                    $data['keywords'] = $keywords_arr;
                }
            }
            return json_encode($data);
        }else{
            return 'No Result';
        }
    }

    public function postCover(Request $request){
        $id = $request->get('id');
        $file = $request->file('images');
        if ($file->isValid()) {
            $name = $file->getClientOriginalName();
            $name = getNameImage($name);
            $extension = $file->getClientOriginalExtension();
            $fileName = $name.'-'.time().'.'.$extension;

            $check = FilmDetail::find($id);
            if($check == null) {
                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->table,['cover' => $path, 'cover_link' => null]);
            } else {
                if($check->cover != null){
                    deleteImage($check->cover);
                }
                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->table,['cover' => $path, 'cover_link' => null], ['id' => $check->id]);
            }
        }
    }

    public function postDeleteCover(Request $request){
        $check = FilmDetail::find($request->get('id'));
        if($check != null){
            if($check->cover != null) {
                deleteImage($check->cover);
                MasterModel::CreateContent($this->table,['cover' => null], ['id' => $check->id]);
            }
        }
        return Response::json(['status' => 200]);
    }

    public function postCoverLink(Request $request){
        $id = $request->get('id');
        $cover_link = $request->cover_link;
        if($cover_link){
            $check = FilmDetail::find($id);
            if($check){
                if($check->cover){
                    deleteImage($check->cover);
                    $check->cover = '';
                }
                $check->cover_link = $cover_link;
                $check->save();
            }
        }
        Session::flash('success','success');
        return Redirect::to('admin/film-series/edit/'.$id);
    }

    public function getDeleteCoverLink($id){
        $check = FilmDetail::find($id);
        if($check != null){
            $check->cover_link = '';
            $check->save();
        }
        Session::flash('success','success');
        return Redirect::to('admin/film-series/edit/'.$id);
    }

    public function postBackground(Request $request){
        $id = $request->get('id');
        $file = $request->file('images');
        if ($file->isValid()) {
            $name = $file->getClientOriginalName();
            $name = getNameImage($name);
            $extension = $file->getClientOriginalExtension();
            $fileName = $name.'-'.time().'.'.$extension;

            $check = FilmDetail::find($id);
            if($check == null) {
                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->table,['background' => $path, 'background_link' => null]);
            } else {
                if($check->background != null){
                    deleteImage($check->background);
                }
                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->table,['background' => $path, 'background_link' => null], ['id' => $check->id]);
            }
        }
    }

    public function postDeleteBackground(Request $request){
        $check = FilmDetail::find($request->get('id'));
        if($check != null){
            if($check->background != null) {
                deleteImage($check->background);
                MasterModel::CreateContent($this->table,['background' => null], ['id' => $check->id]);
            }
        }
        return Response::json(['status' => 200]);
    }

    public function postBackgroundLink(Request $request){
        $id = $request->get('id');
        $background_link = $request->background_link;
        if($background_link){
            $check = FilmDetail::find($id);
            if($check){
                if($check->background){
                    deleteImage($check->background);
                    $check->background = '';
                }
                $check->background_link = $background_link;
                $check->save();
            }
        }
        Session::flash('success','success');
        return Redirect::to('admin/film-series/edit/'.$id);
    }

    public function getDeleteBackgroundLink($id){
        $check = FilmDetail::find($id);
        if($check != null){
            $check->background_link = '';
            $check->save();
        }
        Session::flash('success','success');
        return Redirect::to('admin/film-series/edit/'.$id);
    }

    public function getMultiLink($id){
        $film = Film::where('id', $id)->with('film_detail')->first();
        $server_play = ServerPlayFilm::where('publish',1)->orderby('name')->get();

        return view("admin.film_series.multi_link", [
            'user' => Auth::user(),
            'active' => 'film-series',
            'action' => 'Edit',
            'setting' => Setting::first(),
            'server_play'   => $server_play,
            'film'      => $film
        ]);
    }

//    public function postMultiLink(Request $request){
//        $film_id = $request->film_id;
//        $episode = $request->episode;
//        $server_play = $request->server_play;
//        $link = $request->link;
//        if(!$film_id){
//            return redirect()->back();
//        }
//
//        $film_detail_old = FilmDetail::where('film_id',$film_id)->get();
//        if(count($film_detail_old)>0){
//            foreach ($film_detail_old as $item){
//                FilmDetail::DeleteById($item->id);
//            }
//        }
//
//        for($i=0; $i<count($episode); $i++){
//            $film_detail = FilmDetail::where('film_id',$film_id)->where('name',"Episode $episode[$i]")->first();
//            if(!$film_detail){
//                $film_detail = new FilmDetail;
//                $film_detail->name = 'Episode '.$episode[$i];
//                $film_detail->slug = clearUnicode('Episode '.$episode[$i]);
//                $film_detail->quality = 'sd';
//                $film_detail->film_id = $film_id;
//                $film_detail->publish = 1;
//                $film_detail->created_at = gmdate('Y-m-d H:i:s', time());
//                $film_detail->updated_at = gmdate('Y-m-d H:i:s', time());
//                $film_detail->save();
//            }
//            if($link[$i]!=null) {
//                $link_film = new LinkFilmDetail;
//                $link_film->link = $link[$i];
//                $link_film->type = 'film-detail';
//                $link_film->server_play = $server_play[$i];
//                $link_film->film_detail_id = $film_detail->id;
//                $link_film->save();
//            }
//        }
//        Session::flash('delete','success');
//        return redirect()->back();
//    }
    public function postMultiLink(Request $request){
        $film_id = $request->film_id;
        $server_id = $request->serve_id;
        if($film_id){
            $data = $request->textarea_data_content;
            if($data){
                $data_array = explode(';', str_replace("\r\n","",$data));
                if(count($data_array)>0){
                    foreach ($data_array as $element){
                        if($element) {
                            $result = explode('|', $element);
                            if (isset($result[0]) && trim($result[0])) {
                                $check_episode = FilmDetail::where('film_id', $film_id)->where('name', trim($result[0]))->first();
                                if (!$check_episode) {
                                    $check_episode = new FilmDetail;
                                    $check_episode->name = trim($result[0]);
                                    $check_episode->slug = clearUnicode(trim($result[0]));
                                    $check_episode->film_id = $film_id;
                                    $check_episode->publish = 1;
                                    $check_episode->created_at = gmdate('Y-m-d H:i:s', time());
                                    $check_episode->updated_at = gmdate('Y-m-d H:i:s', time());
                                    $check_episode->save();
                                }
                                if (isset($result[1])) {
                                    $check_link = LinkFilmDetail::where('link', trim($result[1]))->where('server_play', $server_id)->where('film_detail_id', $check_episode->id)->first();
                                } else {
                                    $check_link = LinkFilmDetail::where('server_play', $server_id)->where('film_detail_id', $check_episode->id)->first();
                                }
                                if (!$check_link) {
                                    $check_link = new LinkFilmDetail;
                                    $check_link->link = isset($result[1]) ? trim($result[1]) : '';
                                    if (isset($result[2])) {
                                        $check_link->email = $result[2];
                                    }
                                    $check_link->server_play = $server_id;
                                    $check_link->film_detail_id = $check_episode->id;
                                    $check_link->type = 'film-detail';
                                    $check_link->created_at = gmdate('Y-m-d H:i:s', time());
                                    $check_link->updated_at = gmdate('Y-m-d H:i:s', time());
                                    $check_link->save();
                                }
                            }
                        } else {
                            return Response::json(['status' => 'error', 'result' => 'Link exist']);
                        }
                    }
                }
            }
        }
        return Response::json(['status'=>200]);
    }

    public function postListEpisodeDelete(Request $request){
        if(!Auth::check() || Auth::user()->type != 1)
        {
            return Redirect::to('admin');
        }
        $film_id = $request->get('film_id');
        $film = Film::find($film_id);
        if($film){
            $id = $request->get('id');
            if(!empty($id)) {
                if($film->movie_series==1){
                    foreach ($id as $item)
                    {
                        FilmDetail::DeleteById($item);
                    }
                }
            }
            Session::flash('success','Success');
            return Redirect::back();
        }
        Session::flash('error','Erorr');
        return Redirect::back();
    }

    public function postServerFilmData(Request $request){
        $server_id = $request->serve_id;
        $film_id = $request->film_id;
        if($server_id && $film_id){
            $film_detail = FilmDetail::where('film_id',$film_id)->get();
            if(count($film_detail)>0){
                $link_play_html = '';
                foreach ($film_detail as $value){
                    $link_play = LinkFilmDetail::where('type','film-detail')->where('film_detail_id',$value->id)->where('server_play',$server_id)->get();
                    if(count($link_play)>0){
                        foreach ($link_play as $item){
                            $link_play_html .= $value->name.'|'.$item->link.'|'.$item->email.";\n";
                        }
                    }
                }
            }
            return Response::json(['status'=>200, 'result'=>$link_play_html]);
        }else{
            return Response::json(['status'=>'error']);
        }
    }

    public function getLinkError(Request $request){
        $key = $request->key;
        $sort = $request->sort;
        if(!$sort){
            $sort = 'desc';
        }
        if($key) {
            $link_film_detail_error = LinkFilmDetail::where('link_film_details.error', 1)
                ->join('film_details','film_details.id','=','link_film_details.film_detail_id')
                ->join('films','films.id','=','film_details.film_id')
                ->where('films.name','like',"%$key%")
                ->orderby('link_film_details.updated_at',$sort)
                ->orderby('link_film_details.film_detail_id')
                ->orderby('link_film_details.server_play')
                ->select('link_film_details.*')
                ->paginate(15);
        }else{
            $link_film_detail_error = LinkFilmDetail::where('error', 1)->orderby('updated_at',$sort)->orderby('film_detail_id')->orderby('server_play')->paginate(15);
        }
        return view("admin.film_series.list-error", [
            'user' => Auth::user(),
            'active' => 'link-episode-error',
            'link_film_detail_error' => $link_film_detail_error,
            'key'   => $key,
            'sort'  => $sort
        ]);
    }

}
