<?php

namespace App\Http\Controllers\Admin;

use App\Models\Social;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Models\Users;
use Socialite, Auth, Session;

class SocialController extends Controller
{
    /**
   * Redirect the user to the GitHub authentication page.
   *
   * @return Response
   */
  public function redirectToProvider()
  {
      return Socialite::driver('facebook')->redirect();
  }

  /**
   * Obtain the user information from GitHub.
   *
   * @return Response
   */
  public function handleProviderCallback()
  {
      $user = Socialite::driver('facebook')->user();

      $social = Social::where('provider_user_id',$user->id)->where('provider','facebook')->first();
      if($social){
          $u = Users::where('email',$user->email)->first();
          if(Auth::loginUsingId($u->id, true)){
              return redirect('/')->with(['flash_level'=>'success', 'flash_message'=>'Success']);
          } else {
              return redirect()->back()->with(['flash_level'=>'danger', 'flash_message'=>'Email or password not correct']);
          }
      }else{
          $temp = new Social;
          $temp->provider_user_id = $user->id;
          $temp->provider = 'facebook';
          $u = Users::where('email',$user->email)->first();
          if($u){
              if($u->social == 'facebook'){
                  return redirect()->back()->with(['flash_level'=>'danger', 'flash_message'=>'Email already exists']);
              }elseif($u->social != 'google'){
                  $u->social = 'facebook';
                  $u->active = 1;
                  $u->save();
                  $temp->user_id = $u->id;
                  $temp->save();
                  if(Auth::loginUsingId($u->id, true)){
                      return redirect('/')->with(['flash_level'=>'success', 'flash_message'=>'Success']);
                  } else {
                      return redirect()->back()->with(['flash_level'=>'danger', 'flash_message'=>'Email or password not correct']);
                  }
              }else{
                  return redirect()->back()->with(['flash_level'=>'danger', 'flash_message'=>'Email already exists']);
              }
          }else{
              $data = array(
                  'email' => $user->email,
                  'name' => $user->name,
                  'type' => 4,
                  'social' => 'facebook',
                  'images' => $user->avatar
              );

              $user_ck = Users::CreateSocialUser($data);
              if($user_ck == false) {
                  return redirect()->back()->with(['flash_level'=>'danger', 'flash_message'=>'Account has been registered']);
              }
              $temp->user_id = $user_ck->id;
          }

          $temp->save();
          if(Auth::loginUsingId($user_ck->id, true)){
              return redirect('/')->with(['flash_level'=>'success', 'flash_message'=>'Success']);
          } else {
              return redirect()->back()->with(['flash_level'=>'danger', 'flash_message'=>'Email or password not correct']);
          }
      }
  }

  /**
   * Redirect the user to the GitHub authentication page.
   *
   * @return Response
   */
  public function redirectToProviderGoogle()
  {
      return Socialite::driver('google')->redirect();
  }

  /**
   * Obtain the user information from GitHub.
   *
   * @return Response
   */
  public function handleProviderCallbackGoogle()
  {
      $user = Socialite::driver('google')->user();

      $social = Social::where('provider_user_id',$user->id)->where('provider','google')->first();
      if($social){
          $u = Users::where('email',$user->email)->first();
          if(Auth::loginUsingId($u->id, true)){
              return redirect('/')->with(['flash_level'=>'success', 'flash_message'=>'Success']);
          } else {
              return redirect()->back()->with(['flash_level'=>'danger', 'flash_message'=>'Email or password not correct']);
          }
      }else{
          $temp = new Social;
          $temp->provider_user_id = $user->id;
          $temp->provider = 'google';
          $u = Users::where('email',$user->email)->first();
          if($u){
              if($u->social == 'google'){
                  return redirect()->back()->with(['flash_level'=>'danger', 'flash_message'=>'Email already exists']);
              }elseif($u->social != 'facebook'){
                  $u->social = 'google';
                  $u->active = 1;
                  $u->save();
                  $temp->user_id = $u->id;
                  $temp->save();
                  if(Auth::loginUsingId($u->id, true)){
                      if(Auth::user()->type != 4){
                          return redirect('/');
                      }else{
                          return redirect('/');
                      }
                  } else {
                      return redirect()->back()->with(['flash_level'=>'danger', 'flash_message'=>'Email or password not correct']);
                  }
              }else{
                  return redirect()->back()->with(['flash_level'=>'danger', 'flash_message'=>'Email already exists']);
              }
          }else{
              $data = array(
                  'email' => $user->email,
                  'name' => $user->name,
                  'type' => 4,
                  'social' => 'google',
                  'images' => $user->avatar
              );

              $user_ck = Users::CreateSocialUser($data);
              if($user_ck == false) {
                  return redirect()->back()->with(['flash_level'=>'success', 'flash_message'=>'Account has been register']);
              }
              $temp->user_id = $user_ck->id;
          }

          $temp->save();
          if(Auth::loginUsingId($user_ck->id, true)){
              return redirect('/')->with(['flash_level'=>'success', 'flash_message'=>'Success']);
          } else {
              return redirect()->back()->with(['flash_level'=>'danger', 'flash_message'=>'Email or password not correct']);
          }
      }
  }
}
