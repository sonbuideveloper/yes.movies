<?php

namespace App\Http\Controllers\Admin;

use App\Models\CountryFilm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth, Validator, Redirect, Session;
use Models\MasterModel;
use Models\Setting;
use App\Models\Film;

class CountryFilmController extends Controller
{
    protected $table = "country_films";

    public function getIndex()
    {
        return view("admin.country_film.index", [
            'user' => Auth::user(),
            'active' => 'film_country',
            'result' => CountryFilm::RecursiveIndexAdmin()
        ]);
    }

    public function getCreate()
    {
        return view("admin.country_film.edit", [
            'user' => Auth::user(),
            'active' => 'film_country',
            'action' => 'Create',
            'setting' => Setting::first()
        ]);
    }

    public function getEdit($id)
    {
        $result = CountryFilm::getById($id);
        return view("admin.country_film.edit", [
            'user' => Auth::user(),
            'active' => 'film_country',
            'action' => 'Edit',
            'setting' => Setting::first(),
            'result' => $result,
        ]);
    }

    public function getDelete($id)
    {
        if(!Auth::check() || Auth::user()->type != 1)
        {
            return Redirect::to('admin');
        }
        $check = CountryFilm::find($id);
        if(count($check) > 0) {
            $film = Film::where('country_film_id',$id)->get();
            if(count($film)>0){
                foreach ($film as $item) {
                    Film::DeleteById($item->id);
                }
            }
            CountryFilm::DeleteById($id);
        }
        Session::flash('delete','success');
        return Redirect::to('admin/film-country/list');
    }

    public function postDelete(Request $request)
    {
        if(!Auth::check() || Auth::user()->type != 1)
        {
            return Redirect::to('admin');
        }
        $id = $request->get('id');
        if(!empty($id)) {
            foreach ($id as $item)
            {
                $film = Film::where('country_film_id',$item)->get();
                if(count($film)>0){
                    foreach ($film as $value) {
                        Film::DeleteById($value->id);
                    }
                }
                CountryFilm::DeleteById($item);
            }
        }
        Session::flash('delete','success');
        return Redirect::to('admin/film-country/list');
    }

    public function postCreate(Request $request)
    {
        $rules = [
            'name' => 'required'
        ];
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $setting = Setting::getSetting();
        $id = $request->get('id');
        $data = array(
            'name' => $request->get('name'),
            'slug' => $request->get('slug'),
            'title' => $request->get('meta-title'),
            'descriptions' => $request->get('meta-description'),
            'keywords' => $request->get('meta-keywords'),
            'publish' => ($request->get('publish') == 'on')?1:0,
            'created_at' => gmdate('Y-m-d H:i:s', time()),
            'updated_at' => gmdate('Y-m-d H:i:s', time())
        );
        if($id == 0) {
            $check = CountryFilm::where(['name' => $request->get('name'), 'slug' => $request->get('slug')])->first();
            if($check == null) {
                $id = MasterModel::CreateContent($this->table, $data);
            }
        } else {
            MasterModel::CreateContent($this->table, $data, ['id' => $id]);
        }
        Session::flash('success','success');
        return Redirect::to('admin/film-country/list');
    }

    public function getCheckStatus($id)
    {
        $check = CountryFilm::find($id);
        if($check != null)
        {
            if ($check->publish == 1)
            {
                MasterModel::CreateContent($this->table,['publish' => 0], ['id' => $id]);
                return response()->json(['status' => 200, 'result' => 'hidden']);
            } else {
                MasterModel::CreateContent($this->table,['publish' => 1], ['id' => $id]);
                return response()->json(['status' => 200, 'result' => 'publish']);
            }
        }
        return response()->json(['status' => 'error']);
    }
}
