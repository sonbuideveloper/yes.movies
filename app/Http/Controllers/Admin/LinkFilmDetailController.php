<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth, Session, Validator, Redirect, Response;
use Models\Setting;
use App\Models\LinkFilmDetail;
use Models\MasterModel;

class LinkFilmDetailController extends Controller
{
    protected $table = "link_film_details";

    public function postCreate(Request $request)
    {
        $rules = [
            'link' => 'required'
        ];
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $setting = Setting::getSetting();
        $id = $request->get('id');
        if(!$request->film_id){
            Session::flash('error','Please created content before add link');
            return Redirect::back();
        }
        $data = array(
            'link'              => $request->link,
            'server_play'       => $request->server_play,
            'email'             => $request->email,
            'film_detail_id'    => $request->film_id,
            'type'              =>'film-detail',
            'created_at'        => gmdate('Y-m-d H:i:s', time()),
            'updated_at'        => gmdate('Y-m-d H:i:s', time())
        );

        if($id == 0) {
            $check = LinkFilmDetail::where(['link' => $request->get('link'), 'server_play' => $request->get('server_play')])->first();
            if($check == null) {
                $id = MasterModel::CreateContent($this->table, $data);
            }else{
                Session::flash('error','Link exist');
                return redirect()->back();
            }
        } else {
            MasterModel::CreateContent($this->table, $data, ['id' => $id]);
        }

        Session::flash('success','success');
        return Redirect::to('admin/film-series/edit/'.$request->film_id);
    }

    public function getDelete($id)
    {
        if(!Auth::check() || Auth::user()->type != 1)
        {
            return Redirect::to('admin');
        }
        $check = LinkFilmDetail::find($id);
        if(count($check) > 0) {
            $check->delete();
        }
        Session::flash('delete','success');
        return Redirect::back();
    }

    public function postDelete(Request $request)
    {
        if(!Auth::check() || Auth::user()->type != 1)
        {
            return Redirect::to('admin');
        }
        $id = $request->get('id');
        if(!empty($id)) {
            foreach ($id as $item)
            {
                $link_film = LinkFilmDetail::find($item);
                if($link_film){
                    $link_film->delete();
                }
            }
        }
        Session::flash('success','success');
        return Redirect::back();
    }

    public function postActive(Request $request)
    {
        if(!Auth::check() || Auth::user()->type != 1)
        {
            return Redirect::to('admin');
        }
        $id = $request->get('id');
        if(!empty($id)) {
            foreach ($id as $item)
            {
                $link_film = LinkFilmDetail::find($item);
                if($link_film){
                    $link_film->error = 0;
                    $link_film->save();
                }
            }
        }
        Session::flash('success','success');
        return Redirect::back();
    }

    public function postFindLinkFilm(Request $request){
        $link_film_id = $request->link_film_id;
        if($link_film_id){
            $result = LinkFilmDetail::find($link_film_id);
            return Response::json(['status' => 200, 'result' => $result]);
        }else{
            return Response::json(['status' => 'error']);
        }
    }

    public function postEditFilmError(Request $request){
        $link_film_id = $request->id;
        $link = $request->link;
        $check = LinkFilmDetail::find($link_film_id);
        $check->link = $link;
        $check->email = $request->email;
        $check->error = 0;
        $check->save();
        Session::flash('delete','success');
        return Redirect::back();
    }

    public function getCheckStatus($id)
    {
        $check = LinkFilmDetail::find($id);
        if($check != null)
        {
            if ($check->error == 1) {
                $check->error = 0;
                $check->save();
            }
        }
        Session::flash('delete','success');
        return Redirect::back();
    }
}
