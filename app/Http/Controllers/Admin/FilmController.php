<?php

namespace App\Http\Controllers\Admin;

use App\Models\Actor;
use App\Models\ActorRelation;
use App\Models\CategoryFilm;
use App\Models\CategoryFilmRelation;
use App\Models\Chanel;
use App\Models\CountryFilm;
use App\Models\Director;
use App\Models\DirectorRelation;
use App\Models\Film;
use App\Models\FilmDetail;
use App\Models\LinkFilm;
use App\Models\LinkFilmDetail;
use App\Models\Playlist;
use App\Models\Producer;
use App\Models\ServerPlayFilm;
use App\Models\TagFilm;
use App\Models\TagFilmRelation;
use App\Models\TypeFilm;
use App\Models\TypeFilmRelation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth, Validator, Session, Redirect, Response, Cache;
use Models\MasterModel;
use Models\Setting;

class FilmController extends Controller
{
    private $table = "films";

    private $media = "media";

    public function getIndex(Request $request)
    {
        $key = $request->type;
        $sort = $request->sort;
        $keyword = $request->keyword;
        if($sort=='') $sort = 'desc';
        $status = $request->status;
        $type = TypeFilm::where('publish',1)->orderby('name')->get();
        return view("admin.film.index", [
            'user' => Auth::user(),
            'active' => 'film',
            'result' => Film::RecursiveIndexAdmin($key, $sort, $keyword, $status),
            'key'   => $key,
            'sort'  => $sort,
            'type'  => $type,
            'keyword'   => $keyword,
            'status' => $status
        ]);
    }

    public function getCreate()
    {
        $film_country = CountryFilm::orderby('name')->get();
        $film_genre = CategoryFilm::orderby('name')->get();
        $film_type = TypeFilm::orderby('name')->get();

        $cast_all_arr = Actor::select('name')->get();
        $cast_all = array();
        foreach ($cast_all_arr as $item) {
            $cast_all[] = $item->name;
        }
        $director_all_arr = Director::select('name')->get();
        $director_all = array();
        foreach ($director_all_arr as $item) {
            $director_all[] = $item->name;
        }
        $tag_film_array = TagFilm::select('name')->orderby('name')->get();
        $tag_film = array();
        foreach ($tag_film_array as $item){
            $tag_film[] = $item->name;
        }

        $play_list = Playlist::orderby('name')->get();
        $chanel = Chanel::orderby('name')->get();
        $producer = Producer::orderby('name')->get();
        $server_play = ServerPlayFilm::where('publish',1)->orderby('name')->get();

        return view("admin.film.edit", [
            'user' => Auth::user(),
            'active' => 'film',
            'action' => 'Create',
            'setting' => Setting::first(),
            'film_country'  => $film_country,
            'film_genre'    => $film_genre,
            'film_type'     => $film_type,
            'play_list'     => $play_list,
            'cast_all'  => (!empty($cast_all) ? implode($cast_all, ',') : ''),
            'director_all'  => (!empty($diretor_all) ? implode($director_all, ',') : ''),
            'tag_film_all'  => (!empty($tag_film) ? implode($tag_film, ',') : ''),
            'server_play'   => $server_play,
            'chanel'        => $chanel,
            'producer'      => $producer
        ]);
    }

    public function getEdit($id)
    {
        $result = Film::getById($id);
        $cast_name = "";
        if(!empty($result) && count($result->castRelation) > 0)
        {
            foreach ($result->castRelation as $item)
            {
                $cast_name .= $item->actor->name.",";
            }
            $cast_name = rtrim($cast_name, ",");
        }
        $cast_all_arr = Actor::select('name')->get();
        $cast_all = array();
        if(count($cast_all_arr) > 0){
            foreach ($cast_all_arr as $item) {
                $cast_all[] = $item->name;
            }
        }

        $director_name = "";
        if(!empty($result) && count($result->directorRelation) > 0)
        {
            foreach ($result->directorRelation as $item)
            {
                $director_name .= $item->director->name.",";
            }
            $director_name = rtrim($director_name, ",");
        }


        $director_all_arr = Director::select('name')->get();
        $director_all = array();
        if(count($director_all_arr) > 0){
            foreach ($director_all_arr as $item) {
                $director_all[] = $item->name;
            }
        }

        $tag_film_array = TagFilm::select('name')->orderby('name')->get();
        $tag_film = array();
        foreach ($tag_film_array as $item){
            $tag_film[] = $item->name;
        }
        $tag_film_item = '';
        if(count($result->tagFilm)>0){
            foreach ($result->tagFilm as $item){
                if($item->type = 'tag_film'){
                    $tag_film_item = $item->tag->name;
                }
            }
        }

        $film_country = CountryFilm::orderby('name')->get();
        $film_genre = CategoryFilm::orderby('name')->get();
        $film_type = TypeFilm::orderby('name')->get();
        $play_list = Playlist::orderby('name')->get();
        $chanel = Chanel::orderby('name')->get();
        $producer = Producer::orderby('name')->get();
        $server_play = ServerPlayFilm::where('publish',1)->orderby('name')->get();

        return view("admin.film.edit", [
            'user' => Auth::user(),
            'active' => 'film',
            'action' => 'Edit',
            'setting' => Setting::first(),
            'result' => $result,
            'cast' => $cast_name,
            'cast_all'  => (!empty($cast_all) ? implode($cast_all, ',') : ''),
            'director_all'  => (!empty($director_all) ? implode($director_all, ',') : ''),
            'tag_film_all'  => (!empty($tag_film) ? implode($tag_film, ',') : ''),
            'director'  => $director_name,
            'tag_film_item' => $tag_film_item,
            'play_list'     => $play_list,
            'film_country'  => $film_country,
            'film_genre'    => $film_genre,
            'film_type'     => $film_type,
            'server_play'   => $server_play,
            'chanel'        => $chanel,
            'producer'      => $producer
        ]);
    }

    public function getDelete($id)
    {
        if(!Auth::check() || Auth::user()->type != 1)
        {
            return Redirect::to('admin');
        }
        $check = Film::find($id);
        if(isset($check)) {
            Film::DeleteById($id);
        }
        Session::flash('delete','success');
        return Redirect::to('admin/film/list');
    }

    /**
     * edit status public or hidden
     * @param $id
     * @return mixed
     */
    public function getCheckStatus($type,$id)
    {
        $check = Film::find($id);
        if($check != null)
        {
            $hidden = "hidden";
            if($type == 'featured'){
                $hidden = "normal";
            }
            if ($check->$type == 1)
            {
                MasterModel::CreateContent($this->table,[$type => 0], ['id' => $id]);
                return Response::json(['status' => 200, 'result' => $hidden]);
            } else {
                MasterModel::CreateContent($this->table,[$type => 1], ['id' => $id]);
                return Response::json(['status' => 200, 'result' => $type]);
            }
        }
        return Response::json(['status' => 'error']);
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     * Update order
     */
    public function getUpdateOrder(Request $request, $id)
    {
        $val = $request->get('val');
        $check = Products::find($id);
        if($check != null)
        {
            MasterModel::CreateContent($this->table,['order_by' => $val], ['id' => $id]);
            return Response::json(['status' => 200,'result' => $val]);
        }
        return Response::json(['status' => 'error']);
    }

    /**
     * Delete all item using checkbox, method post
     * @param Request $request
     * @return mixed
     */
    public function postDelete(Request $request)
    {
        if(!Auth::check() || Auth::user()->type != 1)
        {
            return Redirect::to('admin');
        }
        $id = $request->get('id');
        if(!empty($id)) {
            foreach ($id as $item)
            {
                Film::DeleteById($item);
            }
        }
        Session::flash('delete','success');
        return Redirect::to('admin/film/list');
    }

    /**
     * check detect links
     * @param Request $request
     * @return mixed
     */
    public function getCheckLink(Request $request)
    {
        if($request->get('lang') == 'general') {
            $check = Products::where('slug',$request->get('slug'))->where('id','<>',$request->get('id'))->first();

        } else {
            $language_id = Languages::where('language',$request->get('lang'))->pluck('id');
            $check = Contents::where(['language_id' => $language_id, 'type' => 'films', 'slug' => $request->get('slug')])->where('product_id','<>',$request->get('id'))->first();
        }
        if($check == null){
            $slug = $request->get('slug');
        } else {
            $slug = Products::count()."-".$request->get('slug');
        }
        return Response::json(['status' => 200, 'result' => $slug]);
    }

    public function postCreate(Request $request)
    {
        $rules = [
            'name' => 'required',
        ];
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $setting = Setting::getSetting();
        $id = $request->get('id');
        if(!$id){
            $check_film = Film::where('name', $request->name)->first();
            if($check_film){
                Session::flash('error','Film exist');
                return redirect()->back();
            }
        }
        if($request->get('meta-description') == ''){
            $description = substr(str_replace('<p>', '', str_replace('</p>', '', addslashes($request->get('content')))), 0, 250);
        }else{
            $description = addslashes($request->get('meta-description'));
        }

        $data = array(
            'name'              => $request->get('name'),
            'slug'              => clearUnicode($request->get('slug')),
            'title'             => (!empty($request->get('meta-title'))?$request->get('meta-title'):$request->get('name')),
            'descriptions'      => $description,
            'keywords'          => $request->get('meta-keywords'),
            'country_film_id'   => $request->country_film_id,
            'chanel_id'         => $request->chanel_id,
            'producer_id'       => $request->producer_id,
            'trailer'           => $request->trailer,
            'age'               => (!empty($request->age)?$request->age:0),
            'duration'          => (!empty($request->duration)?$request->duration:0),
            'quality'           => $request->quality,
            'release'           => (!empty($request->release)?$request->release:substr($request->day_release,0,4)),
            'day_release'       => $request->day_release,
            'end_day'           => $request->end_day,
            'IMDB'              => $request->IMDB,
            'sex'               => $request->sex,
            'content'           => addslashes($request->get('content')),
            'publish'           => ($request->get('publish') == 'on')?1:0,
            'slider'            => ($request->get('slider') == 'on')?1:0,
            'movie_series'      => ($request->get('movie_series') == 'on')?1:0,
            'created_at'        => gmdate('Y-m-d H:i:s', time()),
            'updated_at'        => gmdate('Y-m-d H:i:s', time())
        );

        if($id == 0) {
            $data['user_id']     = Auth::user()->id;
            $data['images_link'] = $request->image_api;
            $data['cover_link'] = $request->multi_image_api;
            $check = Film::where(['name' => $request->get('name'), 'slug' => $request->get('slug')])->first();
            if($check == null) {
                $id = MasterModel::CreateContent($this->table, $data);
            }
        } else {
            MasterModel::CreateContent($this->table, $data, ['id' => $id]);
        }

        if($request->genre_film_id){
            $genre_film_id = $request->genre_film_id;
            CategoryFilmRelation::DeleteNotInArray($id, $genre_film_id);
            foreach($genre_film_id as $item){
                $check_genre = CategoryFilmRelation::where('film_id',$id)->where('category_film_id',$item)->first();
                if(!$check_genre){
                    $genre = new CategoryFilmRelation;
                    $genre->film_id = $id;
                    $genre->category_film_id = $item;
                    $genre->save();
                }
            }
        }

        if($request->type_film_id){
            $type_film_id = $request->type_film_id;
            TypeFilmRelation::DeleteNotInArray($id, $type_film_id);
            foreach($type_film_id as $item){
                $check_type = TypeFilmRelation::where('film_id',$id)->where('type_film_id',$item)->first();
                if(!$check_type){
                    $type = new TypeFilmRelation;
                    $type->film_id = $id;
                    $type->type_film_id = $item;
                    $type->save();
                }
            }
        }

        if($request->cast) {
            $cast = explode(",", $request->cast);
            ActorRelation::DeleteNotInArray($id, $cast);
            foreach($cast as $item){
                $check_cast = Actor::where('name',$item)->first();
                if(!$check_cast){
                    $check_cast = new Actor;
                    $check_cast->name = $item;
                    $check_cast->slug = clearUnicode($item);
                    $check_cast->created_at = gmdate('Y-m-d H:i:s', time());
                    $check_cast->updated_at = gmdate('Y-m-d H:i:s', time());
                    $check_cast->save();
                }
                $check_cast_relation = ActorRelation::where('film_id',$id)->where('actor_id',$check_cast->id)->first();
                if(!$check_cast_relation){
                    $cast_item = new ActorRelation;
                    $cast_item->film_id = $id;
                    $cast_item->actor_id = $check_cast->id;
                    $cast_item->created_at = gmdate('Y-m-d H:i:s', time());
                    $cast_item->updated_at = gmdate('Y-m-d H:i:s', time());
                    $cast_item->save();
                }
            }
        }

        if($request->director) {
            $director = explode(",", $request->director);
            DirectorRelation::DeleteNotInArray($id, $director);
            foreach($director as $item){
                $check_director = Director::where('name',$item)->first();
                if(!$check_director){
                    $check_director = new Director();
                    $check_director->name = $item;
                    $check_director->slug = clearUnicode($item);
                    $check_director->created_at = gmdate('Y-m-d H:i:s', time());
                    $check_director->updated_at = gmdate('Y-m-d H:i:s', time());
                    $check_director->save();
                }
                $check_director_relation = DirectorRelation::where('film_id',$id)->where('director_id',$check_director->id)->first();
                if(!$check_director_relation){
                    $director_item = new DirectorRelation;
                    $director_item->film_id = $id;
                    $director_item->director_id = $check_director->id;
                    $director_item->created_at = gmdate('Y-m-d H:i:s', time());
                    $director_item->updated_at = gmdate('Y-m-d H:i:s', time());
                    $director_item->save();
                }
            }
        }

        if($request->get('tag-film')){
            $tag_film = $request->get('tag-film');
            TagFilmRelation::DeleteNotInArray($id, $tag_film, 'tag_film');
            $tag_film_check = TagFilm::where('name',$tag_film)->first();
            if(!$tag_film_check){
                $tag_film_check = new TagFilm;
                $tag_film_check->name = $tag_film;
                $tag_film_check->slug = clearUnicode($tag_film);
                $tag_film_check->type = 'tag_film';
                $tag_film_check->created_at = gmdate('Y-m-d H:i:s', time());
                $tag_film_check->updated_at = gmdate('Y-m-d H:i:s', time());
                $tag_film_check->save();
            }
            $tag_film_relation_check = TagFilmRelation::where('type','tag_film')->where('film_id',$id)->first();
            if(!$tag_film_relation_check){
                $tag_film_item = new TagFilmRelation;
                $tag_film_item->film_id = $id;
                $tag_film_item->tag_id = $tag_film_check->id;
                $tag_film_item->type = 'tag_film';
                $tag_film_item->created_at = gmdate('Y-m-d H:i:s', time());
                $tag_film_item->updated_at = gmdate('Y-m-d H:i:s', time());
                $tag_film_item->save();
            }
        }
        Cache::flush();
        Session::flash('success','success');
        return Redirect::to('admin/film/edit/'.$id);
    }

    public function postAvatar(Request $request) {
        $id = $request->get('id');
        $file = $request->file('images');
        if ($file->isValid()) {
            $name = $file->getClientOriginalName();
            $name = getNameImage($name);
            $extension = $file->getClientOriginalExtension();
            $fileName = $name.'-'.time().'.'.$extension;

            $check = Film::find($id);
            if($check == null) {
                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->table,['images' => $path, 'images_link' => null]);
            } else {
                if($check->images != null){
                    deleteImage($check->images);
                }
                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->table,['images' => $path, 'images_link' => null], ['id' => $check->id]);
            }
        }
    }

    public function postImagesLink(Request $request){
        $id = $request->get('id');
        $images_link = $request->images_link;
        if($images_link){
            $check = Film::find($id);
            if($check){
                if($check->images){
                    deleteImage($check->images);
                    $check->images = '';
                }
                $check->images_link = $images_link;
                $check->save();
            }
        }
        Session::flash('success','success');
        return Redirect::to('admin/film/edit/'.$id);
    }

    public function postMultipleImages(Request $request) {
        $id = $request->get('id');
        $files = $request->file('images');
        foreach ($files as $file){
            if ($file->isValid()) {
                $name = $file->getClientOriginalName();
                $name = getNameImage($name);
                $extension = $file->getClientOriginalExtension();
                $fileName = $name.'-'.time().'.'.$extension;

                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->media,['images' => $path, 'type' => 'product', 'product_id' => $id]);
            }
        }

    }

    public function postDeleteImage(Request $request) {
        $check = Film::find($request->get('id'));
        if($check != null){
            if($check->images != null) {
                deleteImage($check->images);
                MasterModel::CreateContent($this->table,['images' => null], ['id' => $check->id]);
            }
        }
        return Response::json(['status' => 200]);
    }

    public function getDeleteImageLink($id){
        $check = Film::find($id);
        if($check != null){
            $check->images_link = '';
            $check->save();
        }
        Session::flash('success','success');
        return Redirect::to('admin/film/edit/'.$id);
    }

    public function postSearchInfo(Request $request){
        $key_url = $request->get('key');
        $key_arr = explode('/', $key_url);
        if(isset($key_arr[4])){
            if(strpos($key_arr[4], '-')){
                $key = substr($key_arr[4], 0, strpos($key_arr[4], '-'));
            }else{
                $key = $key_arr[4];
            }
        }else{
            return 'No Result';
        }

        if(isset($key) && isset($key_arr[3])) {
            $data = array();
            if($key_arr[3]=='movie') {
                $detail_data = file_get_contents('https://api.themoviedb.org/3/movie/' . $key . '?api_key=14e3baafa454ddf1dfc40627b16016d3&append_to_response=videos');
                $cast_data = file_get_contents('https://api.themoviedb.org/3/movie/'.$key.'/credits?api_key=14e3baafa454ddf1dfc40627b16016d3');
                $keywords_data = file_get_contents('https://api.themoviedb.org/3/movie/'.$key.'/keywords?api_key=14e3baafa454ddf1dfc40627b16016d3');

                if(isset($detail_data)){
                    $detail_arr = json_decode($detail_data);
                    if($detail_arr){
                        $data['data'] = $detail_arr;
                        $data['slug'] = clearUnicode($detail_arr->title);
                    }
                }
                $data['type'] = 'movie';
            }else if($key_arr[3]=='tv'){
                $detail_data = file_get_contents('https://api.themoviedb.org/3/tv/' . $key . '?api_key=14e3baafa454ddf1dfc40627b16016d3&append_to_response=videos');
                $cast_data = file_get_contents('https://api.themoviedb.org/3/tv/'.$key.'/credits?api_key=14e3baafa454ddf1dfc40627b16016d3');
                $keywords_data = file_get_contents('https://api.themoviedb.org/3/tv/'.$key.'/keywords?api_key=14e3baafa454ddf1dfc40627b16016d3');

                if(isset($detail_data)){
                    $detail_arr = json_decode($detail_data);
                    if($detail_arr){
                        $data['data'] = $detail_arr;
                        $data['slug'] = clearUnicode($detail_arr->original_name);
                    }
                }
                $data['type'] = 'tv';
            }else{
                return 'No Result';
            }

            if(isset($cast_data)){
                $cast_arr = json_decode($cast_data);
                if($cast_arr){
                    $data['relation'] = $cast_arr;
                }
            }
            if(isset($keywords_data)){
                $keywords_arr = json_decode($keywords_data);
                if($keywords_data){
                    $data['keywords'] = $keywords_arr;
                }
            }

            return json_encode($data);
        }else{
            return 'No Result';
        }
    }

    public function postCover(Request $request){
        $id = $request->get('id');
        $file = $request->file('images');
        if ($file->isValid()) {
            $name = $file->getClientOriginalName();
            $name = getNameImage($name);
            $extension = $file->getClientOriginalExtension();
            $fileName = $name.'-'.time().'.'.$extension;

            $check = Film::find($id);
            if($check == null) {
                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->table,['cover' => $path, 'cover_link' => null]);
            } else {
                if($check->cover != null){
                    deleteImage($check->cover);
                }
                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->table,['cover' => $path, 'cover_link' => null], ['id' => $check->id]);
            }
        }
    }

    public function postDeleteCover(Request $request){
        $check = Film::find($request->get('id'));
        if($check != null){
            if($check->cover != null) {
                deleteImage($check->cover);
                MasterModel::CreateContent($this->table,['cover' => null], ['id' => $check->id]);
            }
        }
        return Response::json(['status' => 200]);
    }

    public function postCoverLink(Request $request){
        $id = $request->get('id');
        $cover_link = $request->cover_link;
        if($cover_link){
            $check = Film::find($id);
            if($check){
                if($check->cover){
                    deleteImage($check->cover);
                    $check->cover = '';
                }
                $check->cover_link = $cover_link;
                $check->save();
            }
        }
        Session::flash('success','success');
        return Redirect::to('admin/film/edit/'.$id);
    }

    public function getDeleteCoverLink($id){
        $check = Film::find($id);
        if($check != null){
            $check->cover_link = '';
            $check->save();
        }
        Session::flash('success','success');
        return Redirect::to('admin/film/edit/'.$id);
    }

    public function postBackground(Request $request){
        $id = $request->get('id');
        $file = $request->file('images');
        if ($file->isValid()) {
            $name = $file->getClientOriginalName();
            $name = getNameImage($name);
            $extension = $file->getClientOriginalExtension();
            $fileName = $name.'-'.time().'.'.$extension;

            $check = Film::find($id);
            if($check == null) {
                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->table,['background' => $path, 'background_link' => null]);
            } else {
                if($check->background != null){
                    deleteImage($check->background);
                }
                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->table,['background' => $path, 'background_link' => null], ['id' => $check->id]);
            }
        }
    }

    public function postDeleteBackground(Request $request){
        $check = Film::find($request->get('id'));
        if($check != null){
            if($check->background != null) {
                deleteImage($check->background);
                MasterModel::CreateContent($this->table,['background' => null], ['id' => $check->id]);
            }
        }
        return Response::json(['status' => 200]);
    }

    public function postBackgroundLink(Request $request){
        $id = $request->get('id');
        $background_link = $request->background_link;
        if($background_link){
            $check = Film::find($id);
            if($check){
                if($check->background){
                    deleteImage($check->background);
                    $check->background = '';
                }
                $check->background_link = $background_link;
                $check->save();
            }
        }
        Session::flash('success','success');
        return Redirect::to('admin/film/edit/'.$id);
    }

    public function getDeleteBackgroundLink($id){
        $check = Film::find($id);
        if($check != null){
            $check->background_link = '';
            $check->save();
        }
        Session::flash('success','success');
        return Redirect::to('admin/film/edit/'.$id);
    }

    public function getLinkError(Request $request){
        $key = $request->key;
        $sort = $request->sort;
        if(!$sort){
            $sort = 'desc';
        }
        if($key){
            $link_film_error = LinkFilm::where('link_films.error',1)
                ->join('films', 'films.id', '=', 'link_films.film_id')
                ->where('films.name','like', "%$key%")
                ->orderby('link_films.updated_at',$sort)
                ->orderby('film_id')
                ->orderby('server_play')
                ->with('film')
                ->paginate(15);
        }else{
            $link_film_error = LinkFilm::where('error',1)->orderby('updated_at',$sort)->orderby('film_id')->orderby('server_play')->with('film')->paginate(15);
        }

        return view("admin.film.list-error", [
            'user' => Auth::user(),
            'active' => 'link-film-error',
            'link_film_error' => $link_film_error,
            'key'       => $key,
            'sort'  => $sort
        ]);
    }

    public function getSliderHome(){
        return view("admin.film.slider-home", [
            'user' => Auth::user(),
            'active' => 'slider-home',
            'result' => Film::getSliderHome()
        ]);
    }

    public function getMultiLink($id){
        $film = Film::where('id', $id)->first();
        $server_play = ServerPlayFilm::where('publish',1)->orderby('name')->get();

        return view("admin.film.multi_link", [
            'user' => Auth::user(),
            'active' => 'film',
            'action' => 'Edit',
            'setting' => Setting::first(),
            'server_play'   => $server_play,
            'film'      => $film
        ]);
    }

    public function postMultiLink(Request $request){
        $film_id = $request->film_id;
        $server_id = $request->serve_id;

        if($film_id && $server_id){
            $data = $request->textarea_data_content;
            if($data){
                $data_array = explode(';', str_replace("\r\n","",$data));

                if(count($data_array)>0){
                    foreach ($data_array as $element){
                        if($element) {
                            $result = explode('|', $element);
                            if (isset($result[0]) && trim($result[0]) != null) {
                                $check_link = LinkFilm::where('link', trim($result[0]))->where('server_play', $server_id)->where('film_id', $film_id)->first();
                                if (!$check_link) {
                                    $check_link = new LinkFilm;
                                    $check_link->link = trim($result[0]);
                                    if (!empty($result[1])) {
                                        $check_link->email = $result[1];
                                    }
                                    $check_link->server_play = $server_id;
                                    $check_link->film_id = $film_id;
                                    $check_link->type = 'film';
                                    $check_link->created_at = gmdate('Y-m-d H:i:s', time());
                                    $check_link->updated_at = gmdate('Y-m-d H:i:s', time());
                                    $check_link->save();
                                }
                            }
                        }else{
                            return Response::json(['status' => 'error', 'result' => 'Format link film not legal']);
                        }
                    }
                }
            }
        }
        return Response::json(['status'=>200]);
    }

    public function postAjaxCountry(Request $request){
        $country_name = $request->country_name;
        if($country_name){
            $check = CountryFilm::where('name',$country_name)->first();
            if(!$check){
                $check = new CountryFilm;
                $check->name = $country_name;
                $check->slug = clearUnicode($country_name);
                $check->publish = 1;
                $check->created_at = gmdate('Y-m-d H:i:s', time());
                $check->updated_at = gmdate('Y-m-d H:i:s', time());
                $check->save();
            }
            $country_html = "";
            $country_list = CountryFilm::where('publish',1)->get();

            foreach ($country_list as $item){
                $country_html .= "<option value=\"".$item->id."\" ".($item->id==$check->id?'selected':'').">".$item->name."</option>";
            }
            return Response::json(['status'=>200, 'result'=>$country_html]);
        }else{
            return Response::json(['status'=>'error']);
        }
    }

    public function postAjaxGenres(Request $request){
        $genres = $request->genres;
        if($genres){
            $genres_active = array();
            foreach ($genres as $item){
                $check = CategoryFilm::where('name',$item['name'])->first();
                if(!$check){
                    $check = new CategoryFilm;
                    $check->name = $item['name'];
                    $check->slug = clearUnicode($item['name']);
                    $check->publish = 1;
                    $check->created_at = gmdate('Y-m-d H:i:s', time());
                    $check->updated_at = gmdate('Y-m-d H:i:s', time());
                    $check->save();
                }
                $genres_active[] = $check->id;
            }

            $genres_html = "";
            $genres_list = CategoryFilm::where('publish',1)->get();
            foreach ($genres_list as $item){
                $status_genre = 0;
                if(count($genres_active)>0){
                    foreach ($genres_active as $element){
                        if($element == $item->id){
                            $status_genre = 1;
                            break;
                        }
                    }
                }
                $genres_html .= "<div class=\"col-md-6\">";
                    $genres_html .= "<label class=\"checkbox-inline\" for=\"genre_film_id_".$item->id."\">";
                        $genres_html .= "<input ".($status_genre?'checked':'')." type=\"checkbox\" id=\"genre_film_id_".$item->id."\" name=\"genre_film_id[]\" value=\"".$item->id."\">".$item->name;
                    $genres_html .= "</label>";
                $genres_html .= "</div>";
            }
            return Response::json(['status'=>200, 'result'=>$genres_html]);
        }else{
            return Response::json(['status'=>'error']);
        }
    }

    public function postAjaxProducer(Request $request){
        $producer_name = $request->producer_name;
        if($producer_name){
            $check = Producer::where('name',$producer_name)->where('type','producer ')->first();
            if(!$check){
                $check = new Producer;
                $check->name = $producer_name;
                $check->slug = clearUnicode($producer_name);
                $check->publish = 1;
                $check->user_id = Auth::user()->id;
                $check->type = 'producer';
                $check->created_at = gmdate('Y-m-d H:i:s', time());
                $check->updated_at = gmdate('Y-m-d H:i:s', time());
                $check->save();
            }
            $producer_html = "";
            $producer_list = Producer::where('publish',1)->get();

            foreach ($producer_list as $item){
                $producer_html .= "<option value=\"".$item->id."\" ".($item->id==$check->id?'selected':'').">".$item->name."</option>";
            }
            return Response::json(['status'=>200, 'result'=>$producer_html]);
        }else{
            return Response::json(['status'=>'error']);
        }
    }

    public function postServerFilmData(Request $request){
        $server_id = $request->serve_id;
        $film_id = $request->film_id;
        if($server_id && $film_id){
            $link_play = LinkFilm::where('type','film')->where('film_id',$film_id)->where('server_play',$server_id)->get();
            $link_play_html = '';
            if(count($link_play)>0){
                foreach ($link_play as $item){
                    $link_play_html .= $item->link.'|'.$item->email.";\n";
                }
            }
            return Response::json(['status'=>200, 'result'=>$link_play_html]);
        }else{
            return Response::json(['status'=>'error']);
        }
    }

    public function getUpload(){
        return view("admin.film.upload", [
            'user' => Auth::user(),
            'active' => 'upload',
            'action' => 'upload'
        ]);
    }

    public function postUpload(Request $request){
        if($request->file('file')){
            $path = $request->file('file')->path();
            $content = json_decode(file_get_contents($path), true);
            if(isset($content) && count($content) > 0){
                foreach ($content as $item){
                    if(isset($item['type']) && $item['type'] == 1){                 // TV Serie
                        if(isset($item['title'])) {
                            $check = Film::where('name', $item['title'])->first();
                            if (!$check) {
                                if (isset($item['country'])) {
                                    $country_film = CountryFilm::where('name', $item['country'])->first();
                                } else {
                                    $country_film = CountryFilm::first();
                                }
                                if (isset($country_film)) {
                                    $film = new Film();
                                    $film->name = $item['title'];
                                    $film->slug = clearUnicode($item['title']);
                                    if (isset($item['thumb'])) {
                                        $film->images_link = $item['thumb'];
                                    }
                                    if (isset($item['cover'])) {
                                        $film->cover_link = $item['cover'];
                                    }
                                    if (isset($item['background'])) {
                                        $film->background_link = $item['background'];
                                    }
                                    $film->title = $item['title'];
                                    if (isset($item['description'])) {
                                        $film->descriptions = str_replace('"', '', stripslashes(strip_tags($item['description'], 'p')));
                                    }
                                    if (isset($item['keywords'])) {
                                        if(count($item['keywords']) > 1) {
                                            $film->keywords = str_replace('"', '', stripslashes(implode(',', $item['keywords'])));
                                        }else{
                                            $film->keywords = str_replace('"', '', stripslashes($item['keywords']));
                                        }
                                    }
                                    $film->movie_series = 1;
                                    $film->user_id = Auth::user()->id;
                                    if (isset($item['trailer'][0])) {
                                        $film->trailer = $item['trailer'][0];
                                    }
                                    $film->country_film_id = $country_film->id;
                                    if(isset($item['year'])){
                                        $film->release = $item['year'];
                                    }
                                    if(isset($item['duration'])){
                                        $film->duration = substr($item['duration'],0,strpos($item['duration'],' '));
                                    }
                                    if ($film->save()) {
                                        if(isset($item['Genres']) && count($item['Genres'])>0){
                                            foreach($item['Genres'] as $value){
                                                $check_genre = CategoryFilm::where('name',$value)->first();
                                                if(!$check_genre){
                                                    $check_genre = new CategoryFilm();
                                                    $check_genre->name = $value;
                                                    $check_genre->slug = clearUnicode($value);
                                                    $check_genre->publish = 1;
                                                    $check_genre->created_at = gmdate('Y-m-d H:i:s', time());
                                                    $check_genre->updated_at = gmdate('Y-m-d H:i:s', time());
                                                    $check_genre->save();
                                                }

                                                $check_genre_relation = CategoryFilmRelation::where('film_id',$film->id)->where('category_film_id',$check_genre->id)->first();
                                                if(!$check_genre_relation){
                                                    $check_genre_relation = new CategoryFilmRelation;
                                                    $check_genre_relation->film_id = $film->id;
                                                    $check_genre_relation->category_film_id = $check_genre->id;
                                                    $check_genre_relation->created_at = gmdate('Y-m-d H:i:s', time());
                                                    $check_genre_relation->updated_at = gmdate('Y-m-d H:i:s', time());
                                                    $check_genre_relation->save();
                                                }
                                            }
                                        }

                                        if(isset($item['cast']) && count($item['cast'])>0) {
                                            foreach($item['cast'] as $value){
                                                $check_cast = Actor::where('name',$value)->first();
                                                if(!$check_cast){
                                                    $check_cast = new Actor;
                                                    $check_cast->name = $value;
                                                    $check_cast->slug = clearUnicode($value);
                                                    $check_cast->created_at = gmdate('Y-m-d H:i:s', time());
                                                    $check_cast->updated_at = gmdate('Y-m-d H:i:s', time());
                                                    $check_cast->save();
                                                }
                                                $check_cast_relation = ActorRelation::where('film_id',$film->id)->where('actor_id',$check_cast->id)->first();
                                                if(!$check_cast_relation){
                                                    $cast_item = new ActorRelation;
                                                    $cast_item->film_id = $film->id;
                                                    $cast_item->actor_id = $check_cast->id;
                                                    $cast_item->created_at = gmdate('Y-m-d H:i:s', time());
                                                    $cast_item->updated_at = gmdate('Y-m-d H:i:s', time());
                                                    $cast_item->save();
                                                }
                                            }
                                        }

                                        if(isset($item['director']) && count($item['director'])>0) {
                                            foreach($item['director'] as $value){
                                                $check_director = Director::where('name',$value)->first();
                                                if(!$check_director){
                                                    $check_director = new Director();
                                                    $check_director->name = $value;
                                                    $check_director->slug = clearUnicode($value);
                                                    $check_director->created_at = gmdate('Y-m-d H:i:s', time());
                                                    $check_director->updated_at = gmdate('Y-m-d H:i:s', time());
                                                    $check_director->save();
                                                }
                                                $check_director_relation = DirectorRelation::where('film_id',$film->id)->where('director_id',$check_director->id)->first();
                                                if(!$check_director_relation){
                                                    $director_item = new DirectorRelation;
                                                    $director_item->film_id = $film->id;
                                                    $director_item->director_id = $check_director->id;
                                                    $director_item->created_at = gmdate('Y-m-d H:i:s', time());
                                                    $director_item->updated_at = gmdate('Y-m-d H:i:s', time());
                                                    $director_item->save();
                                                }
                                            }
                                        }

                                        if (isset($item['linkPlay']) && count($item['linkPlay']) > 0) {
                                            foreach ($item['linkPlay'] as $detail) {
                                                $film_detail = new FilmDetail();
                                                if (isset($detail['name'])) {
                                                    $film_detail->name = $detail['name'];
                                                    $film_detail->slug = clearUnicode($detail['name']);
                                                    $film_detail->film_id = $film->id;
                                                    $film_detail->publish = 1;
                                                    if ($film_detail->save()) {
                                                        if (isset($detail['link'])) {
                                                            foreach ($detail['link'] as $link) {
                                                                if (isset($link['name_server'])) {
                                                                    $server_play = ServerPlayFilm::where('name', $link['name_server'])->first();
                                                                    if (isset($server_play)) {
                                                                        $link_film = new LinkFilmDetail();
                                                                        $link_film->link = $link['link'];
                                                                        $link_film->type = "film-detail";
                                                                        $link_film->film_detail_id = $film_detail->id;
                                                                        $link_film->server_play = $server_play->id;
                                                                        $link_film->error = 0;
                                                                        $link_film->save();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }else{  //Phim le
                        if(isset($item['title'])) {
                            $check = Film::where('name', $item['title'])->first();
                            if (!$check) {
                                if (isset($item['country'])) {
                                    $country_film = CountryFilm::where('name', $item['country'])->first();
                                } else {
                                    $country_film = CountryFilm::first();
                                }
                                if (isset($country_film)) {
                                    $film = new Film();
                                    $film->name = $item['title'];
                                    $film->slug = clearUnicode($item['title']);
                                    if (isset($item['thumb'])) {
                                        $film->images_link = $item['thumb'];
                                    }
                                    if (isset($item['cover'])) {
                                        $film->cover_link = $item['cover'];
                                    }
                                    if (isset($item['background'])) {
                                        $film->background_link = $item['background'];
                                    }
                                    $film->title = $item['title'];
                                    if (isset($item['description'])) {
                                        $film->descriptions = str_replace('"', '', stripslashes(strip_tags($item['description'], 'p')));
                                    }
                                    if (isset($item['keywords'])) {
                                        if(count($item['keywords']) > 1) {
                                            $film->keywords = str_replace('"', '', stripslashes(implode(',', $item['keywords'])));
                                        }else{
                                            $film->keywords = str_replace('"', '', stripslashes($item['keywords']));
                                        }
                                    }
                                    $film->movie_series = 0;
                                    $film->user_id = Auth::user()->id;
                                    if (isset($item['trailer'][0])) {
                                        $film->trailer = $item['trailer'][0];
                                    }
                                    $film->country_film_id = $country_film->id;
                                    if(isset($item['year'])){
                                        $film->release = $item['year'];
                                    }
                                    if(isset($item['duration'])){
                                        $film->duration = substr($item['duration'],0,strpos($item['duration'],' '));
                                    }

                                    if ($film->save()) {
                                        if(isset($item['Genres']) && count($item['Genres'])>0){
                                            foreach($item['Genres'] as $value){
                                                $check_genre = CategoryFilm::where('name',$value)->first();
                                                if(!$check_genre){
                                                    $check_genre = new CategoryFilm();
                                                    $check_genre->name = $value;
                                                    $check_genre->slug = clearUnicode($value);
                                                    $check_genre->publish = 1;
                                                    $check_genre->created_at = gmdate('Y-m-d H:i:s', time());
                                                    $check_genre->updated_at = gmdate('Y-m-d H:i:s', time());
                                                    $check_genre->save();
                                                }

                                                $check_genre_relation = CategoryFilmRelation::where('film_id',$film->id)->where('category_film_id',$check_genre->id)->first();
                                                if(!$check_genre_relation){
                                                    $check_genre_relation = new CategoryFilmRelation;
                                                    $check_genre_relation->film_id = $film->id;
                                                    $check_genre_relation->category_film_id = $check_genre->id;
                                                    $check_genre_relation->created_at = gmdate('Y-m-d H:i:s', time());
                                                    $check_genre_relation->updated_at = gmdate('Y-m-d H:i:s', time());
                                                    $check_genre_relation->save();
                                                }
                                            }
                                        }

                                        if(isset($item['cast']) && count($item['cast'])>0) {
                                            foreach($item['cast'] as $value){
                                                $check_cast = Actor::where('name',$value)->first();
                                                if(!$check_cast){
                                                    $check_cast = new Actor;
                                                    $check_cast->name = $value;
                                                    $check_cast->slug = clearUnicode($value);
                                                    $check_cast->created_at = gmdate('Y-m-d H:i:s', time());
                                                    $check_cast->updated_at = gmdate('Y-m-d H:i:s', time());
                                                    $check_cast->save();
                                                }
                                                $check_cast_relation = ActorRelation::where('film_id',$film->id)->where('actor_id',$check_cast->id)->first();
                                                if(!$check_cast_relation){
                                                    $cast_item = new ActorRelation;
                                                    $cast_item->film_id = $film->id;
                                                    $cast_item->actor_id = $check_cast->id;
                                                    $cast_item->created_at = gmdate('Y-m-d H:i:s', time());
                                                    $cast_item->updated_at = gmdate('Y-m-d H:i:s', time());
                                                    $cast_item->save();
                                                }
                                            }
                                        }

                                        if(isset($item['director']) && count($item['director'])>0) {
                                            foreach($item['director'] as $value){
                                                $check_director = Director::where('name',$value)->first();
                                                if(!$check_director){
                                                    $check_director = new Director();
                                                    $check_director->name = $value;
                                                    $check_director->slug = clearUnicode($value);
                                                    $check_director->created_at = gmdate('Y-m-d H:i:s', time());
                                                    $check_director->updated_at = gmdate('Y-m-d H:i:s', time());
                                                    $check_director->save();
                                                }
                                                $check_director_relation = DirectorRelation::where('film_id',$film->id)->where('director_id',$check_director->id)->first();
                                                if(!$check_director_relation){
                                                    $director_item = new DirectorRelation;
                                                    $director_item->film_id = $film->id;
                                                    $director_item->director_id = $check_director->id;
                                                    $director_item->created_at = gmdate('Y-m-d H:i:s', time());
                                                    $director_item->updated_at = gmdate('Y-m-d H:i:s', time());
                                                    $director_item->save();
                                                }
                                            }
                                        }

                                        if (isset($item['linkPlay']) && count($item['linkPlay']) > 0) {
                                            foreach($item['linkPlay'] as $value) {
                                                if (isset($value['name_server'])) {
                                                    $server_play = ServerPlayFilm::where('name', $value['name_server'])->first();
                                                    if (isset($server_play)) {
                                                        $link_film = new LinkFilm();
                                                        $link_film->link = $value['link'];
                                                        $link_film->type = "film";
                                                        $link_film->film_id = $film->id;
                                                        $link_film->server_play = $server_play->id;
                                                        $link_film->error = 0;
                                                        $link_film->save();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return redirect()->to('admin/film/list');
            }else{
                return redirect()->back()->with(['flash_level'=>'danger', 'flash_message'=>'File error']);
            }
        }else{
            return redirect()->back()->with(['flash_level'=>'danger', 'flash_message'=>'File not exist']);
        }
    }
}
