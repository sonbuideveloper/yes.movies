<?php

namespace App\Http\Controllers\Admin;

use App\Models\TypeFilm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth, Validator, Redirect, Session;
use Models\MasterModel;
use Models\Setting;

class TypeFilmController extends Controller
{
    protected $table = "type_films";

    public function getIndex()
    {
        return view("admin.type.index", [
            'user' => Auth::user(),
            'active' => 'type',
            'result' => TypeFilm::RecursiveIndexAdmin()
        ]);
    }

    public function getCreate()
    {
        return view("admin.type.edit", [
            'user' => Auth::user(),
            'active' => 'type',
            'action' => 'Create',
            'setting' => Setting::first()
        ]);
    }

    public function getEdit($id)
    {
        $result = TypeFilm::getById($id);
        return view("admin.type.edit", [
            'user' => Auth::user(),
            'active' => 'type',
            'action' => 'Edit',
            'setting' => Setting::first(),
            'result' => $result,
        ]);
    }

    public function getDelete($id)
    {
        if(!Auth::check() || Auth::user()->type != 1)
        {
            return Redirect::to('admin');
        }
        $check = TypeFilm::find($id);
        if(count($check) > 0) {
            TypeFilm::DeleteById($id);
        }
        Session::flash('delete','success');
        return Redirect::to('admin/type/list');
    }

    public function postDelete(Request $request)
    {
        if(!Auth::check() || Auth::user()->type != 1)
        {
            return Redirect::to('admin');
        }
        $id = $request->get('id');
        if(!empty($id)) {
            foreach ($id as $item)
            {
                TypeFilm::DeleteById($item);
            }
        }
        Session::flash('delete','success');
        return Redirect::to('admin/type/list');
    }

    public function postCreate(Request $request)
    {
        $rules = [
            'name' => 'required'
        ];
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $setting = Setting::getSetting();
        $id = $request->get('id');
        $data = array(
            'name' => $request->get('name'),
            'slug' => $request->get('slug'),
            'title' => $request->get('meta-title'),
            'descriptions' => $request->get('meta-description'),
            'keywords' => $request->get('meta-keywords'),
            'publish' => ($request->get('publish') == 'on')?1:0,
            'created_at' => gmdate('Y-m-d H:i:s', time()),
            'updated_at' => gmdate('Y-m-d H:i:s', time())
        );
        if($id == 0) {
            $check = TypeFilm::where(['name' => $request->get('name'), 'slug' => $request->get('slug')])->first();
            if($check == null) {
                $id = MasterModel::CreateContent($this->table, $data);
            }
        } else {
            MasterModel::CreateContent($this->table, $data, ['id' => $id]);
        }
        Session::flash('success','success');
        return Redirect::to('admin/type/edit/'.$id);
    }

    public function getCheckStatus($id)
    {
        $check = TypeFilm::find($id);
        if($check != null)
        {
            if ($check->publish == 1)
            {
                MasterModel::CreateContent($this->table,['publish' => 0], ['id' => $id]);
                return response()->json(['status' => 200, 'result' => 'hidden']);
            } else {
                MasterModel::CreateContent($this->table,['publish' => 1], ['id' => $id]);
                return response()->json(['status' => 200, 'result' => 'publish']);
            }
        }
        return response()->json(['status' => 'error']);
    }
}
