<?php

namespace App\Http\Controllers\Admin;

use App\Models\CategoryFilm;
use App\Models\CategoryFilmRelation;
use App\Models\FavoriteFilm;
use App\Models\Film;
use App\Models\FilmDetail;
use App\Models\LinkFilm;
use App\Models\LinkFilmDetail;
use App\Models\RateFilm;
use App\Models\Social;
use App\Models\SubtitleFilmDetail;
use App\Models\TypeFilmRelation;
use Models\Article;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth,Input,Validator,Redirect,Session,Response,Excel,PDF,Hash;
use Models\MasterModel;
use Models\Users;

class UsersController extends Controller
{
    private $table = "users";

    public function __construct()
    {
        if(!Auth::check() || Auth::user()->type == 4) {
            return false;
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Users
     */
    public function getListUser(Request $request)
    {
        $key = $request->key;
        return view("admin.users.index", [
            'user' => Auth::user(),
            'active' => 'users',
            'result' => Users::RecursiveIndexAdmin($key),
            'key'   => $key
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * Dashboard
     */
    public function getIndex() {
        return view("admin.dashboard",[
             'user'         => Auth::user(),
             'active'       => 'dashboard',             
             'users'        => Users::count(),
             'posts'        => Article::count(),
             'latest_reviews' => Article::where('publish',1)->where('type','review')->orderby('created_at','desc')->take(5)->get(),
             'latest_photos'  => Article::where('publish',1)->where('type','gallery')->orderby('created_at','desc')->with('mediaData')->take(5)->get(),
             'error_link'   => LinkFilm::where('error',1)->count() + LinkFilmDetail::where('error',1)->count(),
             'films'        => Film::count()
         ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * get create user page
     */
    public function getCreate($id = null)
    {
        $action = "Create";
        if($id != null)
        {
            $action = "Edit";
            $result = Users::find($id);
        }
        return view("admin.users.edit", [
            'user' => Auth::user(),
            'active' => 'users',
            'action' => $action,
            'result' => (!empty($result))?$result:""
        ]);
    }
    /**
     * @param $id
     * @param Request $request
     * @return mixed
     * change group index
     */
    public function getChangeGroup($id, Request $request) {
        $val = $request->get('val');
        $check = Users::find($id);
        if($check != '') {
            $data = array(
                'type' => $val
            );
            MasterModel::CreateContent($this->table, $data, ['id' => $id]);
        }
        return Response::json(['status' => 200]);
    }

    /**
     * @param Request $request
     * post create user
     */
    public function postCreate(Request $request)
    {
        $id = $request->get('id');
        if($id == 0) {
            $rules = array(
                'email' => 'required|email',
                'password' => 'required|max:32|min:6'
            );
            $messages = array(
                'email.required' => 'Email is required',
                'email.email'   => 'Email is malformed',
                'password.required' => 'Password is required'
            );
            $validator = Validator::make($request->all(), $rules, $messages);
            if($validator->fails())
            {
                return Redirect::back()->withErrors($validator)->withInput();
            }            
            $data = array(
                'email' => $request->get('email'),
                'name'  => $request->name,
                'age' => $request->get('age'),
                'type_video' => $request->get('type_video'),
                'password' => $request->get('password', false),
                'type' => $request->get('type',4)
            );
            $user = Users::CreateUser($data);
            if($user == false) {
                Session::flash('error', 'Email has been registered');
                return Redirect::back()->withInput();
            }
            $id = $user->id;
        } else {
            $rules = array(
                'email' => 'required|email'
            );
            $messages = array(
                'email.required' => 'Email is required',
                'email.email'   => 'Email is malformed'
            );
            $validator = Validator::make($request->all(), $rules, $messages);
            if($validator->fails())
            {
                return Redirect::back()->withErrors($validator)->withInput();
            }            
            $data = array(
                'id' => $id,
                'email' => $request->get('email'),
                'name'  => $request->name,
                'age' => $request->get('age'),
                'type_video' => $request->get('type_video'),
                'password' => $request->get('password', false),
                'type' => $request->get('type',4)
            );
            $user = Users::ChangePasswordAdmin($data);
        }
        Session::flash('success', 'success');
        return Redirect::to('admin/user/edit/'.$id);
    }

    /**
     * Delete item by id using method get
     * @param $id
     * @return mixed
     */
    public function getDelete($id)
    {
        $check = Users::find($id);
        if(isset($check)) {
            Users::DeleteById($id);
        }
        Session::flash('delete','success');
        return Redirect::to('admin/user/list');
    }

    /**
     * Delete all item using checkbox, method post
     * @param Request $request
     * @return mixed
     */
    public function postDelete(Request $request)
    {
        $id = $request->get('id');
        if(!empty($id)) {
            foreach ($id as $item)
            {
                Users::DeleteById($item);
            }
        }
        Session::flash('delete','success');
        return Redirect::to('admin/user/list');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * get login page
     */
    public function getLogin() {
        return view("admin.users.login");
    }

    /**
     * @return Redirect
     * post login with email
     */
    public function postLogin() {
        $email = Input::get('login-email', false);
        $password = Input::get('login-password');
        if(strstr($email,'@')){
          $status_login = 'email';
        }else{
          $status_login = 'username';
        }
        if($status_login == 'email'){
          if(Auth::attempt(['email' => $email, 'password' => $password], true)){
              if(Auth::user()->type != 4){
                  return redirect('admin/dashboard');
              }else{
                  return redirect('/');
              }
          } else {
              Session::flash('error', 'Email or password not correct');
              return redirect('admin/login');
          }
        }else{
          if(Auth::attempt(['username' => $email, 'password' => $password], true)){
              if(Auth::user()->type != 4){
                  return redirect('admin/dashboard');
              }else{
                  return redirect('/');
              }
          } else {
              Session::flash('error', 'Username or password not correct');
              return redirect('admin/login');
          }
        }
    }

    /**
     * @return mixed
     * edit profile user
     */
    public function postEditProfile() {
        $rules = array(
            'user-settings-email' => 'required|email'
        );
        $validation = Validator::make(Input::All(), $rules);
        if($validation->fails()){
            return Redirect::back()->withError( $validation )->withInput();
        }
        $data = array(
            'id' => Auth::user()->id,
            'email' => Input::get('user-settings-email', false),
            'password' => Input::get('user-settings-password', false),
            'firstname' => Auth::user()->firstname,
            'lastname' => Auth::user()->lastname,
        );
        Users::ChangePasswordAdmin($data);
        return Redirect::back();
    }

    public function postForgotPassword(Request $request){
        $setting = Setting::with('metaData')->first();
        $email = $request->get('reminder-email');
        $user_check = Users::where('email',$email)->first();
        if(!$user_check){
            Session::flash('error', 'Success | New password has been sent to email');
            return redirect('admin/login');
        }else{
            $password = str_random(8);
            $user_check->password = bcrypt($password);
            $user_check->save();
            Mail::send('emails.reset-password', ['password' => $password], function ($m) use ($email, $setting) {
                $m->from($setting->email, 'Email password reset!');
                $m->to($email)->subject('Email password reset!');
            });
            Session::flash('error', 'Success | New password has been sent to email');
            return redirect('admin/login');
        }
    }

    /**
     * @return Redirect
     * post create user
     */
    public function postRegister() {
        $data = array(
            'email' => Input::get('register-email', ""),
            'firstname' => Input::get('register-firstname', ""),
            'lastname' => Input::get('register-lastname', ""),
            'password' => Input::get('register-password', ""),
            'type' => 4,
        );
        $user = Users::CreateUser($data);
        if($user == false) {
            Session::flash('error_register', 'Email has been registered');
            return redirect('admin/login#register');
        }
        if(Auth::attempt(['email' => $user->email, 'password' => Input::get('register-password')], true)){
            return redirect('admin/article/list');
        }

    }

    /**
     * @return mixed
     * logout
     */
    public function getLogout() {
        Auth::logout();
        Session::flush();
        return redirect()->to('/');
    }

    /**
     * export data to excel
     */
    public function exportXLS()
    {
        $contact = Users::all();
        Excel::create('Users List', function($excel) use ($contact) {

            $excel->sheet('Users List', function($sheet) use ($contact) {

                $sheet->fromArray($contact);

            });

        })->export('xlsx');
    }

    /**
     * export data to pdf
     */
    public function exportPDF()
    {
        $pdf = PDF::loadView('admin.users.pdf', ['data' => Users::all()]);
        return $pdf->download('Users_List.pdf');
    }

    /**
     * upload images
     * @param Request $request
     */
    public function postAvatar(Request $request) {
        $id = $request->get('id');
        $file = $request->file('images');
        if ($file->isValid()) {
            $name = $file->getClientOriginalName();
            $name = getNameImage($name);
            $extension = $file->getClientOriginalExtension();
            $fileName = $name.'-'.time().'.'.$extension;

            $check = Users::find($id);
            if($check == null) {
                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->table,['images' => $path]);
            } else {
                if($check->images != null){
                    deleteImage($check->images);
                }
                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->table,['images' => $path], ['id' => $check->id]);
            }
        }

    }

    /**
     * Delete image
     * @param Request $request
     * @return mixed
     */
    public function postDeleteImage(Request $request) {
        $check = Users::find($request->get('id'));
        if($check != null){
            if($check->images != null) {
                deleteImage($check->images);
                MasterModel::CreateContent($this->table,['images' => null], ['id' => $check->id]);
            }
        }
        return Response::json(['status' => 200]);
    }

    public function getCheckActive($id){
        $check = Users::find($id);
        if($check != null)
        {
            if($check->id == Auth::user()->id){
              return Response::json(['status' => 'error']);
            }
            if($check->active == 1){
                $status = 'Inactive';
            }else{
                $status = 'Active';
            }
            MasterModel::CreateContent($this->table,['active' => ($check->active+1)%2], ['id' => $id]);
            return Response::json(['status' => 200, 'result' => $status]);
        }
        return Response::json(['status' => 'error']);
    }
}
