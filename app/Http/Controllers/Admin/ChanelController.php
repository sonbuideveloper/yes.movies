<?php

namespace App\Http\Controllers\Admin;

use App\Models\Chanel;
use App\Models\TagFilm;
use App\Models\TagFilmRelation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth, Session, Redirect, Response, Validator;
use Models\MasterModel;
use Models\Setting;

class ChanelController extends Controller
{
    protected $table = "chanels";

    public function getIndex()
    {
        return view("admin.chanel.index", [
            'user' => Auth::user(),
            'active' => 'chanel',
            'result' => Chanel::RecursiveIndexAdmin('chanel')
        ]);
    }

    public function getCreate()
    {
        $tag_film_array = TagFilm::where('type','tag_film')->select('name')->orderby('name')->get();
        $tag_film = array();
        foreach ($tag_film_array as $item){
            $tag_film[] = $item->name;
        }

        return view("admin.chanel.edit", [
            'user' => Auth::user(),
            'active' => 'chanel',
            'action' => 'Create',
            'setting' => Setting::first(),
            'tag_film_all'  => (!empty($tag_film) ? implode($tag_film, ',') : ''),
        ]);
    }

    public function getEdit($id)
    {
        $result = Chanel::getById($id);

        $tag_film_array = TagFilm::select('name')->orderby('name')->get();
        $tag_film = array();
        foreach ($tag_film_array as $item){
            $tag_film[] = $item->name;
        }

        $tag_film_item = '';
        if(count($result->tagFilm)>0){
            foreach ($result->tagFilm as $item){
                if($item->type = 'chanel'){
                    $tag_film_item = $item->tag->name;
                }
            }
        }

        return view("admin.chanel.edit", [
            'user' => Auth::user(),
            'active' => 'chanel',
            'action' => 'Edit',
            'setting' => Setting::first(),
            'result' => $result,
            'tag_film_all'  => (!empty($tag_film) ? implode($tag_film, ',') : ''),
            'tag_film_item' => $tag_film_item
        ]);
    }

    public function getDelete($id)
    {
        if(!Auth::check() || Auth::user()->type != 1)
        {
            return Redirect::to('admin');
        }
        $check = Chanel::find($id);
        if(count($check) > 0) {
            Chanel::DeleteById($id);
        }
        Session::flash('delete','success');
        return Redirect::to('admin/chanel/list');
    }

    public function postDelete(Request $request)
    {
        if(!Auth::check() || Auth::user()->type != 1)
        {
            return Redirect::to('admin');
        }
        $id = $request->get('id');
        if(!empty($id)) {
            foreach ($id as $item)
            {
                Chanel::DeleteById($item);
            }
        }
        Session::flash('delete','success');
        return Redirect::to('admin/chanel/list');
    }

    public function postCreate(Request $request)
    {
        $rules = [
            'name' => 'required'
        ];
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $id = $request->get('id');
        $data = array(
            'name' => $request->get('name'),
            'slug' => $request->get('slug'),
            'title' => $request->get('meta-title'),
            'descriptions' => $request->get('meta-description'),
            'keywords' => $request->get('meta-keywords'),
            'user_id'  => Auth::user()->id,
            'publish' => ($request->get('publish') == 'on')?1:0,
            'type'      => 'chanel',
            'created_at' => gmdate('Y-m-d H:i:s', time()),
            'updated_at' => gmdate('Y-m-d H:i:s', time())
        );
        if($id == 0) {
            $check = Chanel::where(['name' => $request->get('name'), 'slug' => $request->get('slug')])->first();
            if($check == null) {
                $id = MasterModel::CreateContent($this->table, $data);
            }
        } else {
            MasterModel::CreateContent($this->table, $data, ['id' => $id]);
        }

        if($request->get('tag-film')){
            $tag_film = $request->get('tag-film');
            TagFilmRelation::DeleteNotInArray($id, $tag_film, 'chanel');
            $tag_film_check = TagFilm::where('name',$tag_film)->first();
            if(!$tag_film_check){
                $tag_film_check = new TagFilm;
                $tag_film_check->name = $tag_film;
                $tag_film_check->slug = clearUnicode($tag_film);
                $tag_film_check->type = 'tag_film';
                $tag_film_check->created_at = gmdate('Y-m-d H:i:s', time());
                $tag_film_check->updated_at = gmdate('Y-m-d H:i:s', time());
                $tag_film_check->save();
            }
            $tag_film_relation_check = TagFilmRelation::where('type','chanel')->where('film_id',$id)->first();
            if(!$tag_film_relation_check){
                $tag_film_item = new TagFilmRelation;
                $tag_film_item->film_id = $id;
                $tag_film_item->tag_id = $tag_film_check->id;
                $tag_film_item->type = 'chanel';
                $tag_film_item->created_at = gmdate('Y-m-d H:i:s', time());
                $tag_film_item->updated_at = gmdate('Y-m-d H:i:s', time());
                $tag_film_item->save();
            }
        }

        Session::flash('success','success');
        return Redirect::to('admin/chanel/edit/'.$id);
    }

    public function getCheckStatus($id)
    {
        $check = Chanel::find($id);
        if($check != null)
        {
            if ($check->publish == 1)
            {
                MasterModel::CreateContent($this->table,['publish' => 0], ['id' => $id]);
                return response()->json(['status' => 200, 'result' => 'hidden']);
            } else {
                MasterModel::CreateContent($this->table,['publish' => 1], ['id' => $id]);
                return response()->json(['status' => 200, 'result' => 'publish']);
            }
        }
        return response()->json(['status' => 'error']);
    }

    public function postAvatar(Request $request) {
        $id = $request->get('id');
        $file = $request->file('images');
        if ($file->isValid()) {
            $name = $file->getClientOriginalName();
            $name = getNameImage($name);
            $extension = $file->getClientOriginalExtension();
            $fileName = $name.'-'.time().'.'.$extension;

            $check = Chanel::find($id);
            if($check == null) {
                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->table,['images' => $path, 'images_link' => null]);
            } else {
                if($check->images != null){
                    deleteImage($check->images);
                }
                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->table,['images' => $path, 'images_link' => null], ['id' => $check->id]);
            }
        }
    }

    public function postImagesLink(Request $request){
        $id = $request->get('id');
        $images_link = $request->images_link;
        if($images_link){
            $check = Chanel::find($id);
            if($check){
                if($check->images){
                    deleteImage($check->images);
                    $check->images = '';
                }
                $check->images_link = $images_link;
                $check->save();
            }
        }
        Session::flash('success','success');
        return Redirect::to('admin/chanel/edit/'.$id);
    }

    public function postMultipleImages(Request $request) {
        $id = $request->get('id');
        $files = $request->file('images');
        foreach ($files as $file){
            if ($file->isValid()) {
                $name = $file->getClientOriginalName();
                $name = getNameImage($name);
                $extension = $file->getClientOriginalExtension();
                $fileName = $name.'-'.time().'.'.$extension;

                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->media,['images' => $path, 'type' => 'product', 'product_id' => $id]);
            }
        }

    }

    public function postDeleteImage(Request $request) {
        $check = Chanel::find($request->get('id'));
        if($check != null){
            if($check->images != null) {
                deleteImage($check->images);
                MasterModel::CreateContent($this->table,['images' => null], ['id' => $check->id]);
            }
        }
        return Response::json(['status' => 200]);
    }

    public function getDeleteImageLink($id){
        $check = Chanel::find($id);
        if($check != null){
            $check->images_link = '';
            $check->save();
        }
        Session::flash('success','success');
        return Redirect::to('admin/chanel/edit/'.$id);
    }

    public function postCover(Request $request){
        $id = $request->get('id');
        $file = $request->file('images');
        if ($file->isValid()) {
            $name = $file->getClientOriginalName();
            $name = getNameImage($name);
            $extension = $file->getClientOriginalExtension();
            $fileName = $name.'-'.time().'.'.$extension;

            $check = Chanel::find($id);
            if($check == null) {
                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->table,['cover' => $path, 'cover_link' => null]);
            } else {
                if($check->cover != null){
                    deleteImage($check->cover);
                }
                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->table,['cover' => $path, 'cover_link' => null], ['id' => $check->id]);
            }
        }
    }

    public function postDeleteCover(Request $request){
        $check = Chanel::find($request->get('id'));
        if($check != null){
            if($check->cover != null) {
                deleteImage($check->cover);
                MasterModel::CreateContent($this->table,['cover' => null], ['id' => $check->id]);
            }
        }
        return Response::json(['status' => 200]);
    }

    public function postCoverLink(Request $request){
        $id = $request->get('id');
        $cover_link = $request->cover_link;
        if($cover_link){
            $check = Chanel::find($id);
            if($check){
                if($check->cover){
                    deleteImage($check->cover);
                    $check->cover = '';
                }
                $check->cover_link = $cover_link;
                $check->save();
            }
        }
        Session::flash('success','success');
        return Redirect::to('admin/chanel/edit/'.$id);
    }

    public function getDeleteCoverLink($id){
        $check = Chanel::find($id);
        if($check != null){
            $check->cover_link = '';
            $check->save();
        }
        Session::flash('success','success');
        return Redirect::to('admin/chanel/edit/'.$id);
    }

    public function postBackground(Request $request){
        $id = $request->get('id');
        $file = $request->file('images');
        if ($file->isValid()) {
            $name = $file->getClientOriginalName();
            $name = getNameImage($name);
            $extension = $file->getClientOriginalExtension();
            $fileName = $name.'-'.time().'.'.$extension;

            $check = Chanel::find($id);
            if($check == null) {
                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->table,['background' => $path, 'background_link' => null]);
            } else {
                if($check->background != null){
                    deleteImage($check->background);
                }
                $path = putUploadImage($file, $fileName);
                MasterModel::CreateContent($this->table,['background' => $path, 'background_link' => null], ['id' => $check->id]);
            }
        }
    }

    public function postDeleteBackground(Request $request){
        $check = Chanel::find($request->get('id'));
        if($check != null){
            if($check->background != null) {
                deleteImage($check->background);
                MasterModel::CreateContent($this->table,['background' => null], ['id' => $check->id]);
            }
        }
        return Response::json(['status' => 200]);
    }

    public function postBackgroundLink(Request $request){
        $id = $request->get('id');
        $background_link = $request->background_link;
        if($background_link){
            $check = Chanel::find($id);
            if($check){
                if($check->background){
                    deleteImage($check->background);
                    $check->background = '';
                }
                $check->background_link = $background_link;
                $check->save();
            }
        }
        Session::flash('success','success');
        return Redirect::to('admin/chanel/edit/'.$id);
    }

    public function getDeleteBackgroundLink($id){
        $check = Chanel::find($id);
        if($check != null){
            $check->background_link = '';
            $check->save();
        }
        Session::flash('success','success');
        return Redirect::to('admin/chanel/edit/'.$id);
    }
}
