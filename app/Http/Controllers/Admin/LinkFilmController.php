<?php

namespace App\Http\Controllers\Admin;

use App\Models\LinkFilm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth, Redirect, Validator, Session, Response;
use Models\MasterModel;
use Models\Setting;

class LinkFilmController extends Controller
{
    protected $table = "link_films";

    public function postCreate(Request $request)
    {
        $rules = [
            'link' => 'required'
        ];
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $setting = Setting::getSetting();
        $id = $request->get('id');

        $data = array(
            'link'              => $request->link,
            'server_play'       => $request->server_play,
            'email'             => $request->email,
            'film_id'           => $request->film_id,
            'type'              =>'film',
            'created_at'        => gmdate('Y-m-d H:i:s', time()),
            'updated_at'        => gmdate('Y-m-d H:i:s', time())
        );

        if($id == 0) {
            $check = LinkFilm::where(['link' => $request->get('link'), 'server_play' => $request->get('server_play')])->first();
            if($check == null) {
                $id = MasterModel::CreateContent($this->table, $data);
            }
        } else {
            MasterModel::CreateContent($this->table, $data, ['id' => $id]);
        }

        Session::flash('success','success');
        return Redirect::to('admin/film/edit/'.$request->film_id);
    }

    public function postAjaxCreate(Request $request){
        $film_link_id = $request->film_link_id;
        $server_play = $request->server_play;
        $link = $request->link;
        $email = $request->email;
        if($film_link_id && $server_play && $link){
            $link_check = LinkFilm::where('link',$link)->where('type','film')->where('server_play',$server_play)->where('film_id',$film_link_id)->first();
            if($link_check){
                return response()->json(['status'=>'error', 'result'=>'Link Exist']);
            }else{
                $link_check = new LinkFilm;
                $link_check->link = $link;
                $link_check->type = 'film';
                $link_check->server_play = $server_play;
                $link_check->film_id = $film_link_id;
                $link_check->email = $email;
                $link_check->created_at = gmdate('Y-m-d H:i:s', time());
                $link_check->updated_at = gmdate('Y-m-d H:i:s', time());
                $link_check->save();
                return response()->json(['status'=>200, 'result'=>$link_check]);
            }
        }else{
            return response()->json(['status'=>'error', 'result'=>'Error']);
        }
    }

    public function postAjaxEdit(Request $request){
        $film_link_id = $request->film_link_id;
        $server_play = $request->server_play;
        $link = $request->link;
        $email = $request->email;
        $film_id = $request->film_id;

        if(!$film_link_id){
            return response()->json(['status'=>'error', 'result'=>'Link Not Found']);
        }
        if($film_id && $server_play && $link){
            $link_check = LinkFilm::find($film_link_id);
            if(!$link_check){
                return response()->json(['status'=>'error', 'result'=>'Link Not Found']);
            }else{
                $data = array(
                    'link'              => $link,
                    'server_play'       => $server_play,
                    'film_id'           => $film_id,
                    'type'              =>'film'
                );

                $link_check_new = LinkFilm::where('id','<>',$film_link_id)->where($data)->first();
                if($link_check_new){
                    return response()->json(['status'=>'error', 'result'=>'Link Exist']);
                }

                $link_check->link = $link;
                $link_check->type = 'film';
                $link_check->server_play = $server_play;
                $link_check->film_id = $film_id;
                $link_check->email = $email;
                $link_check->updated_at = gmdate('Y-m-d H:i:s', time());
                $link_check->save();
                return response()->json(['status'=>200, 'result'=>$link_check]);
            }
        }else{
            return response()->json(['status'=>'error', 'result'=>'Error']);
        }
    }



//    public function getDelete($id)
//    {
//        if(!Auth::check() || Auth::user()->type != 1)
//        {
//            return Redirect::to('admin');
//        }
//        $check = LinkFilm::find($id);
//        if(count($check) > 0) {
//            $check->delete();
//        }
//        Session::flash('delete','success');
//        return Redirect::back();
//    }

    public function postDelete(Request $request)
    {
        if(!Auth::check() || Auth::user()->type != 1)
        {
            return Redirect::to('admin');
        }
        $id = $request->id;
        if(!empty($id)) {
            if(is_array($id)){
                foreach ($id as $item)
                {
                    $link_film = LinkFilm::find($item);
                    if($link_film){
                        $link_film->delete();
                    }
                }
            }else{
                $check = LinkFilm::find($id);
                if(count($check) > 0) {
                    $check->delete();
                }
            }
        }
        Session::flash('success','Success');
        return Redirect::back();
    }

    public function postActive(Request $request)
    {
        if(!Auth::check() || Auth::user()->type != 1)
        {
            return Redirect::to('admin');
        }
        $id = $request->id;
        if(!empty($id)) {
            if(is_array($id)){
                foreach ($id as $item)
                {
                    $link_film = LinkFilm::find($item);
                    if($link_film){
                        $link_film->error = 0;
                        $link_film->save();
                    }
                }
            }else{
                $check = LinkFilm::find($id);
                if(count($check) > 0) {
                    $check->error = 0;
                    $check->save();
                }
            }
        }
        Session::flash('success','Success');
        return Redirect::back();
    }

    public function postAjaxDelete(Request $request)
    {
        if(!Auth::check() || Auth::user()->type != 1)
        {
            return Redirect::to('admin');
        }
        $id = $request->id;
        if(!empty($id)) {
            if(is_array($id)){
                foreach ($id as $item)
                {
                    $link_film = LinkFilm::find($item);
                    if($link_film){
                        $link_film->delete();
                    }
                }
            }else{
                $check = LinkFilm::find($id);
                if(count($check) > 0) {
                    $check->delete();
                }
            }
            return response()->json(['status'=>200]);
        }
        return response()->json(['status'=>'Error']);
    }

    public function postAjaxActive(Request $request)
    {
        if(!Auth::check() || Auth::user()->type != 1)
        {
            return Redirect::to('admin');
        }
        $id = $request->id;
        if(!empty($id)) {
            if(is_array($id)){
                foreach ($id as $item)
                {
                    $link_film = LinkFilm::find($item);
                    if($link_film){
                        $link_film->error = 0;
                        $link_film->save();
                    }
                }
            }else{
                $check = LinkFilm::find($id);
                if(count($check) > 0) {
                    $check->error = 0;
                    $check->save();
                }
            }
            return response()->json(['status'=>200]);
        }
        return response()->json(['status'=>'error']);
    }

    public function postFindLinkFilm(Request $request){
        $link_film_id = $request->link_film_id;
        if($link_film_id){
            $result = LinkFilm::find($link_film_id);
            return Response::json(['status' => 200, 'result' => $result]);
        }else{
            return Response::json(['status' => 'error']);
        }
    }

    public function postEditFilmError(Request $request){
        $link_film_id = $request->id;
        $link = $request->link;
        $check = LinkFilm::find($link_film_id);
        $check->link = $link;
        $check->email = $request->email;
        $check->error = 0;
        $check->save();
        Session::flash('delete','success');
        return Redirect::back();
    }

    public function getCheckStatus($id)
    {
        $check = LinkFilm::find($id);
        if($check != null)
        {
            if ($check->error == 1) {
                $check->error = 0;
                $check->save();
            }
        }
        Session::flash('delete','success');
        return Redirect::back();
    }
}
