<?php

namespace App\Http\Controllers;

use App\Models\Ads;
use App\Models\CategoryFilm;
use App\Models\CountryFilm;
use App\Models\FavoriteFilm;
use App\Models\Film;
use App\Models\TypeFilm;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\App;
use Models\Languages;
use Models\MasterModel;
use Models\Setting;
use Models\Users;
use Redirect, Auth, Session;

class UsersController extends Controller
{
    //

    public function getActive(Request $request)
    {
        $token = $request->get('token');
        $check = Users::where(['active_token' => $token])->first();
        if($check != null) {
            MasterModel::CreateContent("users",['active_token' => null, 'active' => 1],['id' => $check->id] );
        }
        return redirect('/')->with([
            'flash_level'   =>  'success',
            'flash_message' =>  'Congratulations | You have successfully activate your account.'
        ]);
    }

    public function getLogout(){
        Auth::logout();
        Session::flush();
        return redirect()->to('/');
    }

    public function getProfile(){
        $lang = App::getLocale();
        $lang_id = Session::get('lang_id');
        $language_list = Languages::get();
        $setting = Setting::first();
        $meta = array(
            'title'         => $setting->title,
            'description'   => $setting->description,
            'keywords'      => $setting->keywords,
            'images'        => $setting->meta_images
        );
        $genres = CategoryFilm::where('publish',1)->orderby('name')->get();
        $country = CountryFilm::where('publish',1)->orderby('name')->get();

        $films = Film::where('publish',1)->where('movie_series',1)->orderby('IMDB','desc')->paginate($setting->film_page);
        $footer = Ads::where('type','footer')->first();

        return view('frontend.pages.user-profile')->with([
            'page'                  => '',
            'sub_active'            => 'profile',
            'setting'               => $setting,
            'meta'                  => $meta,
            'lang'                  => $lang,
            'lang_id'               => $lang_id,
            'language_list'         => $language_list,
            'genres'                => $genres,
            'country'               => $country,
            'results'               => $films,
            'title'                 => 'Profile',
            'footer'                => (!empty($footer)?$footer:null)
        ]);
    }

    public function getFavorite(){
        $lang = App::getLocale();
        $lang_id = Session::get('lang_id');
        $language_list = Languages::get();
        $setting = Setting::first();
        $meta = array(
            'title'         => $setting->title,
            'description'   => $setting->description,
            'keywords'      => $setting->keywords,
            'images'        => $setting->meta_images
        );
        $genres = CategoryFilm::where('publish',1)->orderby('name')->get();
        $country = CountryFilm::where('publish',1)->orderby('name')->get();
        $type = TypeFilm::where('publish',1)->orderby('name')->get();

        $films = FavoriteFilm::where('user_id',Auth::user()->id)->with('films')->paginate($setting->film_page);
        $footer = Ads::where('type','footer')->first();
        $page_list_top_page = Ads::where('slug','page-list-top-page')->where('publish',1)->where('type','ads')->first();
        $page_list_bottom_page = Ads::where('slug','page-list-bottom-page')->where('publish',1)->where('type','ads')->first();

        return view('frontend.theme_'.$setting->theme.'.pages.user-favorite')->with([
            'page'                  => '',
            'sub_active'            => 'favorite',
            'setting'               => $setting,
            'meta'                  => $meta,
            'lang'                  => $lang,
            'lang_id'               => $lang_id,
            'language_list'         => $language_list,
            'genres'                => $genres,
            'country'               => $country,
            'type'                  => $type,
            'results'               => $films,
            'title'                 => 'My Favorites',
            'footer'                => (!empty($footer)?$footer:null),
            'page_list_top_page'    => (!empty($page_list_top_page)?$page_list_top_page:null),
            'page_list_bottom_page' => (!empty($page_list_bottom_page)?$page_list_bottom_page:null)
        ]);
    }

}
