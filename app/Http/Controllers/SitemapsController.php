<?php

namespace App\Http\Controllers;

use App\Models\CategoryFilm;
use App\Models\CountryFilm;
use App\Models\Film;
use App\Models\TypeFilm;
use Illuminate\Http\Request;
use Models\Article;
use Sitemap;

class SitemapsController extends Controller
{
    public function index()
    {
        Sitemap::addTag(url('/'), null, null, '1.0');

        $genres = CategoryFilm::where('publish',1)->orderby('name')->get();
        if(count($genres)>0){
            foreach ($genres as $item){
                Sitemap::addTag(url('genres/'.$item->slug), $item->updated_at, null, '0.8');
            }
        }

        $country = CountryFilm::where('publish',1)->orderby('name')->get();
        if(count($country)>0){
            foreach ($country as $item){
                Sitemap::addTag(url('country/'.$item->slug), $item->updated_at, null, '0.8');
            }
        }

        Sitemap::addTag(url('featured'), null, null, '0.8');
        Sitemap::addTag(url('movies'), null, null, '0.8');
        Sitemap::addTag(url('tv-series'), null, null, '0.8');

        $type = TypeFilm::where('publish',1)->orderby('name')->get();
        if(count($type)>0){
            foreach ($type as $item){
                Sitemap::addTag(url('type/'.$item->slug), $item->updated_at, null, '0.8');
            }
        }

        Sitemap::addTag(url('top-imdb/all'), null, null, '0.8');
        Sitemap::addTag(url('library-film'), null, null, '0.8');
        Sitemap::addTag(url('ranking-film'), null, null, '0.8');

        $films = Film::where('publish',1)->orderby('name')->get();
        if(count($films)>0){
            foreach ($films as $item){
                Sitemap::addTag(url('watch/'.$item->slug.'-'.$item->id), $item->updated_at, null, '0.8');
            }
        }

        $comming_soon = Article::where('type','article')->where('publish',1)->orderby('name')->get();
        if(count($comming_soon)>0){
            foreach ($comming_soon as $item){
                Sitemap::addTag(url('coming-soon/'.$item->slug), $item->updated_at, null, '0.8');
            }
        }

        return Sitemap::render();
    }
}
