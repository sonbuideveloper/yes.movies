<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\Constraints\PageConstraint;
use Models\Media;
use App\Models\FilmDetail;
use Models\Users;

class Film extends Model
{
    protected $table = "films";

    public function mediaData()
    {
        return $this->hasMany('Models\Media','film_id','id');
    }

    public function genres(){
        return $this->belongsToMany('App\Models\CategoryFilm','category_film_relations','film_id','category_film_id');
    }

    public function type(){
        return $this->belongsToMany('App\Models\TypeFilm','type_film_relations','film_id','type_film_id');
    }

    public function tagFilm()
    {
        return $this->hasMany('App\Models\TagFilmRelation', 'film_id');
    }

    public function castRelation()
    {
        return $this->hasMany('App\Models\ActorRelation', 'film_id');
    }

    public function directorRelation()
    {
        return $this->hasMany('App\Models\DirectorRelation', 'film_id');
    }

    public function link_play()
    {
        return $this->hasMany('App\Models\LinkFilm', 'film_id');
    }

    public function subtitle()
    {
        return $this->hasMany('App\Models\SubtitleFilm', 'film_id');
    }

    public function country(){
        return $this->hasOne('App\Models\CountryFilm', 'id', 'country_film_id');
    }

    public function actors(){
        return $this->belongsToMany('App\Models\Actor','actor_relations','film_id','actor_id');
    }

    public function directors(){
        return $this->belongsToMany('App\Models\Director','director_relations','film_id','director_id');
    }

    public function user(){
        return $this->hasOne('Models\Users', 'id', 'user_id');
    }

    public function film_detail(){
        return $this->hasMany('App\Models\FilmDetail', 'film_id');
    }

    public function favorite(){
        return $this->hasMany('App\Models\FavoriteFilm', 'film_id');
    }

    public function rate(){
        return $this->hasMany('App\Models\RateFilm', 'film_id');
    }

    public function producer(){
        return $this->belongsTo('App\Models\Producer', 'producer_id', 'id');
    }

//    public function view(){
//        return $this->hasMany('App\Models\ViewFilm', 'film_id');
//    }

    public static function getById($id)
    {
        return self::with('mediaData')->with('genres')->with('type')->find($id);
    }
    /**
     * @param $id
     * Delete only one row
     */
    public static function DeleteById($id)
    {
        $check = self::find($id);
        if($check != null) {
            if($check->images){
                deleteImage($check->images);
            }
            if($check->cover){
                deleteImage($check->cover);
            }
            if($check->background){
                deleteImage($check->background);
            }
            $tag_film_relation = TagFilmRelation::where('type','tag_film')->where('film_id',$id)->get();
            if(count($tag_film_relation)>0){
                foreach ($tag_film_relation as $item){
                    $item->delete();
                }
            }
            $cast_film_relation = ActorRelation::where('film_id',$id)->get();
            if(count($cast_film_relation)>0){
                foreach ($cast_film_relation as $item){
                    $item->delete();
                }
            }
            $director_film_relation = DirectorRelation::where('film_id',$id)->get();
            if(count($director_film_relation)>0){
                foreach ($director_film_relation as $item){
                    $item->delete();
                }
            }
            $category_film_relation = CategoryFilmRelation::where('film_id',$id)->get();
            if(count($category_film_relation)>0){
                foreach ($category_film_relation as $item){
                    $item->delete();
                }
            }
            if($check->movie_series == 1){
                $movie_series = FilmDetail::where('film_id',$check->id)->get();
                if(count($movie_series)>0){
                    foreach ($movie_series as $element){
                        FilmDetail::DeleteById($element->id);
                    }
                }
            }
            $link_film = LinkFilm::where('film_id',$id)->get();
            if(count($link_film)>0){
                foreach ($link_film as $item){
                    $item->delete();
                }
            }
            $favorite = FavoriteFilm::where('film_id',$id)->get();
            if(count($favorite)>0){
                foreach ($favorite as $item){
                    $item->delete();
                }
            }
            $subtitle = SubtitleFilm::where('film_id',$id)->get();
            if(count($subtitle)>0){
                foreach ($subtitle as $item){
                    $item->delete();
                }
            }
            $type_film_relation = TypeFilmRelation::where('film_id',$id)->get();
            if(count($type_film_relation)>0){
                foreach ($type_film_relation as $item){
                    $item->delete();
                }
            }
            $rate_film = RateFilm::where('film_id',$id)->get();
            if(count($rate_film)>0){
                foreach ($rate_film as $item){
                    $item->delete();
                }
            }
            $view_film = ViewFilm::where('film_id',$id)->get();
            if(count($view_film)>0){
                foreach ($view_film as $item){
                    $item->delete();
                }
            }

            self::find($check->id)->delete();
        }
    }

    /**
     * @param $where
     * Delete multiple rows
     */
    public static function DeleteMultiple($where)
    {
        $check = self::where($where)->get();
        if(count($check) > 0)
        {
            foreach ($check as $item)
            {
                Contents::DeleteData(['type' => 'product', 'product_id' => $item->id]);
                Media::DeleteMultiple(['type' => 'product', 'product_id' => $item->id]);
                if($item->images){
                    deleteImage($item->images);
                }
                if($item->cover){
                    deleteImage($item->cover);
                }
                if($item->background){
                    deleteImage($item->background);
                }
                $tag_film_relation = TagFilmRelation::where('type','tag_film')->where('film_id',$id)->get();
                if(count($tag_film_relation)>0){
                    foreach ($tag_film_relation as $value){
                        $value->delete();
                    }
                }
                $cast_film_relation = ActorRelation::where('film_id',$id)->get();
                if(count($cast_film_relation)>0){
                    foreach ($cast_film_relation as $value){
                        $value->delete();
                    }
                }
                $director_film_relation = DirectorRelation::where('film_id',$id)->get();
                if(count($director_film_relation)>0){
                    foreach ($director_film_relation as $value){
                        $value->delete();
                    }
                }
                $category_film_relation = CategoryFilmRelation::where('film_id',$id)->get();
                if(count($category_film_relation)>0){
                    foreach ($category_film_relation as $value){
                        $value->delete();
                    }
                }
                if($item->movie_series == 1){
                    $movie_series = FilmDetail::where('film_id',$item->id)->get();
                    if(count($movie_series)>0){
                        foreach ($movie_series as $value){
                            $value->delete();
                        }
                    }
                }
                self::find($item->id)->delete();
            }
        }
    }

    public static function RecursiveMenu($id = 0, $parent = 0, $check = 0, $arr = "")
    {
        $html = "";
        $query = CategoryProducts::where('parent_id',$parent)->get();
        if(!empty($query)){
            foreach ($query as $item){
                $checked = "";
                if($check > 0){
                    foreach ($check as $value){
                        if($value['category_id'] == $item->id){
                            $checked = "selected";
                        }
                    }
                }
                $html .=  "<option value=\"".$item->id."\" ".$checked.">".$arr.$item->name."</option>";
                $html .= self::RecursiveMenu($id, $item->id, $check, $arr."&rarr;");
            }
        }
        return $html;
    }

    /**
     * @param string $rarr
     * @return string
     *
     */
    public static function RecursiveIndexAdmin($key, $sort, $keyword, $status)
    {
        $html = "";
        switch ($status){
            case 'publish':
                $status_where = array('films.publish'=>1);
                break;
            case 'hidden':
                $status_where = array('films.publish'=>0);
                break;
            case 'featured':
                $status_where = array('films.featured'=>1);
                break;
            case 'normal':
                $status_where = array('films.featured'=>0);
                break;
            default:
                $status_where = null;
        }

        if($status_where) {
            if ($keyword) {
                $user = Users::where('name',$keyword)->first();
                if($user){
                    $user_id = $user->id;
                    if ($key == 'movies') {
                        $query = self::where('movie_series', 0)
                            ->where('name', 'like', "%$keyword%")
                            ->where($status_where)
                            ->orWhere('user_id',$user_id)
                            ->with('user')
                            ->orderby('created_at', $sort)
                            ->paginate(10);
                    } else if ($key == 'tv-series') {
                        $query = self::where('movie_series', 1)
                            ->where('name', 'like', "%$keyword%")
                            ->where($status_where)
                            ->orWhere('user_id',$user_id)
                            ->with('user')
                            ->orderby('created_at', $sort)
                            ->paginate(10);
                    } else if ($key == 'all') {
                        $query = self::where('name', 'like', "%$keyword%")
                            ->where($status_where)
                            ->orWhere('user_id',$user_id)
                            ->with('user')
                            ->orderby('created_at', $sort)
                            ->paginate(10);
                    } else if ($key) {
                        $type = TypeFilm::where('slug', $key)->first();
                        $query = self::join('type_film_relations', 'type_film_relations.film_id', '=', 'films.id')
                            ->where('type_film_relations.type_film_id', $type->id)
                            ->where($status_where)
                            ->where('films.name', 'like', "%$keyword%")
                            ->orWhere('films.user_id',$user_id)
                            ->select('films.*')
                            ->with('user')
                            ->orderby('films.created_at', $sort)
                            ->paginate(10);
                    } else {
                        $query = self::where('name', 'like', "%$keyword%")
                            ->where($status_where)
                            ->orWhere('user_id',$user_id)
                            ->with('user')
                            ->orderby('created_at', $sort)
                            ->paginate(10);
                    }
                }else{
                    if ($key == 'movies') {
                        $query = self::where('movie_series', 0)
                            ->where('name', 'like', "%$keyword%")
                            ->where($status_where)
                            ->with('user')
                            ->orderby('created_at', $sort)
                            ->paginate(10);
                    } else if ($key == 'tv-series') {
                        $query = self::where('movie_series', 1)
                            ->where('name', 'like', "%$keyword%")
                            ->where($status_where)
                            ->with('user')
                            ->orderby('created_at', $sort)
                            ->paginate(10);
                    } else if ($key == 'all') {
                        $query = self::where('name', 'like', "%$keyword%")
                            ->where($status_where)
                            ->with('user')
                            ->orderby('created_at', $sort)
                            ->paginate(10);
                    } else if ($key) {
                        $type = TypeFilm::where('slug', $key)->first();
                        $query = self::join('type_film_relations', 'type_film_relations.film_id', '=', 'films.id')
                            ->where('type_film_relations.type_film_id', $type->id)
                            ->where($status_where)
                            ->where('films.name', 'like', "%$keyword%")
                            ->select('films.*')
                            ->with('user')
                            ->orderby('films.created_at', $sort)
                            ->paginate(10);
                    } else {
                        $query = self::where('name', 'like', "%$keyword%")
                            ->where($status_where)
                            ->with('user')
                            ->orderby('created_at', $sort)
                            ->paginate(10);
                    }
                }


            } else {
                if ($key == 'movies') {
                    $query = self::where('movie_series', 0)
                        ->where($status_where)
                        ->with('user')
                        ->orderby('created_at', $sort)
                        ->paginate(10);
                } else if ($key == 'tv-series') {
                    $query = self::where('movie_series', 1)
                        ->where($status_where)
                        ->with('user')
                        ->orderby('created_at', $sort)
                        ->paginate(10);
                } else if ($key == 'all') {
                    $query = self::where($status_where)
                        ->with('user')
                        ->orderby('created_at', $sort)
                        ->paginate(10);
                } else if ($key) {
                    $type = TypeFilm::where('slug', $key)->first();
                    $query = self::join('type_film_relations', 'type_film_relations.film_id', '=', 'films.id')
                        ->where('type_film_relations.type_film_id', $type->id)
                        ->where($status_where)
                        ->select('films.*')
                        ->with('user')
                        ->orderby('films.created_at', $sort)
                        ->paginate(10);
                } else {
                    $query = self::where($status_where)
                        ->with('user')
                        ->orderby('created_at', $sort)
                        ->paginate(10);
                }
            }
        }else{
            if ($keyword) {
                $user = Users::where('name',$keyword)->first();
                if($user) {
                    $user_id = $user->id;
                    if ($key == 'movies') {
                        $query = self::where('movie_series', 0)
                            ->where('name', 'like', "%$keyword%")
                            ->orWhere('user_id',$user_id)
                            ->with('user')
                            ->orderby('created_at', $sort)
                            ->paginate(10);
                    } else if ($key == 'tv-series') {
                        $query = self::where('movie_series', 1)
                            ->where('name', 'like', "%$keyword%")
                            ->orWhere('user_id',$user_id)
                            ->with('user')
                            ->orderby('created_at', $sort)
                            ->paginate(10);
                    } else if ($key == 'all') {
                        $query = self::where('name', 'like', "%$keyword%")
                            ->orWhere('user_id',$user_id)
                            ->with('user')
                            ->orderby('created_at', $sort)
                            ->paginate(10);
                    } else if ($key) {
                        $type = TypeFilm::where('slug', $key)->first();
                        $query = self::join('type_film_relations', 'type_film_relations.film_id', '=', 'films.id')
                            ->where('type_film_relations.type_film_id', $type->id)
                            ->where('films.name', 'like', "%$keyword%")
                            ->orWhere('films.user_id',$user_id)
                            ->select('films.*')
                            ->with('user')
                            ->orderby('films.created_at', $sort)
                            ->paginate(10);
                    } else {
                        $query = self::where('name', 'like', "%$keyword%")
                            ->orWhere('user_id',$user_id)
                            ->with('user')
                            ->orderby('created_at', $sort)
                            ->paginate(10);
                    }
                }else{
                    if ($key == 'movies') {
                        $query = self::where('movie_series', 0)
                            ->where('name', 'like', "%$keyword%")
                            ->with('user')
                            ->orderby('created_at', $sort)
                            ->paginate(10);
                    } else if ($key == 'tv-series') {
                        $query = self::where('movie_series', 1)
                            ->where('name', 'like', "%$keyword%")
                            ->with('user')
                            ->orderby('created_at', $sort)
                            ->paginate(10);
                    } else if ($key == 'all') {
                        $query = self::where('name', 'like', "%$keyword%")
                            ->with('user')
                            ->orderby('created_at', $sort)
                            ->paginate(10);
                    } else if ($key) {
                        $type = TypeFilm::where('slug', $key)->first();
                        $query = self::join('type_film_relations', 'type_film_relations.film_id', '=', 'films.id')
                            ->where('type_film_relations.type_film_id', $type->id)
                            ->where('films.name', 'like', "%$keyword%")
                            ->select('films.*')
                            ->with('user')
                            ->orderby('films.created_at', $sort)
                            ->paginate(10);
                    } else {
                        $query = self::where('name', 'like', "%$keyword%")->with('user')->orderby('created_at', $sort)->paginate(10);
                    }
                }
            } else {
                if ($key == 'movies') {
                    $query = self::where('movie_series', 0)->with('user')->orderby('created_at', $sort)->paginate(10);
                } else if ($key == 'tv-series') {
                    $query = self::where('movie_series', 1)->with('user')->orderby('created_at', $sort)->paginate(10);
                } else if ($key == 'all') {
                    $query = self::with('user')->orderby('created_at', $sort)->paginate(10);
                } else if ($key) {
                    $type = TypeFilm::where('slug', $key)->first();
                    $query = self::join('type_film_relations', 'type_film_relations.film_id', '=', 'films.id')
                        ->where('type_film_relations.type_film_id', $type->id)
                        ->select('films.*')
                        ->with('user')
                        ->orderby('films.created_at', $sort)
                        ->paginate(10);
                } else {
                    $query = self::with('user')->orderby('created_at', $sort)->paginate(10);
                }
            }
        }

        if($query != null){
            foreach ($query as $item){
                $status_item = ($item->publish == 1)?"publish":"hidden";
                $featured = ($item->featured == 1)?"featured":"normal";
                $class = ($item->publish == 1)?"label-success":"label-danger";
                $class_featured = ($item->featured == 1)?"label-success":"label-danger";
                if($item->images != ''){
                    $images = "<a href=\"".url(getImage($item->images))."\" data-toggle='lightbox-image'>
                            <img src=\"".url(getImage($item->images))."\" class='img-responsive center-block' style='max-width: 40px;'>
                          </a>";
                }elseif ($item->images_link != '') {
                    $images = "<a href=\"".$item->images_link."\" data-toggle='lightbox-image'>
                            <img src=\"".$item->images_link."\" class='img-responsive center-block' style='max-width: 40px;'>
                          </a>";
                }else{
                    $images = '';
                }
                $html .= "<tr>";
                $html .= "<td class=\"text-center\">";
                $html .= "<input type=\"checkbox\" id=\"checkbox-".$item->id ."\" name=\"id[]\" value=\"".$item->id."\">";
                $html .= "</td>";
                $html .= "<td><a href=\"".url('admin/film/edit/'.$item->id)."\">".$item->name."</a></td>";
                $html .= "<td>".$images."</td>";
                $html .= "<td>".$item->user->name."</td>";
                $html .= "<td>
                            <span class=\"label ".$class." check_status\" data-type='publish' data-url=\"".url('admin/film/check-status/publish/'.$item->id)."\">".$status_item."</span>
                            <span class=\"label ".$class_featured." check_status\" data-type='publish' data-url=\"".url('admin/film/check-status/featured/'.$item->id)."\">".$featured."</span>
                         </td>";
                $html .= "<td>".convertDateTime($item->created_at)."</td>";
                $html .= "<td class=\"text-center\">";
                $html .= "<div class=\"btn-group btn-group-xs\">";
                if($item->movie_series==1){
                    $html .= "<a href=\"".url('admin/film-series/add-episode/'.$item->id)."\" data-toggle=\"tooltip\" title=\"Add Episode\" class=\"btn btn-success\"><i class=\"fa fa-plus\"></i></a>";
                    $html .= "<a href=\"".url('admin/film-series/list-episode/'.$item->id)."\" data-toggle=\"tooltip\" title=\"List Episode\" class=\"btn btn-primary\"><i class=\"fa fa-list\"></i></a>";
                    $html .= "<a href=\"".url('admin/film-series/multi-link/'.$item->id)."\" data-toggle=\"tooltip\" title=\"Multi Link\" class=\"btn btn-warning\"><i class=\"fa fa-link\"></i></a>";
                    $html .= "<a href=\"".url('admin/subtitle-detail/multi/'.$item->id)."\" data-toggle=\"tooltip\" title=\"Multi Subtitle\" class=\"btn btn-primary\"><i class=\"fa fa-external-link\"></i></a>";
                }else{
                    $html .= "<a href=\"".url('admin/film/multi-link/'.$item->id)."\" data-toggle=\"tooltip\" title=\"Multi Link\" class=\"btn btn-warning\"><i class=\"fa fa-link\"></i></a>";
                    $html .= "<a href=\"".url('admin/subtitle/multi/'.$item->id)."\" data-toggle=\"tooltip\" title=\"Multi Subtitle\" class=\"btn btn-primary\"><i class=\"fa fa-external-link\"></i></a>";
                }
                $html .= "<a href=\"".url('admin/film/edit/'.$item->id)."\" data-toggle=\"tooltip\" title=\"Edit\" class=\"btn btn-default\"><i class=\"fa fa-pencil\"></i></a>";
                $html .= HtmlDeleteRecord(url('admin/film/delete/'.$item->id));
                $html .= "</div>";
                $html .= "</td>";
                $html .= "</tr>";
            }

        }
        $data = array(
            'html'      => $html,
            'paginate'  => $query->appends(['type' => $key, 'sort'=>$sort, 'status' => $status, 'keyword' => $keyword])->render()
        );
        return $data;
    }

    /**
     * @param int $id
     * @param int $parent
     * @param int $check
     * @return string
     */
    public static function RecursiveTag($id = 0, $parent = 0, $check = 0, $arr = "")
    {
        $html = "";
        $query = TagsProduct::where('parent_id',$parent)->get();
        if(!empty($query)){
            foreach ($query as $item){
                $checked = "";
                if($check > 0){
                    foreach ($check as $value){
                        if($value['tags_id'] == $item->id){
                            $checked = "selected";
                        }
                    }
                }
                $html .=  "<option value=\"".$item->id."\" ".$checked.">".$arr. "&nbsp;" .$item->name."</option>";
                $html .= self::RecursiveTag($id, $item->id, $check, $arr."&rarr;");
            }
        }
        return $html;
    }

    /**
     * @param int $id
     * @param int $parent
     * @param int $check
     * @return string
     */
    public static function RecursiveSize($id = 0, $parent = 0, $check = 0, $arr = "")
    {
        $html = "";
        $query = SizeProduct::where('parent_id',$parent)->get();
        if(!empty($query)){
            foreach ($query as $item){
                $checked = "";
                if($check > 0){
                    foreach ($check as $value){
                        if($value['size_id'] == $item->id){
                            $checked = "selected";
                        }
                    }
                }
                $html .=  "<option value=\"".$item->id."\" ".$checked.">".$arr. "&nbsp;" .$item->name."</option>";
                $html .= self::RecursiveSize($id, $item->id, $check, $arr."&rarr;");
            }
        }
        return $html;
    }

    /**
     * @param int $id
     * @param int $parent
     * @param int $check
     * @return string
     */
    public static function RecursiveColor($id = 0, $parent = 0, $check = 0, $arr = "")
    {
        $html = "";
        $query = ColorProduct::where('parent_id',$parent)->get();
        if(!empty($query)){
            foreach ($query as $item){
                $checked = "";
                if($check > 0){
                    foreach ($check as $value){
                        if($value['color_id'] == $item->id){
                            $checked = "selected";
                        }
                    }
                }
                $html .=  "<option value=\"".$item->id."\" ".$checked.">".$arr.$item->name."</option>";
                $html .= self::RecursiveColor($id, $item->id, $check, $arr."&rarr;");
            }
        }
        return $html;
    }

    public static function getSliderHome(){
        $html = "";
        $query = self::where('slider',1)->with('user')->orderby('created_at','DESC')->get();

        if($query != null){
            foreach ($query as $item){
                $status = ($item->publish == 1)?"publish":"hidden";
                $featured = ($item->featured == 1)?"featured":"normal";
                $class = ($item->publish == 1)?"label-success":"label-danger";
                $class_featured = ($item->featured == 1)?"label-success":"label-danger";
                if($item->images != ''){
                    $images = "<a href=\"".url(getImage($item->images))."\" data-toggle='lightbox-image'>
                            <img src=\"".url(getImage($item->images))."\" class='img-responsive center-block' style='max-width: 40px;'>
                          </a>";
                }elseif ($item->images_link != '') {
                    $images = "<a href=\"".$item->images_link."\" data-toggle='lightbox-image'>
                            <img src=\"".$item->images_link."\" class='img-responsive center-block' style='max-width: 40px;'>
                          </a>";
                }else{
                    $images = '';
                }
                $html .= "<tr>";
                $html .= "<td class=\"text-center\">";
                $html .= "<input type=\"checkbox\" id=\"checkbox-".$item->id ."\" name=\"id[]\" value=\"".$item->id."\">";
                $html .= "</td>";
                $html .= "<td><a href=\"".url('admin/film/edit/'.$item->id)."\">".$item->name."</a></td>";
                $html .= "<td>".$images."</td>";
                $html .= "<td>".$item->user->name."</td>";
                $html .= "<td>
                            <span class=\"label ".$class." check_status\" data-type='publish' data-url=\"".url('admin/film/check-status/publish/'.$item->id)."\">".$status."</span>
                            <span class=\"label ".$class_featured." check_status\" data-type='publish' data-url=\"".url('admin/film/check-status/featured/'.$item->id)."\">".$featured."</span>
                         </td>";
                $html .= "<td>".convertDateTime($item->created_at)."</td>";
                $html .= "<td class=\"text-center\">";
                $html .= "<div class=\"btn-group btn-group-xs\">";
                if($item->movie_series==1){
                    $html .= "<a href=\"".url('admin/film-series/add-episode/'.$item->id)."\" data-toggle=\"tooltip\" title=\"Add Episode\" class=\"btn btn-success\"><i class=\"fa fa-plus\"></i></a>";
                    $html .= "<a href=\"".url('admin/film-series/list-episode/'.$item->id)."\" data-toggle=\"tooltip\" title=\"List Episode\" class=\"btn btn-primary\"><i class=\"fa fa-list\"></i></a>";
                    $html .= "<a href=\"".url('admin/film-series/multi-link/'.$item->id)."\" data-toggle=\"tooltip\" title=\"Multi Link\" class=\"btn btn-warning\"><i class=\"fa fa-link\"></i></a>";
                }
                $html .= "<a href=\"".url('admin/film/edit/'.$item->id)."\" data-toggle=\"tooltip\" title=\"Edit\" class=\"btn btn-default\"><i class=\"fa fa-pencil\"></i></a>";
                $html .= HtmlDeleteRecord(url('admin/film/delete/'.$item->id));
                $html .= "</div>";
                $html .= "</td>";
                $html .= "</tr>";
            }

        }
        return $html;
    }
}
