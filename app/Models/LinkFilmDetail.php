<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LinkFilmDetail extends Model
{
    protected $hidden = [
        'created_at', 'email', 'type', 'updated_at'
    ];

    public function server()
    {
        return $this->belongsTo('App\Models\ServerPlayFilm', 'server_play');
    }


    public function film(){
        return $this->belongsTo('App\Models\FilmDetail', 'film_detail_id');
    }

    static public function DeleteById($id){
        $check = self::find($id);
        dd($check);
        if($check != null) {
            self::find($id)->delete();
        }
    }
}
