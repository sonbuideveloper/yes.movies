<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DirectorRelation extends Model
{
    protected $table = "director_relations";

    public function director()
    {
        return $this->belongsTo('App\Models\Director', 'director_id');
    }

    public function films()
    {
        return $this->belongsTo('App\Models\Film', 'film_id');
    }

    public static function DeleteNotInArray($film_id, $array_category)
    {
        $array_actor_id = array();
        foreach ($array_category as $item){
            $check_actor = Director::where('name',$item)->first();
            if($check_actor){
                $array_actor_id[] = $check_actor->id;
            }
        }
        return self::where('film_id', $film_id)->whereNotIn('director_id', $array_actor_id)->delete();
    }
}
