<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LinkFilm extends Model
{
	protected $table = "link_films";

    protected $hidden = [
        'created_at', 'type', 'updated_at'
    ];
    
    public function server()
    {
        return $this->belongsTo('App\Models\ServerPlayFilm', 'server_play');
    }

    public function film(){
        return $this->belongsTo('App\Models\Film', 'film_id');
    }

    static public function DeleteById($id){
        $check = self::find($id);
        if($check != null) {
            self::find($id)->delete();
        }
    }
}
