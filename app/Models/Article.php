<?php

namespace Models;

use App\Models\CategoryProductRelation;
use App\Models\ColorProduct;
use Illuminate\Database\Eloquent\Model;
use Auth;
class Article extends Model
{
    //
    protected $table = "articles";

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function metaData()
    {
        return $this->hasMany('Models\Contents','article_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mediaData()
    {
        return $this->hasMany('Models\Media','article_id','id');
    }


    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|Model|null|static|static[]
     */
    public static function getById($id)
    {
        return self::find($id);
    }
    /**
     * @param $id
     * Delete only one row
     */
    public static function DeleteById($id)
    {
        $check = self::find($id);
        if($check != null) {
            deleteImage($check->images);
            self::find($check->id)->delete();
        }
    }

    /**
     * @param $where
     * Delete multiple rows
     */
    public static function DeleteMultiple($where)
    {
        $check = self::where($where)->get();
        if(count($check) > 0)
        {
            foreach ($check as $item)
            {
                Contents::DeleteData(['type' => 'article', 'article_id' => $item->id]);
                Media::DeleteMultiple(['type' => 'article', 'article_id' => $item->id]);
                if ($item->images != null)
                {
                    deleteImage($item->images);
                    self::find($item->id)->delete();
                }
            }
        }
    }

    public static function RecursiveMenu($id = 0, $parent = 0, $check = 0)
    {
        $html = "";
        $query = Menus::where('parent_id',$parent)->get();
        if(!empty($query)){
            $html .=  "<ul class=\"list-unstyled\">";
            foreach ($query as $item){
                $checked = "";
                if($check == $item->id){
                    $checked = "checked";
                }
                $html .=  "<li>";
                $html .=  "<label><input type=\"radio\" name=\"parent_id\" value=\"".$item->id."\" ".$checked.">".$item->name."</label>";
                $html .= self::RecursiveMenu($id, $item->id, $check);
                $html .=  "</li>";

            }
            $html .=  "</ul>";
        }
        return $html;
    }

    public static function RecursiveCategory($id = 0, $parent = 0, $check = 0, $query)
    {
        $html = "";

        if(!empty($query)){
            $html .=  '<select name="parent_id" class="select-chosen" data-placeholder="Please choose type..." style="width: 250px;">';
            $html .=  '<option></option>';
            foreach ($query as $item){
                $checked = "";
                if($check == $item->id){
                    $checked = "selected";
                }
//                $html .=  "<ul class=\"list-unstyled\">";
//                $html .=  "<li>";
//                $html .=  "<label><input type=\"radio\" name=\"parent_id\" value=\"".$item->id."\" ".$checked.">".$item->name."</label>";
//                $html .=  "</li>";


                $html .=  "<option value=\"".$item->id."\" ".$checked.">".$item->name."</option>";

            }
            $html .=  '</select>';
        }
        return $html;
    }

    public static function RecursiveIndexAdmin($type,$rarr = "")
    {
        $html = "";
        if(Auth::user()->type == 1){
            $query = self::where('type',$type)->orderby('created_at','desc')->get();
        }else{
            $query = self::where('type',$type)->where('user_id',Auth::user()->id)->orderby('created_at','desc')->get();
        }
        
        if($query != null){
            foreach ($query as $item) {
                switch ($item->parent_id) {
                    case 1:
                        $parent_name = 'Film';
                        $category = Products::where('id', $item->category_id)->first();
                        break;
                    case 2:
                        $parent_name = 'Cast';
                        $category = CategoryProducts::where('id', $item->category_id)->first();
                        break;
                    case 3:
                        $parent_name = 'Director';
                        $category = ColorProduct::where('id', $item->category_id)->first();
                        break;
                    default:
                        $parent_name = 'None';
                        break;
                }

                if (isset($category)) {
                    $category_name = $category->name;
                } else {
                    $category_name = '';
                }
                if($item->user_id){
                    $user_by = Users::where('id',$item->user_id)->first();
                    if($user_by){
                        $by = $user_by->name;
                    }else{
                        $by = 'None';
                    }
                }else{
                    $by = '';
                }
            
                $status = ($item->publish == 1) ? "publish" : "hidden";
                $featured = ($item->featured == 1) ? "featured" : "normal";
                $class = ($item->publish == 1) ? "label-success" : "label-danger";
                $class_featured = ($item->featured == 1) ? "label-success" : "label-danger";
                $html .= "<tr>";
                $html .= "<td class=\"text-center\">";
                $html .= "<input type=\"checkbox\" id=\"checkbox-" . $item->id . "\" name=\"id[]\" value=\"" . $item->id . "\">";
                $html .= "</td>";
                $html .= "<td><a href=\"" . url('admin/' . $type . '/edit/' . $item->id) . "\">" . $rarr . " " . $item->name . "</a></td>";
                if(Auth::user()->type == 1){
                    $html .= "<td><span class=\"label ".$class." check_status\" data-type='publish' data-url=\"".url('admin/'.$type.'/check-status/publish/'.$item->id)."\">".$status."</span></td>";
                }else{
                    $html .= "<td><span class=\"label ".$class." check_status\" data-type='publish'>".$status."</span></td>";
                }
//                $html .= "<td><input class='form-control update_order' data-url=\"".url('admin/'.$type.'/update-order/'.$item->id)."\" value='".$item->order_by."'></td>";
                $html .= "<td class=\"text-center\">";
                $html .= "<div class=\"btn-group btn-group-xs\">";
                $html .= "<a href=\"".url('admin/'.$type.'/edit/'.$item->id)."\" data-toggle=\"tooltip\" title=\"Edit\" class=\"btn btn-default\"><i class=\"fa fa-pencil\"></i></a>";
                $html .= HtmlDeleteRecord(url('admin/'.$type.'/delete/'.$item->id));
                $html .= "</div>";
                $html .= "</td>";
                $html .= "</tr>";
            }

        }
        return $html;
    }

    //*******************Demon Dragon******************//
    public static function getArticleFeature($category_id){
        return self::whereIn('category_id',$category_id)->where('publish',1)->where('featured', 1)->orderBy('order_by')->with('mediaData')->with('categories')->get();
    }

    public static function getArticleRelated($category_id, $id){
        return self::whereIn('category_id',$category_id)->where('id','<>',$id)->where('publish',1)->orderBy('order_by')->get();
    }
}
