<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    //
    protected $table = "setting";
    
    public static function getSetting(){
        return self::first();
    }
}
