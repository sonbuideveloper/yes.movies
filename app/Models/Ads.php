<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Models\Users;

class Ads extends Model
{
    protected $table = "ads";

    public static function getById($id)
    {
        return self::find($id);
    }

    public static function RecursiveIndexAdmin($type,$rarr = "")
    {
        $html = "";
        if(Auth::user()->type == 1){
            $query = self::where('type',$type)->orderby('created_at','desc')->get();
        }else{
            $query = self::where('type',$type)->where('user_id',Auth::user()->id)->orderby('created_at','desc')->get();
        }

        if($query != null){
            foreach ($query as $item) {
                if($item->user_id){
                    $user_by = Users::where('id',$item->user_id)->first();
                    if($user_by){
                        $by = $user_by->name;
                    }else{
                        $by = 'None';
                    }
                }else{
                    $by = '';
                }

                $status = ($item->publish == 1) ? "publish" : "hidden";
                $featured = ($item->featured == 1) ? "featured" : "normal";
                $class = ($item->publish == 1) ? "label-success" : "label-danger";
                $class_featured = ($item->featured == 1) ? "label-success" : "label-danger";
                $html .= "<tr>";
                $html .= "<td><a href=\"" . url('admin/' . $type . '/edit/' . $item->id) . "\">" . $rarr . " " . $item->name . "</a></td>";
                if(Auth::user()->type == 1){
                    $html .= "<td><span class=\"label ".$class." check_status\" data-type='publish' data-url=\"".url('admin/'.$type.'/check-status/publish/'.$item->id)."\">".$status."</span></td>";
                }else{
                    $html .= "<td><span class=\"label ".$class." check_status\" data-type='publish'>".$status."</span></td>";
                }
                $html .= "<td class=\"text-center\">";
                $html .= "<div class=\"btn-group btn-group-xs\">";
                $html .= "<a href=\"".url('admin/'.$type.'/edit/'.$item->id)."\" data-toggle=\"tooltip\" title=\"Edit\" class=\"btn btn-default\"><i class=\"fa fa-pencil\"></i></a>";
                $html .= "</div>";
                $html .= "</td>";
                $html .= "</tr>";
            }

        }
        return $html;
    }
}
