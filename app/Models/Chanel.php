<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chanel extends Model
{
    protected $table = "chanels";

    public static function getById($id)
    {
        return self::find($id);
    }

    public function tagFilm()
    {
        return $this->hasMany('App\Models\TagFilmRelation', 'film_id');
    }

    public static function DeleteById($id)
    {
        $tag_film_relation = TagFilmRelation::where('type','chanel')->where('film_id',$id)->get();
        if(count($tag_film_relation)>0){
            foreach ($tag_film_relation as $item){
                $item->delete();
            }
        }
        $check = self::find($id);
        if($check->images){
            deleteImage($check->images);
        }
        if($check->cover){
            deleteImage($check->cover);
        }
        if($check->background){
            deleteImage($check->background);
        }
        return self::find($id)->delete();
    }

    public static function RecursiveIndexAdmin($type, $rarr = "")
    {
        $html = "";
        $query = self::where('type', $type)->orderby('name')->get();
        if($query != null){
            foreach ($query as $item){
                $status = ($item->publish == 1)?"publish":"hidden";
                $class = ($item->publish == 1)?"label-success":"label-danger";
                if($item->images != ''){
                    $images = "<a href=\"".url(getImage($item->images))."\" data-toggle='lightbox-image'>
                            <img src=\"".url(getImage($item->images))."\" class='img-responsive center-block' style='max-width: 40px;'>
                          </a>";
                }elseif ($item->images_link != '') {
                    $images = "<a href=\"".$item->images_link."\" data-toggle='lightbox-image'>
                            <img src=\"".$item->images_link."\" class='img-responsive center-block' style='max-width: 40px;'>
                          </a>";
                }else{
                    $images = '';
                }

                $tag_film = TagFilmRelation::where('film_id',$item->id)->where('type', 'chanel')->first();
                if($tag_film){
                    $film = TagFilm::where('id',$tag_film->tag_id)->first();
                }

                $html .= "<tr>";
                $html .= "<td class=\"text-center\">";
                $html .= "<input type=\"checkbox\" id=\"checkbox-".$item->id ."\" name=\"id[]\" value=\"".$item->id."\">";
                $html .= "</td>";
                $html .= "<td><a href=\"".url('admin/chanel/edit/'.$item->id)."\">".$rarr." ".$item->name."</a></td>";
                $html .= "<td>".$images."</td>";
                $html .= "<td>".(isset($film)?$film->name:'')."</td>";
                $html .= "<td><span class=\"label ".$class." check_status\" data-url=\"".url('admin/chanel/check-status/'.$item->id)."\">".$status."</span></td>";
                $html .= "<td class=\"text-center\">";
                $html .= "<div class=\"btn-group btn-group-xs\">";
                $html .= "<a href=\"".url('admin/chanel/edit/'.$item->id)."\" data-toggle=\"tooltip\" title=\"Edit\" class=\"btn btn-default\"><i class=\"fa fa-pencil\"></i></a>";
                $html .= HtmlDeleteRecord(url('admin/chanel/delete/'.$item->id));
                $html .= "</div>";
                $html .= "</td>";
                $html .= "</tr>";
            }

        }
        return $html;
    }
}
