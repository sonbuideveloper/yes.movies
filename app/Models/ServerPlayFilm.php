<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServerPlayFilm extends Model
{
    protected $table = "server_play_films";

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public static function getById($id)
    {
        return self::find($id);
    }

    public static function DeleteById($id)
    {
        $link_film = LinkFilm::where('server_play',$id)->get();
        if(count($link_film)>0){
            foreach ($link_film as $item){
                $item->delete();
            }
        }
        return self::find($id)->delete();
    }

    public static function RecursiveIndexAdmin($rarr = "")
    {
        $html = "";
        $query = self::orderby('name')->get();
        if($query != null){
            foreach ($query as $item){
                $status = ($item->publish == 1)?"publish":"hidden";
                $class = ($item->publish == 1)?"label-success":"label-danger";
                $html .= "<tr>";
                $html .= "<td class=\"text-center\">";
                $html .= "<input type=\"checkbox\" id=\"checkbox-".$item->id ."\" name=\"id[]\" value=\"".$item->id."\">";
                $html .= "</td>";
                $html .= "<td><a href=\"".url('admin/server-film/edit/'.$item->id)."\">".$rarr." ".$item->name."</a></td>";
                $html .= "<td>".($item->embedded==1?'Embed':'API')."</td>";
                $html .= "<td><span class=\"label ".$class." check_status\" data-url=\"".url('admin/server-film/check-status/'.$item->id)."\">".$status."</span></td>";
                $html .= "<td class=\"text-center\">";
                $html .= "<div class=\"btn-group btn-group-xs\">";
                $html .= "<a href=\"".url('admin/server-film/edit/'.$item->id)."\" data-toggle=\"tooltip\" title=\"Edit\" class=\"btn btn-default\"><i class=\"fa fa-pencil\"></i></a>";
                $html .= HtmlDeleteRecord(url('admin/server-film/delete/'.$item->id));
                $html .= "</div>";
                $html .= "</td>";
                $html .= "</tr>";
            }

        }
        return $html;
    }
}
