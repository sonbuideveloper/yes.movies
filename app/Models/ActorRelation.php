<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActorRelation extends Model
{
    protected $table = "actor_relations";

    public function actor()
    {
        return $this->belongsTo('App\Models\Actor', 'actor_id');
    }

    public function films()
    {
        return $this->belongsTo('App\Models\Film', 'film_id');
    }

    public static function DeleteNotInArray($film_id, $array_category)
    {
        $array_actor_id = array();
        foreach ($array_category as $item){
            $check_actor = Actor::where('name',$item)->first();
            if($check_actor){
                $array_actor_id[] = $check_actor->id;
            }
        }
        return self::where('film_id', $film_id)->whereNotIn('actor_id', $array_actor_id)->delete();
    }
}
