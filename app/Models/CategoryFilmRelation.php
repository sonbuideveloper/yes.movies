<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryFilmRelation extends Model
{
    protected $table = "category_film_relations";

    public static function DeleteNotInArray($film_id, $array_category)
    {
        return self::where('film_id', $film_id)->whereNotIn('category_film_id', $array_category)->delete();
    }

    public function films(){
        return $this->belongsTo('App\Models\Film', 'film_id');
    }
}
