<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\TypeFilmRelation;

class TypeFilm extends Model
{
    protected $table = "type_films";

    public static function getById($id)
    {
        return self::find($id);
    }

    public static function DeleteById($id)
    {
        $type_film_relation = TypeFilmRelation::where('type_film_id',$id)->get();
        if(count($type_film_relation)>0){
            foreach ($type_film_relation as $item){
                $item->delete();
            }
        }
        return self::find($id)->delete();
    }

    public static function RecursiveIndexAdmin($rarr = "")
    {
        $html = "";
        $query = self::orderby('name')->get();
        if($query != null){
            foreach ($query as $item){
                $status = ($item->publish == 1)?"publish":"hidden";
                $class = ($item->publish == 1)?"label-success":"label-danger";
                $html .= "<tr>";
                $html .= "<td class=\"text-center\">";
                $html .= "<input type=\"checkbox\" id=\"checkbox-".$item->id ."\" name=\"id[]\" value=\"".$item->id."\">";
                $html .= "</td>";
                $html .= "<td><a href=\"".url('admin/type/edit/'.$item->id)."\">".$rarr." ".$item->name."</a></td>";
                $html .= "<td><span class=\"label ".$class." check_status\" data-url=\"".url('admin/type/check-status/'.$item->id)."\">".$status."</span></td>";
                $html .= "<td class=\"text-center\">";
                $html .= "<div class=\"btn-group btn-group-xs\">";
                $html .= "<a href=\"".url('admin/type/edit/'.$item->id)."\" data-toggle=\"tooltip\" title=\"Edit\" class=\"btn btn-default\"><i class=\"fa fa-pencil\"></i></a>";
                $html .= HtmlDeleteRecord(url('admin/type/delete/'.$item->id));
                $html .= "</div>";
                $html .= "</td>";
                $html .= "</tr>";
            }

        }
        return $html;
    }
}
