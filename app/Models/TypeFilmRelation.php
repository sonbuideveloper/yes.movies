<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeFilmRelation extends Model
{
    protected $table = "type_film_relations";

    public static function DeleteNotInArray($film_id, $array_category)
    {
        return self::where('film_id', $film_id)->whereNotIn('type_film_id', $array_category)->delete();
    }
}
