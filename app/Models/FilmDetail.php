<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FilmDetail extends Model
{
    protected $table = "film_details";

    public function mediaData()
    {
        return $this->hasMany('Models\Media','film_id','id');
    }

    public function film()
    {
        return $this->belongsTo('App\Models\Film','film_id');
    }

    public function rate(){
        return $this->hasMany('App\Models\Rate', 'film_id');
    }

    public function link_play()
    {
        return $this->hasMany('App\Models\LinkFilmDetail', 'film_detail_id');
    }

    public function subtitle()
    {
        return $this->hasMany('App\Models\SubtitleFilmDetail', 'film_id');
    }

    public static function getById($id)
    {
        return self::with('mediaData')->with('link_play')->find($id);
    }
    /**
     * @param $id
     * Delete only one row
     */
    public static function DeleteById($id)
    {
        $check = self::find($id);
        if($check != null) {
            $tag_film_relation = TagFilmRelation::where('type','tag_film')->where('film_id',$id)->get();
            if(count($tag_film_relation)>0){
                foreach ($tag_film_relation as $item){
                    $item->delete();
                }
            }
            $cast_film_relation = ActorRelation::where('film_id',$id)->get();
            if(count($cast_film_relation)>0){
                foreach ($cast_film_relation as $item){
                    $item->delete();
                }
            }
            $director_film_relation = DirectorRelation::where('film_id',$id)->get();
            if(count($director_film_relation)>0){
                foreach ($director_film_relation as $item){
                    $item->delete();
                }
            }
            $link_film_detail = LinkFilmDetail::where('film_detail_id',$id)->get();
            if(count($link_film_detail)>0){
                foreach ($link_film_detail as $item){
                    $item->delete();
                }
            }
            $subtitle_film_detail = SubtitleFilmDetail::where('film_id',$id)->get();
            if(count($subtitle_film_detail)>0){
                foreach ($subtitle_film_detail as $item){
                    $item->delete();
                }
            }
            self::find($check->id)->delete();
        }
    }

    /**
     * @param $where
     * Delete multiple rows
     */
    public static function DeleteMultiple($where)
    {
        $check = self::where($where)->get();
        if(count($check) > 0)
        {
            foreach ($check as $item)
            {
                Contents::DeleteData(['type' => 'product', 'product_id' => $item->id]);
                Media::DeleteMultiple(['type' => 'product', 'product_id' => $item->id]);
                if ($item->images != null)
                {
                    deleteImage($item->images);
                    self::find($item->id)->delete();
                }
            }
        }
    }

    public static function RecursiveMenu($id = 0, $parent = 0, $check = 0, $arr = "")
    {
        $html = "";
        $query = CategoryProducts::where('parent_id',$parent)->get();
        if(!empty($query)){
            foreach ($query as $item){
                $checked = "";
                if($check > 0){
                    foreach ($check as $value){
                        if($value['category_id'] == $item->id){
                            $checked = "selected";
                        }
                    }
                }
                $html .=  "<option value=\"".$item->id."\" ".$checked.">".$arr.$item->name."</option>";
                $html .= self::RecursiveMenu($id, $item->id, $check, $arr."&rarr;");
            }
        }
        return $html;
    }

    /**
     * @param string $rarr
     * @return string
     *
     */
    public static function RecursiveIndexAdmin($key)
    {
        $html = "";
        if($key){
            $query = self::orderby('film_details.film_id')
                ->where('film_details.name','like',"%$key%")
                ->join('films', 'films.id', '=', 'film_details.film_id')
                ->orWhere('films.name','like',"%$key%")
                ->select('film_details.*')
                ->orderby('film_details.created_at','desc')
                ->paginate(15);
        }else{
            $query = self::orderby('film_id')->orderby('created_at','desc')->paginate(15);
        }
        if($query != null){
            foreach ($query as $item){
                $status = ($item->publish == 1)?"publish":"hidden";
                $featured = ($item->featured == 1)?"featured":"normal";
                $class = ($item->publish == 1)?"label-success":"label-danger";
                $class_featured = ($item->featured == 1)?"label-success":"label-danger";
                if($item->images != ''){
                    $images = "<a href=\"".url(getImage($item->images))."\" data-toggle='lightbox-image'>
                            <img src=\"".url(getImage($item->images))."\" class='img-responsive center-block' style='max-width: 40px;'>
                          </a>";
                }elseif ($item->images_link != '') {
                    $images = "<a href=\"".$item->images_link."\" data-toggle='lightbox-image'>
                            <img src=\"".$item->images_link."\" class='img-responsive center-block' style='max-width: 40px;'>
                          </a>";
                }else{
                    $images = '';
                }
                $html .= "<tr>";
                $html .= "<td class=\"text-center\">";
                $html .= "<input type=\"checkbox\" id=\"checkbox-".$item->id ."\" name=\"id[]\" value=\"".$item->id."\">";
                $html .= "</td>";
                $html .= "<td><a href=\"".url('admin/film-series/edit/'.$item->id)."\">".$item->name."</a></td>";
                $html .= "<td><a href=\"".url('admin/film/edit/'.$item->film->id)."\">".$item->film->name."</a></td>";
                $html .= "<td>
                            <span class=\"label ".$class." check_status\" data-type='publish' data-url=\"".url('admin/film-series/check-status/publish/'.$item->id)."\">".$status."</span>
                         </td>";
                $html .= "<td>".convertDateTime($item->created_at)."</td>";
                $html .= "<td class=\"text-center\">";
                $html .= "<div class=\"btn-group btn-group-xs\">";
                $html .= "<a href=\"".url('admin/film-series/edit/'.$item->id)."\" data-toggle=\"tooltip\" title=\"Edit\" class=\"btn btn-default\"><i class=\"fa fa-pencil\"></i></a>";
                $html .= HtmlDeleteRecord(url('admin/film-series/delete/'.$item->id));
                $html .= "</div>";
                $html .= "</td>";
                $html .= "</tr>";
            }

        }
        $data = array(
            'html'      => $html,
            'paginate'  => $query->appends(['key' => $key])->render()
        );
        return $data;
    }

    public static function RecursiveIndexAdminEposide($id){
        $html = "";
        $query = self::where('film_id',$id)->orderby('name','desc')->get();
        if($query != null){
            foreach ($query as $item){
                $status = ($item->publish == 1)?"publish":"hidden";
                $featured = ($item->featured == 1)?"featured":"normal";
                $class = ($item->publish == 1)?"label-success":"label-danger";
                $class_featured = ($item->featured == 1)?"label-success":"label-danger";
                if($item->images != ''){
                    $images = "<a href=\"".url(getImage($item->images))."\" data-toggle='lightbox-image'>
                            <img src=\"".url(getImage($item->images))."\" class='img-responsive center-block' style='max-width: 40px;'>
                          </a>";
                }elseif ($item->images_link != '') {
                    $images = "<a href=\"".$item->images_link."\" data-toggle='lightbox-image'>
                            <img src=\"".$item->images_link."\" class='img-responsive center-block' style='max-width: 40px;'>
                          </a>";
                }else{
                    $images = '';
                }
                $html .= "<tr>";
                $html .= "<td class=\"text-center\">";
                $html .= "<input type=\"checkbox\" id=\"checkbox-".$item->id ."\" name=\"id[]\" value=\"".$item->id."\">";
                $html .= "</td>";
                $html .= "<td><a href=\"".url('admin/film-series/edit/'.$item->id)."\">".$item->name."</a></td>";
                $html .= "<td>
                            <span class=\"label ".$class." check_status\" data-type='publish' data-url=\"".url('admin/film-series/check-status/publish/'.$item->id)."\">".$status."</span>
                         </td>";
                $html .= "<td>".convertDateTime($item->created_at)."</td>";
                $html .= "<td class=\"text-center\">";
                $html .= "<div class=\"btn-group btn-group-xs\">";
                $html .= "<a href=\"".url('admin/film-series/edit/'.$item->id)."\" data-toggle=\"tooltip\" title=\"Edit\" class=\"btn btn-default\"><i class=\"fa fa-pencil\"></i></a>";
                $html .= HtmlDeleteRecord(url('admin/film-series/delete/'.$item->id));
                $html .= "</div>";
                $html .= "</td>";
                $html .= "</tr>";
            }

        }
        return $html;
    }

    /**
     * @param int $id
     * @param int $parent
     * @param int $check
     * @return string
     */
    public static function RecursiveTag($id = 0, $parent = 0, $check = 0, $arr = "")
    {
        $html = "";
        $query = TagsProduct::where('parent_id',$parent)->get();
        if(!empty($query)){
            foreach ($query as $item){
                $checked = "";
                if($check > 0){
                    foreach ($check as $value){
                        if($value['tags_id'] == $item->id){
                            $checked = "selected";
                        }
                    }
                }
                $html .=  "<option value=\"".$item->id."\" ".$checked.">".$arr. "&nbsp;" .$item->name."</option>";
                $html .= self::RecursiveTag($id, $item->id, $check, $arr."&rarr;");
            }
        }
        return $html;
    }

    /**
     * @param int $id
     * @param int $parent
     * @param int $check
     * @return string
     */
    public static function RecursiveSize($id = 0, $parent = 0, $check = 0, $arr = "")
    {
        $html = "";
        $query = SizeProduct::where('parent_id',$parent)->get();
        if(!empty($query)){
            foreach ($query as $item){
                $checked = "";
                if($check > 0){
                    foreach ($check as $value){
                        if($value['size_id'] == $item->id){
                            $checked = "selected";
                        }
                    }
                }
                $html .=  "<option value=\"".$item->id."\" ".$checked.">".$arr. "&nbsp;" .$item->name."</option>";
                $html .= self::RecursiveSize($id, $item->id, $check, $arr."&rarr;");
            }
        }
        return $html;
    }

    /**
     * @param int $id
     * @param int $parent
     * @param int $check
     * @return string
     */
    public static function RecursiveColor($id = 0, $parent = 0, $check = 0, $arr = "")
    {
        $html = "";
        $query = ColorProduct::where('parent_id',$parent)->get();
        if(!empty($query)){
            foreach ($query as $item){
                $checked = "";
                if($check > 0){
                    foreach ($check as $value){
                        if($value['color_id'] == $item->id){
                            $checked = "selected";
                        }
                    }
                }
                $html .=  "<option value=\"".$item->id."\" ".$checked.">".$arr.$item->name."</option>";
                $html .= self::RecursiveColor($id, $item->id, $check, $arr."&rarr;");
            }
        }
        return $html;
    }
}
