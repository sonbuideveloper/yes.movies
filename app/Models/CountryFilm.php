<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CountryFilm extends Model
{
    protected $table = "country_films";

    public static function getById($id)
    {
        return self::find($id);
    }

    public static function DeleteById($id)
    {
        return self::find($id)->delete();
    }

    public static function RecursiveIndexAdmin($rarr = "")
    {
        $html = "";
        $query = self::orderby('name')->get();
        if($query != null){
            foreach ($query as $item){
                $status = ($item->publish == 1)?"publish":"hidden";
                $class = ($item->publish == 1)?"label-success":"label-danger";
                $html .= "<tr>";
                $html .= "<td class=\"text-center\">";
                $html .= "<input type=\"checkbox\" id=\"checkbox-".$item->id ."\" name=\"id[]\" value=\"".$item->id."\">";
                $html .= "</td>";
                $html .= "<td><a href=\"".url('admin/film-country/edit/'.$item->id)."\">".$rarr." ".$item->name."</a></td>";
                $html .= "<td><span class=\"label ".$class." check_status\" data-url=\"".url('admin/film-country/check-status/'.$item->id)."\">".$status."</span></td>";
                $html .= "<td class=\"text-center\">";
                $html .= "<div class=\"btn-group btn-group-xs\">";
                $html .= "<a href=\"".url('admin/film-country/edit/'.$item->id)."\" data-toggle=\"tooltip\" title=\"Edit\" class=\"btn btn-default\"><i class=\"fa fa-pencil\"></i></a>";
                $html .= HtmlDeleteRecord(url('admin/film-country/delete/'.$item->id));
                $html .= "</div>";
                $html .= "</td>";
                $html .= "</tr>";
            }

        }
        return $html;
    }
}
