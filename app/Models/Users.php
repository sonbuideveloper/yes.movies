<?php

namespace Models;

use App\Mail\RegisterUser;
use App\Models\CategoryFilmRelation;
use App\Models\FavoriteFilm;
use App\Models\Film;
use App\Models\FilmDetail;
use App\Models\LinkFilm;
use App\Models\LinkFilmDetail;
use App\Models\Producer;
use App\Models\RateFilm;
use App\Models\Social;
use App\Models\SubtitleFilm;
use App\Models\SubtitleFilmDetail;
use App\Models\TypeFilm;
use App\Models\TypeFilmRelation;
use Illuminate\Database\Eloquent\Model;
use Mail, Auth, Session;

class Users extends Model
{
    //
    protected $table = "users";

    /**
     * @param $data
     * @return bool|Users
     * create user
     */
    public static function CreateUser($data) {
        $check = self::where('email', $data['email'])->first();
        $setting = Setting::first();

        if(count($check) == 0) {
            $user = new self;
            $user->email = $data['email'];
            $user->password = bcrypt($data['password']);
            $user->name = $data['name'];
            $user->type = $data['type'];
            $user->active = 1;
            $user->active_token = str_random(60);
            if($user->save()){
//                Mail::send('emails.password', ['token' => $user->active_token], function ($m) use ($user, $setting) {
//                    $m->from($setting->email, 'Confirm your account!');
//                    $m->to($user->email, $user->name)->subject('Just one more step to get started');
//                });
            };
            return $user;
        }
        return false;
    }

    public static function CreateSocialUser($data) {
        $check = self::where('email', $data['email'])->first();
        if(count($check) == 0) {
            $user = new self;
            $user->email = $data['email'];
            $user->name = $data['name'];
            $user->type = $data['type'];
            $user->images = $data['images'];
            $user->active = 1;
            $user->social = $data['social'];
            $user->active_token = str_random(60);
            $user->save();
//            if($user->save()){
//                Mail::send('emails.password', ['token' => $user->active_token], function ($m) use ($user) {
//                    $m->from('sonbuideveloper@gmail.com', 'Confirm your account!');
//
//                    $m->to($user->email, $user->name)->subject('Just one more step to get started');
//                });
//            };
            return $user;
        }
        return false;
    }

    /**
     * @param $data
     * @return bool
     * change password for admin
     */
    public static function ChangePasswordAdmin($data) {
        $check_email = self::where('id', '<>', $data['id'])->where('email', $data['email'])->first();
        if ($check_email != null) {
            return false;
        }
        $check = self::find($data['id']);
        if(count($check) == 0)
            return false;
        if($check->email != $data['email']) {
            Mail::to($check->email, $check->name)->send(new RegisterUser($check->remember_token));
        }
        if ($data['password']) {
            return self::where('id', $check->id)->update([
              'email' => $data['email'],
              'name'    => $data['name'],
              'age' => $data['age'],
              'type_video' => $data['type_video'],
              'password' => bcrypt($data['password']),
              'type'    => $data['type']
            ]);
        }
        return self::where('id', $check->id)->update([
          'email' => $data['email'],
          'name'    => $data['name'],
          'age' => $data['age'],
          'type_video' => $data['type_video'],
          'type'    => $data['type']
        ]);
    }

    /**
     * @param $data
     * @return bool
     * send mail change password
     */
    public static function SendMailChangePassword($data) {
        $check = self::find($data['id']);
        if(count($check) == 0)
            return false;
        /*if($check->email != $data['email']) {
            Mail::send('emails.password', ['token' => $check->remember_token], function ($m) use ($check,$data) {
                $m->from('leduyphuong64@gmail.com', 'Your Application');

                $m->to($data['email'], $check->name)->subject('Your Reminder!');
            });
        }*/
        if ($data['password']) {
            return self::where('id', $check->id)->update(['email' => $data['email'], 'password' => bcrypt($data['password'])]);
        }
        return self::where('id', $check->id)->update(['email' => $data['email']]);
    }

    /**
     * @param string $rarr
     * @return string
     *
     */
    public static function RecursiveIndexAdmin($key)
    {
        $html = "";
        $http_url = env('HTTP_URL');
        if($key){
            $query = self::where('name','like',"%$key%")->orWhere('email','like',"%$key%")->orderBy('active', 'asc')->paginate(20);
        }else{
            $query = self::orderBy('active', 'asc')->paginate(20);
        }
        if($query != null){
            foreach ($query as $item){
                $admin = "";
                if($item->type == 1) {
                    $admin = "selected";
                }
                $sub_admin = "";
                if($item->type == 2) {
                    $sub_admin = "selected";
                }
                $editor = "";
                if($item->type == 3) {
                    $editor = "selected";
                }
                $other = "";
                if($item->type == 4){
                    $other = "selected";
                }
                $status = ($item->active == 1)?"Active":"Inactive";
                $class = ($item->active == 1)?"label-success":"label-danger";
                $html .= "<tr>";
                $html .= "<td class=\"text-center\">";
                $html .= "<input type=\"checkbox\" id=\"checkbox-".$item->id ."\" name=\"id[]\" value=\"".$item->id."\">";
                $html .= "</td>";
                $html .= "<td><a href=\"".$http_url('admin/user/edit/'.$item->id)."\">".$item->name."</a></td>";
                $html .= "<td>".$item->email."</td>";
                $html .= "<td>".$item->social."</td>";
                $html .= "<td>";
                $html .= "<select class=\"select-select2 change_group\" style=\"width: 100%;\" data-placeholder=\"Choose one..\" data-url=\"".url('admin/user/change-group/'.$item->id)."\">";
                $html .= "<option value='1' ".$admin.">Admin</option>";
                $html .= "<option value='4' ".$other.">Other</option>";
                $html .= "</select>";
                $html .= "</td>";
                $html .= "<td class=\"text-center\">
                            <span class=\"label ".$class." check_status\" data-type='active' data-url=\"".$http_url('admin/user/check-active/'.$item->id)."\">".$status."</span>
                        </td>";
                $html .= "<td class='text-center'>";
                $html .= "<div class=\"btn-group btn-group-xs\">";
                $html .= "<a href=\"".$http_url('admin/user/edit/'.$item->id)."\" data-toggle=\"tooltip\" title=\"Edit\" class=\"btn btn-default\"><i class=\"fa fa-pencil\"></i></a>";
                $html .= HtmlDeleteRecord($http_url('admin/user/delete/'.$item->id));
                $html .= "</div>";
                $html .= "</td>";
                $html .= "</tr>";
            }

        }
        $data = array(
            'html'      => $html,
            'paginate'  => $query->appends(['key' => $key])->render()
        );
        return $data;
    }

    public static function DeleteById($id)
    {
        if(Auth::user()->id == $id){
            Session::flash('error','You can not delete yourself');
            return redirect('admin/user/list');
        }
        $check = self::find($id);
        if($check != null)
        {
            $films = Film::where('user_id', $id)->get();
            if(count($films) > 0){
                foreach ($films as $film) {
                    $cate_film = CategoryFilmRelation::where('film_id', $film->id)->get();
                    if(count($cate_film) > 0){
                        foreach ($cate_film as $catefilm){
                            $catefilm->delete();
                        }
                    }
                    $film_details = FilmDetail::where('film_id', $film->id)->get();
                    if(count($film_details) > 0){
                        foreach ($film_details as $film_detail){
                            $link_films_details = LinkFilmDetail::where('film_detail_id', $film_detail->id)->get();
                            foreach ($link_films_details as $link_films_detail){
                                $link_films_detail->delete();
                            }

                            $sub_details = SubtitleFilmDetail::where('film_id', $film_detail->id)->get();
                            if(count($sub_details) > 0){
                                foreach ($sub_details as $sub_detail){
                                    $sub_detail->delete();
                                }
                            }
                            $film_detail->delete();
                        }
                    }
                    $type_films = TypeFilmRelation::where('film_id', $film->id)->get();
                    if(count($type_films) > 0){
                        foreach ($type_films as $type_film){
                            $type_film->delete();
                        }
                    }
                    $favatites = FavoriteFilm::where('film_id', $film->id)->get();
                    if(count($favatites) > 0){
                        foreach ($favatites as $favatite){
                            $favatite->delete();
                        }
                    }
                    $rates = RateFilm::where('film_id', $film->id)->get();
                    if(count($rates) > 0){
                        foreach ($rates as $rate){
                            $rate->delete();
                        }
                    }
                    $link_films = LinkFilm::where('film_id', $film->id)->get();
                    if(count($link_films) > 0){
                        foreach ($link_films as $link_film){
                            $link_film->delete();
                        }
                    }

                    $sub_films = SubtitleFilm::where('film_id', $film->id)->get();
                    if(count($sub_films) > 0){
                        foreach ($sub_films as $sub_film){
                            $sub_film->delete();
                        }
                    }

                    $film->delete();
                }
            }

            $favorite_film = FavoriteFilm::where('user_id',$id)->get();
            if(count($favorite_film)>0){
                foreach ($favorite_film as $value){
                    $film = Film::find($value->film_id)->first();
                    if($film){
                        $film->favorite_total = $film->favorite_total-1;
                        $film->save();
                    }
                    FavoriteFilm::find($value->id)->delete();
                }
            }
            $rate_film = RateFilm::where('user_id',$id)->get();
            if(count($rate_film)>0){
                foreach ($rate_film as $item){
                    RateFilm::find($item->id)->delete();
                }
            }
            $film_user = Film::where('user_id', $id)->first();
            if($film_user){
                Film::DeleteById($film_user->id);
            }
            if($check->social){
                Social::where('user_id',$check->id)->delete();
            }

            $producers = Producer::where('user_id', $id)->get();
            if(count($producers) > 0){
                foreach ($producers as $producer){
                    $producer->delete();
                }
            }
        }

        return self::where('id',$id)->delete();
    }
}
