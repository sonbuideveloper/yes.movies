<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TagFilmRelation extends Model
{
    protected $table = "tag_film_relations";

    public function tag() {
        return $this->belongsTo('App\Models\TagFilm', 'tag_id');
    }

    public function films()
    {
        return $this->belongsTo('App\Models\Film', 'film_id');
    }

    public static function DeleteNotInArray($film_id, $tag_name, $type){
        $check_tag = TagFilm::where('type',$type)->where('name',$tag_name)->first();

        if($check_tag){
            $check_tag_id = $check_tag->id;
        }else{
            $check_tag_id = 0;
        }
        return self::where('film_id', $film_id)->where('tag_id','<>', $check_tag_id)->where('type', $type)->delete();
    }
}
