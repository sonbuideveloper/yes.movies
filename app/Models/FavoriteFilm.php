<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FavoriteFilm extends Model
{
    protected $table = "favorite_films";

    public function films(){
        return $this->belongsTo('App\Models\Film', 'film_id');
    }
}
