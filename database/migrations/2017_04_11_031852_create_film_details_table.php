<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilmDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('film_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->nullable();
            $table->string('duration')->nullable();
            $table->string('release')->nullable();
            $table->string('day_release')->nullable();
            $table->string('end_day')->nullable();
            $table->string('IMDB')->nullable();
            $table->longText('content')->nullable();
            $table->string('quality')->nullable();
            $table->integer('publish')->default(1);
            $table->integer('film_id')->unsigned();
            $table->foreign('film_id')->references('id')->on('films');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('film_details');
    }
}
