<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChanelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chanels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->nullable();
            $table->string('images')->nullable();
            $table->string('images_link')->nullable();
            $table->string('cover')->nullable();
            $table->string('cover_link')->nullable();
            $table->string('background')->nullable();
            $table->string('background_link')->nullable();
            $table->text('title')->nullable();
            $table->text('descriptions')->nullable();
            $table->text('keywords')->nullable();
            $table->integer('views')->default(0);
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('publish')->default(1);
            $table->string('type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chanels');
    }
}
