<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->nullable();
            $table->string('images')->nullable();
            $table->string('images_link')->nullable();
            $table->string('cover')->nullable();
            $table->string('cover_link')->nullable();
            $table->string('background')->nullable();
            $table->string('background_link')->nullable();
            $table->text('title')->nullable();
            $table->text('descriptions')->nullable();
            $table->text('keywords')->nullable();
            $table->integer('age')->nullable();
            $table->string('sex')->nullable();
            $table->integer('duration')->nullable();
            $table->string('release')->nullable();
            $table->string('day_release')->nullable();
            $table->string('end_day')->nullable();
            $table->string('IMDB')->nullable();
            $table->longText('content')->nullable();
            $table->string('quality')->nullable();
            $table->integer('movie_series')->default(0);
            $table->double('view')->default(0);
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('country_film_id')->unsigned();
            $table->foreign('country_film_id')->references('id')->on('country_films');
            $table->string('trailer')->nullable();
            $table->integer('chanel_id')->default(0);
            $table->integer('producer_id')->default(0);
            $table->integer('publish')->default(1);
            $table->integer('featured')->default(0);
            $table->integer('slider')->default(0);
            $table->double('favorite_total')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('films');
    }
}
