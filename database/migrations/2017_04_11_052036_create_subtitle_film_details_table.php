<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubtitleFilmDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subtitle_film_details', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->string('language');

            $table->integer('film_id')->unsigned();
            $table->foreign('film_id')->references('id')->on('film_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subtitle_film_details');
    }
}
