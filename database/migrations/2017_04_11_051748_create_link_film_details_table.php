<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinkFilmDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_film_details', function (Blueprint $table) {
            $table->increments('id');
            $table->text('link');
            $table->string('type');
            $table->integer('error')->default(0);
            $table->string('email')->nullable();
            $table->integer('server_play')->default(0);

            $table->integer('film_detail_id')->unsigned();
            $table->foreign('film_detail_id')->references('id')->on('film_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('link_film_details');
    }
}
