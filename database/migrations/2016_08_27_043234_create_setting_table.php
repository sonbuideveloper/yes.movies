<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('language')->nullable();
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->text('keywords')->nullable();
            $table->text('phone')->nullable();
            $table->text('email')->nullable();
            $table->text('address')->nullable();
            $table->string('facebook_url')->nullable();
            $table->string('google_url')->nullable();
            $table->string('twitter_url')->nullable();
            $table->string('images')->nullable();
            $table->string('meta_images')->nullable();
            $table->string('app_id_face')->nullable();
            $table->string('google_analytic')->nullable();
            $table->string('api_player')->nullable();
            $table->string('api_youtube')->nullable();
            $table->string('api_download')->nullable();
            $table->integer('upload')->nullable();
            $table->integer('cache')->default(0);
            $table->integer('film_home')->default(0);
            $table->integer('film_page')->default(0);
            $table->integer('theme')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('setting');
    }
}
